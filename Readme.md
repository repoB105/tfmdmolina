# README #

EMG control respository

### What is this repository for? ###

* Configuration and communication between a GUI interface running in Windows and an STM32 embedded system for EMG data acquisition.

### How do I get set up? ###

* Copy YetiOS dependencies.

### How does the testing system function? ###

* The system has a predefined test for each module.
* Once the test system is started, it will ask for a test file corresponding to one of the modules.
* After selection the file, the testing will take place.
* The result will be displayed inside a dialog at the end.