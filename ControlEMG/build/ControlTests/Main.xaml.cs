﻿/*
CONTROL EMG 2021

Main.cs

- Description: Contains the application entry point
- Author: David Molina Toro
- Date: 28 - 04 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System.Windows;

namespace ControlTests
{
    /*
    Application entry point
    */
    public partial class App : Application
    {

    }
}