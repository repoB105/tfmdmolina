﻿/*
CONTROL EMG 2021

MainWindow.cs

- Description: Application main window
- Author: David Molina Toro
- Date: 28 - 04 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System;
using System.IO;
using System.Windows;
using System.Diagnostics;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace ControlTests
{
    public partial class MainWindow : Window
    {
        /*
        Public constructor
        */
        public MainWindow()
        {
            InitializeComponent();

            //Center the main window
            this.Left   = (SystemParameters.PrimaryScreenWidth / 2) - (this.Width / 2);
            this.Top    = (SystemParameters.PrimaryScreenHeight / 2) - (this.Height / 2);

            //Enable port detection system
            ControlLib.PortDetection.EnablePortDetection(true);

            //Bind the connection event
            ControlLibConnection.IControlConnection.Instance.GetConnected().PropertyChanged += HandleConnectedEvent;

            //Create a timer for the label fading
            Timer timerFade     = new Timer();
            timerFade.Tick      += TimerFade_Tick;
            timerFade.Interval  = 20;
            timerFade.Enabled   = true;

            //Create a connection check thread
            System.Threading.Thread threadConnect = new System.Threading.Thread(() => CheckConection())
            {
                IsBackground = true
            };
            threadConnect.SetApartmentState(System.Threading.ApartmentState.STA);
            threadConnect.Start();
        }

        //Fading status flag
        private bool bFading = true;

        /*
        Fade timer tick function
        */
        private void TimerFade_Tick(object sender, EventArgs e)
        {
            //Check the status of the fading text
            if (bFading)
            {
                //Decrease the opacity variable
                labelTesting.Opacity -= 0.02;

                //Check if the text is completely invisible
                if (labelTesting.Opacity <= 0)
                {
                    bFading = false;
                }
            }
            else
            {
                //Increase the opacity of the text
                labelTesting.Opacity += 0.02;

                //Check if the text is completely visible
                if (labelTesting.Opacity >= 1)
                {
                    bFading = true;
                }
            }
        }

        /*
        Connection timeout function
        */
        private void CheckConection()
        {
            //Wait for the specified time
            System.Threading.Thread.Sleep(10000);

            //Check if the device is not connected
            if (!ControlLibConnection.IControlConnection.Instance.GetConnected().Checked)
            {
                //Unbind the connection event
                ControlLibConnection.IControlConnection.Instance.GetConnected().PropertyChanged -= HandleConnectedEvent;

                //Show the connection error message
                System.Windows.Forms.MessageBox.Show(ControlRegion.Dialogs.Tests_ConnectionError, "Tests EMG", MessageBoxButtons.OK, MessageBoxIcon.Error);

                //Close the application
                Dispatcher.Invoke(new Action(() => Close()));
            }
        }

        /*
        Connection status event handler
        */
        private void HandleConnectedEvent(object sender, PropertyChangedEventArgs e)
        {
            //Select the file for testing
            OpenFileDialog dialog = new OpenFileDialog()
            {
#if DEBUG
                InitialDirectory    = Path.Combine(ControlLib.Common.sExeDir, "Files"),
#else
                InitialDirectory = Path.Combine(ControlLib.Common.sExeDir, "files"),
#endif
                Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*",
                FilterIndex         = 1,
                RestoreDirectory    = true
            };

            //Show the file dialog
            string sFile = "";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                sFile = dialog.FileName;
            }

            //Check if a file has been selected
            if (!string.IsNullOrEmpty(sFile))
            {
                //Create a testing thread
                System.Threading.Thread threadTest = new System.Threading.Thread(() => Test(sFile))
                {
                    IsBackground = true
                };
                threadTest.SetApartmentState(System.Threading.ApartmentState.STA);
                threadTest.Start();
            }
            else
            {
                //Close the application
                Dispatcher.Invoke(new Action(() => Close()));
            }
        }

        private void Test(string sFile)
        {
            try
            {
                //Delete the output file if able
                if (File.Exists("Output.txt"))
                {
                    File.Delete("Output.txt");
                }

                //Set the trace output file
                Trace.Listeners.Add(new TextWriterTraceListener("Output.txt"));
                Trace.AutoFlush = true;

                //Open the test file and create an output file
                TextReader readerTest = File.OpenText(sFile);

                //Read the test file
                string sLine;
                bool bArgumentError = false;
                while (!string.IsNullOrEmpty(sLine = readerTest.ReadLine()))
                {
                    //Check the command to execute
                    string[] sParts = sLine.Split(' ');
                    if (sParts.Length > 1)
                    {
                        switch (sParts[0])
                        {
                            case "WAIT":
                                //Parse the wait time
                                if (int.TryParse(sParts[1], out int iWait))
                                {
                                    System.Threading.Thread.Sleep(iWait);
                                }
                                else
                                {
                                    bArgumentError = true;
                                }
                                break;
                            default:
                                //Switch the type of command to send
                                switch (sParts[0])
                                {
                                    case "CTRL":
                                        //Get the command
                                        if (int.TryParse(sParts[1], out int iCommand))
                                        {
                                            ControlLibConnection.IControlComms.Instance.SendCommand((ControlLib.Command)iCommand);
                                        }
                                        else
                                        {
                                            bArgumentError = true;
                                        }
                                        break;
                                    case "CONF":
                                        //Create a new configuration structure
                                        ControlLib.ConfigMessage config = new ControlLib.ConfigMessage(false);

                                        //Fill the channel information
                                        for (int i = 0; i < ControlLib.Constants.CHANNEL_NUM; i++)
                                        {
                                            if (ushort.TryParse(sParts[i + 1], out ushort iChannel))
                                            {
                                                config.iChannel[i] = iChannel;
                                            }
                                            else
                                            {
                                                bArgumentError = true;
                                            }
                                        }

                                        //Fill the gain information
                                        for (int i = 0; i < ControlLib.Constants.CHANNEL_NUM; i++)
                                        {
                                            if (ushort.TryParse(sParts[i + 5], out ushort iGain))
                                            {
                                                config.iGain[i] = iGain;
                                            }
                                            else
                                            {
                                                bArgumentError = true;
                                            }
                                        }

                                        //Fill the filter information
                                        if (uint.TryParse(sParts[9], out uint iFilter))
                                        {
                                            config.iFilter = iFilter;
                                        }
                                        else
                                        {
                                            bArgumentError = true;
                                        }

                                        //Send the configuration command
                                        ControlLibConnection.IControlComms.Instance.SendConfig(config);
                                        break;
                                    case "CAPT":
                                        //Create a new capture structure
                                        ControlLib.CaptureMessage capture = new ControlLib.CaptureMessage(false);

                                        //Fill the time information
                                        capture.iUnixTime = DateTimeOffset.Now.ToUnixTimeSeconds();

                                        //Fill the label information
                                        for (int i = 1; i < sParts.Length; i++)
                                        {
                                            if (char.TryParse(sParts[i], out char cCharacter))
                                            {
                                                capture.cTestLabel[i - 1] = cCharacter;
                                            }
                                            else
                                            {
                                                bArgumentError = true;
                                            }
                                        }

                                        //Send the configuration command
                                        ControlLibConnection.IControlComms.Instance.SendCapture(capture);
                                        break;
                                }


                                break;
                        }
                    }
                }

                //Close all test files
                readerTest.Close();

                //Check if there has been any argument error
                if (bArgumentError)
                {
                    System.Windows.Forms.MessageBox.Show(ControlRegion.Dialogs.Tests_ArgumentError, "Tests EMG", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    //Close all trace listeners
                    for (int i = 0; i < Trace.Listeners.Count; i++)
                    {
                        Trace.Listeners[i].Close();
                    }

                    //Clear all trace listeners
                    Trace.Listeners.Clear();

                    //Open the check file and read all the lines
                    TextReader readerCheck = File.OpenText(sFile.Substring(0, sFile.LastIndexOf('_')) + "_Check.txt");
                    string sCheck = readerCheck.ReadToEnd();

                    //Open the check file and read all the lines
                    TextReader readerOutput = File.OpenText("Output.txt");
                    string sOutput = readerOutput.ReadToEnd();

                    //Compare both strings
                    if (sOutput == sCheck)
                    {
                        System.Windows.Forms.MessageBox.Show(ControlRegion.Dialogs.Test_CompletedSuccess, "Tests EMG", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show(ControlRegion.Dialogs.TestCompletedError, "Tests EMG", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show(ControlRegion.Dialogs.Tests_ReadError, "Tests EMG", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //Close the application
            Dispatcher.Invoke(new Action(() => Close()));
        }
    }
}
