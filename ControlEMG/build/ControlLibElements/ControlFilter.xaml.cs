﻿/*
CONTROL EMG 2021

ControlFilter.cs

- Description: Filter configuration control
- Author: David Molina Toro
- Date: 08 - 06 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;

//Common library
using ControlLib;

namespace ControlLibElements
{
    public partial class ControlFilter : UserControl
    {
        /*
        Public constructor
        */
        public ControlFilter()
        {
            InitializeComponent();
        }

        /*
        User control loaded function
        */
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //Set the filter mode combo box initial selection
            comboMode.SelectedIndex = ControlLibConfig.IControlConfig.Instance.GetFilterMode();

            //Set the divider text box initial value
            textboxDivider.Text = ControlLibConfig.IControlConfig.Instance.GetFilterDivider().ToString("0");
        }

        /*
        Returns the selected parameters
        */
        public void GetConfig(out uint iMode, out uint iSingleCycle, out uint iDivider)
        {
            //Set the mode selection
            switch (comboMode.SelectedValue)
            {
                case "SYNC3 + ZERO":
                    iMode           = Constants.SYNC_3_FILTER;
                    iSingleCycle    = Constants.SINGLE_CYCLE_ENABLED;
                    break;
                case "SYNC3 + FILT.":
                    iMode           = Constants.SYNC_3_FAST_FILTER;
                    iSingleCycle    = Constants.SINGLE_CYCLE_DISABLED;
                    break;
                case "SYNC4 + ZERO":
                    iMode           = Constants.SYNC_4_FILTER;
                    iSingleCycle    = Constants.SINGLE_CYCLE_ENABLED;
                    break;
                case "SYNC4 + FILT.":
                    iMode           = Constants.SYNC_4_FAST_FILTER;
                    iSingleCycle    = Constants.SINGLE_CYCLE_DISABLED;
                    break;
                default:
                    iMode           = Constants.SYNC_4_FILTER;
                    iSingleCycle    = Constants.SINGLE_CYCLE_ENABLED;
                    break;
            }

            //Check if the divider text can be parsed
            iDivider = Constants.DEFAULT_DIVIDER;
            if (ushort.TryParse(textboxDivider.Text, out ushort iSelectedDiv))
            {
                //Check if the divider is valid
                if (iSelectedDiv > Constants.MIN_DIVIDER_VAL && iSelectedDiv < Constants.MAX_DIVIDER_VAL)
                {
                    iDivider = iSelectedDiv;
                }
            }
        }

        /*
        Combo box mode selection changed function
        */
        private void ComboMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CalculateDataRate();
        }

        /*
        Divider selection text changed function
        */
        private void TextboxDivider_TextChanged(object sender, TextChangedEventArgs e)
        {
            CalculateDataRate();
        }

        /*
        Calculates the output data rate
        */
        private void CalculateDataRate()
        {
            //Check if the divider text can be parsed
            if (int.TryParse(textboxDivider.Text, out int iFreqDivider))
            {
                //Check if the divider is valid
                if (iFreqDivider > Constants.MIN_DIVIDER_VAL && iFreqDivider < Constants.MAX_DIVIDER_VAL)
                {
                    //Check the type of filter selected
                    float fClockDivider;
                    switch (comboMode.SelectedValue.ToString())
                    {
                        case "SYNC3 + ZERO":
                            fClockDivider = (float)Constants.SYNC_3_CLOCK_DIV * Constants.ADC_CLOCK_DIV;
                            break;
                        case "SYNC3 + FILT.":
                            fClockDivider = (float)(Constants.SYNC_3_CLOCK_DIV + Constants.FILTER_AVERAGE - 1) * Constants.ADC_CLOCK_DIV;
                            break;
                        case "SYNC4 + ZERO":
                            fClockDivider = (float)Constants.SYNC_4_CLOCK_DIV * Constants.ADC_CLOCK_DIV;
                            break;
                        case "SYNC4 + FILT.":
                            fClockDivider = (float)(Constants.SYNC_4_CLOCK_DIV + Constants.FILTER_AVERAGE - 1) * Constants.ADC_CLOCK_DIV;
                            break;
                        default:
                            fClockDivider = Constants.DEFAULT_DIVIDER;
                            break;
                    }

                    //Calculate the final output rate per channel
                    float fRate = Constants.ADC_CLOCK_RATE / Constants.CHANNEL_NUM / fClockDivider / iFreqDivider;

                    //Set the rate label content
                    labelRate.Content = fRate.ToString("0") + " Hz";
                }
                else
                {
                    MessageBox.Show(ControlRegion.Dialogs.ControlFilter_DividerError, "Control EMG", MessageBoxButton.OK, MessageBoxImage.Error);
                    textboxDivider.Text = "2";
                }

                //Save the filter mode value
                ControlLibConfig.IControlConfig.Instance.SaveFilterMode(comboMode.SelectedIndex);

                //Save the filter divider value
                ControlLibConfig.IControlConfig.Instance.SaveFilterDivider(int.Parse(textboxDivider.Text));
            }
        }

        /*
        Divider selection preview text input function
        */
        private void TextBoxDivider_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //Handle the event if able
            e.Handled = !char.IsDigit(e.Text.ToCharArray()[0]);
        }

        /*
        Divider selection focus lost function
        */
        private void TextboxDivider_LostFocus(object sender, RoutedEventArgs e)
        {
            //Check if the divider text can be parsed
            if (int.TryParse(textboxDivider.Text, out int iFreqDivider))
            {
                //Check if the divider is valid
                if (!(iFreqDivider > Constants.MIN_DIVIDER_VAL && iFreqDivider < Constants.MAX_DIVIDER_VAL))
                {
                    MessageBox.Show(ControlRegion.Dialogs.ControlFilter_DividerError, "Control EMG", MessageBoxButton.OK, MessageBoxImage.Error);
                    textboxDivider.Text = "2";
                }
            }
            else
            {
                MessageBox.Show(ControlRegion.Dialogs.ControlFilter_DividerError, "Control EMG", MessageBoxButton.OK, MessageBoxImage.Error);
                textboxDivider.Text = "2";
            }

            //Calculate the final data rate
            CalculateDataRate();
        }
    }
}
