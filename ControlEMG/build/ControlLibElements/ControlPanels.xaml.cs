﻿/*
CONTROL EMG 2021

ControlPanels.cs

- Description: Panel system control
- Author: David Molina Toro
- Date: 08 - 06 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System.Windows.Media;
using System.Windows.Controls;

namespace ControlLibElements
{
    public partial class ControlPanels : UserControl
    {
        /*
        Public constructor
        */
        public ControlPanels()
        {
            InitializeComponent();
        }

        //Panel selection event
        public delegate void SetPanel(string sPanel);
        public event SetPanel SetPanelEvent;

        /*
        Panel control buttonc lick function
        */
        private void ButtonPanel_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //Enable all buttons
            buttonConfig.IsEnabled  = true;
            buttonData.IsEnabled    = true;
            buttonInfo.IsEnabled    = true;

            //Set all grid backgrounds
            gridConfig.Background   = new SolidColorBrush(ControlLib.Constants.COLOR_LAYOUT_ENABLED);
            gridData.Background     = new SolidColorBrush(ControlLib.Constants.COLOR_LAYOUT_ENABLED);
            gridInfo.Background     = new SolidColorBrush(ControlLib.Constants.COLOR_LAYOUT_ENABLED);

            //Disable the sender button
            ((Button)sender).IsEnabled = false;

            //Set the sender button background
            ((Grid)((Button)sender).Content).Background = new SolidColorBrush(ControlLib.Constants.COLOR_LAYOUT_DISABLED);

            //Invoke the panel selection event
            SetPanelEvent?.Invoke(((Button)sender).Name.Replace("button", ""));
        }
    }
}
