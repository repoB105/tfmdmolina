﻿/*
CONTROL EMG 2021

ControlPlot.cs

- Description: Plot system control
- Author: David Molina Toro
- Date: 10 - 06 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System;
using System.Windows.Controls;

//ScottPlot library
using ScottPlot.Plottable;

namespace ControlLibElements
{
    public partial class ControlPlot : UserControl
    {
        /*
        Public constructor
        */
        public ControlPlot()
        {
            InitializeComponent();
        }

        //Plot label variable
        private int iChannel = 0;

        //Plot activation status flag
        private bool bActive = false;

        //Plot first measurement flag
        private bool bFirstData = true;

        //Plot data array object
        private double[] dPlotData = new double[ControlLib.Constants.PLOT_DATA_COUNT];

        /*
        Main plot configuration function
        */
        public void ConfigurePlot(int iLabel)
        {
            //Store the plot label
            iChannel = iLabel - 1;

            //Set the plot title
            labelTitle.Content = "CHANNEL " + iLabel.ToString();

            //Set the plot style
            plotMain.Plot.Style(ScottPlot.Style.Gray1);

            //Disable the X axis configuration
            plotMain.Plot.AxisAutoX(0);
            plotMain.Plot.XAxis.Ticks(false);
            plotMain.Configuration.MiddleClickAutoAxisMarginX = 0;

            //Set the axis limits
            plotMain.Plot.SetAxisLimits(xMin: 0, xMax: ControlLib.Constants.PLOT_DATA_COUNT, yMin: 0, yMax:ControlLib.Constants.EXT_VOLTAGE_REFERENCE);

            //Add the data values to the plot
            SignalPlot signal   = plotMain.Plot.AddSignal(dPlotData);
            signal.LineWidth    = 2;

            //Disable all plot actions
            plotMain.IsEnabled = false;

            //Clear the plot
            ClearPlot(false);
        }

        /*
        Clear plot function
        */
        public void ClearPlot(bool bBind)
        {
            //Clear all the data values
            for (int i = 0; i < ControlLib.Constants.PLOT_DATA_COUNT; i++)
            {
                dPlotData[i] = 0;
            }

            //Set the axis limits
            plotMain.Plot.SetAxisLimits(xMin: 0, xMax: ControlLib.Constants.PLOT_DATA_COUNT, yMin: 0, yMax: ControlLib.Constants.EXT_VOLTAGE_REFERENCE);

            //Check the bind status
            if (bBind)
            {
                //Bind the new data event to this class
                ControlLibConnection.IControlComms.Instance.NewDataEvent += HandleNewDataEvent;
                bActive = true;
            }
            else
            {
                //Unbind the new data event to this class
                ControlLibConnection.IControlComms.Instance.NewDataEvent -= HandleNewDataEvent;
                bActive = false;
            }

            //Set the first data flag
            bFirstData = true;

            //Update the plot
            UpdatePlot();
        }

        /*
        Update plot function
        */
        public void UpdatePlot()
        {
            //Update the main plot
            Dispatcher.Invoke(new Action(() => plotMain.Render()));
        }

        /*
        New data received event handler
        */
        private void HandleNewDataEvent(double[] dData)
        {
            //Check the activation status
            if (bActive)
            {
                //Check if the first data flag is active
                if (bFirstData && !double.IsNaN(dData[iChannel]))
                {
                    //Set all the data values for the plot
                    for (int i = 0; i < ControlLib.Constants.PLOT_DATA_COUNT; i++)
                    {
                        dPlotData[i] = dData[iChannel];
                    }

                    //Reset the first data flag
                    bFirstData = false;
                }

                //Move the existing data one place
                Array.Copy(dPlotData, 1, dPlotData, 0, dPlotData.Length - 1);

                //Check if the data is missing
                if (!double.IsNaN(dData[iChannel]))
                {
                    //Add the new data to the last place
                    dPlotData[dPlotData.Length - 1] = Math.Abs(dData[iChannel]);
                }
                else
                {
                    //Set the previous data as the new
                    dPlotData[dPlotData.Length - 1] = dPlotData[dPlotData.Length - 2];
                }

                //Update the Y axis zoom
                plotMain.Plot.AxisAutoY(0.1);
            }
        }
    }
}
