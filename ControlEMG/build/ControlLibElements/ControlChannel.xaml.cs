﻿/*
CONTROL EMG 2021

ControlChannel.cs

- Description: Channel configuration control
- Author: David Molina Toro
- Date: 08 - 06 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;

//Common library
using ControlLib;

namespace ControlLibElements
{
    public partial class ControlChannel : UserControl
    {
        /*
        Public constructor
        */
        public ControlChannel()
        {
            InitializeComponent();

            //Set the range configurations
            for (int i = 0; i < Constants.GAIN_CONFIG_NUM; i++)
            {
                //Get the range value
                double dValue = Constants.EXT_VOLTAGE_REFERENCE / Math.Pow(2, i);

                //Check the size of the value
                if (dValue >= 1)
                {
                    comboRange.Items.Add("±" + dValue.ToString("0.00") + " V");
                }
                else
                {
                    comboRange.Items.Add("±" + (dValue * 1000).ToString("0.00") + " mV");
                }
            }
        }

        /*
        User control loaded function
        */
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //Set the channel label
            labelChannel.Content = "CHANNEL " + iLabel.ToString();

            //Set the range combo box initial selection
            comboRange.SelectedIndex = ControlLibConfig.IControlConfig.Instance.GetChannelRange(iLabel);

            //Set the channel status
            if (ControlLibConfig.IControlConfig.Instance.GetChannelStatus(iLabel))
            {
                //Set the element as disabled
                labelEnable.Content = "ON";
                this.Opacity        = 1.0;

                //Enable the range combo box
                comboRange.IsEnabled = true;

                //Enable the buffer status buttons
                buttonRefBuffer.IsEnabled = true;
                buttonAinBuffer.IsEnabled = true;
            }
            else
            {
                //Set the element layout as disabled
                labelEnable.Content = "OFF";
                this.Opacity        = 0.5;

                //Disable the range combo box
                comboRange.IsEnabled = false;

                //Disable the buffer status buttons
                buttonRefBuffer.IsEnabled = false;
                buttonAinBuffer.IsEnabled = false;
            }

            //Set the reference buffer status button
            bool bRefBuffer = ControlLibConfig.IControlConfig.Instance.GetRefBuffer(iLabel);
            gridRefBuffer.Background = bRefBuffer ? new SolidColorBrush(Constants.COLOR_SWITCH_ENABLED) : new SolidColorBrush(Constants.COLOR_SWITCH_DISABLED);

            //Set the analog input buffer status button
            bool bAinBuffer = ControlLibConfig.IControlConfig.Instance.GetAinBuffer(iLabel);
            gridAinBuffer.Background = bAinBuffer ? new SolidColorBrush(Constants.COLOR_SWITCH_ENABLED) : new SolidColorBrush(Constants.COLOR_SWITCH_DISABLED);
        }

        //Label variable
        private int iLabel;

        /*
        Sets the channel label
        */
        public void SetLabel(int iValue)
        {
            iLabel = iValue;
        }

        /*
        Returns the selected parameters
        */
        public void GetConfig(out ushort iEnabled, out ushort iRefBuffer, out ushort iAinBuffer, out ushort iRange)
        {
            //Set the channel status
            iEnabled = this.Opacity == 1.0 ? Constants.CHANNEL_ENABLED : Constants.CHANNEL_DISABLED;

            //Set the reference buffer status
            iRefBuffer = ((SolidColorBrush)gridRefBuffer.Background).Color == Constants.COLOR_SWITCH_ENABLED ? Constants.CHANNEL_BUFFERED : Constants.CHANNEL_UNBUFFERED;

            //Set the analog input buffer status
            iAinBuffer = ((SolidColorBrush)gridAinBuffer.Background).Color == Constants.COLOR_SWITCH_ENABLED ? Constants.CHANNEL_BUFFERED : Constants.CHANNEL_UNBUFFERED;

            //Set the gain selection
            iRange = (ushort)(comboRange.SelectedIndex < 0 ? Constants.DEFAULT_GAIN : Math.Pow(2, comboRange.SelectedIndex));
        }

        /*
        Channel enabling button click event handler
        */
        private void ButtonEnable_Click(object sender, RoutedEventArgs e)
        {
            //Check the channel status
            if ((string)labelEnable.Content == "ON")
            {
                //Set the element layout as disabled
                labelEnable.Content = "OFF";
                this.Opacity        = 0.5;

                //Disable the range combo box
                comboRange.IsEnabled = false;

                //Disable the buffer status
                buttonRefBuffer.IsEnabled = false;
                buttonAinBuffer.IsEnabled = false;

                //Save the channel status
                ControlLibConfig.IControlConfig.Instance.SaveChannelStatus(false, iLabel);
            }
            else
            {
                //Set the element as enabled
                labelEnable.Content = "ON";
                this.Opacity        = 1.0;

                //Enable the combo boxes
                comboRange.IsEnabled = true;

                //Enable the buffer status buttons
                buttonRefBuffer.IsEnabled = true;
                buttonAinBuffer.IsEnabled = true;

                //Save the channel status
                ControlLibConfig.IControlConfig.Instance.SaveChannelStatus(true, iLabel);
            }
        }

        /*
        Channel reference buffer status button click event handler
        */
        private void ButtonRefBuffer_Click(object sender, RoutedEventArgs e)
        {
            //Check the channel reference buffer status
            if (((SolidColorBrush)gridRefBuffer.Background).Color == Constants.COLOR_SWITCH_ENABLED)
            {
                //Set the reference buffer button color
                gridRefBuffer.Background = new SolidColorBrush(Constants.COLOR_SWITCH_DISABLED);

                //Save the channel reference buffer status
                ControlLibConfig.IControlConfig.Instance.SaveRefBuffer(false, iLabel);
            }
            else
            {
                //Set the reference buffer button color
                gridRefBuffer.Background = new SolidColorBrush(Constants.COLOR_SWITCH_ENABLED);

                //Save the channel reference buffer status
                ControlLibConfig.IControlConfig.Instance.SaveRefBuffer(true, iLabel);
            }
        }

        /*
        Channel analog input buffer status button click event handler
        */
        private void ButtonAinBuffer_Click(object sender, RoutedEventArgs e)
        {
            //Check the channel analog input buffer status
            if (((SolidColorBrush)gridAinBuffer.Background).Color == Constants.COLOR_SWITCH_ENABLED)
            {
                //Set the analog input buffer button color
                gridAinBuffer.Background = new SolidColorBrush(Constants.COLOR_SWITCH_DISABLED);

                //Save the channel analog input buffer status
                ControlLibConfig.IControlConfig.Instance.SaveAinBuffer(false, iLabel);
            }
            else
            {
                //Set the analog input buffer button color
                gridAinBuffer.Background = new SolidColorBrush(Constants.COLOR_SWITCH_ENABLED);

                //Save the channel analog input buffer status
                ControlLibConfig.IControlConfig.Instance.SaveAinBuffer(true, iLabel);
            }
        }

        /*
        Range combo box selection changed function
        */
        private void ComboRange_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Save the range selection
            ControlLibConfig.IControlConfig.Instance.SaveChannelRange(comboRange.SelectedIndex, iLabel);
        }
    }
}
