/*
CONTROL EMG 2021

IControlConnection.cs

- Description: System communication class
- Author: David Molina Toro
- Date: 16 - 02 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System;
using System.IO.Ports;
using System.Windows.Forms;

//Common library
using ControlLib;

namespace ControlLibConnection
{
    public sealed class IControlConnection
    {
        private static volatile IControlConnection instance = null;
        private static readonly object padlock = new object();

        /*
        Public static constructor
        */
        static IControlConnection()
        {

        }

        /*
        Private constructor called from the static constructor
        */
        private IControlConnection()
        {
            //Connect the port connection events to this class
            PortDetection.listPortsCOM.OnAdd    += HandlePortCOMAdded;
            PortDetection.listPortsCOM.OnRemove += HandlePortCOMRemoved;
        }

        /*
        Instance object for the singleton instantiation
        */
        public static IControlConnection Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new IControlConnection();
                        }
                    }
                }

                return instance;
            }
        }

        //Serial port objects
        private SerialPort serialHeimdall;
        private object lockHeimdall     = new object();
        private object lockConnected    = new object();
        private BoolState bConnected    = new BoolState(false);
        
        /*
        Returns the Heimdall serial port
        */
        public SerialPort GetSerialPort()
        {
            lock (lockHeimdall)
            {
                return serialHeimdall;
            }
        }

        /*
        Returns the connected status
        */
        public BoolState GetConnected()
        {
            lock (lockConnected)
            {
                return bConnected;
            }
        }

        /*
        Search for the correct Heimdall serial port on added
        */
        public void HandlePortCOMAdded(object sender, EventArgs e)
        {
            for (int i = 0; i < PortDetection.listPortsCOM.Count; i++)
            {
                string[] sParts     = PortDetection.listPortsCOM[i].Split(':');
                string[] sPortInfo  = PortDetection.GetPortInformation().Split(':');
                if (sParts[1] == sPortInfo[0] && sParts[2] == sPortInfo[1] && !bConnected.Checked)
                {
                    lock (lockHeimdall)
                    {
                        try
                        {
                            //Initialize the serial port object
                            serialHeimdall = new SerialPort()
                            {
                                PortName = "COM" + sParts[0],
                                BaudRate = PortDetection.GetPortRate()
                            };
                            serialHeimdall.Open();

                            //Clean the serial buffers
                            serialHeimdall.DiscardInBuffer();
                            serialHeimdall.DiscardOutBuffer();

                            //Connect the received data event
                            serialHeimdall.DataReceived += IControlComms.Instance.SerialPort_DataReceived;

                            //Set the serial port as connected
                            lock (lockConnected)
                            {
                                bConnected.Checked = true;
                            }

                            break;
                        }
                        catch
                        {
                            MessageBox.Show(ControlRegion.Dialogs.Connection_SerialError, "Control EMG", MessageBoxButtons.OK, MessageBoxIcon.Error);

                            //Set the serial port as disconnected
                            lock (lockConnected)
                            {
                                bConnected.Checked = false;
                            }
                        }
                    }
                }
            }
        }

        /*
        Search for the correct Heimdall serial port on removed
        */
        public void HandlePortCOMRemoved(object sender, EventArgs e)
        {
            bool bFound = false;
            for (int i = 0; i < PortDetection.listPortsCOM.Count; i++)
            {
                string[] sParts     = PortDetection.listPortsCOM[i].Split(':');
                string[] sPortInfo  = PortDetection.GetPortInformation().Split(':');
                if (sParts[1] == sPortInfo[0] && sParts[2] == sPortInfo[1])
                {
                    bFound = true;
                    break;
                }
            }

            if (!bFound)
            {
                //Set the serial port as disconnected
                lock (lockConnected)
                {
                    bConnected.Checked = false;
                }

                lock (lockHeimdall)
                {
                    if (serialHeimdall != null)
                    {
                        serialHeimdall.DataReceived -= IControlComms.Instance.SerialPort_DataReceived;
                        serialHeimdall.Dispose();
                        serialHeimdall = null;
                    }
                }
            }
        }
    }
}
