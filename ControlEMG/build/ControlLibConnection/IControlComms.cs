﻿/*
CONTROL EMG 2021

IControlComms.cs

- Description: System communication class
- Author: David Molina Toro
- Date: 16 - 02 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;

//Common library
using ControlLib;

namespace ControlLibConnection
{
    public sealed class IControlComms
    {
        private static volatile IControlComms instance = null;
        private static readonly object padlock = new object();

        /*
        Public static constructor
        */
        static IControlComms()
        {

        }

        /*
        Private constructor called from the static constructor
        */
        private IControlComms()
        {

        }

        /*
        Instance object for the singleton instantiation
        */
        public static IControlComms Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new IControlComms();
                        }
                    }
                }

                return instance;
            }
        }

        //Data message received event
        public delegate void NewData(double[] dData);
        public event NewData NewDataEvent;

        /*
        System command call function
        */
        public bool SendCommand(Command iCommand)
        {
            return Instance.SerialSend("CTRL", new ControlMessage(iCommand, 0).Serialize());
        }

        /*
        System configuration call function
        */
        public bool SendConfig(ConfigMessage config)
        {
            return Instance.SerialSend("CONF", config.Serialize());
        }

        /*
        System capture start call function
        */
        public bool SendCapture(CaptureMessage capture)
        {
            return Instance.SerialSend("CAPT", capture.Serialize());
        }

        /*
        Sends the given data to the controller
        */
        public bool SerialSend(string sCommand, byte[] data)
        {
            //Check if the controller is connected
            if (IControlConnection.Instance.GetConnected().Checked)
            {
                try
                {
                    //List of bytes to send
                    List<byte> listBytes = new List<byte>();

                    //Add the argument bytes
                    listBytes.AddRange(data);

                    //Process the checksum to send
                    ushort iChecksum = ProcessChecksum(listBytes.ToArray());
                    listBytes.AddRange(BitConverter.GetBytes(iChecksum));

                    //Add the header bytes
                    listBytes.InsertRange(0, Encoding.ASCII.GetBytes(sCommand));

                    //Send the control command
                    IControlConnection.Instance.GetSerialPort().Write(listBytes.ToArray(), 0, listBytes.Count);

                    Debug.WriteLine("EMG data sent: " + sCommand + ", " + listBytes.Count);
                    return true;
                }
                catch
                {
                    Debug.WriteLine("EMG error: Sending data via serial");
                    return false;
                }
            }
            else
            {
                MessageBox.Show(ControlRegion.Dialogs.Comms_DisconnectedEMG, "Control EMG", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
        }

        //Serial buffer objects
        private List<byte> listBuffer = new List<byte>();

        /*
        Data receiving function
        */
        public void SerialPort_DataReceived(object obj, SerialDataReceivedEventArgs e)
        {
            //Get the bytes from the buffer
            byte[] bytesRead = new byte[IControlConnection.Instance.GetSerialPort().BytesToRead];
            int iBytesRead = IControlConnection.Instance.GetSerialPort().Read(bytesRead, 0, bytesRead.Length);

            //Check if there is a received message
            listBuffer.AddRange(bytesRead.Take(iBytesRead).ToArray());
            while (listBuffer.Count >= Constants.STATUS_MESSAGE_SIZE)
            {
                //Process the header of the received message
                byte[] bytesMessage;
                byte[] bytesChecksum;
                string sHeader = Encoding.ASCII.GetString(listBuffer.GetRange(0, Constants.HEADER_MESSAGE_SIZE).ToArray());
                switch (sHeader)
                {
                    case "STAT":
                        //Check if the message is valid
                        bytesMessage    = listBuffer.GetRange(Constants.HEADER_MESSAGE_SIZE, Constants.STATUS_MESSAGE_SIZE - Constants.HEADER_MESSAGE_SIZE - Constants.CHECKSUM_SIZE).ToArray();
                        bytesChecksum   = listBuffer.GetRange(Constants.STATUS_MESSAGE_SIZE - Constants.CHECKSUM_SIZE, Constants.CHECKSUM_SIZE).ToArray();
                        if (BitConverter.ToUInt16(bytesChecksum, 0) == ProcessChecksum(bytesMessage))
                        {
                            //Parse the status  message
                            StatusMessage statusMessage = new StatusMessage(bytesMessage);

                            //Check if there have been any errors
                            if (statusMessage.iError == Constants.NO_ERROR)
                            {
                                //Show the received status message
                                MessageBox.Show(ControlRegion.Dialogs.Comms_StatusReceived + (State)statusMessage.iLastState + ", " + (State)statusMessage.iCurrentState, "Control EMG", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                //Show the error status message
                                MessageBox.Show(ControlRegion.Dialogs.Comms_ErrorReceived + (State)statusMessage.iLastState + ", " + (State)statusMessage.iCurrentState, "Control EMG", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                            //Clear the status message from the buffer
                            listBuffer.RemoveRange(0, Constants.STATUS_MESSAGE_SIZE);
                        }
                        else
                        {
                            //Remove the first byte from the buffer
                            listBuffer.RemoveRange(0, Constants.STATUS_MESSAGE_SIZE);
                        }
                        break;
                    case "DATA":
                        //Check if the message is valid
                        bytesMessage    = listBuffer.GetRange(Constants.HEADER_MESSAGE_SIZE, Constants.DATA_MESSAGE_SIZE - Constants.HEADER_MESSAGE_SIZE - Constants.CHECKSUM_SIZE).ToArray();
                        bytesChecksum   = listBuffer.GetRange(Constants.DATA_MESSAGE_SIZE - Constants.CHECKSUM_SIZE, Constants.CHECKSUM_SIZE).ToArray();
                        if (BitConverter.ToUInt16(bytesChecksum, 0) == ProcessChecksum(bytesMessage))
                        {
                            //Get the message bytes
                            byte[] bytesData = listBuffer.GetRange(Constants.HEADER_MESSAGE_SIZE, Constants.DATA_MESSAGE_SIZE - Constants.HEADER_MESSAGE_SIZE).ToArray();

                            //Parse the data  message
                            DataMessage dataMessage = new DataMessage(bytesData);

                            //Show the received data message
                            Debug.WriteLine("EMG data frame received: " + dataMessage.iFrameID + ", " + string.Join(" ", dataMessage.iChannelData));

                            //Convert the received data
                            double[] dConverted = new double[Constants.CHANNEL_NUM];
                            for (int i = 0; i < Constants.CHANNEL_NUM; i++)
                            {
                                //Check if the data is missing
                                if (dataMessage.iChannelData[i] != Constants.MISSING_SAMPLE)
                                {
                                    dConverted[i] = Functions.ToValueADC(dataMessage.iChannelData[i], Constants.EXT_VOLTAGE_REFERENCE, Constants.DEFAULT_GAIN);
                                }
                                else
                                {
                                    dConverted[i] = double.NaN;
                                }
                            }

                            //Send the new data received event
                            NewDataEvent?.Invoke(dConverted);

                            //Clear the data message from the buffer
                            listBuffer.RemoveRange(0, Constants.DATA_MESSAGE_SIZE);
                        }
                        break;
                    default:
                        //Remove the first byte from the buffer
                        listBuffer.RemoveAt(0);
                        break;
                }
            }
        }

        /*
        Return if the received checksum is valid
        */
        private ushort ProcessChecksum(byte[] data)
        {
            ushort iChecksum = 0;
            for (int i = 0; i < data.Length / 2; i++)
            {
                iChecksum ^= (ushort)(data[2 * i] + (data[2 * i + 1] << 8));
            }

            if ((data.Length % 2) != 0)
            {
                iChecksum ^= data[data.Length - 1];
            }

            return (ushort)~iChecksum;
        }
    }
}
