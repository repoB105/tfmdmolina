﻿/*
CONTROL EMG 2021

IControlConfig.cs

- Description: System configuration class
- Author: David Molina Toro
- Date: 16 - 02 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System.IO;
using System.Diagnostics;
using System.Configuration;
using System.Globalization;

namespace ControlLibConfig
{
    public sealed class IControlConfig
    {
        private static volatile IControlConfig instance = null;
        private static readonly object padlock = new object();

        /*
        Public static constructor
        */
        static IControlConfig()
        {

        }

        /*
        Private constructor called from the static constructor
        */
        private IControlConfig()
        {
            //Set the local configuration file
            string sConfigPath = Path.Combine(ControlLib.Common.sExeDir, "config");
            sConfigPath = Path.Combine(sConfigPath, "EMG.config");

#if DEBUG
            sConfigPath = Path.Combine(ControlLib.Common.sExeDir, "EMG.config");
#endif

            configMap       = new ConfigurationFileMap(sConfigPath);
            configEMG       = ConfigurationManager.OpenMappedMachineConfiguration(configMap);
            sectionParams   = configEMG.GetSection("Params") as ClientSettingsSection;

        }

        /*
        Instance object for the singleton instantiation
        */
        public static IControlConfig Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new IControlConfig();
                        }
                    }
                }

                return instance;
            }
        }

        //Configuration objects
        private Configuration configEMG;
        private ConfigurationFileMap configMap;
        private ClientSettingsSection sectionParams;

        /*
        Returns the given channel status
        */
        public bool GetChannelStatus(int iLabel)
        {
            return int.Parse(sectionParams.Settings.Get("CHANNEL_STATUS").Value.ValueXml.InnerText.Split(';')[iLabel - 1], CultureInfo.InvariantCulture) == 1;
        }

        /*
        Saves the given channel status
        */
        public bool SaveChannelStatus(bool bEnabled, int iLabel)
        {
            try
            {
                //Get the channel status
                string sStatus = sectionParams.Settings.Get("CHANNEL_STATUS").Value.ValueXml.InnerText;

                //Set the channel status
                string[] sParts = sStatus.Split(';');
                for (int i = 0; i < ControlLib.Constants.CHANNEL_NUM; i++)
                {
                    //Check if the channel corresponds
                    if (i == iLabel - 1)
                    {
                        sParts[i] = bEnabled ? "1" : "0";
                    }
                }
                sStatus = string.Join(";", sParts);

                //Save the channel information
                sectionParams.Settings.Get("CHANNEL_STATUS").Value.ValueXml.InnerText = sStatus;
                sectionParams.SectionInformation.ForceSave = true;
                configEMG.Save(ConfigurationSaveMode.Full);
            }
            catch (ConfigurationErrorsException)
            {
                Debug.WriteLine("EMG error: Saving EMG channel status");
                return false;
            }

            return true;
        }

        /*
        Returns the given channel range
        */
        public int GetChannelRange(int iLabel)
        {
            return int.Parse(sectionParams.Settings.Get("CHANNEL_RANGE").Value.ValueXml.InnerText.Split(';')[iLabel - 1], CultureInfo.InvariantCulture);
        }

        /*
        Saves the given channel range
        */
        public bool SaveChannelRange(int iRange, int iLabel)
        {
            try
            {
                //Get the channel range
                string sRange = sectionParams.Settings.Get("CHANNEL_RANGE").Value.ValueXml.InnerText;

                //Set the channel range
                string[] sParts = sRange.Split(';');
                for (int i = 0; i < ControlLib.Constants.CHANNEL_NUM; i++)
                {
                    //Check if the channel corresponds
                    if (i == iLabel - 1)
                    {
                        sParts[i] = iRange.ToString(CultureInfo.InvariantCulture);
                    }
                }
                sRange = string.Join(";", sParts);

                //Save the channel information
                sectionParams.Settings.Get("CHANNEL_RANGE").Value.ValueXml.InnerText = sRange;
                sectionParams.SectionInformation.ForceSave = true;
                configEMG.Save(ConfigurationSaveMode.Full);
            }
            catch (ConfigurationErrorsException)
            {
                Debug.WriteLine("EMG error: Saving EMG channel range");
                return false;
            }

            return true;
        }

        /*
        Returns the given channel reference buffer status
        */
        public bool GetRefBuffer(int iLabel)
        {
            return int.Parse(sectionParams.Settings.Get("CHANNEL_REF_BUFFER").Value.ValueXml.InnerText.Split(';')[iLabel - 1], CultureInfo.InvariantCulture) == 1;
        }

        /*
        Saves the given channel reference buffer status
        */
        public bool SaveRefBuffer(bool bStatus, int iLabel)
        {
            try
            {
                //Get the channel reference buffer status
                string sRefBuffer = sectionParams.Settings.Get("CHANNEL_REF_BUFFER").Value.ValueXml.InnerText;

                //Set the channel reference buffer status
                string[] sParts = sRefBuffer.Split(';');
                for (int i = 0; i < ControlLib.Constants.CHANNEL_NUM; i++)
                {
                    //Check if the channel corresponds
                    if (i == iLabel - 1)
                    {
                        sParts[i] = bStatus ? "1" : "0";
                    }
                }
                sRefBuffer = string.Join(";", sParts);

                //Save the channel information
                sectionParams.Settings.Get("CHANNEL_REF_BUFFER").Value.ValueXml.InnerText = sRefBuffer;
                sectionParams.SectionInformation.ForceSave = true;
                configEMG.Save(ConfigurationSaveMode.Full);
            }
            catch (ConfigurationErrorsException)
            {
                Debug.WriteLine("EMG error: Saving EMG channel reference buffer status");
                return false;
            }

            return true;
        }

        /*
        Returns the given channel analog input buffer status
        */
        public bool GetAinBuffer(int iLabel)
        {
            return int.Parse(sectionParams.Settings.Get("CHANNEL_AIN_BUFFER").Value.ValueXml.InnerText.Split(';')[iLabel - 1], CultureInfo.InvariantCulture) == 1;
        }

        /*
        Saves the given channel analog input buffer status
        */
        public bool SaveAinBuffer(bool bStatus, int iLabel)
        {
            try
            {
                //Get the channel analog input buffer status
                string sAinBuffer = sectionParams.Settings.Get("CHANNEL_AIN_BUFFER").Value.ValueXml.InnerText;

                //Set the channel analog input buffer status
                string[] sParts = sAinBuffer.Split(';');
                for (int i = 0; i < ControlLib.Constants.CHANNEL_NUM; i++)
                {
                    //Check if the channel corresponds
                    if (i == iLabel - 1)
                    {
                        sParts[i] = bStatus ? "1" : "0";
                    }
                }
                sAinBuffer = string.Join(";", sParts);

                //Save the channel information
                sectionParams.Settings.Get("CHANNEL_AIN_BUFFER").Value.ValueXml.InnerText = sAinBuffer;
                sectionParams.SectionInformation.ForceSave = true;
                configEMG.Save(ConfigurationSaveMode.Full);
            }
            catch (ConfigurationErrorsException)
            {
                Debug.WriteLine("EMG error: Saving EMG channel analog input buffer status");
                return false;
            }

            return true;
        }

        /*
        Returns the filter mode
        */
        public int GetFilterMode()
        {
            return int.Parse(sectionParams.Settings.Get("FILTER_MODE").Value.ValueXml.InnerText, CultureInfo.InvariantCulture);
        }

        /*
        Saves the filter mode
        */
        public bool SaveFilterMode(int iMode)
        {
            try
            {
                //Save the filter mode information
                sectionParams.Settings.Get("FILTER_MODE").Value.ValueXml.InnerText = iMode.ToString(CultureInfo.InvariantCulture);
                sectionParams.SectionInformation.ForceSave = true;
                configEMG.Save(ConfigurationSaveMode.Full);
            }
            catch (ConfigurationErrorsException)
            {
                Debug.WriteLine("EMG error: Saving EMG filter mode");
                return false;
            }

            return true;
        }

        /*
        Returns the filter divider
        */
        public int GetFilterDivider()
        {
            return int.Parse(sectionParams.Settings.Get("FILTER_DIVIDER").Value.ValueXml.InnerText, CultureInfo.InvariantCulture);
        }

        /*
        Saves the filter divider
        */
        public bool SaveFilterDivider(int iDivider)
        {
            try
            {
                //Save the filter divider information
                sectionParams.Settings.Get("FILTER_DIVIDER").Value.ValueXml.InnerText = iDivider.ToString(CultureInfo.InvariantCulture);
                sectionParams.SectionInformation.ForceSave = true;
                configEMG.Save(ConfigurationSaveMode.Full);
            }
            catch (ConfigurationErrorsException)
            {
                Debug.WriteLine("EMG error: Saving EMG filter divider");
                return false;
            }

            return true;
        }
    }
}
