﻿/*
CONTROL EMG 2021

Common.cs

- Description: Common properties class
- Author: David Molina Toro
- Date: 16 - 02 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System.IO;

namespace ControlLib
{
    /*
    Common resources
    */
    public static class Common
    {
        //Executable directory for relative paths
        public static string sExeDir = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
    }
}
