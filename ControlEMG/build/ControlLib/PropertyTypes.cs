﻿/*
CONTROL EMG 2021

PropertyTypes.cs

- Description: Property types class
- Author: David Molina Toro
- Date: 16 - 02 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ControlLib
{
    /*
    Common delegate for device connection
    */
    public delegate void ConnectedEvent(bool bConnected);

    /*
    State type class for its use as a boolean with notify abilities
    */
    public class BoolState : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private bool _checked;

        /*
        Public constructor
        */
        public BoolState(bool bValue)
        {
            this.Checked = bValue;
            bLast = bValue;
        }

        //Internal checking attribute
        bool bLast;

        /*
        Checked attribute, this is the boolean behaviour part
        */
        public bool Checked
        {
            get { return _checked; }
            set { _checked = value; OnPropertyChanged(); }
        }

        /*
        Toggle function from true to false and viceversa
        */
        public void Toggle()
        {
            Checked = !Checked;
            bLast   = Checked;
        }

        /*
        Notify the change of the state to other C# classes
        */
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (Checked != bLast)
                {
                    bLast = Checked;
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }
    }

    /*
    State type class for its use as an integer with notify abilities
    */
    public class IntState : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private int _value;

        /*
        Public constructor
        */
        public IntState(int iValue)
        {
            this.Value = iValue;
            iLast = iValue;
        }

        int iLast;

        /*
        Checked attribute, this is the int behaviour part
        */
        public int Value
        {
            get { return _value; }
            set { _value = value; OnPropertyChanged(); }
        }

        /*
        Notify the change of the state to other C# classes
        */
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (Value != iLast)
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                    iLast = Value;
                }
            }
        }
    }

    /*
    State type class for its use as a string with notify abilities
    */
    public class StringState : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private string _string;

        /*
        Public constructor
        */
        public StringState(string sValue)
        {
            this.String = sValue;
            sLast = sValue;
        }

        string sLast;

        /*
        Checked attribute, this is the string behaviour part
        */
        public string String
        {
            get { return _string; }
            set { _string = value; OnPropertyChanged(); }
        }

        /*
        Notify the change of the state to other C# classes
        */
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (String != sLast)
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                    sLast = String;
                }
            }
        }
    }

    /*
    List type class for its use as a list with notify abilities
    */
    public class ListState<T> : List<T>
    {
        public event EventHandler<ListStateArgs<T>> OnAdd;
        public event EventHandler<ListStateArgs<T>> OnRemove;

        /*
        Override add function
        */
        public new void Add(T item)
        {
            base.Add(item);
            RaiseEvent(OnAdd, item);
        }

        /*
        Override remove function
        */
        public new void Remove(T item)
        {
            if (base.Remove(item))
            {
                RaiseEvent(OnRemove, item);
            }
        }

        /*
        Override remove at function
        */
        public new void RemoveAt(int iPos)
        {
            T item = base[iPos];
            base.RemoveAt(iPos);
            RaiseEvent(OnRemove, item);
        }

        /*
        Event raiser function
        */
        private void RaiseEvent(EventHandler<ListStateArgs<T>> eventHandler, T item)
        {
            var handler = eventHandler;
            handler?.Invoke(this, new ListStateArgs<T>(item));
        }
    }

    /*
    List state arguments class
    */
    public class ListStateArgs<T> : EventArgs
    {
        public T Item { get; }

        public ListStateArgs(T item)
        {
            Item = item;
        }
    }

    /*
    Dictionary type class for its use as a list with notify abilities
    */
    public class DictionaryState<TKey, TValue> : Dictionary<TKey, TValue>
    {
        public event EventHandler<DictionaryStateArgs<TKey, TValue>> Changed;

        /*
        Changed dictionary element event raiser
        */
        public void Change(TKey key, TValue value)
        {
            if (!base[key].Equals(value))
            {
                base[key] = value;
                RaiseEvent(Changed, key, value);
            }
        }

        /*
        Event raiser function
        */
        private void RaiseEvent(EventHandler<DictionaryStateArgs<TKey, TValue>> eventHandler, TKey key, TValue value)
        {
            var handler = eventHandler;
            handler?.Invoke(this, new DictionaryStateArgs<TKey, TValue>(key, value));
        }
    }

    /*
    Dictionary state arguments class
    */
    public class DictionaryStateArgs<TKey, TValue> : EventArgs
    {
        public TKey Key { get; }
        public TValue Value { get; }

        public DictionaryStateArgs(TKey key, TValue value)
        {
            Key     = key;
            Value   = value;
        }
    }
}
