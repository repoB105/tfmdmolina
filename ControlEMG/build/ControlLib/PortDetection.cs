﻿/*
CONTROL EMG 2021

PortDetection.cs

- Description: Port detection system
- Author: David Molina Toro
- Date: 16 - 02 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System;
using System.IO;
using System.Management;
using System.Configuration;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace ControlLib
{
    /*
    Class containing common functions
    */
    public static class PortDetection
    {
        /*
        Static constructor
        */
        static PortDetection()
        {
            //Load the port configuration
            LoadPortConfig();

            //Initialize available COM ports vector
            listPortsCOM = new ListState<string>();

            //Timer for COM port availability checking
            timerPortCOM = new Timer
            {
                Enabled     = false,
                Interval    = Constants.CONNECTION_UPDATE_PERIOD
            };
            timerPortCOM.Tick += new EventHandler(TimerPortCOM_Tick);

            //Background worker for COM port availability checking
            backPortCOM = new BackgroundWorker();
            backPortCOM.DoWork             += new DoWorkEventHandler(BackPortCOM_DoWork);
            backPortCOM.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BackPortCOM_RunWorkerCompleted);
        }

        //Available COM ports list
        public static ListState<string> listPortsCOM;

        //Available COM ports timer and background worker
        private static Timer timerPortCOM;
        private static BackgroundWorker backPortCOM;

        //Configuration objects
        private static Configuration configData;
        private static ConfigurationFileMap configMap;
        private static ClientSettingsSection sectionPorts;

        /*
        Loads the port configuration
        */
        private static void LoadPortConfig()
        {
            //Set the local configuration file
            string sConfigPath = Path.Combine(Common.sExeDir, "config");
            sConfigPath = Path.Combine(sConfigPath, "Ports.config");

#if DEBUG
            sConfigPath = Path.Combine(Common.sExeDir, "Ports.config");
#endif

            //Set the configuration map
            configMap   = new ConfigurationFileMap(sConfigPath);
            configData  = ConfigurationManager.OpenMappedMachineConfiguration(configMap);

            //Get the ports section from the  configuration map
            sectionPorts = configData.GetSection("Ports") as ClientSettingsSection;
        }

        /*
        Returns the controller VID and PID information
        */
        public static string GetPortInformation()
        {
            return sectionPorts.Settings.Get("HEIMDALL_INFO").Value.ValueXml.InnerText;
        }

        /*
        Returns the controller VID and PID information
        */
        public static int GetPortRate()
        {
            return int.Parse(sectionPorts.Settings.Get("HEIMDALL_RATE").Value.ValueXml.InnerText);
        }

        /*
        Enables the port detection system
        */
        public static void EnablePortDetection(bool bEnable)
        {
            timerPortCOM.Enabled = bEnable;
        }

        /*
        Called each USB_UPDATE_PERIOD to check for COM port availability
        */
        private static void TimerPortCOM_Tick(object sender, EventArgs e)
        {
            if (!backPortCOM.IsBusy)
            {
                backPortCOM.RunWorkerAsync();
            }
        }

        /*
        Background task called from the timer in order to check for COM port availability
        */
        private static void BackPortCOM_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                //Get the USB devices
                ManagementObjectCollection collection;
                using (var searcher = new ManagementObjectSearcher("root\\CIMV2", "Select * From Win32_PnPEntity WHERE ClassGuid=\"{4d36e978-e325-11ce-bfc1-08002be10318}\""))
                {
                    collection = searcher.Get();
                }

                //Search the desired COM ports
                List<string> listPorts = new List<string>();
                foreach (var device in collection)
                {
                    string sName        = device.GetPropertyValue("Name").ToString();
                    string[] sPartsID   = device.GetPropertyValue("PNPDeviceID").ToString().Split('\\');

                    //Check the format of the port details
                    if (sName.Contains("COM") && sPartsID.Length == 3)
                    {
                        if (sPartsID[1].Contains("VID_") && sPartsID[1].Contains("PID_"))
                        {
                            //Get the vendor and product identifiers
                            string sVID = sPartsID[1].Substring(sPartsID[1].IndexOf("VID_") + 4, 4);
                            string sPID = sPartsID[1].Substring(sPartsID[1].IndexOf("PID_") + 4, 4);

                            //Check if the port contains any FT information
                            string[] sParts = sPartsID[1].Split('+');
                            if (sParts.Length == 3)
                            {
                                listPorts.Add(sName[sName.IndexOf("COM") + 3] + ":" + sVID + ":" + sPID + ":" + sParts[2]);
                            }
                            else
                            {
                                listPorts.Add(sName[sName.IndexOf("COM") + 3] + ":" + sVID + ":" + sPID + ":NULL");
                            }
                        }
                    }
                }

                collection.Dispose();
                e.Result = listPorts;
            }
            catch
            {
                MessageBox.Show(ControlRegion.Dialogs.PortDetection_ErrorPortCOM, "Control EMG", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Result = new List<string>();
            }
        }

        /*
        Background connection task completed function
        */
        private static void BackPortCOM_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            int i = 0;
            List<string> listPorts = (List<string>)e.Result;
            while (i < listPortsCOM.Count)
            {
                if (!listPorts.Contains(listPortsCOM[i]))
                {
                    listPortsCOM.Remove(listPortsCOM[i]);
                }
                i++;
            }

            i = 0;
            while (i < listPorts.Count)
            {
                if (!listPortsCOM.Contains(listPorts[i]))
                {
                    listPortsCOM.Add(listPorts[i]);
                }
                i++;
            }
        }
    }
}
