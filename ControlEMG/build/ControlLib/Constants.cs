﻿/*
CONTROL EMG 2021

Constants.cs

- Description: Constants class
- Author: David Molina Toro
- Date: 16 - 02 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System.Windows.Media;

namespace ControlLib
{
    /*
    Contants class containing some pre-defined values for C# code
    */
    public static class Constants
    {
        //COMMON CONSTANTS
        public const int CONNECTION_UPDATE_PERIOD   = 5000;     //Connection status update period given in milliseconds
        public const int SERIAL_BUFFER_SIZE         = 256;      //Serial buffer size in bytes
        public const int STATUS_MESSAGE_SIZE        = 12;       //Status message size in bytes
        public const int DATA_MESSAGE_SIZE          = 26;       //Data message size in bytes
        public const int HEADER_MESSAGE_SIZE        = 4;        //Message header size in bytes
        public const int CHECKSUM_SIZE              = 2;        //Checksum size in bytes
        public const int CHANNEL_NUM                = 4;        //Number of EMG channels present
        public const int MAX_LABEL_LENGTH           = 11;       //Maximum length of a test label
        public const int NO_ERROR                   = 0;        //No error status value for control

        //DEFAULT REGISTERS
        public const ushort CH0_DEFAULT_REG     = 0x8001;       //Channel 0 default configuration register
        public const ushort CH1_DEFAULT_REG     = 0x9043;       //Channel 1 default configuration register
        public const ushort CH2_DEFAULT_REG     = 0xA085;       //Channel 2 default configuration register
        public const ushort CH3_DEFAULT_REG     = 0xB0C7;       //Channel 3 default configuration register
        public const ushort DEFAULT_CONFIG_REG  = 0x0860;       //Default channel configuration register
        public const uint DEFAULT_FILTER_REG    = 0x010002;     //Default channel filter register

        //DEFAULT SELECTION BITS
        public const ushort CHANNEL_ENABLED         = 0x1;  //Default channel bit enabled
        public const ushort CHANNEL_DISABLED        = 0x0;  //Default channel bit disabled
        public const ushort CHANNEL_BUFFERED        = 0x3;  //Default selection bits for buffered channel pin
        public const ushort CHANNEL_UNBUFFERED      = 0x0;  //Default selection bits for unbuffered channel pin
        public const ushort SYNC_4_FILTER           = 0x0;  //Default Sync4 filter configuration bits
        public const ushort SYNC_3_FILTER           = 0x2;  //Default Sync3 filter configuration bits
        public const ushort SYNC_4_FAST_FILTER      = 0x4;  //Default Sync4 fast settling filter configuration bits
        public const ushort SYNC_3_FAST_FILTER      = 0x5;  //Default Sync3 fast settling filter configuration bits
        public const ushort DEFAULT_DIVIDER         = 0x1;  //Default frequency divider for filtering
        public const ushort SINGLE_CYCLE_ENABLED    = 0x1;  //Default single cycle selection bit enabled
        public const ushort SINGLE_CYCLE_DISABLED   = 0x0;  //Default single cycle selection bit disabled

        //CONFIGURATION BIT POSITIONS
        public const ushort CHANNEL_ENABLE_POS      = 0xF;  //Channel status bit position
        public const ushort CHANNEL_ENABLE_NUM      = 0x1;  //Number of bits for the channel status
        public const ushort CHANNEL_REF_BUFFER_POS  = 0x7;  //Channel reference buffer status bit position
        public const ushort CHANNEL_REF_BUFFER_NUM  = 0x2;  //Number of bits for the channel reference buffer status
        public const ushort CHANNEL_AIN_BUFFER_POS  = 0x5;  //Channel analog input buffer status bit position
        public const ushort CHANNEL_AIN_BUFFER_NUM  = 0x2;  //Number of bits for the channel analog input buffer status
        public const ushort GAIN_RANGE_POS          = 0x0;  //Gain range bit position
        public const ushort GAIN_RANGE_NUM          = 0x3;  //Number of bits for the gain range
        public const ushort FILTER_MODE_POS         = 0x15; //Filter mode bit position
        public const ushort FILTER_MODE_NUM         = 0x3;  //Number of bits for the filter mode
        public const ushort FILTER_SINGLE_POS       = 0x10; //Single cycle bit position
        public const ushort FILTER_SINGLE_NUM       = 0x1;  //Number of bits for the single cycle
        public const ushort FILTER_DIVIDER_POS      = 0x0;  //Filter divider bit position
        public const ushort FILTER_DIVIDER_NUM      = 0xB;  //Number of bits for the filter divider

        //DEFAULT CONFIGURATION VALUES
        public const int GAIN_CONFIG_NUM            = 8;        //Number of gain configurations
        public const float DEFAULT_GAIN             = 1;        //ADC gain for the default configuration
        public const float EXT_VOLTAGE_REFERENCE    = 3.3F;     //ADC external reference voltage for the default configuration

        //REGISTER SIZES
        public const ushort CH_REG_SIZE     = 2;    //Channel register size in bytes for communications
        public const ushort GAIN_REG_SIZE   = 2;    //Gain register size in bytes for communications
        public const uint FILTER_REG_SIZE   = 4;    //Filter register size in bytes for communications

        //FILTER CONFIGURATION
        public const int ADC_CLOCK_RATE     = 614400;   //Main ADC clock rate in Hz (full power)
        public const int ADC_CLOCK_DIV      = 32;       //Clock divider for data rate calculation
        public const int SYNC_3_CLOCK_DIV   = 3;        //Sync3 filter divider for data rate calculation
        public const int SYNC_4_CLOCK_DIV   = 4;        //Sync4 filter divider for data rate calculation
        public const int MIN_DIVIDER_VAL    = 0;        //Minimum divider value (not included)
        public const int MAX_DIVIDER_VAL    = 2048;     //Maximum divider value (not included)
        public const int FILTER_AVERAGE     = 16;       //Filter average values (full power)

        //CONVERSION CONSTANTS
        public const int ADC_CODING_CONST               = 0x800000;                     //ADC output code conversion constant
        public const int CODED_LINE_SIZE                = 22;                           //Size of a raw ADC coded output data
        public const int DATA_FRAME_SIZE                = 20;                           //Coded output ADC frame size in bytes
        public const int FRAME_END_COUNT                = 2;                            //Number of bytes for each frame end
        public const uint MISSING_SAMPLE                = 0xFFFFFFFF;                   //Missing sample identifier for the coded data
        public static readonly byte[] FRAME_END_BYTES   = new byte[] { 0x0D, 0x0A };    //Frame end bytes for the output file

        //PLOTTING CONSTANTS
        public const int PLOT_DATA_COUNT    = 300;  //Amount of data values present in a single plot by time
        public const int PLOT_UPDATE_PERIOD = 100;  //Period required to update all plots in milliseconds

        //COLOR OBJECTS
        public static readonly Color COLOR_BUTTON_ENABLED   = Color.FromRgb(62, 83, 92);    //Enabled color for layout button
        public static readonly Color COLOR_BUTTON_DISABLED  = Color.FromRgb(100, 100, 100); //Disabled color for layout button
        public static readonly Color COLOR_SWITCH_ENABLED   = Color.FromRgb(75, 113, 129);  //Enabled color for layout switch
        public static readonly Color COLOR_SWITCH_DISABLED  = Color.FromRgb(112, 112, 112); //Disabled color for layout switch
        public static readonly Color COLOR_LAYOUT_ENABLED   = Color.FromRgb(45, 45, 62);    //Enabled color for panel control button
        public static readonly Color COLOR_LAYOUT_DISABLED  = Color.FromRgb(63, 63, 70);    //Disabled color for panel control button
    }
}
