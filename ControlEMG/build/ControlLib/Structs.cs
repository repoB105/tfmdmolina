﻿/*
CONTROL EMG 2021

Structs.cs

- Description: Structs class
- Author: David Molina Toro
- Date: 16 - 02 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System;
using System.Collections.Generic;

namespace ControlLib
{
    /*
    Status message definition
    */
    public struct StatusMessage
    {
        //Attributes
        public ushort iCurrentState;
        public ushort iLastState;
        public ushort iError;

        /*
        Parses the given byte array
        */
        public StatusMessage(byte[] data)
        {
            iCurrentState   = BitConverter.ToUInt16(data, 0 * sizeof(ushort));
            iLastState      = BitConverter.ToUInt16(data, 1 * sizeof(ushort));
            iError          = BitConverter.ToUInt16(data, 2 * sizeof(ushort));
        }
    };

    /*
    Control message definition
    */
    public struct ControlMessage
    {
        //Attributes
        public ushort iCommand;
        public ushort iError;

        /*
        Sets the command structure
        */
        public ControlMessage(Command iCommand_Value, ushort iError_Value)
        {
            iCommand = (ushort)iCommand_Value;
            iError   = iError_Value;
        }

        /*
        Parses the given byte array
        */
        public ControlMessage(byte[] data)
        {
            iCommand    = BitConverter.ToUInt16(data, 0 * sizeof(ushort));
            iError      = BitConverter.ToUInt16(data, 1 * sizeof(ushort));
        }

        /*
        Serializes the control structure
        */
        public byte[] Serialize()
        {
            //Create a new data list
            List<byte> listBytes = new List<byte>();

            //Parse the arguments as bytes
            listBytes.AddRange(BitConverter.GetBytes(iCommand));
            listBytes.AddRange(BitConverter.GetBytes(iError));

            return listBytes.ToArray();
        }
    };

    /*
    Configuration message definition
    */
    public struct ConfigMessage
    {
        //Attributes
        public ushort[] iChannel;
        public ushort[] iGain;
        public uint iFilter;

        /*
        Public constructor
        */
        public ConfigMessage(bool bDefault)
        {
            //Check if the default configuration should be applied
            if (bDefault)
            {
                //Set the default channel configuration
                iChannel = new ushort[]
                {
                    Constants.CH0_DEFAULT_REG,
                    Constants.CH1_DEFAULT_REG,
                    Constants.CH2_DEFAULT_REG,
                    Constants.CH3_DEFAULT_REG
                };

                //Set the default gain configuration
                iGain = new ushort[]
                {
                    Constants.DEFAULT_CONFIG_REG,
                    Constants.DEFAULT_CONFIG_REG,
                    Constants.DEFAULT_CONFIG_REG,
                    Constants.DEFAULT_CONFIG_REG
                };

                //Set the default filter configuration
                iFilter = Constants.DEFAULT_FILTER_REG;
            }
            else
            {
                iChannel    = new ushort[Constants.CHANNEL_NUM];
                iGain       = new ushort[Constants.CHANNEL_NUM];
                iFilter     = 0;
            }
        }

        /*
        Serializes the configuration structure
        */
        public byte[] Serialize()
        {
            //Create a new data list
            List<byte> listBytes = new List<byte>();

            //Parse the channels status as bytes
            for (int i = 0; i < Constants.CHANNEL_NUM; i++)
            {
                listBytes.AddRange(BitConverter.GetBytes(iChannel[i]));
            }

            //Parse the gain configuration as bytes
            for (int i = 0; i < Constants.CHANNEL_NUM; i++)
            {
                listBytes.AddRange(BitConverter.GetBytes(iGain[i]));
            }

            //Parse the filter as bytes
            listBytes.AddRange(BitConverter.GetBytes(iFilter));

            return listBytes.ToArray();
        }
    };

    /*
    Capture message definition
    */
    public struct CaptureMessage
    {
        //Attributes
        public long iUnixTime;
        public char[] cTestLabel;

        /*
        Public constructor
        */
        public CaptureMessage(bool bDefault)
        {
            //Check if the default capture should be applied
            if (bDefault)
            {
                //Set the default time
                iUnixTime = DateTimeOffset.Now.ToUnixTimeSeconds() + (long)(DateTime.Now - DateTime.UtcNow).TotalSeconds;

                //Set the default test label
                cTestLabel = new char[Constants.MAX_LABEL_LENGTH];
                Array.Copy("TEST".ToCharArray(), cTestLabel, "TEST".Length);
            }
            else
            {
                iUnixTime   = -1;
                cTestLabel  = new char[Constants.MAX_LABEL_LENGTH];
            }
        }

        /*
        Serializes the capture structure
        */
        public byte[] Serialize()
        {
            //Create a new data list
            List<byte> listBytes = new List<byte>();

            //Parse the arguments as bytes
            listBytes.AddRange(BitConverter.GetBytes(iUnixTime));
            for (int i = 0; i < Constants.MAX_LABEL_LENGTH; i++)
            {
                listBytes.Add((byte)cTestLabel[i]);
            }

            return listBytes.ToArray();
        }
    };

    /*
    Data message definition
    */
    public struct DataMessage
    {
        //Attributes
        public uint iFrameID;
        public uint[] iChannelData;

        /*
        Parses the given byte array
        */
        public DataMessage(byte[] data)
        {
            //Set the frame identifier
            iFrameID = BitConverter.ToUInt32(data, 0);

            //Initialize the data array
            iChannelData = new uint[Constants.CHANNEL_NUM];

            //Fill the channel data
            for (int i = 0; i < Constants.CHANNEL_NUM; i++)
            {
                iChannelData[i] = BitConverter.ToUInt32(data, sizeof(uint) + (i * sizeof(int)));
            }
        }
    };
}