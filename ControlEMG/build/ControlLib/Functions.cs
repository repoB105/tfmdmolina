﻿/*
CONTROL EMG 2021

Functions.cs

- Description: Common functions class
- Author: David Molina Toro
- Date: 08 - 06 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System;
using System.Windows;
using System.Windows.Media;
using System.Collections.Generic;

namespace ControlLib
{
    /*
    Class containing common functions
    */
    public static class Functions
    {

        /*
        Serializes a list of arguments to bytes
        */
        public static byte[] ToByteArray(List<ushort> listArguments)
        {
            //Create a new data list
            List<byte> listBytes = new List<byte>();

            //Parse the given arguments as bytes
            for (int i = 0; i < listArguments.Count; i++)
            {
                listBytes.AddRange(BitConverter.GetBytes(listArguments[i]));
            }

            return listBytes.ToArray();
        }

        /*
        Transform the output ADC code into an analog voltage value
        */
        public static double ToValueADC(uint iCode, float fReference, float fGain)
        {
            //Return the value based on the bipolar equation given by the manufacturer
            return (((float)iCode / Constants.ADC_CODING_CONST) - 1) * fReference / fGain;
        }


        /*
        Changes the bits in a value for a short
        */
        public static void ChangeBits(ref ushort iValue, ushort iBits, ushort iLength, ushort iPos)
        {
            //Get the mask from the length and position
            ushort iMask = (ushort)(((1 << iLength) - 1) << iPos);

            //Set the new values
            ushort iNewValues = (ushort)(iBits << iPos);

            //Set the resulting bits
            iValue = (ushort)((iValue & ~iMask) | iNewValues);
        }

        /*
        Changes the bits in a value for an integer
        */
        public static void ChangeBits(ref uint iValue, uint iBits, ushort iLength, ushort iPos)
        {
            //Get the mask from the length and position
            uint iMask = (uint)(((1 << iLength) - 1) << iPos);

            //Set the new values
            uint iNewValues = iBits << iPos;

            //Set the resulting bits
            iValue = (iValue & ~iMask) | iNewValues;
        }

        /*
        Returns the children of the given object with the given name
        */
        public static T FindChild<T>(DependencyObject parent, string sChildName) where T : DependencyObject
        {
            //Create a new found child object
            T foundChild = default;

            //Look for the child inside the given object
            int iChildrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < iChildrenCount; i++)
            {
                //Get the current child
                var child = VisualTreeHelper.GetChild(parent, i);

                //Check for the child type
                if (!(child is T))
                {
                    //Repeat the process until a child is found
                    foundChild = FindChild<T>(child, sChildName);

                    //Break if the child is found
                    if (foundChild != null)
                    {
                        break;
                    }
                }
                else if (!string.IsNullOrEmpty(sChildName))
                {
                    //Search for the child
                    if (child is FrameworkElement frameworkElement && frameworkElement.Name == sChildName)
                    {
                        //The child has been found by name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    //The child has been found the first time
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }
    }
}
