﻿/*
CONTROL EMG 2021

Enums.cs

- Description: Enumerations class
- Author: David Molina Toro
- Date: 16 - 02 - 2021
- Version: 1.0

Property of B105 UPM
*/

namespace ControlLib
{
	/*
	FSM states enumeration
	*/
	public enum State
	{
		DEFAULT		= 0,
		IDLE		= 1,
		PREPARED	= 2,
		CONFIGURING = 3,
		CAPTURING	= 4
	}

	/*
    FSM commands enumeration
    */
	public enum Command
    {
		NONE	= 0,
		INIT	= 1,
		START	= 2,
		STOP	= 3,
		CONFIG	= 4,
		ADCSET	= 5,
		STOERR	= 6,
		STATUS	= 7,
		RESTART	= 8
	}
}
