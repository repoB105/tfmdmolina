﻿/*
CONTROL EMG 2021

PanelInfo.cs

- Description: Application information panel
- Author: David Molina Toro
- Date: 08 - 06 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System.Windows.Controls;

namespace ControlGUI
{
    public partial class PanelInfo : UserControl
    {
        /*
        Public constructor
        */
        public PanelInfo()
        {
            InitializeComponent();

            //Set the information text
            textInfo.Text = ControlRegion.Strings.PanelInfo_Information;
        }
    }
}
