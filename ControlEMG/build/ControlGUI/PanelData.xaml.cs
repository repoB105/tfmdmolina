﻿/*
CONTROL EMG 2021

PanelData.cs

- Description: Application data visualization panel
- Author: David Molina Toro
- Date: 08 - 06 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System;
using System.IO;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Globalization;
using System.ComponentModel;
using System.Windows.Controls;
using System.Collections.Generic;

//Common library
using ControlLib;

namespace ControlGUI
{
    public partial class PanelData : UserControl
    {
        /*
        Public constructor
        */
        public PanelData()
        {
            InitializeComponent();

            //Configure all plots
            controlPlot_1.ConfigurePlot(1);
            controlPlot_2.ConfigurePlot(2);
            controlPlot_3.ConfigurePlot(3);
            controlPlot_4.ConfigurePlot(4);

            //Initialize the update timer
            timerUpdate             = new Timer(Constants.PLOT_UPDATE_PERIOD);
            timerUpdate.Enabled     = false;
            timerUpdate.Elapsed     += TimerUpdate_Ellapsed;
            timerUpdate.AutoReset   = true;

            //Connect the controller status event to this class
            ControlLibConnection.IControlConnection.Instance.GetConnected().PropertyChanged += HandleConnectedEvent;
        }

        /*
        User control loaded function
        */
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        /*
        Connection status event handler
        */
        private void HandleConnectedEvent(object sender, PropertyChangedEventArgs e)
        {
            //Get the connection status
            bool bConnected = ControlLibConnection.IControlConnection.Instance.GetConnected().Checked;

            //Set the start button status
            buttonStart.IsEnabled   = bConnected;
            gridStart.Background    = bConnected ? new SolidColorBrush(Constants.COLOR_BUTTON_ENABLED) : new SolidColorBrush(Constants.COLOR_BUTTON_DISABLED);

            //Set the stop button status
            buttonStop.IsEnabled    = bConnected;
            gridStop.Background     = bConnected ? new SolidColorBrush(Constants.COLOR_BUTTON_ENABLED) : new SolidColorBrush(Constants.COLOR_BUTTON_DISABLED);
        }

        /*
        Start capturing data function
        */
        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            //Set a new capture message
            CaptureMessage captureMessage = new CaptureMessage(false);

            //Set the unix time
            captureMessage.iUnixTime = DateTimeOffset.Now.ToUnixTimeSeconds() + (long)(DateTime.Now - DateTime.UtcNow).TotalSeconds;

            //Set the capture message label
            Array.Copy("TEST".ToCharArray(), captureMessage.cTestLabel, "TEST".Length);

            //Send the capture message
            ControlLibConnection.IControlComms.Instance.SendCapture(captureMessage);

            //Enable the plot update timer
            timerUpdate.Enabled = true;

            //Clear all plots
            ClearPlots(true);
        }

        /*
        Stop capturing data function
        */
        private void ButtonStop_Click(object sender, RoutedEventArgs e)
        {
            //Clear all plots
            ClearPlots(false);

            //Disable the plot update timer
            timerUpdate.Enabled = false;

            //Send the stop message
            ControlLibConnection.IControlComms.Instance.SendCommand(Command.STOP);
        }

        //Update timer object
        Timer timerUpdate;

        /*
        Timer update ellapsed callback
        */
        private void TimerUpdate_Ellapsed(object sender, ElapsedEventArgs e)
        {
            //Update all the plots
            controlPlot_1.UpdatePlot();
            controlPlot_2.UpdatePlot();
            controlPlot_3.UpdatePlot();
            controlPlot_4.UpdatePlot();
        }

        /*
        Clears all the panel plots
        */
        private void ClearPlots(bool bBind)
        {
            //Clear all the plots
            controlPlot_1.ClearPlot(bBind);
            controlPlot_2.ClearPlot(bBind);
            controlPlot_3.ClearPlot(bBind);
            controlPlot_4.ClearPlot(bBind);
        }

        /*
        Process the selected data function
        */
        private void ButtonProcess_Click(object sender, RoutedEventArgs e)
        {
            //Select the folder to process
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //Change the cursor and disable the panel
                Mouse.OverrideCursor    = Cursors.Wait;
                this.IsEnabled          = false;

                //Check if the data directory exists
                string sDataDir = Path.Combine(Common.sExeDir, "data");
                if (!Directory.Exists(sDataDir))
                {
                    Directory.CreateDirectory(sDataDir);
                }

                //Check if the target log file already exists
                string sLogPath = Path.Combine(sDataDir, "Session.log");
                if (File.Exists(sLogPath))
                {
                    File.Delete(sLogPath);
                }

                //Create a new stream writer for the log statistics
                StreamWriter writerLog = File.CreateText(sLogPath);

                //Get all binary files from the directory and iterate through them
                bool bError     = false;
                string[] sFiles = Directory.GetFiles(dialog.SelectedPath, "*.bin", SearchOption.AllDirectories);
                for (int i = 0; i < sFiles.Length; i++)
                {
                    //Set a catch for the cursor to stay safe
                    try
                    {
                        //Store the test name
                        writerLog.WriteLine("Test name: " + Path.GetFileNameWithoutExtension(sFiles[i]));

                        //Read the file as bytes
                        List<byte> listBytes = new List<byte>(File.ReadAllBytes(sFiles[i]));

                        //Check if the target file already exists
                        string sFilePath = Path.Combine(sDataDir, Path.GetFileNameWithoutExtension(sFiles[i]) + ".csv");
                        if (File.Exists(sFilePath))
                        {
                            File.Delete(sFilePath);
                        }

                        //Create a new stream writer for the converted data
                        StreamWriter writer = File.CreateText(sFilePath);

                        //Statistic variables declaration
                        int iTotal      = 0;
                        int[] iMissing  = new int[Constants.CHANNEL_NUM] { 0, 0, 0, 0 };

                        //Iterate through all the read data
                        int iCount = 0;
                        while (iCount < listBytes.Count - Constants.CODED_LINE_SIZE)
                        {
                            //Check the end frame bytes
                            byte[] bytesEnd = listBytes.GetRange(iCount + Constants.CODED_LINE_SIZE - Constants.FRAME_END_COUNT, Constants.FRAME_END_COUNT).ToArray();
                            if (bytesEnd[0] == Constants.FRAME_END_BYTES[0] && bytesEnd[1] == Constants.FRAME_END_BYTES[1])
                            {
                                //Set the data frame from the raw information
                                DataMessage dataMessage = new DataMessage(listBytes.GetRange(iCount, Constants.DATA_FRAME_SIZE).ToArray());

                                //Write the frame identifier
                                writer.Write(dataMessage.iFrameID.ToString(CultureInfo.InvariantCulture));

                                //Convert the output code into a value
                                for (int j = 0; j < Constants.CHANNEL_NUM; j++)
                                {
                                    //Check if the value is missing
                                    if (dataMessage.iChannelData[j] == Constants.MISSING_SAMPLE)
                                    {
                                        iMissing[j]++;
                                    }

                                    double dValue = Functions.ToValueADC(dataMessage.iChannelData[j], Constants.EXT_VOLTAGE_REFERENCE, Constants.DEFAULT_GAIN);

                                    //Write the converted data
                                    writer.Write("," + dValue.ToString(CultureInfo.InvariantCulture));
                                }

                                //Increase the total samples
                                iTotal++;

                                //Write a new line
                                writer.WriteLine("");

                                //Increment the file counter
                                iCount += Constants.CODED_LINE_SIZE;
                            }
                            else
                            {
                                //Increment the file counter
                                iCount++;
                            }
                        }

                        //Store the missing statistics
                        for (int j = 0; j < Constants.CHANNEL_NUM; j++)
                        {
                            writerLog.WriteLine("Missing in channel " + j.ToString("0") + ": " + ((float)iMissing[j] / iTotal * 100).ToString("0.00") + "%");
                        }

                        //Write the samples recorded
                        writerLog.WriteLine("Samples recorded: " + iTotal.ToString("0"));
                        writerLog.WriteLine("");

                        //Close the file writer
                        writer.Close();
                    }
                    catch
                    {
                        bError = true;
                    }
                }

                //Close the statistics file
                writerLog.Close();

                //Check if there have been any errors
                if (bError)
                {
                    MessageBox.Show(ControlRegion.Dialogs.PanelData_ProcessError, "Control EMG", MessageBoxButton.OK, MessageBoxImage.Error);

                    //Reset the cursor and enable the panel
                    Mouse.OverrideCursor = null;
                    this.IsEnabled = true;
                }

                //Reset the cursor and enable the panel
                Mouse.OverrideCursor = Cursors.Arrow;
                this.IsEnabled = true;
            }
        }
    }
}
