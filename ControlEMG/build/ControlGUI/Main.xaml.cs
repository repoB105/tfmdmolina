﻿/*
CONTROL EMG 2021

Main.cs

- Description: Contains the application entry point
- Author: David Molina Toro
- Date: 10 - 02 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System.Windows;

namespace ControlGUI
{
    /*
    Application entry point
    */
    public partial class App : Application
    {

    }
}