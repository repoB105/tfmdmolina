﻿/*
CONTROL EMG 2021

PanelConfig.cs

- Description: Application configuration panel
- Author: David Molina Toro
- Date: 17 - 02 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System.Windows;
using System.Windows.Media;
using System.ComponentModel;
using System.Windows.Controls;

//Common library
using ControlLib;

namespace ControlGUI
{
    public partial class PanelConfig : UserControl
    {
        /*
        Public constructor
        */
        public PanelConfig()
        {
            InitializeComponent();

            //Connect the controller status event to this class
            ControlLibConnection.IControlConnection.Instance.GetConnected().PropertyChanged += HandleConnectedEvent;

            //Set the channel control labels
            controlChannel_1.SetLabel(1);
            controlChannel_2.SetLabel(2);
            controlChannel_3.SetLabel(3);
            controlChannel_4.SetLabel(4);
        }

        /*
        Connection status event handler
        */
        private void HandleConnectedEvent(object sender, PropertyChangedEventArgs e)
        {
            //Get the connection status
            bool bConnected = ControlLibConnection.IControlConnection.Instance.GetConnected().Checked;

            //Set the apply button activation
            buttonApply.IsEnabled   = bConnected;
            gridApply.Background    = bConnected ? new SolidColorBrush(Constants.COLOR_BUTTON_ENABLED) : new SolidColorBrush(Constants.COLOR_BUTTON_DISABLED);

            //Set the status button activation
            buttonStatus.IsEnabled  = bConnected;
            gridStatus.Background   = bConnected ? new SolidColorBrush(Constants.COLOR_BUTTON_ENABLED) : new SolidColorBrush(Constants.COLOR_BUTTON_DISABLED);
        }

        /*
        Configuration button click event handler
        */
        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            //Create a new configuration message
            ConfigMessage configMessage = new ConfigMessage(true);

            //Get the channel configurations
            for (int i = 0; i < Constants.CHANNEL_NUM; i++)
            {
                //Get the current channel configuration element
                ControlLibElements.ControlChannel controlChannel = Functions.FindChild<ControlLibElements.ControlChannel>(gridMain, "controlChannel_" + (i + 1).ToString("0"));

                //Get the configuration parameters of the channel
                controlChannel.GetConfig(out ushort iEnabled, out ushort iRefBuffer, out ushort iAinBuffer, out ushort iRange);

                //Set the channel status configuration
                Functions.ChangeBits(ref configMessage.iChannel[i], iEnabled, Constants.CHANNEL_ENABLE_NUM, Constants.CHANNEL_ENABLE_POS);

                //Set the channel reference buffer status configuration
                Functions.ChangeBits(ref configMessage.iGain[i], iRefBuffer, Constants.CHANNEL_REF_BUFFER_NUM, Constants.CHANNEL_REF_BUFFER_POS);

                //Set the channel analog input buffer status configuration
                Functions.ChangeBits(ref configMessage.iGain[i], iAinBuffer, Constants.CHANNEL_AIN_BUFFER_NUM, Constants.CHANNEL_AIN_BUFFER_POS);

                //Set the gain range configuration
                Functions.ChangeBits(ref configMessage.iGain[i], iRange, Constants.GAIN_RANGE_NUM, Constants.GAIN_RANGE_POS);
            }

            //Get the filter configuration
            controlFilter.GetConfig(out uint iMode, out uint iSingleCycle, out uint iDivider);

            //Set the filter mode configuration
            Functions.ChangeBits(ref configMessage.iFilter, iMode, Constants.FILTER_MODE_NUM, Constants.FILTER_MODE_POS);

            //Set the single cycle configuration
            Functions.ChangeBits(ref configMessage.iFilter, iSingleCycle, Constants.FILTER_SINGLE_NUM, Constants.FILTER_SINGLE_POS);

            //Set the filter mode configuration
            Functions.ChangeBits(ref configMessage.iFilter, iDivider, Constants.FILTER_DIVIDER_NUM, Constants.FILTER_DIVIDER_POS);

            //Send the configuration message
            ControlLibConnection.IControlComms.Instance.SendConfig(configMessage);
        }

        /*
        Status button click event handler
        */
        private void ButtonStatus_Click(object sender, RoutedEventArgs e)
        {
            //Send the status command
            ControlLibConnection.IControlComms.Instance.SendCommand(Command.STATUS);
        }
    }
}
