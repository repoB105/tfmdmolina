﻿/*
CONTROL EMG 2021

MainWindow.cs

- Description: Application main window
- Author: David Molina Toro
- Date: 10 - 02 - 2021
- Version: 1.0

Property of B105 UPM
*/

using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;

namespace ControlGUI
{
    public partial class MainWindow : Window
    {
        /*
        Public constructor
        */
        public MainWindow()
        {
            InitializeComponent();

            //Center the main window
            this.Left   = (SystemParameters.PrimaryScreenWidth / 2) - (this.Width / 2);
            this.Top    = (SystemParameters.PrimaryScreenHeight / 2) - (this.Height / 2);

            //Enable port detection system
            ControlLib.PortDetection.EnablePortDetection(true);

            //Bind the panel selection event
            controlPanels.SetPanelEvent += HandleSetPanelEvent;
        }

        /*
        Close button function
        */
        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            //Close the application
            Application.Current.Shutdown(0);
        }

        /*
        Close button function
        */
        private void ButtonMinimize_Click(object sender, RoutedEventArgs e)
        {
            //Minimize the main window
            WindowState = WindowState.Minimized;
        }

        /*
        Top grid mouse down function
        */
        private void GridTop_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //Drag the main window if able
            if (e.ChangedButton == MouseButton.Left)
            {
                this.DragMove();
            }
        }

        /*
        Panel selection event handler
        */
        private void HandleSetPanelEvent(string sPanel)
        {
            //Hide all panels
            gridConfig.Visibility   = Visibility.Hidden;
            gridData.Visibility     = Visibility.Hidden;
            gridInfo.Visibility     = Visibility.Hidden;

            //Show the selected panel
            ControlLib.Functions.FindChild<Grid>(gridMain, "grid" + sPanel).Visibility = Visibility.Visible;
        }
    }
}
