/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * uart_stdio.c
 *
 *  Created on: 7 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file uart_stdio.c
 */

#include "uart_arch.h"
#include "system_api.h"

#define OUTPUT_BUFFER_SIZE	128

/**
 *
 * @return
 */
retval_t uart_stdio_init(void){
#if (ENABLE_STDIO_FUNCS && (STDIO_INTERFACE==UART_STDIO))
	uart_arch_init();
	uart_arch_start_storing_data();
	uart_arch_enable_echo();
	return RET_OK;
#endif
	return RET_ERROR;
}

/**
 *
 * @return
 */
retval_t uart_stdio_deinit(void){
#if (ENABLE_STDIO_FUNCS && (STDIO_INTERFACE==UART_STDIO))
	uart_arch_stop_storing_data();
	uart_arch_deinit();
	uart_arch_disable_echo();
	return RET_OK;
#endif
	return RET_ERROR;
}

/**
 *
 * @param format
 */
void uart_printf(char* format, ...){
#if (ENABLE_STDIO_FUNCS && (STDIO_INTERFACE==UART_STDIO))

	uint8_t* buff = (uint8_t* ) ytMalloc(OUTPUT_BUFFER_SIZE);
	va_list argptr;
	va_start(argptr, format);
	vsnprintf((char*)buff, OUTPUT_BUFFER_SIZE-1, format, argptr);
	va_end(argptr);
	uart_arch_write(buff, strlen((char*)buff), YT_WAIT_FOREVER);
	ytFree(buff);

#endif

}

/**
 *
 * @param data
 * @param size
 * @return
 */
retval_t uart_stdout_send(uint8_t* data, uint16_t size, uint32_t timeout){
#if (ENABLE_STDIO_FUNCS && (STDIO_INTERFACE==UART_STDIO))
	if(size < OUTPUT_BUFFER_SIZE){
		return uart_arch_write(data, size, timeout);
	}

#endif
	return RET_ERROR;
}

retval_t uart_stdin_read(uint8_t* data, uint16_t size, uint32_t timeout){
#if (ENABLE_STDIO_FUNCS && (STDIO_INTERFACE==UART_STDIO))
	return uart_arch_read(data, size, timeout);
#endif
	return RET_ERROR;
}

/**
 *
 * @param data
 * @param size
 * @param timeout
 * @return
 */
retval_t uart_stdin_read_line(uint8_t* data, uint16_t size, uint32_t timeout){
#if (ENABLE_STDIO_FUNCS && (STDIO_INTERFACE==UART_STDIO))
	return uart_arch_read_line(data, size, timeout);
#endif
	return RET_ERROR;
}


