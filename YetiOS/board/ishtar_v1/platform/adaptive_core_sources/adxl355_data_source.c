/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adxl355_data_source.c
 *
 *  Created on: 20 de jul. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adxl355_data_source.c
 */

#include "system_api.h"


#if ADXL355_ACEL_ADAPT_SRC

#define ADXL355_SAMPLE_PERIOD	30

#include "data_source.h"
#include "adxl355_driver.h"
#include "adxl355_data_source.h"

#define BUFFER_SIZE		8

static data_source_t adxl355_data_source;

/* Init and de-init functions */
static retval_t init_adxl355_source(void);
static retval_t deinit_adxl355_source(void);

static retval_t adxl355_sample_func(data_source_t* data_source);

/* Init and de-init functions */
/**
 *
 * @return
 */
static retval_t init_adxl355_source(void){
	uint32_t adxl355_fd;

	if(init_data_source(&adxl355_data_source, ADXL355_SAMPLE_PERIOD, adxl355_sample_func, ADXL355_SOURCE) != RET_OK){
		return RET_ERROR;
	}

	adxl355_fd = ytOpen(ADXL355_DEV, 0);
	ytIoctl(adxl355_fd, (uint16_t) ADXL355_SET_SCALE, (void*) ADXL355_FULLSCALE_2);
	ytIoctl(adxl355_fd, (uint16_t) ADXL355_SET_ODR, (void*) ADXL355_ODR_4000Hz);
	ytIoctl(adxl355_fd, (uint16_t) ADXL355_SET_AXIS, (void*) (AXIS_X_ENABLE | AXIS_Y_ENABLE | AXIS_Z_ENABLE));
	ytIoctl(adxl355_fd, (uint16_t) ADXL355_SET_MODE, (void*) ADXL355_NORMAL);
	ytClose(adxl355_fd);

	adxl355_data_source.buffer_size = BUFFER_SIZE;
	adxl355_data_source.data_buffer = (void*) ytMalloc(sizeof(adlx355_data_source_t)*BUFFER_SIZE);

	if(register_data_source(&adxl355_data_source) != RET_OK){
		ytFree(adxl355_data_source.data_buffer);
		deinit_data_source(&adxl355_data_source);
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 *
 * @return
 */
static retval_t deinit_adxl355_source(void){
	ytFree(adxl355_data_source.data_buffer);
	deinit_data_source(&adxl355_data_source);
	return RET_OK;
}


/* Sampling function */
/**
 *
 * @param data_source
 * @return
 */
static retval_t adxl355_sample_func(data_source_t* data_source){
	if(data_source == NULL){
		return RET_ERROR;
	}

	uint32_t adxl355_fd;
	adlx355_data_source_t* adxl355_buffer = (adlx355_data_source_t*) adxl355_data_source.data_buffer;

	if(data_source->buffer_counter >= BUFFER_SIZE){
		data_source->buffer_counter = 0;
	}


	if((adxl355_fd = ytOpen(ADXL355_DEV, 0))){
		ytRead(adxl355_fd, &adxl355_buffer[data_source->buffer_counter], 1);		//Read 1 data from each axis
		ytClose(adxl355_fd);

		data_source->buffer_counter++;

		return RET_OK;
	}


	//adxl355_fd cannot be opened -> Error
	return RET_ERROR;
}


/*Initialization macros*/
init_source(init_adxl355_source);
deinit_source(deinit_adxl355_source);

#endif
