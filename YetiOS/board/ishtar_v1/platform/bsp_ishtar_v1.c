/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * bsp_ishtar_v1.c
 *
 *  Created on: 27 de dic. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file bsp_ishtar_v1.c
 */

#include "bsp_ishtar_v1.h"
#include "netstack.h"
#include "sys_gpio.h"
#include "process.h"
#include "system_api.h"

retval_t init_pattern(void);
retval_t yetimote_fs_init(void);

retval_t init_platform(void){

	if(initProcessList() != RET_OK){
		return RET_ERROR;
	}

	rtc_time_init();

	leds_init();

	gpio_init();

#if ENABLE_TIME_MEASURE_FUNCS
	init_time_meas();
#endif

#if INIT_MOUNT_SD_FILESYSTEM
	yetimote_fs_init();
#endif

	stdio_init();

	//CS del ADXL355 debe estar a nivel alto para que funcione el stack radio
	y_gpio_init_pin(GPIO_PIN_C13, GPIO_PIN_OUTPUT_OPEN_DRAIN, GPIO_PIN_PULLUP);
	gpio_pin_set(GPIO_PIN_C13);

	init_devices();

	init_pattern();

	initUserProcesses();

#if ENABLE_NETSTACK_ARCH
	netstack_init();
#endif

	ytDelay(10);

	_printf(">System Started\r\n>");
	return RET_OK;
}


retval_t yetimote_fs_init(void){

    while(f_mount(&YetimoteFatFs, "", 1) != FR_OK){
#ifdef ERROR_LED
    	ytLedsOn(ERROR_LED);
#endif
    	ytDelay(50);
    }

    leds_off(LEDS_RED1);
	return RET_OK;
}

retval_t init_pattern(void){

	ytLedsOn(LEDS_BLUE);
	ytDelay(50);
	ytLedsOn(LEDS_GREEN);
	ytDelay(50);
	ytLedsOn(LEDS_RED1);
	ytDelay(50);
	ytLedsOn(LEDS_RED2);
	ytDelay(50);

	ytLedsOff(LEDS_RED2);
	ytDelay(50);
	ytLedsOff(LEDS_RED1);
	ytDelay(50);
	ytLedsOff(LEDS_GREEN);
	ytDelay(50);
	ytLedsOff(LEDS_BLUE);

	return RET_OK;

}
