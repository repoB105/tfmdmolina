/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define GPIO_Analog_free_PC13_Pin GPIO_PIN_13
#define GPIO_Analog_free_PC13_GPIO_Port GPIOC
#define CC2500_GDO2_EXTI0_Pin GPIO_PIN_0
#define CC2500_GDO2_EXTI0_GPIO_Port GPIOC
#define CC2500_GDO2_EXTI0_EXTI_IRQn EXTI0_IRQn
#define GPIO_Analog_Free_Current_M_Pin GPIO_PIN_1
#define GPIO_Analog_Free_Current_M_GPIO_Port GPIOC
#define GPIO3_868_EXTI2_Pin GPIO_PIN_2
#define GPIO3_868_EXTI2_GPIO_Port GPIOC
#define GPIO3_868_EXTI2_EXTI_IRQn EXTI2_IRQn
#define GPIO_Free_PC3_Pin GPIO_PIN_3
#define GPIO_Free_PC3_GPIO_Port GPIOC
#define RI_ADC1__Pin GPIO_PIN_0
#define RI_ADC1__GPIO_Port GPIOA
#define RI_ADC1_A1_Pin GPIO_PIN_1
#define RI_ADC1_A1_GPIO_Port GPIOA
#define GPIO_Analog_Free_BAT_LVL_Pin GPIO_PIN_4
#define GPIO_Analog_Free_BAT_LVL_GPIO_Port GPIOA
#define RQ_ADC2__Pin GPIO_PIN_6
#define RQ_ADC2__GPIO_Port GPIOA
#define RQ_ADC2_A7_Pin GPIO_PIN_7
#define RQ_ADC2_A7_GPIO_Port GPIOA
#define GPIO3_433_EXTI4_Pin GPIO_PIN_4
#define GPIO3_433_EXTI4_GPIO_Port GPIOC
#define GPIO3_433_EXTI4_EXTI_IRQn EXTI4_IRQn
#define GPIO_Analog_Free_Solar_M_Pin GPIO_PIN_5
#define GPIO_Analog_Free_Solar_M_GPIO_Port GPIOC
#define CS_868_Pin GPIO_PIN_0
#define CS_868_GPIO_Port GPIOB
#define BUTTON_EXTI1_Pin GPIO_PIN_1
#define BUTTON_EXTI1_GPIO_Port GPIOB
#define BUTTON_EXTI1_EXTI_IRQn EXTI1_IRQn
#define CS_CC2500_Pin GPIO_PIN_2
#define CS_CC2500_GPIO_Port GPIOB
#define GPIO_Free_PB10_Pin GPIO_PIN_10
#define GPIO_Free_PB10_GPIO_Port GPIOB
#define GPIO_Free_PB11_Pin GPIO_PIN_11
#define GPIO_Free_PB11_GPIO_Port GPIOB
#define CS_433_Pin GPIO_PIN_12
#define CS_433_GPIO_Port GPIOB
#define SDC_868_Pin GPIO_PIN_6
#define SDC_868_GPIO_Port GPIOC
#define SDN_433_Pin GPIO_PIN_7
#define SDN_433_GPIO_Port GPIOC
#define LD4_GPIO_Pin GPIO_PIN_8
#define LD4_GPIO_GPIO_Port GPIOA
#define LD3_GPIO_Pin GPIO_PIN_15
#define LD3_GPIO_GPIO_Port GPIOA
#define LD2_GPIO_Pin GPIO_PIN_4
#define LD2_GPIO_GPIO_Port GPIOB
#define LD1_GPIO_Pin GPIO_PIN_5
#define LD1_GPIO_GPIO_Port GPIOB
#define GPIO_Free_PB8_Pin GPIO_PIN_8
#define GPIO_Free_PB8_GPIO_Port GPIOB
#define GPIO_Free_PB9_Pin GPIO_PIN_9
#define GPIO_Free_PB9_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
