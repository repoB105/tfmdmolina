/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * platform-conf.h
 *
 *  Created on: 28 de ago. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file platform-conf.h
 */
#ifndef APPLICATION_BOARD_PLATFORM_PLATFORM_CONF_H_
#define APPLICATION_BOARD_PLATFORM_PLATFORM_CONF_H_

#include "types.h"
#include "platform-cpu.h"
#include "cmsis_os.h"
#include "FreeRTOSConfig.h"
#include "sys_def.h"
#include "arm_math.h"
#include "leds.h"

/* SD Card Filesystem */
#define INIT_MOUNT_SD_FILESYSTEM	0	//Select to mount the sd card filesystem on init.

/* Error led */
#define ERROR_LED	LED_BLUE

/* LOW POWER FEATURES */
#define LOW_POWER_IDLE_RTOS			configUSE_TICKLESS_IDLE		//Select using or not a tickless iddle in FreeRTOS. Change the configUSE_TICKLESS_IDLE define to the desired value: 1 enable low power, 0 disable low power
#if LOW_POWER_IDLE_RTOS
	#define USE_STOP_MODE			1							//Use uC stop mode in low power mode. It achieves the lowest consumption. Default:1
	#define USE_SLEEP_MODE			!USE_STOP_MODE				//Use uC sleep mode instad of stop. The power consumption is much higher as peripherals remains activated
#endif
#define ENABLE_STOP_MODE_DEBUG		0							//Selects to enable debug when the uC is in stop mode. This option must be disabled to reduce power consumption as much as possible
#define HAL_SYNC_WITH_RTOS_SYSTICK	0							//Select to synchronize the rtos systick with HAL tick when the uC is in sleep node. Otherwise, HAL_Delay() will wait actively the selected time plus the sleep time. Default: 0


/* TRACE AND STATS OPTIONS */
#define ENABLE_TRACE_RTOS			configUSE_TRACE_FACILITY		//Enables or disables taking rtos traces.
#if ENABLE_TRACE_RTOS
	#define USE_TRACEALYZER_SW		1								//Enables using Tracealyzer Percepio software. User software that uses USB will not work, as the trace recorder facility makes use of the USB_CDC device
																	//If this option is used the system will not start until the Percepio Tracealyzer software connects to it
	#define ENABLE_RUNTIME_STATS	configGENERATE_RUN_TIME_STATS	//Enables or disables taking runtime stats of the executing tasks. These stats can be collected by vTaskGetRunTimeStats(char*) function
#endif

/* STANDARD INPUT OUTPUT */
#define ENABLE_STDIO_FUNCS			1					//Enable or disables using the STDIO funcs
#if ENABLE_STDIO_FUNCS
	#define STDIO_INTERFACE				USB_STDIO			//Selects the STDIO peripheral interface. It could be UART or USB

	#if USE_TRACEALYZER_SW
		#undef STDIO_INTERFACE
		#define STDIO_INTERFACE		UART_STDIO			//Selects the STDIO peripheral interface. It could be UART or USB
	#endif
	#define USE_YETISHELL			1					//Enable or disables using the yetishell
#endif



/* HARDWARE WATCHDOG ENABLE */
#define USE_HW_WATCHDOG			0

/* ENABLED DRIVERS */
#define ENABLE_SPI_DRIVER		1
#define ENABLE_I2C_DRIVER		1
#define ENABLE_ADC_DRIVER		1

#if ((ENABLE_STDIO_FUNCS) && (STDIO_INTERFACE != UART_STDIO)) || (!ENABLE_STDIO_FUNCS)
	#define ENABLE_UART_DRIVER		1
#endif

#if (((ENABLE_STDIO_FUNCS) && (STDIO_INTERFACE != USB_STDIO)) || (!ENABLE_STDIO_FUNCS)) && (!USE_TRACEALYZER_SW)
	#define ENABLE_USB_DRIVER		1
#endif

#if USE_STOP_MODE			//USB NOT WORK WHEN USING STOP MODE
	#undef ENABLE_USB_DRIVER
	#if ((ENABLE_STDIO_FUNCS) && (STDIO_INTERFACE == USB_STDIO))
		#undef STDIO_INTERFACE
		#undef ENABLE_STDIO_FUNCS
	#endif
#endif

/* PERIPHERAL DRIVERS */
#if ENABLE_SPI_DRIVER
	#define SPI2_DEVICE_ID	2
	#define SPI_DEV_2	"DEV_SPI2"
#endif
#if ENABLE_I2C_DRIVER
	#define I2C_DEVICE_ID	3
	#define I2C_DEV		"DEV_I2C"
#endif
#if ENABLE_UART_DRIVER
	#define UART_DEVICE_ID	4
	#define UART_DEV	"DEV_UART"
#endif
#if ENABLE_ADC_DRIVER
	#define ADC_DEVICE_ID	5
	#define ADC_DEV		"DEV_ADC"
#endif
#if ENABLE_USB_DRIVER
	#define USB_DEVICE_ID	6
	#define USB_DEV		"DEV_USB"
#endif

/* RADAR PROCESSING*/
#if USE_ARM_DSP_LIB
	#define USE_RADAR_PROCESSING 0
#endif

/* RTC TIME */
#define ENABLE_RTC_TIMESTAMP	1
#define INIT_TIMESTAMP 1475582400000	//4th October 2016 12:00 //TIMESTAMP IN MS


/* TIME MEASUREMENT FUNCTIONS */
#define ENABLE_TIME_MEASURE_FUNCS	1					//Enable compiling and using functions for time measurements
#if ENABLE_TIME_MEASURE_FUNCS
	#define USE_HIGH_RES_TIMER		1							//Use high resolution timer with microseconds resolution. It is important to note that high res timers may be off during uC sleep, so measurements could be unreliable in these cases
	#define HIGH_SPEED_TIMER_FREQ		1000000					//Frequency of the timer. Default 1 MHz
#endif

/* OPERATING SYSTEM PROCESSES */
#define MAX_NUM_PROCESSES					24			//Numero m�ximo de procesos simult�neos que el sistema operativo soporta
#define CHILD_PROCESSES_LINKED_WITH_PARENTS	1			//Selecciona si se desea que los procesos hijos dependan de los padres o no

/*NODE ID LOCATION STM32L4*/
#define NODEID_LOCATION_BASE ((uint32_t*)0x1FFF7590)
#define USE_16_BITS_UNIQUE_ID	0
#define USE_32_BITS_UNIQUE_ID	0
#define USE_64_BITS_UNIQUE_ID	0
#define USE_96_BITS_UNIQUE_ID	0

/* NETSTACK CONFIG */
#define ENABLE_NETSTACK_ARCH		0

#if ENABLE_NETSTACK_ARCH
	#define NETSTACK_PROCESS_PRIORITY		HIGH_PRIORITY_PROCESS

	/* PHY LAYER */
	#define PLATFORM_PHY_LAYER 				CERBERUS_PHY_LAYER		//AT86RF215_PHY_LAYER
	#define ENABLE_CERBERUS_PHY_LAYER 		1
	#define PHY_LAYER_PROCESS_PRIORITY		HIGH_PRIORITY_PROCESS


	/* MAC LAYER */
	#define PLATFORM_MAC_LAYER			YETI_MAC_LAYER
	#if PLATFORM_MAC_LAYER == YETI_MAC_LAYER
		#if ENABLE_RTC_TIMESTAMP
			#define PLATFORM_USE_SYNC			1			//Selects to use or not the synchornized mac protocol with TDMA slots.
			#define PLATFORM_SYNC_OFFSET_MS		7			//Experimental Param used to synchronize nodes in the yetimac protocol
			#define YETIMAC_LAYER_PROCESS_PRIORITY		VERY_HIGH_PRIORITY_PROCESS		//The higher this priority is the best the reliability is, although it may interfere in real-time applications
		#endif
	#endif

	/* ROUTING LAYER */
	#define PLATFORM_ROUTING_LAYER			YETI_ROUTING_LAYER

	/* TRANSPORT LAYER */
	#define PLATFORM_TRANSPORT_LAYER		YETI_TRANSPORT_LAYER
#endif

/* MEMORY MANAGEMENT*/
#define USE_YETI_OS_HEAP_MAN		0						//Use heap_4_yetios.c memory management file, which links reserved memory to processes. Recommended for unexperienced developers. Low Efficiency.
#define USE_FREERTOS_HEAP_MAN		!USE_YETI_OS_HEAP_MAN	//Use heap_4.c memory management file provided by FreeRTOS. High efficiency.


/* YETIOS ADAPTIVE CORE DATA SOURCES*/
#if ENABLE_ADXL355_DRIVER
	#define ADXL355_ACEL_ADAPT_SRC		0		//Load the adxl355 data source for the adaptive core
#endif
/* YETIOS ADAPTIVE CORE ALGORITHMS*/
#define ADAPTIVE_IO_MODULES_ALG			0		//Load the adaptive Input/Output modules example algorithm

#endif /* PLATFORM_CONF_H_ */
