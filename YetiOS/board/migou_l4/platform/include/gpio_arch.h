/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * gpio_arch.h
 *
 *  Created on: 20 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file gpio_arch.h
 */
#ifndef APPLICATION_BOARD_PLATFORM_GPIO_GPIO_ARCH_H_
#define APPLICATION_BOARD_PLATFORM_GPIO_GPIO_ARCH_H_

#include "gpio.h"
#include "platform-conf.h"

#define PLATFORM_PINS 				\
		GPIO_PIN_A0 =	0x00010000,	\
		GPIO_PIN_A1	= 	0x00020000,	\
		GPIO_PIN_A5	=	0x00200000,	\
		GPIO_PIN_A6	=	0x00400000,	\
		GPIO_PIN_A7	=	0x00800000,	\
									\
		GPIO_PIN_B0	=	0x00010400,	\
		GPIO_PIN_B1	=	0x00020400,	\
		GPIO_PIN_B4	=	0x00100400,	\
		GPIO_PIN_B12=	0x10000400,	\
									\
		GPIO_PIN_C2	=	0x00040800,	\
		GPIO_PIN_C3	=	0x00080800,	\
		GPIO_PIN_C4	=	0x00100800,

#define PLATFORM_AVAILABLE_PINS		\
		GPIO_PIN_A5,				\
		GPIO_PIN_A6,				\
		GPIO_PIN_A7,				\
		GPIO_PIN_B4,				\
		GPIO_PIN_C2,				\
		GPIO_PIN_C3,				\
		GPIO_PIN_C4,

#define PLATFORM_RESERVED_PINS		\
		GPIO_PIN_A0,				\
		GPIO_PIN_A1,				\
		GPIO_PIN_B0,				\
		GPIO_PIN_B1,				\
		GPIO_PIN_B12,

// MIGOU MONITORING MCU PLATFORM PINS
#define USART2_CTS		GPIO_PIN_A0
#define USART2_RTS		GPIO_PIN_A1
#define SPI2_CS			GPIO_PIN_B12
#define RS1_1			GPIO_PIN_A5
#define RS1_2			GPIO_PIN_A6
#define RS2_1			GPIO_PIN_A7
#define RS2_2			GPIO_PIN_C4
#define VSUP_SENSE		GPIO_PIN_B0
#define TRX_VDD_SENSE	GPIO_PIN_B1
#define USER_IN			GPIO_PIN_B4
#define ADC_IN12		GPIO_PIN_C2
#define ADC_IN13		GPIO_PIN_C3


retval_t gpio_arch_init_pin(uint32_t gpio, gpio_mode_t gpio_mode, gpio_pull_t gpio_pull);

retval_t gpio_arch_set_pin(uint32_t gpio);
retval_t gpio_arch_reset_pin(uint32_t gpio);
retval_t gpio_arch_toggle_pin(uint32_t gpio);

retval_t gpio_arch_get_pin(uint32_t gpio);

#endif /* APPLICATION_BOARD_PLATFORM_GPIO_GPIO_ARCH_H_ */
