/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * unique_id.c
 *
 *  Created on: 11 de may. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file unique_id.c
 */

#include "system_api.h"
#include "unique_id.h"


#if USE_16_BITS_UNIQUE_ID
/**
 *
 * @return
 */
uint16_t get_uinque_16_bits_id(void){
	uint16_t unique_id;

	unique_id = ((uint16_t)(NODEID_LOCATION_BASE)[NODEID_OFFSET_0]) | ((uint16_t)((NODEID_LOCATION_BASE)[NODEID_OFFSET_0]>>16));

	return unique_id;
}
#endif


#if USE_32_BITS_UNIQUE_ID
/**
 *
 * @return
 */
uint32_t get_uinque_32_bits_id(void){
	uint32_t unique_id;

	unique_id = (((NODEID_LOCATION_BASE)[NODEID_OFFSET_0]) ^ ((NODEID_LOCATION_BASE)[NODEID_OFFSET_1])) & ((NODEID_LOCATION_BASE)[NODEID_OFFSET_2]);
	return unique_id;
}
#endif

#if USE_64_BITS_UNIQUE_ID
/**
 *
 * @return
 */
uint64_t get_uinque_64_bits_id(void){
	uint64_t unique_id;


	unique_id =   ((uint64_t)(NODEID_LOCATION_BASE)[NODEID_OFFSET_0])  | (((uint64_t)(NODEID_LOCATION_BASE)[NODEID_OFFSET_1])<<32);

	return unique_id;
}
#endif

#if USE_96_BITS_UNIQUE_ID
/**
 *
 * @return
 */
uint32_t* get_uinque_96_bits_id(void){
	static uint32_t unique_id[3];
	unique_id[0] = (NODEID_LOCATION_BASE)[NODEID_OFFSET_0];
	unique_id[1] = (NODEID_LOCATION_BASE)[NODEID_OFFSET_1];
	unique_id[2] = (NODEID_LOCATION_BASE)[NODEID_OFFSET_2];

	return unique_id;
}
#endif
