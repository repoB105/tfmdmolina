/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adc_arch.c
 *
 *  Created on: 20 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adc_arch.c
 */


#include "system_api.h"
#include "adc_arch.h"
#include "adc.h"
#include "leds.h"
#include "process.h"


#define DEFAULT_TIMEOUT	500

extern DMA_HandleTypeDef hdma_adc;

static uint32_t adc_semaphore_id;

static uint32_t adc_arch_get_channel_from_gpio(gpio_pin_t gpio);
static retval_t adc_arch_set_prescaler_sampling_time(uint32_t* speed, uint32_t* prescaler, uint32_t* sampling_time);
static retval_t adc_arch_set_listening_channels(adc_config_t* adc_config);

/**
 *
 * @return
 */
retval_t adc_arch_init(void){
	MX_ADC_Init();
	adc_semaphore_id = ytSemaphoreCreate(1);

	return RET_OK;
}

/**
 *
 * @return
 */
retval_t adc_arch_deinit(void){
	HAL_ADC_DeInit(&hadc);

	ytSemaphoreDelete(adc_semaphore_id);
	return RET_OK;
}

/**
 *
 * @param adc_config
 * @param data
 * @param numSamples
 * @return
 */
retval_t adc_arch_read_channel(adc_config_t* adc_config, uint8_t* data, uint16_t numSamples){
	if(adc_arch_set_listening_channels(adc_config) != RET_OK){
		return RET_ERROR;
	}

	HAL_ADC_Start_DMA(&hadc, (uint32_t*) data, numSamples);			//Leo con el ADC y espero hasta que se termina
	if(ytSemaphoreWait(adc_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
		return RET_ERROR;
	}
	HAL_ADC_Stop_DMA(&hadc);

	return RET_OK;
}

/**
 *
 * @param adc_config
 * @return
 */
retval_t adc_arch_config_channel(adc_config_t* adc_config){
	uint32_t channel_adc;
	uint32_t prescaler, sampling_time;

	ADC_ChannelConfTypeDef sConfig1;

	if( adc_arch_set_prescaler_sampling_time(&(adc_config->channel_speed), &prescaler, &sampling_time) != RET_OK){
		return RET_ERROR;
	}

	adc_config->prescaler = prescaler;
	adc_config->ch_sampling_time = sampling_time;

	/* Single ADC configuration */
	if((channel_adc = adc_arch_get_channel_from_gpio(adc_config->adc_pin)) == 0xFFFFFFFF){
		return RET_ERROR;
	}

	if(ytGpioInitPin(adc_config->adc_pin, GPIO_PIN_ANALOG, GPIO_PIN_NO_PULL) != RET_OK){
		return RET_ERROR;
	}

	/* Config speed */
	hadc.Init.ClockPrescaler = prescaler;
	if (HAL_ADC_Init(&hadc) != HAL_OK)
	{
	_Error_Handler(__FILE__, __LINE__);
	}

	/* Config channel */
	sConfig1.Channel = channel_adc;
	sConfig1.Rank = 1;
	sConfig1.SamplingTime = sampling_time;
	if (HAL_ADC_ConfigChannel(&hadc, &sConfig1) != HAL_OK)
	{
	_Error_Handler(__FILE__, __LINE__);
	}



	return RET_OK;
}


/**
 *
 * @param speed
 * @param prescaler
 * @param sampling_time
 * @return
 */
static retval_t adc_arch_set_prescaler_sampling_time(uint32_t* speed, uint32_t* prescaler, uint32_t* sampling_time){

	uint32_t adc_clk = 16000000;

	if((*speed) > 500000){
		(*prescaler) = ADC_CLOCK_ASYNC_DIV1;
	}
	else if ((*speed) > 250000){
		(*prescaler) = ADC_CLOCK_ASYNC_DIV2;
		adc_clk = adc_clk/2;
	}
	else{
		(*prescaler) = ADC_CLOCK_ASYNC_DIV4;
		adc_clk = adc_clk/4;
	}

	(*sampling_time) = (adc_clk/(*speed)) -12 ;

	if((*sampling_time) <= 4){
		(*sampling_time) = ADC_SAMPLETIME_4CYCLES;
		(*speed) = adc_clk /(12+4);
	}
	else if((*sampling_time) <= 9){
		(*sampling_time) = ADC_SAMPLETIME_9CYCLES;
		(*speed) = adc_clk /(12+9);
	}
	else if((*sampling_time) <= 16){
		(*sampling_time) = ADC_SAMPLETIME_16CYCLES;
		(*speed) = adc_clk /(12+16);
	}
	else if((*sampling_time) <= 24){
		(*sampling_time) = ADC_SAMPLETIME_24CYCLES;
		(*speed) = adc_clk /(12+24);
	}
	else if((*sampling_time) <= 48){
		(*sampling_time) = ADC_SAMPLETIME_48CYCLES;
		(*speed) = adc_clk /(12+48);
	}
	else if((*sampling_time) <= 96){
		(*sampling_time) = ADC_SAMPLETIME_96CYCLES;
		(*speed) = adc_clk /(12+96);
	}
	else if((*sampling_time) <= 192){
		(*sampling_time) = ADC_SAMPLETIME_192CYCLES;
		(*speed) = adc_clk /(12+192);
	}
	else{
		(*sampling_time) = ADC_SAMPLETIME_384CYCLES;
		(*speed) = adc_clk /(12+384);
	}

	return RET_OK;
}

/**
 *
 * @param gpio
 * @return
 */
static uint32_t adc_arch_get_channel_from_gpio(gpio_pin_t gpio){
	switch(gpio){
	case GPIO_PIN_A0:
		return ADC_CHANNEL_0;
		break;
	case GPIO_PIN_A1:
		return ADC_CHANNEL_1;
		break;
	case GPIO_PIN_A2:
		return ADC_CHANNEL_2;
		break;
	case GPIO_PIN_A3:
		return ADC_CHANNEL_3;
		break;
	case GPIO_PIN_A4:
		return ADC_CHANNEL_4;
		break;
	case  GPIO_PIN_A5:
		return ADC_CHANNEL_5;
		break;
	case  GPIO_PIN_A6:
		return ADC_CHANNEL_6;
		break;
	case  GPIO_PIN_A7:
		return ADC_CHANNEL_7;
		break;
	case  GPIO_PIN_B0:
		return ADC_CHANNEL_8;
		break;
	case  GPIO_PIN_B1:
		return ADC_CHANNEL_9;
		break;
	case GPIO_PIN_C0:
		return ADC_CHANNEL_10;
		break;
	case GPIO_PIN_C1:
		return ADC_CHANNEL_11;
		break;
	case GPIO_PIN_C2:
		return ADC_CHANNEL_12;
		break;
	case GPIO_PIN_C3:
		return ADC_CHANNEL_13;
		break;
	case GPIO_PIN_C4:
		return ADC_CHANNEL_14;
		break;
	case GPIO_PIN_C5:
		return ADC_CHANNEL_15;
		break;
	default:
		return 0xFFFFFFFF;
		break;
	}
}

/**
 *
 * @param adc_config
 * @return
 */
static retval_t adc_arch_set_listening_channels(adc_config_t* adc_config){
	uint32_t channel_adc_1;
	ADC_ChannelConfTypeDef sConfig;

	/* Single ADC mode */

		if((channel_adc_1 = adc_arch_get_channel_from_gpio(adc_config->adc_pin)) == 0xFFFFFFFF){
			return RET_ERROR;
		}

		/* Config channel */
		sConfig.Channel = channel_adc_1;
		sConfig.Rank = 1;
		sConfig.SamplingTime = adc_config->ch_sampling_time;
		if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
		{
		_Error_Handler(__FILE__, __LINE__);
		}

	return RET_OK;

}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	ytSemaphoreRelease(adc_semaphore_id);
}

void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc){
#ifdef ERROR_LED
	ytLedsOn(ERROR_LED);
#endif
	while(1);
}
