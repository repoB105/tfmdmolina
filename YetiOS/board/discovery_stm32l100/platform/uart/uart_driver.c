/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * uart_driver.c
 *
 *  Created on: 12 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file uart_driver.c
 */

#include "device.h"
#include "platform-conf.h"
#include "yetimote_stdio.h"
#include "uart_arch.h"
#include "low_power.h"
#include "uart_driver.h"
#include "system_api.h"

#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif


#if ENABLE_UART_DRIVER

/* Device name and Id */
#define UART_DEVICE_NAME UART_DEV


static uint16_t dev_opened;

/* Device Init functions */
static retval_t uart_init(void);
static retval_t uart_exit(void);


/* Device driver operation functions declaration */
static retval_t uart_open(device_t* device, dev_file_t* filep);
static retval_t uart_close(dev_file_t* filep);
static size_t uart_read(dev_file_t* filep, uint8_t* prx, size_t size);
static size_t uart_write(dev_file_t* filep, uint8_t* ptx, size_t size);
static retval_t uart_ioctl(dev_file_t* filep, uint16_t request, void* args);


/* Define driver operations */
static driver_ops_t uart_driver_ops = {
	.open = uart_open,
	.close = uart_close,
	.read = uart_read,
	.write = uart_write,
	.ioctl = uart_ioctl,
};


/**
 *
 * @return
 */
static retval_t uart_init(void){
	if(uart_arch_init() != RET_OK){				//Inicialización del HW del UART
		return RET_ERROR;
	}
	if(registerDevice(UART_DEVICE_ID, &uart_driver_ops, UART_DEVICE_NAME) != RET_OK){
		return RET_ERROR;
	}

	PRINTF(">UART Init Done\r\n");

	return RET_OK;
}

/**
 *
 * @return
 */
static retval_t uart_exit(void){
	if(uart_arch_deinit() != RET_OK){				//De-Inicialización del HW del i2c
		return RET_ERROR;
	}
	unregisterDevice(UART_DEVICE_ID, UART_DEVICE_NAME);

	PRINTF(">UART Exit Done\r\n");

	return RET_OK;
}

/**
 *
 * @param device
 * @param filep
 * @return
 */
static retval_t uart_open(device_t* device, dev_file_t* filep){
	if(device->device_state != DEV_STATE_INIT){
		return RET_ERROR;
	}
	if(!dev_opened){
		uart_arch_start_storing_data();
	}
	dev_opened++;
	return RET_OK;
}

/**
 *
 * @param filep
 * @return
 */
static retval_t uart_close(dev_file_t* filep){
	dev_opened--;
	if(!dev_opened){
		uart_arch_stop_storing_data();
	}
	return RET_OK;
}

/**
 *
 * @param filep
 * @param ptx
 * @param size
 * @return
 */
static size_t uart_write(dev_file_t* filep, uint8_t* ptx, size_t size){

	retval_t ret;

	ret = uart_arch_write(ptx, (uint16_t) size, YT_WAIT_FOREVER);

	if (ret != RET_OK){
		return 0;
	}
	else{
		return size;
	}
}


/**
 *
 * @param filep
 * @param prx
 * @param size
 * @return
 */
static size_t uart_read(dev_file_t* filep, uint8_t* prx, size_t size){

	retval_t ret;

	ret = uart_arch_read(prx, (uint16_t) size, YT_WAIT_FOREVER);

	if (ret != RET_OK){
		return 0;
	}
	else{
		return size;
	}
}


/**
 *
 * @param filep
 * @param request
 * @param args
 * @return
 */
static retval_t uart_ioctl(dev_file_t* filep, uint16_t request, void* args){
	uart_driver_ioctl_cmd_t cmd;
	uart_read_line_params_t* read_line_params;
	retval_t ret = RET_OK;
	if(filep == NULL){
		return RET_ERROR;
	}
	cmd = (uart_driver_ioctl_cmd_t) request;

	switch(cmd){
	case SET_UART_SPEED:

		ret = uart_arch_set_speed((uint32_t) args);

		break;

	case SET_NO_PARITY:

		ret = uart_arch_set_parity(0);

		break;

	case SET_PARITY_EVEN:

		ret = uart_arch_set_parity(1);

		break;
	case SET_PARITY_ODD:

		ret = uart_arch_set_parity(2);

		break;

	case UART_ENABLE_ECHO:

		ret = uart_arch_enable_echo();

		break;

	case UART_DISABLE_ECHO:

		ret = uart_arch_disable_echo();

		break;
	case UART_READ_LINE:
		read_line_params = (uart_read_line_params_t*) args;
		disable_low_power_mode();
		ret = uart_arch_read_line(read_line_params->buff, read_line_params->size, YT_WAIT_FOREVER);
		enable_low_power_mode();
		break;

	default:
		ret = RET_ERROR;
		break;
	}
	return ret;
}

/* Register the init functions in the kernel Init system */
init_device(uart_init);
exit_device(uart_exit);

#endif
