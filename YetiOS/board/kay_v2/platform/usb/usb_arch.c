/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * usb_arch.c
 *
 *  Created on: 28 de ago. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file usb_arch.c
 */

#include "usb_arch.h"
#include "usbd_cdc_if.h"
#include "system_api.h"
#include "low_power.h"

#define USB_RX_BUFFER_SIZE		256
#define USB_RX_TIMER_TIMEOUT	90		//An overflow may occur in the timer if the usb speed and data flow are very high.
										//8 ms timeout prevents overflow with a 256 buffer and up to 250 kbps of continous datarate. Higher speed require a larger buffer
										//When overflow occurs old data is discarded and replaced by new one
#define USB_RX_POLL_TIME		95

#define USB_RX_MAX_READ_SIZE	192

#define USB_DEFAULT_ECHO		0

#define USB_RESET_TIME			5000	//The time lapsed to manually reset the usb as sometimes it gets stopped
#define USB_RESET_COUNT			(USB_RESET_TIME/USB_RX_TIMER_TIMEOUT)


static uint32_t usb_rx_timer_id;

static uint32_t usb_tx_mutex_id;
static uint32_t usb_rx_mutex_id;

static uint8_t usb_rx_buffer[USB_RX_BUFFER_SIZE];
static uint8_t* usb_rx_ptr_head;			//Last byte stored in the buffer
static uint8_t* usb_rx_ptr_tail;			//First byte stored in the buffer
static uint16_t usb_num_stored_bytes;

static uint16_t usb_enable_local_echo = USB_DEFAULT_ECHO;

static uint16_t usb_buffer_downcounter = USB_RX_BUFFER_SIZE;

static uint16_t usb_reset_counter = 0;

static void update_stored_bytes(void);
static void usb_rx_timer_cb(void const * argument);
/**
 *
 * @return
 */
retval_t usb_arch_init(){


	usb_rx_timer_id = ytTimerCreate(ytTimerPeriodic, usb_rx_timer_cb, NULL);
	usb_tx_mutex_id = ytMutexCreate();	//Initialize blocking mutex
	usb_rx_mutex_id = ytMutexCreate();	//Initialize blocking mutex
	usb_num_stored_bytes = 0;

	usb_buffer_downcounter = USB_RX_BUFFER_SIZE;

	return RET_OK;
}

/**
 *
 * @return
 */
retval_t usb_arch_deinit(){

	ytTimerStop(usb_rx_timer_id);
	ytTimerDelete(usb_rx_timer_id);
	ytMutexDelete(usb_tx_mutex_id);
	ytMutexDelete(usb_rx_mutex_id);
	return RET_OK;
}

/**
 *
 * @return
 */
retval_t usb_arch_start_storing_data(void){

	usb_rx_ptr_head = usb_rx_buffer + USB_RX_BUFFER_SIZE - usb_buffer_downcounter;	//Start storing the samples
	usb_rx_ptr_tail = usb_rx_ptr_head;
	usb_num_stored_bytes = 0;
	return ytTimerStart(usb_rx_timer_id, USB_RX_TIMER_TIMEOUT);


}

/**
 *
 * @return
 */
retval_t usb_arch_stop_storing_data(void){

	usb_rx_ptr_head = usb_rx_buffer + USB_RX_BUFFER_SIZE - usb_buffer_downcounter;	//Start storing the samples
	usb_rx_ptr_tail = usb_rx_ptr_head;
	usb_num_stored_bytes = 0;
	return ytTimerStop(usb_rx_timer_id);
}


/**
 *
 * @param txData
 * @param size
 * @return
 */
retval_t usb_arch_write(uint8_t* txData, uint16_t size, uint32_t timeout){

	if(!size){
		return RET_ERROR;
	}
	//The USB has a fixed timeout for sending
	ytMutexWait(usb_tx_mutex_id, YT_WAIT_FOREVER);
	disable_low_power_mode();
	if( CDC_Transmit_FS(txData, size) != USBD_OK){
		enable_low_power_mode();
		ytMutexRelease(usb_tx_mutex_id);
		return RET_ERROR;
	}
	enable_low_power_mode();
	ytMutexRelease(usb_tx_mutex_id);
	return RET_OK;
}

/**
 *
 * @param rxData
 * @param size
 * @return
 */
retval_t usb_arch_read(uint8_t* rxData, uint16_t size, uint32_t timeout){
	uint32_t start_time;
	uint16_t i;
	uint16_t read_bytes = 0;

	ytMutexWait(usb_rx_mutex_id, YT_WAIT_FOREVER);
	disable_low_power_mode();
	update_stored_bytes();
	start_time = ytGetSysTickMilliSec();

	//Now read the buffer depending on the parameter size
	if(size <= USB_RX_MAX_READ_SIZE){
		while(usb_num_stored_bytes<size){		//Wait till the required bytes are available
			ytDelay(USB_RX_POLL_TIME);
			if((ytGetSysTickMilliSec() - start_time) > timeout){
				enable_low_power_mode();
				ytMutexRelease(usb_rx_mutex_id);
				return RET_ERROR;
			}
		}
		while (read_bytes < size){
			rxData[read_bytes] = *usb_rx_ptr_tail;
			usb_rx_ptr_tail++;
			usb_num_stored_bytes--;
			read_bytes++;
			if (usb_rx_ptr_tail >= &usb_rx_buffer[USB_RX_BUFFER_SIZE]){
				usb_rx_ptr_tail = usb_rx_buffer;
			}
		}

	}
	else{
		while(read_bytes < size){

			if((size-read_bytes) > USB_RX_MAX_READ_SIZE){	//If there is no room in the buffer for the remaining bytes
				while(usb_num_stored_bytes < USB_RX_MAX_READ_SIZE){		//Wait till the required bytes are available
					ytDelay(USB_RX_POLL_TIME);
					if((ytGetSysTickMilliSec() - start_time) > timeout){
						enable_low_power_mode();
						ytMutexRelease(usb_rx_mutex_id);
						return RET_ERROR;
					}
				}
				for(i=0; i<USB_RX_MAX_READ_SIZE; i++){
					rxData[read_bytes] = *usb_rx_ptr_tail;
					usb_rx_ptr_tail++;
					usb_num_stored_bytes--;
					read_bytes++;
					if (usb_rx_ptr_tail >= &usb_rx_buffer[USB_RX_BUFFER_SIZE]){
						usb_rx_ptr_tail = usb_rx_buffer;
					}
				}

			}
			else{						//There is room in the buffer for the remaining bytes
				while(usb_num_stored_bytes < (size-read_bytes)){		//Wait till the required bytes are available
					ytDelay(USB_RX_POLL_TIME);
					if((ytGetSysTickMilliSec() - start_time) > timeout){
						enable_low_power_mode();
						ytMutexRelease(usb_rx_mutex_id);
						return RET_ERROR;
					}
				}
				while(read_bytes < size){		//Read the last bytes
					rxData[read_bytes] = *usb_rx_ptr_tail;
					usb_rx_ptr_tail++;
					usb_num_stored_bytes--;
					read_bytes++;
					if (usb_rx_ptr_tail >= &usb_rx_buffer[USB_RX_BUFFER_SIZE]){
						usb_rx_ptr_tail = usb_rx_buffer;
					}
				}
			}



		}

	}

	enable_low_power_mode();
	ytMutexRelease(usb_rx_mutex_id);
	return RET_OK;
}

/**
 *
 * @param rxData
 * @param size
 * @return
 */
retval_t usb_arch_read_line(uint8_t* rxData, uint16_t size, uint32_t timeout){
	uint32_t start_time;
	uint16_t read_bytes = 0;

	ytMutexWait(usb_rx_mutex_id, YT_WAIT_FOREVER);
	disable_low_power_mode();
	update_stored_bytes();

	start_time = ytGetSysTickMilliSec();
	while(read_bytes < size){
		while(usb_num_stored_bytes){
			if(((*usb_rx_ptr_tail) != '\r') && ((*usb_rx_ptr_tail) != '\n')){		//If not line end, store the character

				if((*usb_rx_ptr_tail) != '\b'){

					rxData[read_bytes] = *usb_rx_ptr_tail;
					read_bytes++;
					usb_rx_ptr_tail++;
					usb_num_stored_bytes--;
					if (usb_rx_ptr_tail >= &usb_rx_buffer[USB_RX_BUFFER_SIZE]){
						usb_rx_ptr_tail = usb_rx_buffer;
					}
					if(read_bytes >= size){
						enable_low_power_mode();
						ytMutexRelease(usb_rx_mutex_id);
						return RET_ERROR;
					}
					if(usb_enable_local_echo){
						usb_arch_write(&rxData[read_bytes-1], 1, timeout);
					}
				}else{			//Move Backwards. Dont store the character and reduce one
					if(read_bytes){
						read_bytes--;
					}
					usb_rx_ptr_tail++;
					usb_num_stored_bytes--;
					if (usb_rx_ptr_tail >= &usb_rx_buffer[USB_RX_BUFFER_SIZE]){
						usb_rx_ptr_tail = usb_rx_buffer;
					}
					if(usb_enable_local_echo){
						usb_arch_write((uint8_t*)"\b \b", 3, timeout);
					}
				}

			}
			else{		//Line end detected.
				rxData[read_bytes] = '\0';
				usb_rx_ptr_tail++;
				usb_num_stored_bytes--;
				read_bytes++;
				if (usb_rx_ptr_tail >= &usb_rx_buffer[USB_RX_BUFFER_SIZE]){
					usb_rx_ptr_tail = usb_rx_buffer;
				}

				//Check if the next element is a '\r' or a '\n' which is very feasible when the line end is "\r\n"
				if(usb_num_stored_bytes){
					if(((*usb_rx_ptr_tail) == '\r') || ((*usb_rx_ptr_tail) == '\n')){
						usb_rx_ptr_tail++;
						usb_num_stored_bytes--;
						if (usb_rx_ptr_tail >= &usb_rx_buffer[USB_RX_BUFFER_SIZE]){
							usb_rx_ptr_tail = usb_rx_buffer;
						}
					}
				}

				if(usb_enable_local_echo){
					usb_arch_write((uint8_t*)"\r\n", 2, timeout);
				}
				enable_low_power_mode();
				ytMutexRelease(usb_rx_mutex_id);
				return RET_OK;
			}


		}
		ytDelay(USB_RX_POLL_TIME);
		if((ytGetSysTickMilliSec() - start_time) > timeout){
			enable_low_power_mode();
			ytMutexRelease(usb_rx_mutex_id);
			return RET_ERROR;
		}
	}
	enable_low_power_mode();
	ytMutexRelease(usb_rx_mutex_id);
	return RET_ERROR;
}


/**
 *
 * @return
 */
retval_t usb_arch_enable_echo(void){
	usb_enable_local_echo = 1;
	return RET_OK;
}

/**
 *
 * @return
 */
retval_t usb_arch_disable_echo(void){
	usb_enable_local_echo = 0;
	return RET_OK;
}


/**
 *
 * @param argument
 */
static void usb_rx_timer_cb(void const * argument){
	extern USBD_HandleTypeDef hUsbDeviceFS;

	update_stored_bytes();

	//Sometimes is necessary to manually continue receiving usb packets as it gets stopped
	usb_reset_counter++;
	if(usb_reset_counter > USB_RESET_COUNT){
		USBD_CDC_ReceivePacket(&hUsbDeviceFS);		//Continue receiving packets
		usb_reset_counter = 0;
	}
}


/**
 *
 */
static void update_stored_bytes(void){
	uint8_t* new_usb_rx_ptr_head;
	uint32_t counter = usb_buffer_downcounter;	//remaining downcounter
	uint16_t new_bytes = 0;

	if(!counter){							//Prevent error if the downcounter is 0 (the usb pointer will overflow)
		counter = USB_RX_BUFFER_SIZE;
	}
	new_usb_rx_ptr_head = usb_rx_buffer + USB_RX_BUFFER_SIZE - counter;

	if (new_usb_rx_ptr_head >= usb_rx_ptr_head){
		new_bytes = (uint16_t)(new_usb_rx_ptr_head - usb_rx_ptr_head);
		usb_num_stored_bytes += new_bytes;
	}
	else{
		new_bytes = USB_RX_BUFFER_SIZE - ((uint16_t)(usb_rx_ptr_head - new_usb_rx_ptr_head));
		usb_num_stored_bytes += new_bytes;
	}

	usb_rx_ptr_head = new_usb_rx_ptr_head; //Update head pointer

	if(usb_num_stored_bytes >= USB_RX_BUFFER_SIZE){		//The buffer is full. Move the tail pointer
		usb_num_stored_bytes = USB_RX_BUFFER_SIZE;
		usb_rx_ptr_tail = usb_rx_ptr_head;			//The first writen byte is the last unwritenn byte when the buffer is full
	}
}


/**
 *
 * @param Buf
 * @param Len
 */
void usb_vcom_rcv_callback(uint8_t *Buf, uint32_t Len){		//Callback function called by USB receive. Do not block this function. Store data received in the circular buffer

	uint16_t i = 0;
	extern USBD_HandleTypeDef hUsbDeviceFS;
	for(i=0; i<Len; i++){
		usb_rx_buffer[USB_RX_BUFFER_SIZE - usb_buffer_downcounter] = Buf[i];
		usb_buffer_downcounter--;
		if(usb_buffer_downcounter <= 0){
			usb_buffer_downcounter = USB_RX_BUFFER_SIZE;
		}
	}
	USBD_CDC_ReceivePacket(&hUsbDeviceFS);		//Continue receiving packets

}
