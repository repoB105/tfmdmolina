/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * i2c_driver.c
 *
 *  Created on: 12 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file i2c_driver.c
 */

#include "device.h"
#include "platform-conf.h"
#include "yetimote_stdio.h"
#include "i2c_arch.h"
#include "low_power.h"
#include "i2c_driver.h"
#include "system_api.h"

#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#if ENABLE_I2C_DRIVER

/* Device name and Id */
#define I2C_DEVICE_NAME I2C_DEV

static uint32_t i2c_mutex_id;

/* Device Init functions */
static retval_t i2c_init(void);
static retval_t i2c_exit(void);


/* Device driver operation functions declaration */
static retval_t i2c_open(device_t* device, dev_file_t* filep);
static retval_t i2c_close(dev_file_t* filep);
static size_t i2c_read(dev_file_t* filep, uint8_t* prx, size_t size);
static size_t i2c_write(dev_file_t* filep, uint8_t* ptx, size_t size);
static retval_t i2c_ioctl(dev_file_t* filep, uint16_t request, void* args);


/* Define driver operations */
static driver_ops_t i2c_driver_ops = {
	.open = i2c_open,
	.close = i2c_close,
	.read = i2c_read,
	.write = i2c_write,
	.ioctl = i2c_ioctl,
};


/**
 *
 * @return
 */
static retval_t i2c_init(void){
	if(i2c_arch_init() != RET_OK){				//Inicialización del HW del i2c
		return RET_ERROR;
	}
	if(registerDevice(I2C_DEVICE_ID, &i2c_driver_ops, I2C_DEVICE_NAME) != RET_OK){
		return RET_ERROR;
	}
	i2c_mutex_id = ytMutexCreate();	//Initialize blocking mutex
	PRINTF(">I2C Init Done\r\n");

	return RET_OK;
}

/**
 *
 * @return
 */
static retval_t i2c_exit(void){
	if(i2c_arch_deinit() != RET_OK){				//De-Inicialización del HW del i2c
		return RET_ERROR;
	}
	unregisterDevice(I2C_DEVICE_ID, I2C_DEVICE_NAME);
	ytMutexDelete(i2c_mutex_id);
	PRINTF(">I2C Exit Done\r\n");

	return RET_OK;
}

/**
 *
 * @param device
 * @param filep
 * @return
 */
static retval_t i2c_open(device_t* device, dev_file_t* filep){
	if(device->device_state != DEV_STATE_INIT){
		return RET_ERROR;
	}
	ytMutexWait(i2c_mutex_id, YT_WAIT_FOREVER);
	i2c_data_t  i2c_data = 0;
	filep->private_data = (void*) i2c_data;
	ytMutexRelease(i2c_mutex_id);
	return RET_OK;
}

/**
 *
 * @param filep
 * @return
 */
static retval_t i2c_close(dev_file_t* filep){

	return RET_OK;
}

/**
 *
 * @param filep
 * @param ptx
 * @param size
 * @return
 */
static size_t i2c_write(dev_file_t* filep, uint8_t* ptx, size_t size){
	i2c_data_t i2c_data =  (i2c_data_t) filep->private_data;

	retval_t ret;

	ytMutexWait(i2c_mutex_id, YT_WAIT_FOREVER);
	disable_low_power_mode();
	ret = i2c_arch_write(i2c_data, ptx, (uint16_t) size);
	enable_low_power_mode();
	ytMutexRelease(i2c_mutex_id);


	if (ret != RET_OK){
		return 0;
	}
	else{
		return size;
	}
}


/**
 *
 * @param filep
 * @param prx
 * @param size
 * @return
 */
static size_t i2c_read(dev_file_t* filep, uint8_t* prx, size_t size){

	i2c_data_t i2c_data =  (i2c_data_t) filep->private_data;

	retval_t ret;

	ytMutexWait(i2c_mutex_id, YT_WAIT_FOREVER);
	disable_low_power_mode();
	ret = i2c_arch_read(i2c_data, prx, (uint16_t) size);
	enable_low_power_mode();
	ytMutexRelease(i2c_mutex_id);


	if (ret != RET_OK){
		return 0;
	}
	else{
		return size;
	}
}


/**
 *
 * @param filep
 * @param request
 * @param args
 * @return
 */
static retval_t i2c_ioctl(dev_file_t* filep, uint16_t request, void* args){
	i2c_data_t i2c_data;
	i2c_reg_ops_args_t* i2c_reg_args;
	i2c_driver_ioctl_cmd_t cmd;
	retval_t ret = RET_OK;
	if(filep == NULL){
		return RET_ERROR;
	}
	i2c_data = (i2c_data_t) filep->private_data;
	cmd = (i2c_driver_ioctl_cmd_t) request;
	ytMutexWait(i2c_mutex_id, osWaitForever);
	switch(cmd){
	case READ_REG:
		i2c_reg_args = (i2c_reg_ops_args_t*) args;
		disable_low_power_mode();
		i2c_arch_read_reg(i2c_data, i2c_reg_args);
		enable_low_power_mode();
		break;

	case WRITE_REG:
		i2c_reg_args = (i2c_reg_ops_args_t*) args;
		disable_low_power_mode();
		i2c_arch_write_reg(i2c_data, i2c_reg_args);
		enable_low_power_mode();
		break;

	case SET_SLAVE_ADDR:
		i2c_data = ((i2c_data_t)(args)) & 0x000000FF;
		filep->private_data = (void*) i2c_data;

		break;

	default:
		ret = RET_ERROR;
		break;
	}
	ytMutexRelease(i2c_mutex_id);
	return ret;
}

/* Register the init functions in the kernel Init system */
init_device(i2c_init);
exit_device(i2c_exit);

#endif
