/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * ad7124_arch.c
 *
 *  Created on: 3 may. 2019
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file ad7124_arch.c
 */

#include "platform-conf.h"
#include "ad7124.h"
#include "spi_driver.h"
#include "system_api.h"

#define AD7124_SPI_SPEED	6000000
#define AD7124_GPIO_CS_PIN 	GPIO_PIN_A4
#define AD7124_SPI_DEV		SPI_DEV_1


int32_t ad7124_config_spi_device(ad7124_dev_t* ad7124_dev){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	spi_driver_ioctl_cmd_t spi_ioctl_cmd;
	if(ad7124_dev == NULL){
		return -1;
	}

	ad7124_dev->spi_fd = ytOpen(AD7124_SPI_DEV, 0);
	if(!ad7124_dev->spi_fd){
		return -1;
	}
	spi_ioctl_cmd = DISABLE_SW_CS;
	if(ytIoctl(ad7124_dev->spi_fd, spi_ioctl_cmd, (void*) AD7124_GPIO_CS_PIN) != RET_OK){
		return -1;
	}
	spi_ioctl_cmd = DISABLE_SW_CS_EACH_BYTE;
	if(ytIoctl(ad7124_dev->spi_fd, spi_ioctl_cmd, NULL) != RET_OK){
		return -1;
	}
	spi_ioctl_cmd = SET_POLARITY_HIGH;
	if(ytIoctl(ad7124_dev->spi_fd, spi_ioctl_cmd, NULL) != RET_OK){
		return -1;
	}
	spi_ioctl_cmd = SET_EDGE_2;
	if(ytIoctl(ad7124_dev->spi_fd, spi_ioctl_cmd, NULL) != RET_OK){
		return -1;
	}
	spi_ioctl_cmd = SET_SPI_MODE_MASTER;
	if(ytIoctl(ad7124_dev->spi_fd, spi_ioctl_cmd, NULL) != RET_OK){
		return -1;
	}
	spi_ioctl_cmd = SET_SPI_SPEED;
	if(ytIoctl(ad7124_dev->spi_fd, spi_ioctl_cmd, (void*) AD7124_SPI_SPEED) != RET_OK){
		return -1;
	}
	ytGpioInitPin(AD7124_GPIO_CS_PIN, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);		//SET THE CS ALWAYS ACTIVE (LOW) TO ENABLE DRY PIN FUNCTION
	ytGpioPinReset(AD7124_GPIO_CS_PIN);
	return 0;
#endif
}
