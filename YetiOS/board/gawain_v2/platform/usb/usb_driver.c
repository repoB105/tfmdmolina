/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * usb_driver.c
 *
 *  Created on: 30 de ago. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file usb_driver.c
 */

#include "device.h"
#include "platform-conf.h"
#include "yetimote_stdio.h"
#include "usb_arch.h"
#include "low_power.h"
#include "usb_driver.h"
#include "system_api.h"

#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif


#if ENABLE_USB_DRIVER

/* Device name and Id */
#define USB_DEVICE_NAME USB_DEV


static uint16_t dev_opened;

/* Device Init functions */
static retval_t usb_init(void);
static retval_t usb_exit(void);


/* Device driver operation functions declaration */
static retval_t usb_open(device_t* device, dev_file_t* filep);
static retval_t usb_close(dev_file_t* filep);
static size_t usb_read(dev_file_t* filep, uint8_t* prx, size_t size);
static size_t usb_write(dev_file_t* filep, uint8_t* ptx, size_t size);
static retval_t usb_ioctl(dev_file_t* filep, uint16_t request, void* args);


/* Define driver operations */
static driver_ops_t usb_driver_ops = {
	.open = usb_open,
	.close = usb_close,
	.read = usb_read,
	.write = usb_write,
	.ioctl = usb_ioctl,
};


/**
 *
 * @return
 */
static retval_t usb_init(void){
	if(usb_arch_init() != RET_OK){				//Inicialización del HW del USB
		return RET_ERROR;
	}
	if(registerDevice(USB_DEVICE_ID, &usb_driver_ops, USB_DEVICE_NAME) != RET_OK){
		return RET_ERROR;
	}

	PRINTF(">USB Init Done\r\n");

	return RET_OK;
}

/**
 *
 * @return
 */
static retval_t usb_exit(void){
	if(usb_arch_deinit() != RET_OK){				//De-Inicialización del HW del i2c
		return RET_ERROR;
	}
	unregisterDevice(USB_DEVICE_ID, USB_DEVICE_NAME);

	PRINTF(">USB Exit Done\r\n");

	return RET_OK;
}

/**
 *
 * @param device
 * @param filep
 * @return
 */
static retval_t usb_open(device_t* device, dev_file_t* filep){
	if(device->device_state != DEV_STATE_INIT){
		return RET_ERROR;
	}
	if(!dev_opened){
		usb_arch_start_storing_data();
	}
	dev_opened++;
	return RET_OK;
}

/**
 *
 * @param filep
 * @return
 */
static retval_t usb_close(dev_file_t* filep){
	dev_opened--;
	if(!dev_opened){
		usb_arch_stop_storing_data();
	}
	return RET_OK;
}

/**
 *
 * @param filep
 * @param ptx
 * @param size
 * @return
 */
static size_t usb_write(dev_file_t* filep, uint8_t* ptx, size_t size){

	retval_t ret;

	ret = usb_arch_write(ptx, (uint16_t) size, YT_WAIT_FOREVER);

	if (ret != RET_OK){
		return 0;
	}
	else{
		return size;
	}
}


/**
 *
 * @param filep
 * @param prx
 * @param size
 * @return
 */
static size_t usb_read(dev_file_t* filep, uint8_t* prx, size_t size){

	retval_t ret;

	ret = usb_arch_read(prx, (uint16_t) size, YT_WAIT_FOREVER);

	if (ret != RET_OK){
		return 0;
	}
	else{
		return size;
	}
}


/**
 *
 * @param filep
 * @param request
 * @param args
 * @return
 */
static retval_t usb_ioctl(dev_file_t* filep, uint16_t request, void* args){
	usb_driver_ioctl_cmd_t cmd;
	usb_read_line_params_t* read_line_params;
	retval_t ret = RET_OK;
	if(filep == NULL){
		return RET_ERROR;
	}
	cmd = (usb_driver_ioctl_cmd_t) request;

	switch(cmd){
	case USB_ENABLE_ECHO:

		ret = usb_arch_enable_echo();

		break;

	case USB_DISABLE_ECHO:

		ret = usb_arch_disable_echo();

		break;
	case USB_READ_LINE:
		read_line_params = (usb_read_line_params_t*) args;
		disable_low_power_mode();
		ret = usb_arch_read_line(read_line_params->buff, read_line_params->size, YT_WAIT_FOREVER);
		enable_low_power_mode();
		break;

	default:
		ret = RET_ERROR;
		break;
	}

	return ret;
}

/* Register the init functions in the kernel Init system */
init_device(usb_init);
exit_device(usb_exit);

#endif
