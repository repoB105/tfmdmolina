set_device \
    -family  SmartFusion2 \
    -die     PA4M5000_N \
    -package fg484 \
    -speed   STD \
    -tempr   {COM} \
    -voltr   {COM}
set_def {VOLTAGE} {1.2}
set_def {VCCI_1.2_VOLTR} {COM}
set_def {VCCI_1.5_VOLTR} {COM}
set_def {VCCI_1.8_VOLTR} {COM}
set_def {VCCI_2.5_VOLTR} {COM}
set_def {VCCI_3.3_VOLTR} {COM}
set_def {PLL_SUPPLY} {PLL_SUPPLY_33}
set_def USE_CONSTRAINTS_FLOW 1
set_netlist -afl {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_50_v1\designer\migou_sf2_50_v1\migou_sf2_50_v1.afl} -adl {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_50_v1\designer\migou_sf2_50_v1\migou_sf2_50_v1.adl}
set_constraints   {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_50_v1\designer\migou_sf2_50_v1\migou_sf2_50_v1.tcml}
set_placement   {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_50_v1\designer\migou_sf2_50_v1\migou_sf2_50_v1.loc}
set_routing     {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_50_v1\designer\migou_sf2_50_v1\migou_sf2_50_v1.seg}
