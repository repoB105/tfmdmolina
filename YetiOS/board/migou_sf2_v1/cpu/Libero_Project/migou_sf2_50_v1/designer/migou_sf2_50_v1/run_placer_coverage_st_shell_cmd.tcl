read_sdc -scenario "place_and_route" -netlist "user" -pin_separator "/" -ignore_errors {D:/Repositorio/yetios/board/migou_sf2_v1/cpu/Libero_Project/migou_sf2_50_v1/designer/migou_sf2_50_v1/place_route.sdc}
set_options -tdpr_scenario "place_and_route" 
save
set_options -analysis_scenario "place_and_route"
set coverage [report \
    -type     constraints_coverage \
    -format   xml \
    -slacks   no \
    {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_50_v1\designer\migou_sf2_50_v1\migou_sf2_50_v1_place_and_route_constraint_coverage.xml}]
set reportfile {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_50_v1\designer\migou_sf2_50_v1\coverage_placeandroute}
set fp [open $reportfile w]
puts $fp $coverage
close $fp
