set_device \
    -fam SmartFusion2 \
    -die PA4M5000_N \
    -pkg fg484
set_input_cfg \
	-path {D:/Repositorio/yetios/board/migou_sf2_v1/cpu/Libero_Project/migou_sf2_50_v1/component/work/migou_sf2_50_v1_sb_MSS/ENVM.cfg}
set_output_efc \
    -path {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_50_v1\designer\migou_sf2_50_v1\migou_sf2_50_v1.efc}
set_proj_dir \
    -path {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_50_v1}
gen_prg -use_init false
