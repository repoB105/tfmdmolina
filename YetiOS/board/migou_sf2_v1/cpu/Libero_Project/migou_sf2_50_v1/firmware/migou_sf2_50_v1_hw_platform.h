#ifndef migou_sf2_50_v1_HW_PLATFORM_H_
#define migou_sf2_50_v1_HW_PLATFORM_H_
/*****************************************************************************
*
*Created by Microsemi SmartDesign  Tue Mar 12 14:44:43 2019
*
*Memory map specification for peripherals in migou_sf2_50_v1
*/

/*-----------------------------------------------------------------------------
* CM3 subsystem memory map
* Master(s) for this subsystem: CM3 
*---------------------------------------------------------------------------*/
#define COREGPIO_0                      0x50000000U


#endif /* migou_sf2_50_v1_HW_PLATFORM_H_*/
