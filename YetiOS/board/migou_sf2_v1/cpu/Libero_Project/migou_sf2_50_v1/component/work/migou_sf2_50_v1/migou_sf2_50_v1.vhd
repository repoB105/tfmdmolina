----------------------------------------------------------------------
-- Created by SmartDesign Thu Apr 11 13:24:33 2019
-- Version: v12.0 12.500.0.22
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library smartfusion2;
use smartfusion2.all;
library COREAPB3_LIB;
use COREAPB3_LIB.all;
use COREAPB3_LIB.components.all;
library COREGPIO_LIB;
use COREGPIO_LIB.all;
use COREGPIO_LIB.migou_sf2_50_v1_CoreGPIO_0_components.all;
----------------------------------------------------------------------
-- migou_sf2_50_v1 entity declaration
----------------------------------------------------------------------
entity migou_sf2_50_v1 is
    -- Port list
    port(
        -- Inputs
        DEVRST_N     : in    std_logic;
        MMUART_0_RXD : in    std_logic;
        SPI_1_DI     : in    std_logic;
        -- Outputs
        M3_SLEEPDEEP : out   std_logic;
        MMUART_0_TXD : out   std_logic;
        SPI_1_DO     : out   std_logic;
        -- Inouts
        FF_CONTROL   : inout std_logic;
        FF_WAKEUP    : inout std_logic;
        FG_GPOUT     : inout std_logic;
        GPIO         : inout std_logic_vector(14 downto 0);
        SPI_1_CLK    : inout std_logic;
        SPI_1_SS0    : inout std_logic;
        TRX_IRQ      : inout std_logic;
        TRX_RSTN     : inout std_logic
        );
end migou_sf2_50_v1;
----------------------------------------------------------------------
-- migou_sf2_50_v1 architecture body
----------------------------------------------------------------------
architecture RTL of migou_sf2_50_v1 is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- AND2
component AND2
    -- Port list
    port(
        -- Inputs
        A : in  std_logic;
        B : in  std_logic;
        -- Outputs
        Y : out std_logic
        );
end component;
-- CoreAPB3   -   Actel:DirectCore:CoreAPB3:4.1.100
-- using entity instantiation for component CoreAPB3
-- migou_sf2_50_v1_CoreGPIO_0_CoreGPIO   -   Actel:DirectCore:CoreGPIO:3.2.102
component migou_sf2_50_v1_CoreGPIO_0_CoreGPIO
    generic( 
        APB_WIDTH       : integer := 32 ;
        FIXED_CONFIG_0  : integer := 0 ;
        FIXED_CONFIG_1  : integer := 0 ;
        FIXED_CONFIG_2  : integer := 0 ;
        FIXED_CONFIG_3  : integer := 0 ;
        FIXED_CONFIG_4  : integer := 0 ;
        FIXED_CONFIG_5  : integer := 0 ;
        FIXED_CONFIG_6  : integer := 0 ;
        FIXED_CONFIG_7  : integer := 0 ;
        FIXED_CONFIG_8  : integer := 0 ;
        FIXED_CONFIG_9  : integer := 0 ;
        FIXED_CONFIG_10 : integer := 0 ;
        FIXED_CONFIG_11 : integer := 0 ;
        FIXED_CONFIG_12 : integer := 0 ;
        FIXED_CONFIG_13 : integer := 0 ;
        FIXED_CONFIG_14 : integer := 0 ;
        FIXED_CONFIG_15 : integer := 0 ;
        FIXED_CONFIG_16 : integer := 0 ;
        FIXED_CONFIG_17 : integer := 0 ;
        FIXED_CONFIG_18 : integer := 1 ;
        FIXED_CONFIG_19 : integer := 0 ;
        FIXED_CONFIG_20 : integer := 0 ;
        FIXED_CONFIG_21 : integer := 0 ;
        FIXED_CONFIG_22 : integer := 0 ;
        FIXED_CONFIG_23 : integer := 0 ;
        FIXED_CONFIG_24 : integer := 0 ;
        FIXED_CONFIG_25 : integer := 0 ;
        FIXED_CONFIG_26 : integer := 0 ;
        FIXED_CONFIG_27 : integer := 0 ;
        FIXED_CONFIG_28 : integer := 0 ;
        FIXED_CONFIG_29 : integer := 0 ;
        FIXED_CONFIG_30 : integer := 0 ;
        FIXED_CONFIG_31 : integer := 0 ;
        INT_BUS         : integer := 1 ;
        IO_INT_TYPE_0   : integer := 7 ;
        IO_INT_TYPE_1   : integer := 7 ;
        IO_INT_TYPE_2   : integer := 7 ;
        IO_INT_TYPE_3   : integer := 7 ;
        IO_INT_TYPE_4   : integer := 7 ;
        IO_INT_TYPE_5   : integer := 7 ;
        IO_INT_TYPE_6   : integer := 7 ;
        IO_INT_TYPE_7   : integer := 7 ;
        IO_INT_TYPE_8   : integer := 7 ;
        IO_INT_TYPE_9   : integer := 7 ;
        IO_INT_TYPE_10  : integer := 7 ;
        IO_INT_TYPE_11  : integer := 7 ;
        IO_INT_TYPE_12  : integer := 7 ;
        IO_INT_TYPE_13  : integer := 7 ;
        IO_INT_TYPE_14  : integer := 7 ;
        IO_INT_TYPE_15  : integer := 7 ;
        IO_INT_TYPE_16  : integer := 7 ;
        IO_INT_TYPE_17  : integer := 7 ;
        IO_INT_TYPE_18  : integer := 7 ;
        IO_INT_TYPE_19  : integer := 7 ;
        IO_INT_TYPE_20  : integer := 7 ;
        IO_INT_TYPE_21  : integer := 7 ;
        IO_INT_TYPE_22  : integer := 7 ;
        IO_INT_TYPE_23  : integer := 7 ;
        IO_INT_TYPE_24  : integer := 7 ;
        IO_INT_TYPE_25  : integer := 7 ;
        IO_INT_TYPE_26  : integer := 7 ;
        IO_INT_TYPE_27  : integer := 7 ;
        IO_INT_TYPE_28  : integer := 7 ;
        IO_INT_TYPE_29  : integer := 7 ;
        IO_INT_TYPE_30  : integer := 7 ;
        IO_INT_TYPE_31  : integer := 7 ;
        IO_NUM          : integer := 15 ;
        IO_TYPE_0       : integer := 0 ;
        IO_TYPE_1       : integer := 0 ;
        IO_TYPE_2       : integer := 0 ;
        IO_TYPE_3       : integer := 0 ;
        IO_TYPE_4       : integer := 0 ;
        IO_TYPE_5       : integer := 0 ;
        IO_TYPE_6       : integer := 0 ;
        IO_TYPE_7       : integer := 0 ;
        IO_TYPE_8       : integer := 0 ;
        IO_TYPE_9       : integer := 0 ;
        IO_TYPE_10      : integer := 0 ;
        IO_TYPE_11      : integer := 0 ;
        IO_TYPE_12      : integer := 0 ;
        IO_TYPE_13      : integer := 0 ;
        IO_TYPE_14      : integer := 0 ;
        IO_TYPE_15      : integer := 0 ;
        IO_TYPE_16      : integer := 0 ;
        IO_TYPE_17      : integer := 0 ;
        IO_TYPE_18      : integer := 0 ;
        IO_TYPE_19      : integer := 0 ;
        IO_TYPE_20      : integer := 0 ;
        IO_TYPE_21      : integer := 0 ;
        IO_TYPE_22      : integer := 0 ;
        IO_TYPE_23      : integer := 0 ;
        IO_TYPE_24      : integer := 0 ;
        IO_TYPE_25      : integer := 0 ;
        IO_TYPE_26      : integer := 0 ;
        IO_TYPE_27      : integer := 0 ;
        IO_TYPE_28      : integer := 0 ;
        IO_TYPE_29      : integer := 0 ;
        IO_TYPE_30      : integer := 0 ;
        IO_TYPE_31      : integer := 0 ;
        IO_VAL_0        : integer := 0 ;
        IO_VAL_1        : integer := 0 ;
        IO_VAL_2        : integer := 0 ;
        IO_VAL_3        : integer := 0 ;
        IO_VAL_4        : integer := 0 ;
        IO_VAL_5        : integer := 0 ;
        IO_VAL_6        : integer := 0 ;
        IO_VAL_7        : integer := 0 ;
        IO_VAL_8        : integer := 0 ;
        IO_VAL_9        : integer := 0 ;
        IO_VAL_10       : integer := 0 ;
        IO_VAL_11       : integer := 0 ;
        IO_VAL_12       : integer := 0 ;
        IO_VAL_13       : integer := 0 ;
        IO_VAL_14       : integer := 0 ;
        IO_VAL_15       : integer := 0 ;
        IO_VAL_16       : integer := 0 ;
        IO_VAL_17       : integer := 0 ;
        IO_VAL_18       : integer := 0 ;
        IO_VAL_19       : integer := 0 ;
        IO_VAL_20       : integer := 0 ;
        IO_VAL_21       : integer := 0 ;
        IO_VAL_22       : integer := 0 ;
        IO_VAL_23       : integer := 0 ;
        IO_VAL_24       : integer := 0 ;
        IO_VAL_25       : integer := 0 ;
        IO_VAL_26       : integer := 0 ;
        IO_VAL_27       : integer := 0 ;
        IO_VAL_28       : integer := 0 ;
        IO_VAL_29       : integer := 0 ;
        IO_VAL_30       : integer := 0 ;
        IO_VAL_31       : integer := 0 ;
        OE_TYPE         : integer := 1 
        );
    -- Port list
    port(
        -- Inputs
        GPIO_IN  : in  std_logic_vector(14 downto 0);
        PADDR    : in  std_logic_vector(7 downto 0);
        PCLK     : in  std_logic;
        PENABLE  : in  std_logic;
        PRESETN  : in  std_logic;
        PSEL     : in  std_logic;
        PWDATA   : in  std_logic_vector(31 downto 0);
        PWRITE   : in  std_logic;
        -- Outputs
        GPIO_OE  : out std_logic_vector(14 downto 0);
        GPIO_OUT : out std_logic_vector(14 downto 0);
        INT      : out std_logic_vector(14 downto 0);
        INT_OR   : out std_logic;
        PRDATA   : out std_logic_vector(31 downto 0);
        PREADY   : out std_logic;
        PSLVERR  : out std_logic
        );
end component;
-- FLASH_FREEZE
component FLASH_FREEZE
    -- Port list
    port(
        -- Outputs
        FF_DONE     : out std_logic;
        FF_TO_START : out std_logic
        );
end component;
-- migou_sf2_50_v1_sb
component migou_sf2_50_v1_sb
    -- Port list
    port(
        -- Inputs
        DEVRST_N            : in    std_logic;
        FAB_RESET_N         : in    std_logic;
        FIC_0_APB_M_PRDATA  : in    std_logic_vector(31 downto 0);
        FIC_0_APB_M_PREADY  : in    std_logic;
        FIC_0_APB_M_PSLVERR : in    std_logic;
        MMUART_0_RXD        : in    std_logic;
        MSS_INT_F2M         : in    std_logic_vector(15 downto 0);
        SPI_1_DI            : in    std_logic;
        -- Outputs
        FIC_0_APB_M_PADDR   : out   std_logic_vector(31 downto 0);
        FIC_0_APB_M_PENABLE : out   std_logic;
        FIC_0_APB_M_PSEL    : out   std_logic;
        FIC_0_APB_M_PWDATA  : out   std_logic_vector(31 downto 0);
        FIC_0_APB_M_PWRITE  : out   std_logic;
        FIC_0_CLK           : out   std_logic;
        FIC_0_LOCK          : out   std_logic;
        INIT_DONE           : out   std_logic;
        M3_SLEEPDEEP        : out   std_logic;
        MMUART_0_TXD        : out   std_logic;
        MSS_READY           : out   std_logic;
        POWER_ON_RESET_N    : out   std_logic;
        SPI_1_DO            : out   std_logic;
        -- Inouts
        GPIO_23_BI          : inout std_logic;
        GPIO_24_BI          : inout std_logic;
        GPIO_29_BI          : inout std_logic;
        GPIO_8_BI           : inout std_logic;
        GPIO_9_BI           : inout std_logic;
        SPI_1_CLK           : inout std_logic;
        SPI_1_SS0           : inout std_logic
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal AND2_0_Y                                       : std_logic;
signal CoreAPB3_0_APBmslave0_PENABLE                  : std_logic;
signal CoreAPB3_0_APBmslave0_PRDATA                   : std_logic_vector(31 downto 0);
signal CoreAPB3_0_APBmslave0_PREADY                   : std_logic;
signal CoreAPB3_0_APBmslave0_PSELx                    : std_logic;
signal CoreAPB3_0_APBmslave0_PSLVERR                  : std_logic;
signal CoreAPB3_0_APBmslave0_PWDATA                   : std_logic_vector(31 downto 0);
signal CoreAPB3_0_APBmslave0_PWRITE                   : std_logic;
signal CoreGPIO_0_INT_OR                              : std_logic;
signal FIC_0_CLK                                      : std_logic;
signal FLASH_FREEZE_0_FF_DONE                         : std_logic;
signal GPIO_1                                         : std_logic_vector(14 downto 0);
signal M3_SLEEPDEEP_1                                 : std_logic;
signal migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PADDR   : std_logic_vector(31 downto 0);
signal migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PENABLE : std_logic;
signal migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PRDATA  : std_logic_vector(31 downto 0);
signal migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PREADY  : std_logic;
signal migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PSELx   : std_logic;
signal migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PSLVERR : std_logic;
signal migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PWDATA  : std_logic_vector(31 downto 0);
signal migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PWRITE  : std_logic;
signal migou_sf2_50_v1_sb_0_POWER_ON_RESET_N          : std_logic;
signal MMUART_0_TXD_net_0                             : std_logic;
signal SPI_1_DO_net_0                                 : std_logic;
signal SPI_1_DO_net_1                                 : std_logic;
signal MMUART_0_TXD_net_1                             : std_logic;
signal GPIO_1_net_0                                   : std_logic_vector(14 downto 0);
signal M3_SLEEPDEEP_1_net_0                           : std_logic;
signal MSS_INT_F2M_net_0                              : std_logic_vector(15 downto 0);
----------------------------------------------------------------------
-- TiedOff Signals
----------------------------------------------------------------------
signal VCC_net                                        : std_logic;
signal MSS_INT_F2M_const_net_0                        : std_logic_vector(15 downto 1);
signal GND_net                                        : std_logic;
signal IADDR_const_net_0                              : std_logic_vector(31 downto 0);
signal PRDATAS1_const_net_0                           : std_logic_vector(31 downto 0);
signal PRDATAS2_const_net_0                           : std_logic_vector(31 downto 0);
signal PRDATAS3_const_net_0                           : std_logic_vector(31 downto 0);
signal PRDATAS4_const_net_0                           : std_logic_vector(31 downto 0);
signal PRDATAS5_const_net_0                           : std_logic_vector(31 downto 0);
signal PRDATAS6_const_net_0                           : std_logic_vector(31 downto 0);
signal PRDATAS7_const_net_0                           : std_logic_vector(31 downto 0);
signal PRDATAS8_const_net_0                           : std_logic_vector(31 downto 0);
signal PRDATAS9_const_net_0                           : std_logic_vector(31 downto 0);
signal PRDATAS10_const_net_0                          : std_logic_vector(31 downto 0);
signal PRDATAS11_const_net_0                          : std_logic_vector(31 downto 0);
signal PRDATAS12_const_net_0                          : std_logic_vector(31 downto 0);
signal PRDATAS13_const_net_0                          : std_logic_vector(31 downto 0);
signal PRDATAS14_const_net_0                          : std_logic_vector(31 downto 0);
signal PRDATAS15_const_net_0                          : std_logic_vector(31 downto 0);
signal PRDATAS16_const_net_0                          : std_logic_vector(31 downto 0);
----------------------------------------------------------------------
-- Inverted Signals
----------------------------------------------------------------------
signal B_IN_POST_INV0_0                               : std_logic;
----------------------------------------------------------------------
-- Bus Interface Nets Declarations - Unequal Pin Widths
----------------------------------------------------------------------
signal CoreAPB3_0_APBmslave0_PADDR_0_7to0             : std_logic_vector(7 downto 0);
signal CoreAPB3_0_APBmslave0_PADDR_0                  : std_logic_vector(7 downto 0);
signal CoreAPB3_0_APBmslave0_PADDR                    : std_logic_vector(31 downto 0);


begin
----------------------------------------------------------------------
-- Constant assignments
----------------------------------------------------------------------
 VCC_net                 <= '1';
 MSS_INT_F2M_const_net_0 <= B"000000000000000";
 GND_net                 <= '0';
 IADDR_const_net_0       <= B"00000000000000000000000000000000";
 PRDATAS1_const_net_0    <= B"00000000000000000000000000000000";
 PRDATAS2_const_net_0    <= B"00000000000000000000000000000000";
 PRDATAS3_const_net_0    <= B"00000000000000000000000000000000";
 PRDATAS4_const_net_0    <= B"00000000000000000000000000000000";
 PRDATAS5_const_net_0    <= B"00000000000000000000000000000000";
 PRDATAS6_const_net_0    <= B"00000000000000000000000000000000";
 PRDATAS7_const_net_0    <= B"00000000000000000000000000000000";
 PRDATAS8_const_net_0    <= B"00000000000000000000000000000000";
 PRDATAS9_const_net_0    <= B"00000000000000000000000000000000";
 PRDATAS10_const_net_0   <= B"00000000000000000000000000000000";
 PRDATAS11_const_net_0   <= B"00000000000000000000000000000000";
 PRDATAS12_const_net_0   <= B"00000000000000000000000000000000";
 PRDATAS13_const_net_0   <= B"00000000000000000000000000000000";
 PRDATAS14_const_net_0   <= B"00000000000000000000000000000000";
 PRDATAS15_const_net_0   <= B"00000000000000000000000000000000";
 PRDATAS16_const_net_0   <= B"00000000000000000000000000000000";
----------------------------------------------------------------------
-- Inversions
----------------------------------------------------------------------
 B_IN_POST_INV0_0 <= NOT FLASH_FREEZE_0_FF_DONE;
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 SPI_1_DO_net_1       <= SPI_1_DO_net_0;
 SPI_1_DO             <= SPI_1_DO_net_1;
 MMUART_0_TXD_net_1   <= MMUART_0_TXD_net_0;
 MMUART_0_TXD         <= MMUART_0_TXD_net_1;
 GPIO_1_net_0         <= GPIO_1;
 GPIO(14 downto 0)    <= GPIO_1_net_0;
 M3_SLEEPDEEP_1_net_0 <= M3_SLEEPDEEP_1;
 M3_SLEEPDEEP         <= M3_SLEEPDEEP_1_net_0;
----------------------------------------------------------------------
-- Concatenation assignments
----------------------------------------------------------------------
 MSS_INT_F2M_net_0 <= ( B"000000000000000" & AND2_0_Y );
----------------------------------------------------------------------
-- Bus Interface Nets Assignments - Unequal Pin Widths
----------------------------------------------------------------------
 CoreAPB3_0_APBmslave0_PADDR_0_7to0(7 downto 0) <= CoreAPB3_0_APBmslave0_PADDR(7 downto 0);
 CoreAPB3_0_APBmslave0_PADDR_0 <= ( CoreAPB3_0_APBmslave0_PADDR_0_7to0(7 downto 0) );

----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- AND2_0
AND2_0 : AND2
    port map( 
        -- Inputs
        A => CoreGPIO_0_INT_OR,
        B => B_IN_POST_INV0_0,
        -- Outputs
        Y => AND2_0_Y 
        );
-- CoreAPB3_0   -   Actel:DirectCore:CoreAPB3:4.1.100
CoreAPB3_0 : entity COREAPB3_LIB.CoreAPB3
    generic map( 
        APB_DWIDTH      => ( 32 ),
        APBSLOT0ENABLE  => ( 1 ),
        APBSLOT1ENABLE  => ( 0 ),
        APBSLOT2ENABLE  => ( 0 ),
        APBSLOT3ENABLE  => ( 0 ),
        APBSLOT4ENABLE  => ( 0 ),
        APBSLOT5ENABLE  => ( 0 ),
        APBSLOT6ENABLE  => ( 0 ),
        APBSLOT7ENABLE  => ( 0 ),
        APBSLOT8ENABLE  => ( 0 ),
        APBSLOT9ENABLE  => ( 0 ),
        APBSLOT10ENABLE => ( 0 ),
        APBSLOT11ENABLE => ( 0 ),
        APBSLOT12ENABLE => ( 0 ),
        APBSLOT13ENABLE => ( 0 ),
        APBSLOT14ENABLE => ( 0 ),
        APBSLOT15ENABLE => ( 0 ),
        FAMILY          => ( 19 ),
        IADDR_OPTION    => ( 0 ),
        MADDR_BITS      => ( 28 ),
        SC_0            => ( 0 ),
        SC_1            => ( 0 ),
        SC_2            => ( 0 ),
        SC_3            => ( 0 ),
        SC_4            => ( 0 ),
        SC_5            => ( 0 ),
        SC_6            => ( 0 ),
        SC_7            => ( 0 ),
        SC_8            => ( 0 ),
        SC_9            => ( 0 ),
        SC_10           => ( 0 ),
        SC_11           => ( 0 ),
        SC_12           => ( 0 ),
        SC_13           => ( 0 ),
        SC_14           => ( 0 ),
        SC_15           => ( 0 ),
        UPR_NIBBLE_POSN => ( 6 )
        )
    port map( 
        -- Inputs
        PRESETN    => GND_net, -- tied to '0' from definition
        PCLK       => GND_net, -- tied to '0' from definition
        PWRITE     => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PWRITE,
        PENABLE    => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PENABLE,
        PSEL       => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PSELx,
        PREADYS0   => CoreAPB3_0_APBmslave0_PREADY,
        PSLVERRS0  => CoreAPB3_0_APBmslave0_PSLVERR,
        PREADYS1   => VCC_net, -- tied to '1' from definition
        PSLVERRS1  => GND_net, -- tied to '0' from definition
        PREADYS2   => VCC_net, -- tied to '1' from definition
        PSLVERRS2  => GND_net, -- tied to '0' from definition
        PREADYS3   => VCC_net, -- tied to '1' from definition
        PSLVERRS3  => GND_net, -- tied to '0' from definition
        PREADYS4   => VCC_net, -- tied to '1' from definition
        PSLVERRS4  => GND_net, -- tied to '0' from definition
        PREADYS5   => VCC_net, -- tied to '1' from definition
        PSLVERRS5  => GND_net, -- tied to '0' from definition
        PREADYS6   => VCC_net, -- tied to '1' from definition
        PSLVERRS6  => GND_net, -- tied to '0' from definition
        PREADYS7   => VCC_net, -- tied to '1' from definition
        PSLVERRS7  => GND_net, -- tied to '0' from definition
        PREADYS8   => VCC_net, -- tied to '1' from definition
        PSLVERRS8  => GND_net, -- tied to '0' from definition
        PREADYS9   => VCC_net, -- tied to '1' from definition
        PSLVERRS9  => GND_net, -- tied to '0' from definition
        PREADYS10  => VCC_net, -- tied to '1' from definition
        PSLVERRS10 => GND_net, -- tied to '0' from definition
        PREADYS11  => VCC_net, -- tied to '1' from definition
        PSLVERRS11 => GND_net, -- tied to '0' from definition
        PREADYS12  => VCC_net, -- tied to '1' from definition
        PSLVERRS12 => GND_net, -- tied to '0' from definition
        PREADYS13  => VCC_net, -- tied to '1' from definition
        PSLVERRS13 => GND_net, -- tied to '0' from definition
        PREADYS14  => VCC_net, -- tied to '1' from definition
        PSLVERRS14 => GND_net, -- tied to '0' from definition
        PREADYS15  => VCC_net, -- tied to '1' from definition
        PSLVERRS15 => GND_net, -- tied to '0' from definition
        PREADYS16  => VCC_net, -- tied to '1' from definition
        PSLVERRS16 => GND_net, -- tied to '0' from definition
        PADDR      => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PADDR,
        PWDATA     => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PWDATA,
        PRDATAS0   => CoreAPB3_0_APBmslave0_PRDATA,
        PRDATAS1   => PRDATAS1_const_net_0, -- tied to X"0" from definition
        PRDATAS2   => PRDATAS2_const_net_0, -- tied to X"0" from definition
        PRDATAS3   => PRDATAS3_const_net_0, -- tied to X"0" from definition
        PRDATAS4   => PRDATAS4_const_net_0, -- tied to X"0" from definition
        PRDATAS5   => PRDATAS5_const_net_0, -- tied to X"0" from definition
        PRDATAS6   => PRDATAS6_const_net_0, -- tied to X"0" from definition
        PRDATAS7   => PRDATAS7_const_net_0, -- tied to X"0" from definition
        PRDATAS8   => PRDATAS8_const_net_0, -- tied to X"0" from definition
        PRDATAS9   => PRDATAS9_const_net_0, -- tied to X"0" from definition
        PRDATAS10  => PRDATAS10_const_net_0, -- tied to X"0" from definition
        PRDATAS11  => PRDATAS11_const_net_0, -- tied to X"0" from definition
        PRDATAS12  => PRDATAS12_const_net_0, -- tied to X"0" from definition
        PRDATAS13  => PRDATAS13_const_net_0, -- tied to X"0" from definition
        PRDATAS14  => PRDATAS14_const_net_0, -- tied to X"0" from definition
        PRDATAS15  => PRDATAS15_const_net_0, -- tied to X"0" from definition
        PRDATAS16  => PRDATAS16_const_net_0, -- tied to X"0" from definition
        IADDR      => IADDR_const_net_0, -- tied to X"0" from definition
        -- Outputs
        PREADY     => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PREADY,
        PSLVERR    => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PSLVERR,
        PWRITES    => CoreAPB3_0_APBmslave0_PWRITE,
        PENABLES   => CoreAPB3_0_APBmslave0_PENABLE,
        PSELS0     => CoreAPB3_0_APBmslave0_PSELx,
        PSELS1     => OPEN,
        PSELS2     => OPEN,
        PSELS3     => OPEN,
        PSELS4     => OPEN,
        PSELS5     => OPEN,
        PSELS6     => OPEN,
        PSELS7     => OPEN,
        PSELS8     => OPEN,
        PSELS9     => OPEN,
        PSELS10    => OPEN,
        PSELS11    => OPEN,
        PSELS12    => OPEN,
        PSELS13    => OPEN,
        PSELS14    => OPEN,
        PSELS15    => OPEN,
        PSELS16    => OPEN,
        PRDATA     => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PRDATA,
        PADDRS     => CoreAPB3_0_APBmslave0_PADDR,
        PWDATAS    => CoreAPB3_0_APBmslave0_PWDATA 
        );
-- CoreGPIO_0   -   Actel:DirectCore:CoreGPIO:3.2.102
CoreGPIO_0 : migou_sf2_50_v1_CoreGPIO_0_CoreGPIO
    generic map( 
        APB_WIDTH       => ( 32 ),
        FIXED_CONFIG_0  => ( 0 ),
        FIXED_CONFIG_1  => ( 0 ),
        FIXED_CONFIG_2  => ( 0 ),
        FIXED_CONFIG_3  => ( 0 ),
        FIXED_CONFIG_4  => ( 0 ),
        FIXED_CONFIG_5  => ( 0 ),
        FIXED_CONFIG_6  => ( 0 ),
        FIXED_CONFIG_7  => ( 0 ),
        FIXED_CONFIG_8  => ( 0 ),
        FIXED_CONFIG_9  => ( 0 ),
        FIXED_CONFIG_10 => ( 0 ),
        FIXED_CONFIG_11 => ( 0 ),
        FIXED_CONFIG_12 => ( 0 ),
        FIXED_CONFIG_13 => ( 0 ),
        FIXED_CONFIG_14 => ( 0 ),
        FIXED_CONFIG_15 => ( 0 ),
        FIXED_CONFIG_16 => ( 0 ),
        FIXED_CONFIG_17 => ( 0 ),
        FIXED_CONFIG_18 => ( 1 ),
        FIXED_CONFIG_19 => ( 0 ),
        FIXED_CONFIG_20 => ( 0 ),
        FIXED_CONFIG_21 => ( 0 ),
        FIXED_CONFIG_22 => ( 0 ),
        FIXED_CONFIG_23 => ( 0 ),
        FIXED_CONFIG_24 => ( 0 ),
        FIXED_CONFIG_25 => ( 0 ),
        FIXED_CONFIG_26 => ( 0 ),
        FIXED_CONFIG_27 => ( 0 ),
        FIXED_CONFIG_28 => ( 0 ),
        FIXED_CONFIG_29 => ( 0 ),
        FIXED_CONFIG_30 => ( 0 ),
        FIXED_CONFIG_31 => ( 0 ),
        INT_BUS         => ( 1 ),
        IO_INT_TYPE_0   => ( 7 ),
        IO_INT_TYPE_1   => ( 7 ),
        IO_INT_TYPE_2   => ( 7 ),
        IO_INT_TYPE_3   => ( 7 ),
        IO_INT_TYPE_4   => ( 7 ),
        IO_INT_TYPE_5   => ( 7 ),
        IO_INT_TYPE_6   => ( 7 ),
        IO_INT_TYPE_7   => ( 7 ),
        IO_INT_TYPE_8   => ( 7 ),
        IO_INT_TYPE_9   => ( 7 ),
        IO_INT_TYPE_10  => ( 7 ),
        IO_INT_TYPE_11  => ( 7 ),
        IO_INT_TYPE_12  => ( 7 ),
        IO_INT_TYPE_13  => ( 7 ),
        IO_INT_TYPE_14  => ( 7 ),
        IO_INT_TYPE_15  => ( 7 ),
        IO_INT_TYPE_16  => ( 7 ),
        IO_INT_TYPE_17  => ( 7 ),
        IO_INT_TYPE_18  => ( 7 ),
        IO_INT_TYPE_19  => ( 7 ),
        IO_INT_TYPE_20  => ( 7 ),
        IO_INT_TYPE_21  => ( 7 ),
        IO_INT_TYPE_22  => ( 7 ),
        IO_INT_TYPE_23  => ( 7 ),
        IO_INT_TYPE_24  => ( 7 ),
        IO_INT_TYPE_25  => ( 7 ),
        IO_INT_TYPE_26  => ( 7 ),
        IO_INT_TYPE_27  => ( 7 ),
        IO_INT_TYPE_28  => ( 7 ),
        IO_INT_TYPE_29  => ( 7 ),
        IO_INT_TYPE_30  => ( 7 ),
        IO_INT_TYPE_31  => ( 7 ),
        IO_NUM          => ( 15 ),
        IO_TYPE_0       => ( 0 ),
        IO_TYPE_1       => ( 0 ),
        IO_TYPE_2       => ( 0 ),
        IO_TYPE_3       => ( 0 ),
        IO_TYPE_4       => ( 0 ),
        IO_TYPE_5       => ( 0 ),
        IO_TYPE_6       => ( 0 ),
        IO_TYPE_7       => ( 0 ),
        IO_TYPE_8       => ( 0 ),
        IO_TYPE_9       => ( 0 ),
        IO_TYPE_10      => ( 0 ),
        IO_TYPE_11      => ( 0 ),
        IO_TYPE_12      => ( 0 ),
        IO_TYPE_13      => ( 0 ),
        IO_TYPE_14      => ( 0 ),
        IO_TYPE_15      => ( 0 ),
        IO_TYPE_16      => ( 0 ),
        IO_TYPE_17      => ( 0 ),
        IO_TYPE_18      => ( 0 ),
        IO_TYPE_19      => ( 0 ),
        IO_TYPE_20      => ( 0 ),
        IO_TYPE_21      => ( 0 ),
        IO_TYPE_22      => ( 0 ),
        IO_TYPE_23      => ( 0 ),
        IO_TYPE_24      => ( 0 ),
        IO_TYPE_25      => ( 0 ),
        IO_TYPE_26      => ( 0 ),
        IO_TYPE_27      => ( 0 ),
        IO_TYPE_28      => ( 0 ),
        IO_TYPE_29      => ( 0 ),
        IO_TYPE_30      => ( 0 ),
        IO_TYPE_31      => ( 0 ),
        IO_VAL_0        => ( 0 ),
        IO_VAL_1        => ( 0 ),
        IO_VAL_2        => ( 0 ),
        IO_VAL_3        => ( 0 ),
        IO_VAL_4        => ( 0 ),
        IO_VAL_5        => ( 0 ),
        IO_VAL_6        => ( 0 ),
        IO_VAL_7        => ( 0 ),
        IO_VAL_8        => ( 0 ),
        IO_VAL_9        => ( 0 ),
        IO_VAL_10       => ( 0 ),
        IO_VAL_11       => ( 0 ),
        IO_VAL_12       => ( 0 ),
        IO_VAL_13       => ( 0 ),
        IO_VAL_14       => ( 0 ),
        IO_VAL_15       => ( 0 ),
        IO_VAL_16       => ( 0 ),
        IO_VAL_17       => ( 0 ),
        IO_VAL_18       => ( 0 ),
        IO_VAL_19       => ( 0 ),
        IO_VAL_20       => ( 0 ),
        IO_VAL_21       => ( 0 ),
        IO_VAL_22       => ( 0 ),
        IO_VAL_23       => ( 0 ),
        IO_VAL_24       => ( 0 ),
        IO_VAL_25       => ( 0 ),
        IO_VAL_26       => ( 0 ),
        IO_VAL_27       => ( 0 ),
        IO_VAL_28       => ( 0 ),
        IO_VAL_29       => ( 0 ),
        IO_VAL_30       => ( 0 ),
        IO_VAL_31       => ( 0 ),
        OE_TYPE         => ( 1 )
        )
    port map( 
        -- Inputs
        PRESETN  => migou_sf2_50_v1_sb_0_POWER_ON_RESET_N,
        PCLK     => FIC_0_CLK,
        PSEL     => CoreAPB3_0_APBmslave0_PSELx,
        PENABLE  => CoreAPB3_0_APBmslave0_PENABLE,
        PWRITE   => CoreAPB3_0_APBmslave0_PWRITE,
        PADDR    => CoreAPB3_0_APBmslave0_PADDR_0,
        PWDATA   => CoreAPB3_0_APBmslave0_PWDATA,
        GPIO_IN  => GPIO_1,
        -- Outputs
        PSLVERR  => CoreAPB3_0_APBmslave0_PSLVERR,
        PREADY   => CoreAPB3_0_APBmslave0_PREADY,
        INT_OR   => CoreGPIO_0_INT_OR,
        PRDATA   => CoreAPB3_0_APBmslave0_PRDATA,
        INT      => OPEN,
        GPIO_OUT => GPIO_1,
        GPIO_OE  => OPEN 
        );
-- FLASH_FREEZE_0
FLASH_FREEZE_0 : FLASH_FREEZE
    port map( 
        -- Outputs
        FF_TO_START => OPEN,
        FF_DONE     => FLASH_FREEZE_0_FF_DONE 
        );
-- migou_sf2_50_v1_sb_0
migou_sf2_50_v1_sb_0 : migou_sf2_50_v1_sb
    port map( 
        -- Inputs
        SPI_1_DI            => SPI_1_DI,
        MMUART_0_RXD        => MMUART_0_RXD,
        FAB_RESET_N         => VCC_net,
        DEVRST_N            => DEVRST_N,
        MSS_INT_F2M         => MSS_INT_F2M_net_0,
        FIC_0_APB_M_PRDATA  => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PRDATA,
        FIC_0_APB_M_PREADY  => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PREADY,
        FIC_0_APB_M_PSLVERR => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PSLVERR,
        -- Outputs
        SPI_1_DO            => SPI_1_DO_net_0,
        MMUART_0_TXD        => MMUART_0_TXD_net_0,
        POWER_ON_RESET_N    => migou_sf2_50_v1_sb_0_POWER_ON_RESET_N,
        INIT_DONE           => OPEN,
        FIC_0_CLK           => FIC_0_CLK,
        FIC_0_LOCK          => OPEN,
        MSS_READY           => OPEN,
        FIC_0_APB_M_PADDR   => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PADDR,
        FIC_0_APB_M_PSEL    => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PSELx,
        FIC_0_APB_M_PENABLE => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PENABLE,
        FIC_0_APB_M_PWRITE  => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PWRITE,
        FIC_0_APB_M_PWDATA  => migou_sf2_50_v1_sb_0_FIC_0_AMBA_MASTER_PWDATA,
        M3_SLEEPDEEP        => M3_SLEEPDEEP_1,
        -- Inouts
        SPI_1_CLK           => SPI_1_CLK,
        SPI_1_SS0           => SPI_1_SS0,
        GPIO_8_BI           => FF_CONTROL,
        GPIO_9_BI           => FF_WAKEUP,
        GPIO_23_BI          => TRX_IRQ,
        GPIO_24_BI          => TRX_RSTN,
        GPIO_29_BI          => FG_GPOUT 
        );

end RTL;
