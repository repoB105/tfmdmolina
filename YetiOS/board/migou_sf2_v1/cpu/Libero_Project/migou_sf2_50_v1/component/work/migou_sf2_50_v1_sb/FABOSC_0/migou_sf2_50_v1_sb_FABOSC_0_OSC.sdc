set_component migou_sf2_50_v1_sb_FABOSC_0_OSC
# Microsemi Corp.
# Date: 2019-Apr-11 13:05:09
#

create_clock -ignore_errors -period 20 [ get_pins { I_RCOSC_25_50MHZ/CLKOUT } ]
