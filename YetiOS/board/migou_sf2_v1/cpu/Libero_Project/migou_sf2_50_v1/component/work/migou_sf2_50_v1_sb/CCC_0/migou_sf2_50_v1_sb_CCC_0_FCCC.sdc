set_component migou_sf2_50_v1_sb_CCC_0_FCCC
# Microsemi Corp.
# Date: 2019-Apr-11 13:05:07
#

create_clock -period 20 [ get_pins { CCC_INST/RCOSC_25_50MHZ } ]
create_generated_clock -multiply_by 2 -divide_by 2 -source [ get_pins { CCC_INST/RCOSC_25_50MHZ } ] -phase 0 [ get_pins { CCC_INST/GL0 } ]
