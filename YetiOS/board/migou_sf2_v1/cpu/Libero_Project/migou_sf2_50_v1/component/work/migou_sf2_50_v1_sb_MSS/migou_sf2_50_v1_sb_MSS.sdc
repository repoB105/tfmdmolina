set_component migou_sf2_50_v1_sb_MSS
# Microsemi Corp.
# Date: 2019-Apr-11 13:05:05
#

create_clock -period 80 [ get_pins { MSS_ADLIB_INST/CLK_CONFIG_APB } ]
set_false_path -ignore_errors -through [ get_pins { MSS_ADLIB_INST/CONFIG_PRESET_N } ]
