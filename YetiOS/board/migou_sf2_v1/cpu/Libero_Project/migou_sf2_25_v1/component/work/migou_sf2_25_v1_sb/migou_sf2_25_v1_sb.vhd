----------------------------------------------------------------------
-- Created by SmartDesign Tue Dec 04 14:37:54 2018
-- Version: v11.9 SP2 11.9.2.1
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library smartfusion2;
use smartfusion2.all;
----------------------------------------------------------------------
-- migou_sf2_25_v1_sb entity declaration
----------------------------------------------------------------------
entity migou_sf2_25_v1_sb is
    -- Port list
    port(
        -- Inputs
        DEVRST_N            : in    std_logic;
        FAB_RESET_N         : in    std_logic;
        FIC_0_APB_M_PRDATA  : in    std_logic_vector(31 downto 0);
        FIC_0_APB_M_PREADY  : in    std_logic;
        FIC_0_APB_M_PSLVERR : in    std_logic;
        MMUART_0_RXD        : in    std_logic;
        MMUART_1_RXD        : in    std_logic;
        MSS_INT_F2M         : in    std_logic_vector(15 downto 0);
        SPI_0_DI            : in    std_logic;
        SPI_1_DI            : in    std_logic;
        -- Outputs
        FIC_0_APB_M_PADDR   : out   std_logic_vector(31 downto 0);
        FIC_0_APB_M_PENABLE : out   std_logic;
        FIC_0_APB_M_PSEL    : out   std_logic;
        FIC_0_APB_M_PWDATA  : out   std_logic_vector(31 downto 0);
        FIC_0_APB_M_PWRITE  : out   std_logic;
        FIC_0_CLK           : out   std_logic;
        FIC_0_LOCK          : out   std_logic;
        INIT_DONE           : out   std_logic;
        MMUART_0_TXD        : out   std_logic;
        MMUART_1_TXD        : out   std_logic;
        MSS_READY           : out   std_logic;
        POWER_ON_RESET_N    : out   std_logic;
        SPI_0_DO            : out   std_logic;
        SPI_0_SS1           : out   std_logic;
        SPI_0_SS2           : out   std_logic;
        SPI_1_DO            : out   std_logic;
        -- Inouts
        I2C_0_SCL           : inout std_logic;
        I2C_0_SDA           : inout std_logic;
        I2C_1_SCL           : inout std_logic;
        I2C_1_SDA           : inout std_logic;
        SPI_0_CLK           : inout std_logic;
        SPI_0_SS0           : inout std_logic;
        SPI_1_CLK           : inout std_logic;
        SPI_1_SS0           : inout std_logic
        );
end migou_sf2_25_v1_sb;
----------------------------------------------------------------------
-- migou_sf2_25_v1_sb architecture body
----------------------------------------------------------------------
architecture RTL of migou_sf2_25_v1_sb is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- migou_sf2_25_v1_sb_CCC_0_FCCC   -   Actel:SgCore:FCCC:2.0.201
component migou_sf2_25_v1_sb_CCC_0_FCCC
    -- Port list
    port(
        -- Inputs
        RCOSC_25_50MHZ : in  std_logic;
        -- Outputs
        GL0            : out std_logic;
        LOCK           : out std_logic
        );
end component;
-- CoreResetP   -   Actel:DirectCore:CoreResetP:7.1.100
component CoreResetP
    generic( 
        DDR_WAIT            : integer := 200 ;
        DEVICE_090          : integer := 0 ;
        DEVICE_VOLTAGE      : integer := 2 ;
        ENABLE_SOFT_RESETS  : integer := 0 ;
        EXT_RESET_CFG       : integer := 0 ;
        FDDR_IN_USE         : integer := 0 ;
        MDDR_IN_USE         : integer := 0 ;
        SDIF0_IN_USE        : integer := 0 ;
        SDIF0_PCIE          : integer := 0 ;
        SDIF0_PCIE_HOTRESET : integer := 1 ;
        SDIF0_PCIE_L2P2     : integer := 1 ;
        SDIF1_IN_USE        : integer := 0 ;
        SDIF1_PCIE          : integer := 0 ;
        SDIF1_PCIE_HOTRESET : integer := 1 ;
        SDIF1_PCIE_L2P2     : integer := 1 ;
        SDIF2_IN_USE        : integer := 0 ;
        SDIF2_PCIE          : integer := 0 ;
        SDIF2_PCIE_HOTRESET : integer := 1 ;
        SDIF2_PCIE_L2P2     : integer := 1 ;
        SDIF3_IN_USE        : integer := 0 ;
        SDIF3_PCIE          : integer := 0 ;
        SDIF3_PCIE_HOTRESET : integer := 1 ;
        SDIF3_PCIE_L2P2     : integer := 1 
        );
    -- Port list
    port(
        -- Inputs
        CLK_BASE                       : in  std_logic;
        CLK_LTSSM                      : in  std_logic;
        CONFIG1_DONE                   : in  std_logic;
        CONFIG2_DONE                   : in  std_logic;
        FAB_RESET_N                    : in  std_logic;
        FIC_2_APB_M_PRESET_N           : in  std_logic;
        FPLL_LOCK                      : in  std_logic;
        POWER_ON_RESET_N               : in  std_logic;
        RCOSC_25_50MHZ                 : in  std_logic;
        RESET_N_M2F                    : in  std_logic;
        SDIF0_PERST_N                  : in  std_logic;
        SDIF0_PRDATA                   : in  std_logic_vector(31 downto 0);
        SDIF0_PSEL                     : in  std_logic;
        SDIF0_PWRITE                   : in  std_logic;
        SDIF0_SPLL_LOCK                : in  std_logic;
        SDIF1_PERST_N                  : in  std_logic;
        SDIF1_PRDATA                   : in  std_logic_vector(31 downto 0);
        SDIF1_PSEL                     : in  std_logic;
        SDIF1_PWRITE                   : in  std_logic;
        SDIF1_SPLL_LOCK                : in  std_logic;
        SDIF2_PERST_N                  : in  std_logic;
        SDIF2_PRDATA                   : in  std_logic_vector(31 downto 0);
        SDIF2_PSEL                     : in  std_logic;
        SDIF2_PWRITE                   : in  std_logic;
        SDIF2_SPLL_LOCK                : in  std_logic;
        SDIF3_PERST_N                  : in  std_logic;
        SDIF3_PRDATA                   : in  std_logic_vector(31 downto 0);
        SDIF3_PSEL                     : in  std_logic;
        SDIF3_PWRITE                   : in  std_logic;
        SDIF3_SPLL_LOCK                : in  std_logic;
        SOFT_EXT_RESET_OUT             : in  std_logic;
        SOFT_FDDR_CORE_RESET           : in  std_logic;
        SOFT_M3_RESET                  : in  std_logic;
        SOFT_MDDR_DDR_AXI_S_CORE_RESET : in  std_logic;
        SOFT_RESET_F2M                 : in  std_logic;
        SOFT_SDIF0_0_CORE_RESET        : in  std_logic;
        SOFT_SDIF0_1_CORE_RESET        : in  std_logic;
        SOFT_SDIF0_CORE_RESET          : in  std_logic;
        SOFT_SDIF0_PHY_RESET           : in  std_logic;
        SOFT_SDIF1_CORE_RESET          : in  std_logic;
        SOFT_SDIF1_PHY_RESET           : in  std_logic;
        SOFT_SDIF2_CORE_RESET          : in  std_logic;
        SOFT_SDIF2_PHY_RESET           : in  std_logic;
        SOFT_SDIF3_CORE_RESET          : in  std_logic;
        SOFT_SDIF3_PHY_RESET           : in  std_logic;
        -- Outputs
        DDR_READY                      : out std_logic;
        EXT_RESET_OUT                  : out std_logic;
        FDDR_CORE_RESET_N              : out std_logic;
        INIT_DONE                      : out std_logic;
        M3_RESET_N                     : out std_logic;
        MDDR_DDR_AXI_S_CORE_RESET_N    : out std_logic;
        MSS_HPMS_READY                 : out std_logic;
        RESET_N_F2M                    : out std_logic;
        SDIF0_0_CORE_RESET_N           : out std_logic;
        SDIF0_1_CORE_RESET_N           : out std_logic;
        SDIF0_CORE_RESET_N             : out std_logic;
        SDIF0_PHY_RESET_N              : out std_logic;
        SDIF1_CORE_RESET_N             : out std_logic;
        SDIF1_PHY_RESET_N              : out std_logic;
        SDIF2_CORE_RESET_N             : out std_logic;
        SDIF2_PHY_RESET_N              : out std_logic;
        SDIF3_CORE_RESET_N             : out std_logic;
        SDIF3_PHY_RESET_N              : out std_logic;
        SDIF_READY                     : out std_logic;
        SDIF_RELEASED                  : out std_logic
        );
end component;
-- migou_sf2_25_v1_sb_FABOSC_0_OSC   -   Actel:SgCore:OSC:2.0.101
component migou_sf2_25_v1_sb_FABOSC_0_OSC
    -- Port list
    port(
        -- Inputs
        XTL                : in  std_logic;
        -- Outputs
        RCOSC_1MHZ_CCC     : out std_logic;
        RCOSC_1MHZ_O2F     : out std_logic;
        RCOSC_25_50MHZ_CCC : out std_logic;
        RCOSC_25_50MHZ_O2F : out std_logic;
        XTLOSC_CCC         : out std_logic;
        XTLOSC_O2F         : out std_logic
        );
end component;
-- migou_sf2_25_v1_sb_MSS
component migou_sf2_25_v1_sb_MSS
    -- Port list
    port(
        -- Inputs
        FIC_0_APB_M_PRDATA     : in    std_logic_vector(31 downto 0);
        FIC_0_APB_M_PREADY     : in    std_logic;
        FIC_0_APB_M_PSLVERR    : in    std_logic;
        FIC_2_APB_M_PRDATA     : in    std_logic_vector(31 downto 0);
        FIC_2_APB_M_PREADY     : in    std_logic;
        FIC_2_APB_M_PSLVERR    : in    std_logic;
        MCCC_CLK_BASE          : in    std_logic;
        MCCC_CLK_BASE_PLL_LOCK : in    std_logic;
        MMUART_0_RXD           : in    std_logic;
        MMUART_1_RXD           : in    std_logic;
        MSS_INT_F2M            : in    std_logic_vector(15 downto 0);
        MSS_RESET_N_F2M        : in    std_logic;
        SPI_0_DI               : in    std_logic;
        SPI_1_DI               : in    std_logic;
        -- Outputs
        FIC_0_APB_M_PADDR      : out   std_logic_vector(31 downto 0);
        FIC_0_APB_M_PENABLE    : out   std_logic;
        FIC_0_APB_M_PSEL       : out   std_logic;
        FIC_0_APB_M_PWDATA     : out   std_logic_vector(31 downto 0);
        FIC_0_APB_M_PWRITE     : out   std_logic;
        FIC_2_APB_M_PADDR      : out   std_logic_vector(15 downto 2);
        FIC_2_APB_M_PCLK       : out   std_logic;
        FIC_2_APB_M_PENABLE    : out   std_logic;
        FIC_2_APB_M_PRESET_N   : out   std_logic;
        FIC_2_APB_M_PSEL       : out   std_logic;
        FIC_2_APB_M_PWDATA     : out   std_logic_vector(31 downto 0);
        FIC_2_APB_M_PWRITE     : out   std_logic;
        MMUART_0_TXD           : out   std_logic;
        MMUART_1_TXD           : out   std_logic;
        MSS_RESET_N_M2F        : out   std_logic;
        SPI_0_DO               : out   std_logic;
        SPI_0_SS1              : out   std_logic;
        SPI_0_SS2              : out   std_logic;
        SPI_1_DO               : out   std_logic;
        -- Inouts
        I2C_0_SCL              : inout std_logic;
        I2C_0_SDA              : inout std_logic;
        I2C_1_SCL              : inout std_logic;
        I2C_1_SDA              : inout std_logic;
        SPI_0_CLK              : inout std_logic;
        SPI_0_SS0              : inout std_logic;
        SPI_1_CLK              : inout std_logic;
        SPI_1_SS0              : inout std_logic
        );
end component;
-- SYSRESET
component SYSRESET
    -- Port list
    port(
        -- Inputs
        DEVRST_N         : in  std_logic;
        -- Outputs
        POWER_ON_RESET_N : out std_logic
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal CORERESETP_0_RESET_N_F2M                           : std_logic;
signal FABOSC_0_RCOSC_25_50MHZ_CCC_OUT_RCOSC_25_50MHZ_CCC : std_logic;
signal FABOSC_0_RCOSC_25_50MHZ_O2F                        : std_logic;
signal FIC_0_APB_MASTER_2_PADDR                           : std_logic_vector(31 downto 0);
signal FIC_0_APB_MASTER_2_PENABLE                         : std_logic;
signal FIC_0_APB_MASTER_2_PSELx                           : std_logic;
signal FIC_0_APB_MASTER_2_PWDATA                          : std_logic_vector(31 downto 0);
signal FIC_0_APB_MASTER_2_PWRITE                          : std_logic;
signal FIC_0_CLK_net_0                                    : std_logic;
signal FIC_0_LOCK_net_0                                   : std_logic;
signal INIT_DONE_net_0                                    : std_logic;
signal migou_sf2_25_v1_sb_MSS_TMP_0_FIC_2_APB_M_PRESET_N  : std_logic;
signal migou_sf2_25_v1_sb_MSS_TMP_0_MSS_RESET_N_M2F       : std_logic;
signal MMUART_0_TXD_net_0                                 : std_logic;
signal MMUART_1_TXD_net_0                                 : std_logic;
signal MSS_READY_net_0                                    : std_logic;
signal POWER_ON_RESET_N_net_0                             : std_logic;
signal SPI_0_DO_net_0                                     : std_logic;
signal SPI_0_SS1_net_0                                    : std_logic;
signal SPI_0_SS2_net_0                                    : std_logic;
signal SPI_1_DO_net_0                                     : std_logic;
signal SPI_0_DO_net_1                                     : std_logic;
signal SPI_1_DO_net_1                                     : std_logic;
signal MMUART_1_TXD_net_1                                 : std_logic;
signal MMUART_0_TXD_net_1                                 : std_logic;
signal POWER_ON_RESET_N_net_1                             : std_logic;
signal INIT_DONE_net_1                                    : std_logic;
signal SPI_0_SS1_net_1                                    : std_logic;
signal SPI_0_SS2_net_1                                    : std_logic;
signal FIC_0_CLK_net_1                                    : std_logic;
signal FIC_0_LOCK_net_1                                   : std_logic;
signal MSS_READY_net_1                                    : std_logic;
signal FIC_0_APB_MASTER_2_PADDR_net_0                     : std_logic_vector(31 downto 0);
signal FIC_0_APB_MASTER_2_PSELx_net_0                     : std_logic;
signal FIC_0_APB_MASTER_2_PENABLE_net_0                   : std_logic;
signal FIC_0_APB_MASTER_2_PWRITE_net_0                    : std_logic;
signal FIC_0_APB_MASTER_2_PWDATA_net_0                    : std_logic_vector(31 downto 0);
----------------------------------------------------------------------
-- TiedOff Signals
----------------------------------------------------------------------
signal VCC_net                                            : std_logic;
signal GND_net                                            : std_logic;
signal PADDR_const_net_0                                  : std_logic_vector(7 downto 2);
signal PWDATA_const_net_0                                 : std_logic_vector(7 downto 0);
signal SDIF0_PRDATA_const_net_0                           : std_logic_vector(31 downto 0);
signal SDIF1_PRDATA_const_net_0                           : std_logic_vector(31 downto 0);
signal SDIF2_PRDATA_const_net_0                           : std_logic_vector(31 downto 0);
signal SDIF3_PRDATA_const_net_0                           : std_logic_vector(31 downto 0);
signal FIC_2_APB_M_PRDATA_const_net_0                     : std_logic_vector(31 downto 0);

begin
----------------------------------------------------------------------
-- Constant assignments
----------------------------------------------------------------------
 VCC_net                        <= '1';
 GND_net                        <= '0';
 PADDR_const_net_0              <= B"000000";
 PWDATA_const_net_0             <= B"00000000";
 SDIF0_PRDATA_const_net_0       <= B"00000000000000000000000000000000";
 SDIF1_PRDATA_const_net_0       <= B"00000000000000000000000000000000";
 SDIF2_PRDATA_const_net_0       <= B"00000000000000000000000000000000";
 SDIF3_PRDATA_const_net_0       <= B"00000000000000000000000000000000";
 FIC_2_APB_M_PRDATA_const_net_0 <= B"00000000000000000000000000000000";
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 SPI_0_DO_net_1                   <= SPI_0_DO_net_0;
 SPI_0_DO                         <= SPI_0_DO_net_1;
 SPI_1_DO_net_1                   <= SPI_1_DO_net_0;
 SPI_1_DO                         <= SPI_1_DO_net_1;
 MMUART_1_TXD_net_1               <= MMUART_1_TXD_net_0;
 MMUART_1_TXD                     <= MMUART_1_TXD_net_1;
 MMUART_0_TXD_net_1               <= MMUART_0_TXD_net_0;
 MMUART_0_TXD                     <= MMUART_0_TXD_net_1;
 POWER_ON_RESET_N_net_1           <= POWER_ON_RESET_N_net_0;
 POWER_ON_RESET_N                 <= POWER_ON_RESET_N_net_1;
 INIT_DONE_net_1                  <= INIT_DONE_net_0;
 INIT_DONE                        <= INIT_DONE_net_1;
 SPI_0_SS1_net_1                  <= SPI_0_SS1_net_0;
 SPI_0_SS1                        <= SPI_0_SS1_net_1;
 SPI_0_SS2_net_1                  <= SPI_0_SS2_net_0;
 SPI_0_SS2                        <= SPI_0_SS2_net_1;
 FIC_0_CLK_net_1                  <= FIC_0_CLK_net_0;
 FIC_0_CLK                        <= FIC_0_CLK_net_1;
 FIC_0_LOCK_net_1                 <= FIC_0_LOCK_net_0;
 FIC_0_LOCK                       <= FIC_0_LOCK_net_1;
 MSS_READY_net_1                  <= MSS_READY_net_0;
 MSS_READY                        <= MSS_READY_net_1;
 FIC_0_APB_MASTER_2_PADDR_net_0   <= FIC_0_APB_MASTER_2_PADDR;
 FIC_0_APB_M_PADDR(31 downto 0)   <= FIC_0_APB_MASTER_2_PADDR_net_0;
 FIC_0_APB_MASTER_2_PSELx_net_0   <= FIC_0_APB_MASTER_2_PSELx;
 FIC_0_APB_M_PSEL                 <= FIC_0_APB_MASTER_2_PSELx_net_0;
 FIC_0_APB_MASTER_2_PENABLE_net_0 <= FIC_0_APB_MASTER_2_PENABLE;
 FIC_0_APB_M_PENABLE              <= FIC_0_APB_MASTER_2_PENABLE_net_0;
 FIC_0_APB_MASTER_2_PWRITE_net_0  <= FIC_0_APB_MASTER_2_PWRITE;
 FIC_0_APB_M_PWRITE               <= FIC_0_APB_MASTER_2_PWRITE_net_0;
 FIC_0_APB_MASTER_2_PWDATA_net_0  <= FIC_0_APB_MASTER_2_PWDATA;
 FIC_0_APB_M_PWDATA(31 downto 0)  <= FIC_0_APB_MASTER_2_PWDATA_net_0;
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- CCC_0   -   Actel:SgCore:FCCC:2.0.201
CCC_0 : migou_sf2_25_v1_sb_CCC_0_FCCC
    port map( 
        -- Inputs
        RCOSC_25_50MHZ => FABOSC_0_RCOSC_25_50MHZ_CCC_OUT_RCOSC_25_50MHZ_CCC,
        -- Outputs
        GL0            => FIC_0_CLK_net_0,
        LOCK           => FIC_0_LOCK_net_0 
        );
-- CORERESETP_0   -   Actel:DirectCore:CoreResetP:7.1.100
CORERESETP_0 : CoreResetP
    generic map( 
        DDR_WAIT            => ( 200 ),
        DEVICE_090          => ( 0 ),
        DEVICE_VOLTAGE      => ( 2 ),
        ENABLE_SOFT_RESETS  => ( 0 ),
        EXT_RESET_CFG       => ( 0 ),
        FDDR_IN_USE         => ( 0 ),
        MDDR_IN_USE         => ( 0 ),
        SDIF0_IN_USE        => ( 0 ),
        SDIF0_PCIE          => ( 0 ),
        SDIF0_PCIE_HOTRESET => ( 1 ),
        SDIF0_PCIE_L2P2     => ( 1 ),
        SDIF1_IN_USE        => ( 0 ),
        SDIF1_PCIE          => ( 0 ),
        SDIF1_PCIE_HOTRESET => ( 1 ),
        SDIF1_PCIE_L2P2     => ( 1 ),
        SDIF2_IN_USE        => ( 0 ),
        SDIF2_PCIE          => ( 0 ),
        SDIF2_PCIE_HOTRESET => ( 1 ),
        SDIF2_PCIE_L2P2     => ( 1 ),
        SDIF3_IN_USE        => ( 0 ),
        SDIF3_PCIE          => ( 0 ),
        SDIF3_PCIE_HOTRESET => ( 1 ),
        SDIF3_PCIE_L2P2     => ( 1 )
        )
    port map( 
        -- Inputs
        RESET_N_M2F                    => migou_sf2_25_v1_sb_MSS_TMP_0_MSS_RESET_N_M2F,
        FIC_2_APB_M_PRESET_N           => migou_sf2_25_v1_sb_MSS_TMP_0_FIC_2_APB_M_PRESET_N,
        POWER_ON_RESET_N               => POWER_ON_RESET_N_net_0,
        FAB_RESET_N                    => FAB_RESET_N,
        RCOSC_25_50MHZ                 => FABOSC_0_RCOSC_25_50MHZ_O2F,
        CLK_BASE                       => FIC_0_CLK_net_0,
        CLK_LTSSM                      => GND_net, -- tied to '0' from definition
        FPLL_LOCK                      => VCC_net, -- tied to '1' from definition
        SDIF0_SPLL_LOCK                => VCC_net, -- tied to '1' from definition
        SDIF1_SPLL_LOCK                => VCC_net, -- tied to '1' from definition
        SDIF2_SPLL_LOCK                => VCC_net, -- tied to '1' from definition
        SDIF3_SPLL_LOCK                => VCC_net, -- tied to '1' from definition
        CONFIG1_DONE                   => VCC_net,
        CONFIG2_DONE                   => VCC_net,
        SDIF0_PERST_N                  => VCC_net, -- tied to '1' from definition
        SDIF1_PERST_N                  => VCC_net, -- tied to '1' from definition
        SDIF2_PERST_N                  => VCC_net, -- tied to '1' from definition
        SDIF3_PERST_N                  => VCC_net, -- tied to '1' from definition
        SDIF0_PSEL                     => GND_net, -- tied to '0' from definition
        SDIF0_PWRITE                   => VCC_net, -- tied to '1' from definition
        SDIF0_PRDATA                   => SDIF0_PRDATA_const_net_0, -- tied to X"0" from definition
        SDIF1_PSEL                     => GND_net, -- tied to '0' from definition
        SDIF1_PWRITE                   => VCC_net, -- tied to '1' from definition
        SDIF1_PRDATA                   => SDIF1_PRDATA_const_net_0, -- tied to X"0" from definition
        SDIF2_PSEL                     => GND_net, -- tied to '0' from definition
        SDIF2_PWRITE                   => VCC_net, -- tied to '1' from definition
        SDIF2_PRDATA                   => SDIF2_PRDATA_const_net_0, -- tied to X"0" from definition
        SDIF3_PSEL                     => GND_net, -- tied to '0' from definition
        SDIF3_PWRITE                   => VCC_net, -- tied to '1' from definition
        SDIF3_PRDATA                   => SDIF3_PRDATA_const_net_0, -- tied to X"0" from definition
        SOFT_EXT_RESET_OUT             => GND_net, -- tied to '0' from definition
        SOFT_RESET_F2M                 => GND_net, -- tied to '0' from definition
        SOFT_M3_RESET                  => GND_net, -- tied to '0' from definition
        SOFT_MDDR_DDR_AXI_S_CORE_RESET => GND_net, -- tied to '0' from definition
        SOFT_FDDR_CORE_RESET           => GND_net, -- tied to '0' from definition
        SOFT_SDIF0_PHY_RESET           => GND_net, -- tied to '0' from definition
        SOFT_SDIF0_CORE_RESET          => GND_net, -- tied to '0' from definition
        SOFT_SDIF0_0_CORE_RESET        => GND_net, -- tied to '0' from definition
        SOFT_SDIF0_1_CORE_RESET        => GND_net, -- tied to '0' from definition
        SOFT_SDIF1_PHY_RESET           => GND_net, -- tied to '0' from definition
        SOFT_SDIF1_CORE_RESET          => GND_net, -- tied to '0' from definition
        SOFT_SDIF2_PHY_RESET           => GND_net, -- tied to '0' from definition
        SOFT_SDIF2_CORE_RESET          => GND_net, -- tied to '0' from definition
        SOFT_SDIF3_PHY_RESET           => GND_net, -- tied to '0' from definition
        SOFT_SDIF3_CORE_RESET          => GND_net, -- tied to '0' from definition
        -- Outputs
        MSS_HPMS_READY                 => MSS_READY_net_0,
        DDR_READY                      => OPEN,
        SDIF_READY                     => OPEN,
        RESET_N_F2M                    => CORERESETP_0_RESET_N_F2M,
        M3_RESET_N                     => OPEN,
        EXT_RESET_OUT                  => OPEN,
        MDDR_DDR_AXI_S_CORE_RESET_N    => OPEN,
        FDDR_CORE_RESET_N              => OPEN,
        SDIF0_CORE_RESET_N             => OPEN,
        SDIF0_0_CORE_RESET_N           => OPEN,
        SDIF0_1_CORE_RESET_N           => OPEN,
        SDIF0_PHY_RESET_N              => OPEN,
        SDIF1_CORE_RESET_N             => OPEN,
        SDIF1_PHY_RESET_N              => OPEN,
        SDIF2_CORE_RESET_N             => OPEN,
        SDIF2_PHY_RESET_N              => OPEN,
        SDIF3_CORE_RESET_N             => OPEN,
        SDIF3_PHY_RESET_N              => OPEN,
        SDIF_RELEASED                  => OPEN,
        INIT_DONE                      => INIT_DONE_net_0 
        );
-- FABOSC_0   -   Actel:SgCore:OSC:2.0.101
FABOSC_0 : migou_sf2_25_v1_sb_FABOSC_0_OSC
    port map( 
        -- Inputs
        XTL                => GND_net, -- tied to '0' from definition
        -- Outputs
        RCOSC_25_50MHZ_CCC => FABOSC_0_RCOSC_25_50MHZ_CCC_OUT_RCOSC_25_50MHZ_CCC,
        RCOSC_25_50MHZ_O2F => FABOSC_0_RCOSC_25_50MHZ_O2F,
        RCOSC_1MHZ_CCC     => OPEN,
        RCOSC_1MHZ_O2F     => OPEN,
        XTLOSC_CCC         => OPEN,
        XTLOSC_O2F         => OPEN 
        );
-- migou_sf2_25_v1_sb_MSS_0
migou_sf2_25_v1_sb_MSS_0 : migou_sf2_25_v1_sb_MSS
    port map( 
        -- Inputs
        SPI_0_DI               => SPI_0_DI,
        SPI_1_DI               => SPI_1_DI,
        MCCC_CLK_BASE          => FIC_0_CLK_net_0,
        MMUART_1_RXD           => MMUART_1_RXD,
        MMUART_0_RXD           => MMUART_0_RXD,
        MCCC_CLK_BASE_PLL_LOCK => FIC_0_LOCK_net_0,
        MSS_RESET_N_F2M        => CORERESETP_0_RESET_N_F2M,
        FIC_0_APB_M_PREADY     => FIC_0_APB_M_PREADY,
        FIC_0_APB_M_PSLVERR    => FIC_0_APB_M_PSLVERR,
        FIC_2_APB_M_PREADY     => VCC_net, -- tied to '1' from definition
        FIC_2_APB_M_PSLVERR    => GND_net, -- tied to '0' from definition
        FIC_0_APB_M_PRDATA     => FIC_0_APB_M_PRDATA,
        FIC_2_APB_M_PRDATA     => FIC_2_APB_M_PRDATA_const_net_0, -- tied to X"0" from definition
        MSS_INT_F2M            => MSS_INT_F2M,
        -- Outputs
        SPI_0_DO               => SPI_0_DO_net_0,
        SPI_1_DO               => SPI_1_DO_net_0,
        MMUART_1_TXD           => MMUART_1_TXD_net_0,
        MMUART_0_TXD           => MMUART_0_TXD_net_0,
        MSS_RESET_N_M2F        => migou_sf2_25_v1_sb_MSS_TMP_0_MSS_RESET_N_M2F,
        SPI_0_SS1              => SPI_0_SS1_net_0,
        SPI_0_SS2              => SPI_0_SS2_net_0,
        FIC_0_APB_M_PSEL       => FIC_0_APB_MASTER_2_PSELx,
        FIC_0_APB_M_PWRITE     => FIC_0_APB_MASTER_2_PWRITE,
        FIC_0_APB_M_PENABLE    => FIC_0_APB_MASTER_2_PENABLE,
        FIC_2_APB_M_PRESET_N   => migou_sf2_25_v1_sb_MSS_TMP_0_FIC_2_APB_M_PRESET_N,
        FIC_2_APB_M_PCLK       => OPEN,
        FIC_2_APB_M_PWRITE     => OPEN,
        FIC_2_APB_M_PENABLE    => OPEN,
        FIC_2_APB_M_PSEL       => OPEN,
        FIC_0_APB_M_PADDR      => FIC_0_APB_MASTER_2_PADDR,
        FIC_0_APB_M_PWDATA     => FIC_0_APB_MASTER_2_PWDATA,
        FIC_2_APB_M_PADDR      => OPEN,
        FIC_2_APB_M_PWDATA     => OPEN,
        -- Inouts
        I2C_0_SDA              => I2C_0_SDA,
        I2C_0_SCL              => I2C_0_SCL,
        I2C_1_SDA              => I2C_1_SDA,
        I2C_1_SCL              => I2C_1_SCL,
        SPI_0_CLK              => SPI_0_CLK,
        SPI_0_SS0              => SPI_0_SS0,
        SPI_1_CLK              => SPI_1_CLK,
        SPI_1_SS0              => SPI_1_SS0 
        );
-- SYSRESET_POR
SYSRESET_POR : SYSRESET
    port map( 
        -- Inputs
        DEVRST_N         => DEVRST_N,
        -- Outputs
        POWER_ON_RESET_N => POWER_ON_RESET_N_net_0 
        );

end RTL;
