set_component migou_sf2_25_v1_sb_FABOSC_0_OSC
# Microsemi Corp.
# Date: 2018-Dec-04 14:37:54
#

create_clock -ignore_errors -period 20 [ get_pins { I_RCOSC_25_50MHZ/CLKOUT } ]
