#ifndef migou_sf2_25_v1_HW_PLATFORM_H_
#define migou_sf2_25_v1_HW_PLATFORM_H_
/*****************************************************************************
*
*Created by Microsemi SmartDesign  Wed Dec 05 12:48:56 2018
*
*Memory map specification for peripherals in migou_sf2_25_v1
*/

/*-----------------------------------------------------------------------------
* CM3 subsystem memory map
* Master(s) for this subsystem: CM3 
*---------------------------------------------------------------------------*/
#define COREGPIO_0                      0x50000000U


#endif /* migou_sf2_25_v1_HW_PLATFORM_H_*/
