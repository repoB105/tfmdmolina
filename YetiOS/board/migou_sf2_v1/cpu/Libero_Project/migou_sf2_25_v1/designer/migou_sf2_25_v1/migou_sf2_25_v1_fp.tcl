new_project \
         -name {migou_sf2_25_v1} \
         -location {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_25_v1\designer\migou_sf2_25_v1\migou_sf2_25_v1_fp} \
         -mode {chain} \
         -connect_programmers {FALSE}
add_actel_device \
         -device {M2S025} \
         -name {M2S025}
enable_device \
         -name {M2S025} \
         -enable {TRUE}
save_project
close_project
