set_device \
    -family  SmartFusion2 \
    -die     PA4M2500_N \
    -package fg484 \
    -speed   STD \
    -tempr   {COM} \
    -voltr   {COM}
set_def {VOLTAGE} {1.2}
set_def {VCCI_1.2_VOLTR} {COM}
set_def {VCCI_1.5_VOLTR} {COM}
set_def {VCCI_1.8_VOLTR} {COM}
set_def {VCCI_2.5_VOLTR} {COM}
set_def {VCCI_3.3_VOLTR} {COM}
set_def {RTG4_MITIGATION_ON} {0}
set_def USE_CONSTRAINTS_FLOW 1
set_def NETLIST_TYPE EDIF
set_name migou_sf2_25_v1
set_workdir {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_25_v1\designer\migou_sf2_25_v1}
set_log     {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_25_v1\designer\migou_sf2_25_v1\migou_sf2_25_v1_coverage_pr.log}
set_design_state pre_layout
