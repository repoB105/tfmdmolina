open_project -project {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_25_v1\designer\migou_sf2_25_v1\migou_sf2_25_v1_fp\migou_sf2_25_v1.pro}\
         -connect_programmers {FALSE}
if { [catch {load_programming_data \
    -name {M2S025} \
    -fpga {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_25_v1\designer\migou_sf2_25_v1\migou_sf2_25_v1.map} \
    -header {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_25_v1\designer\migou_sf2_25_v1\migou_sf2_25_v1.hdr} \
    -spm {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_25_v1\designer\migou_sf2_25_v1\migou_sf2_25_v1.spm} \
    -dca {D:\Repositorio\yetios\board\migou_sf2_v1\cpu\Libero_Project\migou_sf2_25_v1\designer\migou_sf2_25_v1\migou_sf2_25_v1.dca} \
    -gen_bitstream } return_val] } {
    save_project
    close_project
    exit }
set_programming_file -name {M2S025} -no_file
save_project
close_project
