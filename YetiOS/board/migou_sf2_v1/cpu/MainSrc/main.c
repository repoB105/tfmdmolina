//------------------------------------------------------------------------------
// Company: B105 - Electronic Systems Laboratory
//          Escuela T�cnica Superior de Ingenieros de Telecomunicaci�n (ETSIT)
//          Universidad Polit�cnica de Madrid (UPM)
//
// File: main.c
// File history:
//      v0.0: 16/09/2015: Creation
//
// Description:
//
// MSS application program to test the AT86RF215 transceiver.
//
// Tool version: SoftConsole IDE v3.4
// Targeted device: <Family::SmartFusion2> <Die::M2S050> <Package::484 FBGA>
// Author: Ramiro Utrilla <rutrilla@die.upm.es>
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include "gpio.h"
#include "cmsis_os.h"
#include "mss_watchdog.h"
#include "system_m2sxxx.h"
#include "clks_arch.h"


/* Static global variables */


/* Function declarations */
void MX_FREERTOS_Init(void);
static void nvic_freertos_init(void);
static void hal_config_clks(void);

/* Main function */
int main()
{
	/* NVIC FreeRTOS Initialization */
	nvic_freertos_init();

	/* System Initialization */
	SystemInit();
	SystemCoreClockUpdate();

	/* Initialization all necessary hardware components */
	MSS_WD_init();

	hal_gpio_init();
	hal_config_clks();

	/* Program Start */
	/* Call init function for freertos objects (in freertos.c) */
	MX_FREERTOS_Init();

	/* Start scheduler */
	osKernelStart();

	/*This should never be reached*/
	while(1);
}


void HardFault_Handler(void){
	while(1){}
}


/**************************************************************************//***
   configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY is the highest interrupt
   priority that can be used by any interrupt service routine that makes calls
   to interrupt safe FreeRTOS API functions. DO NOT CALL INTERRUPT SAFE FREERTOS
   API FUNCTIONS FROM ANY INTERRUPT THAT HAS A HIGHER PRIORITY THAN THIS!
   (Higher priorities are lower numeric values)
 */
static void nvic_freertos_init(void){

	uint8_t i;

	for(i = 0; i <= 81; i++){
		NVIC_SetPriority((IRQn_Type)i, (uint32_t)configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY);
	}
}


/**************************************************************************//***
   This function reconfigures the MSS DDR PLL and the MSS DDR Fabric Alignment
   Clock Controller (FACC). The purpose is to power down the MPLL, since it is
   not required in MIGOU (MDDR interface is not used) and this action allows to
   reduce the power consumption. Before power it down, it is necessary to
   reconfigure the clock sources to use an alternative one and avoid that the
   system hangs when the MPLL is power down.
 */
static void hal_config_clks(void){

	/*
	 * After flashing the SmartFusion2 with Libero SoC, the default
	 * configuration of the affected registers is as follows:
	 * 		SYSREG->MSSDDR_PLL_STATUS_LOW_CR 	= 0x024C0000;
	 * 		SYSREG->MSSDDR_PLL_STATUS_HIGH_CR 	= 0x00000006;
	 * 		SYSREG->MSSDDR_FACC1_CR 			= 0x0A000000;
	 * 		SYSREG->MSSDDR_FACC2_CR 			= 0x00001E18;
	 */


	/* SYSREG->MSSDDR_FACC1_CR		=	0x0A000000;
	 *
	 *		- [1:0]		DIVISOR_A 		(Ratio CLK_SRC:CLK_A)			1:1
	 *		- [4:2]		APB0_DIVISOR	(Ratio CLK_A:APB_0_CLK)			1:1
	 *		- [7:5]		APB1_DIVISOR	(Ratio CLK_A:APB_1_CLK)			1:1
	 *		- [8]		DDR_CLK_EN		(MDDR_CLK enable bit)			MDDR_CLK is gated off
	 *		- [11:9]	M3_CLK_DIVISOR	(Ratio CLK_A:M3_CLK)			1:1
	 *		- [12]		FACC_GLMUX_SEL	(Clock source of Output NGMUXs)	Stage B dividers
	 *		- [15:13]	FIC_0_DIVISOR	(Ratio CLK_A:FIC_0 clock)		1:1
	 *		- [18:16]	FIC_1_DIVISOR	(Ratio CLK_A:FIC_1 clock)		1:1
	 *		- [21:19]	DDR_FIC_DIVISOR	(Ratio CLK_A:DDR_SMC_FIC_CLK)	1:1
	 *		- [24:22]	BASE_DIVISOR	(Ratio CLK_A:CLK_BASE_REGEN)	1:1
	 *		- [25]		PERSIST_CC		(MSS Reset Controller)			Must always be 1.
	 *		- [26]		CONTROLLER_PLL_INIT:	FACC MUX select lines or clock gate control line comes from the normal run-time configuration signals
	 *		- [27]		FACC_FAB_REF_SEL (Source of MPLL_REF_CLK)		MCCC_CLK_BASE (Fabric clock)
	 *		- [31:28]	Reserved
	 *
	 *	No need to change this configuration.
	 *
	 */


	/* SYSREG->MSSDDR_FACC2_CR		=	0x00001E18;
	 *
	 *		- [1:0]		RTC_CLK_SEL			(Source of RTC_CLK)			XTLOSC_CLK
	 *		- [4:2]		FACC_SRC_SEL		(Source of CLK_SRC)			MPLL_OUT_CLK
	 *		- [5]		FACC_PRE_SRC_SEL:	RCOSC_1MHZ is fed through to the source no-glitch clock multiplexer (must always be 0).
	 *		- [8:6]		FACC_STANDBY_SEL	(Source of CLK_STANDBY)		RCOSC_25_50MHZ
	 *		- [9]		MSS_25_50MHZ_EN		(0-Disable / 1-Enable)		Enable
	 *		- [10]		MSS_1MHZ_EN			(0-Disable / 1-Enable)		Enable
	 *		- [11]		MSS_CLK_ENVM_EN		(0-Disable / 1-Enable)		Enable
	 *		- [12]		MSS_XTAL_EN			(0-Disable / 1-Enable)		Enable 	<- MAIN_XTAL
	 *		- [13]		MSS_XTAL_RTC_EN		(0-Disable / 1-Enable)		Disable <-  AUX_XTAL
	 *		- [31:14]	Reserved
	 *
	 *	1.- FACC_SRC_SEL has to be changed to RCOSC_25_50MHZ to avoid that the
	 *	    system hangs when the MPLL is power down and to avoid a frequency
	 *	    change between the output of stage B dividers and the CLK_STANDBY
	 *	    when entering Flash*Freeze mode (FACC_GLMUX_SEL changes from 0 to 1)
	 *
	 *	    	NOTE: The use of the MPLL is mandatory to achieve the complete
	 *	    		  alignment of the MSS and FPGA clocks and thus achieve a
	 *	    		  safe communication between them.
	 *
	 *	2.- MSS_1MHZ_EN can be disabled since it is not being used at all.
	 *
	 *	New value of SYSREG->MSSDDR_FACC2_CR = 0x00001A08; (Discarded. See the previous Note)
	 *	New value of SYSREG->MSSDDR_FACC2_CR = 0x00001A18;
	 *
	 */
	// Set the source of CLK_SRC to RCOSC_25_50MHZ
//	SYSREG->MSSDDR_FACC2_CR = (SYSREG->MSSDDR_FACC2_CR & CONFIG_CLK_SRC_MASK) | CONFIG_CLK_SRC_RCOSC_25_50MHZ;
	// Disable the RCOSC_1MHZ
	SYSREG->MSSDDR_FACC2_CR = (SYSREG->MSSDDR_FACC2_CR & CONFIG_MSS_1MHZ_EN_MASK) | CONFIG_MSS_1MHZ_DISABLE;


	/* SYSREG->MSSDDR_PLL_STATUS_LOW_CR		=	0x024C0000;
	 *
	 *		- [5:0]		FACC_PLL_DIVR		(MPLL reference divider)			FACC_PLL_DIVR + 1 = 1
	 *		- [15:6]	FACC_PLL_DIVF		(MPLL feedback divider)				FACC_PLL_DIVF + 1 = 1
	 *		- [18:16]	FACC_PLL_DIVQ		(MPLL output divider)				Divided by 16
	 *		- [22:19]	FACC_PLL_RANGE		(MPLL filter range)					46 - 75 MHz
	 *		- [25:23]	FACC_PLL_LOCKWIN	(MPLL phase error window for LOCK)	8000ppm
	 *		- [29:26]	FACC_PLL_LOCKCNT	(MPLL LOCK counter)					2^(FACC_PLL_LOCKCNT + 5) = 32
	 *		- [31:30]	Reserved
	 *
	 *	No need to change this configuration since the MPLL is going to be powered off.
	 *
	 */


	/* SYSREG->MSSDDR_PLL_STATUS_HIGH_CR	=	0x00000006;
	 *
	 *		- [0]		FACC_PLL_BYPASS		(MPLL CORE powers down and I/O bypassed)	MPLL Active
	 *		- [1]		FACC_PLL_MODE_1V2	(MPLL core voltage)							1.2 V
	 *		- [2]		FACC_PLL_MODE_3V3	(MPLL analog voltage)						3.3 V
	 *		- [3]		FACC_PLL_FSE		(MPLL internal and external feedback paths)	Must always be 1.
	 *		- [4]		FACC_PLL_PD			(MPLL powers down and outputs will be LOW)	MPLL Active
	 *		- [5]		FACC_PLL_SSE		(Drives the SSE input of MPLL)				Must always be 0 (not supported).
	 *		- [7:6]		FACC_PLL_SSMD		(Spread Spectrum Modulation Depth)			Must always be 0 (not supported).
	 *		- [12:8]	FACC_PLL_SSMF		(Spread Spectrum Modulation Frequency)		Must always be 0 (not supported).
	 *		- [31:13]	Reserved
	 *
	 *	1.- FACC_PLL_BYPASS value has to be changed to power down the MPLL CORE.
	 *
	 *	2.- FACC_PLL_PD value has to be changed to power down the MPLL.
	 *
	 *	    	NOTE: The use of the MPLL is mandatory to achieve the complete
	 *	    		  alignment of the MSS and FPGA clocks and thus achieve a
	 *	    		  safe communication between them.
	 *
	 *	New value of SYSREG->MSSDDR_PLL_STATUS_HIGH_CR = 0x00000017; (Discarded. See the previous Note)
	 *	New value of SYSREG->MSSDDR_PLL_STATUS_HIGH_CR = 0x00000006;
	 *
	 */
	// Power down the MPLL CORE and the MPLL
//	SYSREG->MSSDDR_PLL_STATUS_HIGH_CR = (SYSREG->MSSDDR_PLL_STATUS_HIGH_CR & CONFIG_MPLL_STATE_MASK) | \
			(CONFIG_MPLL_CORE_STATE_OFF | CONFIG_MPLL_STATE_OFF);
	// Power down only the MPLL CORE
//	SYSREG->MSSDDR_PLL_STATUS_HIGH_CR = (SYSREG->MSSDDR_PLL_STATUS_HIGH_CR & CONFIG_MPLL_STATE_MASK) | \
			CONFIG_MPLL_CORE_STATE_OFF;
}
