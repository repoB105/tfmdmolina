/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * gpio.c
 *
 *  Created on: 13 abr. 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file gpio.c
 */


#include "gpio.h"
#include "platform-conf.h"
#include "migou_sf2_50_v1_hw_platform.h"
#include "hal.h"
#include "m2sxxx.h"

#define ENABLED_PINS	15

void hal_gpio_init(void){

	// GPIOs from CoreGPIO -----------------------------------------------------
	NVIC_DisableIRQ(FabricIrq0_IRQn);

	GPIO_init(&hgpio, COREGPIO_0, GPIO_APB_32_BITS_BUS);

	GPIO_config(&hgpio, GPIO_0, GPIO_INOUT_MODE);
	GPIO_config(&hgpio, GPIO_1, GPIO_INOUT_MODE);
	GPIO_config(&hgpio, GPIO_2, GPIO_INOUT_MODE);
	GPIO_config(&hgpio, GPIO_3, GPIO_INOUT_MODE);
	GPIO_config(&hgpio, GPIO_4, GPIO_INOUT_MODE);
	GPIO_config(&hgpio, GPIO_5, GPIO_INOUT_MODE);
	GPIO_config(&hgpio, GPIO_6, GPIO_INOUT_MODE);
	GPIO_config(&hgpio, GPIO_7, GPIO_INOUT_MODE);
	GPIO_config(&hgpio, GPIO_8, GPIO_INOUT_MODE);
	GPIO_config(&hgpio, GPIO_9, GPIO_INOUT_MODE);
	GPIO_config(&hgpio, GPIO_10, GPIO_INOUT_MODE);
	GPIO_config(&hgpio, GPIO_11, GPIO_INOUT_MODE);
	GPIO_config(&hgpio, GPIO_12, GPIO_INOUT_MODE);
	GPIO_config(&hgpio, GPIO_13, GPIO_INOUT_MODE);
	GPIO_config(&hgpio, GPIO_14, GPIO_INOUT_MODE);

	GPIO_disable_irq(&hgpio, GPIO_0);
	GPIO_disable_irq(&hgpio, GPIO_1);
	GPIO_disable_irq(&hgpio, GPIO_2);
	GPIO_disable_irq(&hgpio, GPIO_3);
	GPIO_disable_irq(&hgpio, GPIO_4);
	GPIO_disable_irq(&hgpio, GPIO_5);
	GPIO_disable_irq(&hgpio, GPIO_6);
	GPIO_disable_irq(&hgpio, GPIO_7);
	GPIO_disable_irq(&hgpio, GPIO_8);
	GPIO_disable_irq(&hgpio, GPIO_9);
	GPIO_disable_irq(&hgpio, GPIO_10);
	GPIO_disable_irq(&hgpio, GPIO_11);
	GPIO_disable_irq(&hgpio, GPIO_12);
	GPIO_disable_irq(&hgpio, GPIO_13);
	GPIO_disable_irq(&hgpio, GPIO_14);

	NVIC_SetPriority(FabricIrq0_IRQn, CORE_GPIO_IRQN_PRIORITY);
	NVIC_EnableIRQ(FabricIrq0_IRQn);
	// -------------------------------------------------------------------------


	// GPIOs from MSS ----------------------------------------------------------
	NVIC_DisableIRQ(GPIO8_IRQn);
	NVIC_DisableIRQ(GPIO9_IRQn);
	NVIC_DisableIRQ(GPIO23_IRQn);
	NVIC_DisableIRQ(GPIO24_IRQn);
	NVIC_DisableIRQ(GPIO29_IRQn);

	MSS_GPIO_init();

	MSS_GPIO_config(MSS_GPIO_8, MSS_GPIO_INOUT_MODE);
//	MSS_GPIO_config(MSS_GPIO_9, MSS_GPIO_INOUT_MODE);	// This pin is reserved for Flash*Freeze Wake Up
	MSS_GPIO_config(MSS_GPIO_23, MSS_GPIO_INOUT_MODE);
	MSS_GPIO_config(MSS_GPIO_24, MSS_GPIO_INOUT_MODE);
	MSS_GPIO_config(MSS_GPIO_29, MSS_GPIO_INOUT_MODE);

	MSS_GPIO_disable_irq(MSS_GPIO_8);
	MSS_GPIO_disable_irq(MSS_GPIO_9);
	MSS_GPIO_disable_irq(MSS_GPIO_23);
	MSS_GPIO_disable_irq(MSS_GPIO_24);
	MSS_GPIO_disable_irq(MSS_GPIO_29);

	//Enable NVIC interrupts
	NVIC_SetPriority(GPIO8_IRQn, MSS_GPIO_IRQN_PRIORITY);
	NVIC_EnableIRQ(GPIO8_IRQn);
	NVIC_SetPriority(GPIO23_IRQn, MSS_GPIO_IRQN_PRIORITY);
	NVIC_EnableIRQ(GPIO23_IRQn);
	NVIC_SetPriority(GPIO24_IRQn, MSS_GPIO_IRQN_PRIORITY);
	NVIC_EnableIRQ(GPIO24_IRQn);
	NVIC_SetPriority(GPIO29_IRQn, MSS_GPIO_IRQN_PRIORITY);
	NVIC_EnableIRQ(GPIO29_IRQn);
	// -------------------------------------------------------------------------
}



void FabricIrq0_IRQHandler(void){

	uint32_t i;
	uint32_t irq_sources = GPIO_get_irq_sources(&hgpio);

	for(i=0; i<ENABLED_PINS; i++){
		if(irq_sources & 0x00000001){
			GPIO_Pin_Callback(i);
			GPIO_clear_irq(&hgpio,(gpio_id_t) i);
		}
		irq_sources = irq_sources >> 1;
	}
	NVIC_ClearPendingIRQ( FabricIrq0_IRQn );
}

void GPIO8_IRQHandler(void){
	GPIO_Pin_Callback((MSS_GPIO_8 | MSS_GPIO_MASK));
	MSS_GPIO_clear_irq(MSS_GPIO_8);
	NVIC_ClearPendingIRQ(GPIO8_IRQn);
}
void GPIO23_IRQHandler(void){
	GPIO_Pin_Callback((MSS_GPIO_23 | MSS_GPIO_MASK));
	MSS_GPIO_clear_irq(MSS_GPIO_23);
	NVIC_ClearPendingIRQ(GPIO23_IRQn);
}
void GPIO24_IRQHandler(void){
	GPIO_Pin_Callback((MSS_GPIO_24 | MSS_GPIO_MASK));
	MSS_GPIO_clear_irq(MSS_GPIO_24);
	NVIC_ClearPendingIRQ(GPIO24_IRQn);
}
void GPIO29_IRQHandler(void){
	GPIO_Pin_Callback((MSS_GPIO_29 | MSS_GPIO_MASK));
	MSS_GPIO_clear_irq(MSS_GPIO_29);
	NVIC_ClearPendingIRQ(GPIO29_IRQn);
}

void GPIO9_IRQHandler(void){	// This interrupt should never be entered. Flash*Freeze wake up pin.
	while(1);
}

__attribute__((weak)) void GPIO_Pin_Callback(uint32_t GPIO_Pin){

}
