/*
 * Copyright (c) 2019, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * flash_freeze_arch.h
 *
 *  Created on: 22/02/2019
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file flash_freeze_arch.h
 */

#ifndef YETIOS_BOARD_PLATFORM_INCLUDE_FLASH_FREEZE_ARCH_H_
#define YETIOS_BOARD_PLATFORM_INCLUDE_FLASH_FREEZE_ARCH_H_

/* Flash*Freeze status */
typedef enum flash_freeze_satus_{
    FF_INACTIVE   	= 0,
	FF_ACTIVE		= 1
} flash_freeze_satus_t;

typedef retval_t (*flash_freeze_cb_t)(void);

void flash_freeze_init(void);
void flash_freeze_deinit(void);
retval_t enter_flash_freeze(void);
retval_t exit_flash_freeze(void);
retval_t set_enter_flash_freeze_cb(flash_freeze_cb_t cb_func);
retval_t set_exit_flash_freeze_cb(flash_freeze_cb_t cb_func);

flash_freeze_satus_t flash_freeze_status(void);	// This function is used by peripherals that can not be used during Flash*Freeze mode.


#endif /* YETIOS_BOARD_PLATFORM_INCLUDE_FLASH_FREEZE_ARCH_H_ */
