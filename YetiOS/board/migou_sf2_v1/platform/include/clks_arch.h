/*
 * Copyright (c) 2019, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * clks_arch.h
 *
 *  Created on: 22/02/2019
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file clks_arch.h
 */

#ifndef YETIOS_BOARD_PLATFORM_INCLUDE_CLKS_ARCH_H_
#define YETIOS_BOARD_PLATFORM_INCLUDE_CLKS_ARCH_H_

#define CONFIG_MPLL_STATE_MASK			0xFFFFFFEEu
#define CONFIG_MPLL_STATE_ON			0x00000000u
#define CONFIG_MPLL_STATE_OFF			0x00000010u
#define CONFIG_MPLL_CORE_STATE_OFF		0x00000001u

#define CONFIG_CLK_SRC_MASK				0xFFFFFFECu
#define CONFIG_CLK_SRC_MPLL				0x00000018u
#define CONFIG_CLK_SRC_RCOSC_25_50MHZ	0x00000008u
#define CONFIG_MSS_1MHZ_EN_MASK			0xFFFFFBFFu
#define CONFIG_MSS_1MHZ_ENABLE			0x00000400u
#define CONFIG_MSS_1MHZ_DISABLE			0x00000000u

#define CONFIG_MSS_CLK_ENVM_EN_MASK		0xFFFFF7FFu
#define	CONFIG_MSS_CLK_ENVM_ENABLE		0x00008000u
#define	CONFIG_MSS_CLK_ENVM_DISABLE		0x00000000u

#endif /* YETIOS_BOARD_PLATFORM_INCLUDE_CLKS_ARCH_H_ */
