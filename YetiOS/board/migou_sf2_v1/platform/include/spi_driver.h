/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spi_driver.h
 *
 *  Created on: 04/06/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file spi_driver.h
 */

#ifndef YETIOS_BOARD_PLATFORM_INCLUDE_SPI_DRIVER_H_
#define YETIOS_BOARD_PLATFORM_INCLUDE_SPI_DRIVER_H_


/* Migou SPI operation modes */
typedef enum migou_spi_mode_{
    MASTER          = 0,
	SLAVE			= 1
} migou_spi_mode_t;


/* Migou available SPI HW chip selects */
 typedef enum migou_spi_hw_cs_{
    SSN0            = 0,
	SSN1			= 1,
	SSN2			= 2,
	TRANSCEIVER		= 3
} migou_spi_hw_cs_t;


/* SPI Configuration struct */
typedef struct spi_config_{
	migou_spi_mode_t mode;
	migou_spi_hw_cs_t hw_cs;
	uint32_t speed;
	uint8_t cpol;
	uint8_t cpha;
	uint8_t pdma_write_adj;
}spi_config_t;


/* Device driver IOCTL commands */
typedef enum spi_driver_ioctl_cmd_{
	CONFIG_SPI = 0,
	SPI_READ_WRITE_OP = 1,
	SPI_NO_CMD = 0xFFFF
}spi_driver_ioctl_cmd_t;


/* Type used for spi_read_write function */
typedef struct spi_driver_read_write_buffs_{
	uint8_t* ptx;
	uint8_t* prx;
	size_t size;
}spi_read_write_buffs_t;

#endif /* YETIOS_BOARD_PLATFORM_INCLUDE_SPI_DRIVER_H_ */
