/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * uart_driver.h
 *
 *  Created on: 30/10/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file uart_driver.h
 */

#ifndef YETIOS_BOARD_PLATFORM_INCLUDE_UART_DRIVER_H_
#define YETIOS_BOARD_PLATFORM_INCLUDE_UART_DRIVER_H_

#include "mss_uart.h"

/* Device driver IOCTL commands */
typedef enum uart_driver_ioctl_cmd_{
	SET_UART_SPEED = 0,
	SET_UART_PARITY = 1,
	SET_UART_DATA_BITS_LENGTH = 2,
	SET_UART_NUMBER_STOP_BITS = 3,

	UART_ENABLE_ECHO = 4,
	UART_DISABLE_ECHO = 5,

	UART_READ_LINE = 6,
	UART_NO_CMD = 0xFFFF,
}uart_driver_ioctl_cmd_t;

typedef struct uart_read_line_params_{
	uint8_t* buff;
	uint16_t size;
}uart_read_line_params_t;


/* Migou UART speed options */
#define UART_110_BAUD			MSS_UART_110_BAUD
#define UART_300_BAUD			MSS_UART_300_BAUD
#define UART_1200_BAUD			MSS_UART_1200_BAUD
#define UART_2400_BAUD			MSS_UART_2400_BAUD
#define UART_4800_BAUD			MSS_UART_4800_BAUD
#define UART_9600_BAUD			MSS_UART_9600_BAUD
#define UART_19200_BAUD			MSS_UART_19200_BAUD
#define UART_38400_BAUD			MSS_UART_38400_BAUD
#define UART_57600_BAUD			MSS_UART_57600_BAUD
#define UART_115200_BAUD		MSS_UART_115200_BAUD
#define UART_230400_BAUD		MSS_UART_230400_BAUD
#define UART_460800_BAUD		MSS_UART_460800_BAUD
#define UART_921600_BAUD		MSS_UART_921600_BAUD

/* Migou UART parity options */
#define UART_NO_PARITY			MSS_UART_NO_PARITY
#define UART_ODD_PARITY			MSS_UART_ODD_PARITY
#define UART_EVEN_PARITY		MSS_UART_EVEN_PARITY
#define UART_STICK_PARITY_0		MSS_UART_STICK_PARITY_0
#define UART_STICK_PARITY_1		MSS_UART_STICK_PARITY_1

/* Migou UART Data Bits Lengths options */
#define UART_DATA_5_BITS		MSS_UART_DATA_5_BITS
#define UART_DATA_6_BITS		MSS_UART_DATA_6_BITS
#define UART_DATA_7_BITS		MSS_UART_DATA_7_BITS
#define UART_DATA_8_BITS		MSS_UART_DATA_8_BITS

// Migou UART Number of Stop Bits options */
#define UART_ONE_STOP_BIT		MSS_UART_ONE_STOP_BIT
#define UART_ONEHALF_STOP_BIT	MSS_UART_ONEHALF_STOP_BIT
#define UART_TWO_STOP_BITS		MSS_UART_TWO_STOP_BITS

#endif /* YETIOS_BOARD_PLATFORM_INCLUDE_UART_DRIVER_H_ */
