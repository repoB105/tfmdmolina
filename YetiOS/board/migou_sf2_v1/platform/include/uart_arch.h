/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * uart_arch.h
 *
 *  Created on: 30/10/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file uart_arch.h
 */

#ifndef YETIOS_BOARD_PLATFORM_INCLUDE_UART_ARCH_H_
#define YETIOS_BOARD_PLATFORM_INCLUDE_UART_ARCH_H_

#include "platform-conf.h"

retval_t uart_arch_init(uint8_t uart_hw_number);
retval_t uart_arch_deinit(uint8_t uart_hw_number);
retval_t uart_arch_start_storing_data(uint8_t uart_hw_number);
retval_t uart_arch_stop_storing_data(uint8_t uart_hw_number);
retval_t uart_arch_write(uint8_t uart_hw_number, uint8_t* txData, uint16_t size, uint32_t timeout);
retval_t uart_arch_read(uint8_t uart_hw_number, uint8_t* rxData, uint16_t size, uint32_t timeout);
retval_t uart_arch_read_line(uint8_t uart_hw_number, uint8_t* rxData, uint16_t size, uint32_t timeout);
retval_t uart_arch_set_speed(uint8_t uart_hw_number, uint32_t speed);
retval_t uart_arch_set_parity(uint8_t uart_hw_number, uint32_t parity);
retval_t uart_arch_set_data_bits_length(uint8_t uart_hw_number, uint32_t length);
retval_t uart_arch_set_number_stop_bits(uint8_t uart_hw_number, uint32_t nstopbits);
retval_t uart_arch_enable_echo(void);
retval_t uart_arch_disable_echo(void);

#endif /* YETIOS_BOARD_PLATFORM_INCLUDE_UART_ARCH_H_ */
