/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * pdma_arch.c
 *
 *  Created on: 08/06/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file pdma_arch.c
 */


#include "pdma_arch.h"
#include "platform-conf.h"

void pdma_init(pdma_priority_ratio_t priority_ratio){

	PDMA_init();

	/* Priority ratio of high to low priority DMA access opportunities */
	PDMA_set_priority_ratio(priority_ratio);

	/* Set priority of DMA interruptions */
	NVIC_DisableIRQ(DMA_IRQn);
	if(PDMA_IRQN_PRIORITY >= MAX_PRIORITY_INTERRUPT_SERVICE){
		NVIC_SetPriority(DMA_IRQn, PDMA_IRQN_PRIORITY);
	}
	else{
		NVIC_SetPriority(DMA_IRQn, MAX_PRIORITY_INTERRUPT_SERVICE);
	}
	NVIC_ClearPendingIRQ(DMA_IRQn);
}
