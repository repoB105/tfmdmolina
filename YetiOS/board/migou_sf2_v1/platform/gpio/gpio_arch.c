/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * gpio_arch.c
 *
 *  Created on: 20 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file gpio_arch.c
 */

#include "sys_gpio.h"
#include "gpio.h"
#include "flash_freeze_arch.h"

extern void sysGpioInterruptCallback(gpio_pin_t gpio_pin);


retval_t gpio_arch_init_pin(uint32_t gpio, gpio_mode_t gpio_mode, gpio_pull_t gpio_pull){

	// MSS GPIO pins
	if(gpio & MSS_GPIO_MASK){
		uint32_t gpio_mss_mode = 0;
		switch(gpio_mode){
			case GPIO_PIN_INPUT:
				gpio_mss_mode = MSS_GPIO_INPUT_MODE;
				break;
			case GPIO_PIN_OUTPUT_PUSH_PULL:
				gpio_mss_mode = MSS_GPIO_OUTPUT_MODE;
				break;
			case GPIO_PIN_OUTPUT_OPEN_DRAIN:
				return RET_ERROR;
				break;
			case GPIO_PIN_INTERRUPT_FALLING:
				gpio_mss_mode = MSS_GPIO_INPUT_MODE | MSS_GPIO_IRQ_EDGE_NEGATIVE;
				break;
			case GPIO_PIN_INTERRUPT_RISING:
				gpio_mss_mode = MSS_GPIO_INPUT_MODE | MSS_GPIO_IRQ_EDGE_POSITIVE;
				break;
			case GPIO_PIN_INTERRUPT_FALLING_RISING:
				gpio_mss_mode = MSS_GPIO_INPUT_MODE | MSS_GPIO_IRQ_EDGE_BOTH ;
				break;
			case GPIO_PIN_ANALOG:
				return RET_ERROR;
				break;
			case GPIO_PIN_ANALOG_ADC_CONTROL:
				return RET_ERROR;
				break;
			default:
				return RET_ERROR;
				break;
		}

		if(gpio_pull != GPIO_PIN_NO_PULL){
			return RET_ERROR;
		}

		MSS_GPIO_config( (mss_gpio_id_t) (gpio & (~MSS_GPIO_MASK)), gpio_mss_mode);

		if((gpio_mode == GPIO_PIN_INTERRUPT_FALLING) || (gpio_mode == GPIO_PIN_INTERRUPT_RISING) || (gpio_mode == GPIO_PIN_INTERRUPT_FALLING_RISING)){
			MSS_GPIO_clear_irq((mss_gpio_id_t) (gpio & (~MSS_GPIO_MASK)));
			MSS_GPIO_enable_irq((mss_gpio_id_t) (gpio & (~MSS_GPIO_MASK)));
		}
		else{
			MSS_GPIO_clear_irq((mss_gpio_id_t) (gpio & (~MSS_GPIO_MASK)));
			MSS_GPIO_disable_irq((mss_gpio_id_t) (gpio & (~MSS_GPIO_MASK)));
		}

		return RET_OK;
	}

	// CoreGPIO pins
	else{
		if(!flash_freeze_status()){		// CoreGPIO pins cannot be used when Flash*Freeze is active
			uint32_t gpio_core_mode = 0;

			switch(gpio_mode){
			case GPIO_PIN_INPUT:
				gpio_core_mode = GPIO_INPUT_MODE;
				break;
			case GPIO_PIN_OUTPUT_PUSH_PULL:
				gpio_core_mode = GPIO_OUTPUT_MODE;
				break;
			case GPIO_PIN_OUTPUT_OPEN_DRAIN:
				return RET_ERROR;
				break;
			case GPIO_PIN_INTERRUPT_FALLING:
				gpio_core_mode = GPIO_INPUT_MODE | GPIO_IRQ_EDGE_NEGATIVE;
				break;
			case GPIO_PIN_INTERRUPT_RISING:
				gpio_core_mode = GPIO_INPUT_MODE | GPIO_IRQ_EDGE_POSITIVE;
				break;
			case GPIO_PIN_INTERRUPT_FALLING_RISING:
				gpio_core_mode = GPIO_INPUT_MODE | GPIO_IRQ_EDGE_BOTH ;
				break;
			case GPIO_PIN_ANALOG:
				return RET_ERROR;
				break;
			case GPIO_PIN_ANALOG_ADC_CONTROL:
				return RET_ERROR;
				break;
			default:
				return RET_ERROR;
				break;
			}

			GPIO_config(&hgpio, (gpio_id_t)gpio, gpio_core_mode);

			if(gpio_pull != GPIO_PIN_NO_PULL){
				return RET_ERROR;
			}

			if((gpio_mode == GPIO_PIN_INTERRUPT_FALLING) || (gpio_mode == GPIO_PIN_INTERRUPT_RISING) || (gpio_mode == GPIO_PIN_INTERRUPT_FALLING_RISING)){
				GPIO_clear_irq(&hgpio, (gpio_id_t) gpio);
				GPIO_enable_irq(&hgpio, (gpio_id_t) gpio);
			}
			else{
				GPIO_clear_irq(&hgpio, (gpio_id_t) gpio);
				GPIO_disable_irq(&hgpio, (gpio_id_t) gpio);
			}

			return RET_OK;
		}
		return RET_ERROR;
	}
}

retval_t gpio_arch_set_pin(uint32_t gpio){
	// MSS GPIO pins
	if(gpio & MSS_GPIO_MASK){
		MSS_GPIO_set_output((mss_gpio_id_t) (gpio & (~MSS_GPIO_MASK)),1);
		return RET_OK;
	}

	// CoreGPIO pins
	else{
		if(!flash_freeze_status()){
			GPIO_set_output(&hgpio, (gpio_id_t) gpio, 1);
			return RET_OK;
		}
		return RET_ERROR;
	}
}

retval_t gpio_arch_reset_pin(uint32_t gpio){
	// MSS GPIO pins
	if(gpio & MSS_GPIO_MASK){
		MSS_GPIO_set_output((mss_gpio_id_t) (gpio & (~MSS_GPIO_MASK)),0);
		return RET_OK;
	}

	// CoreGPIO pins
	else{
		if(!flash_freeze_status()){
			GPIO_set_output(&hgpio, (gpio_id_t) gpio, 0);
			return RET_OK;
		}
		return RET_ERROR;
	}
}

retval_t gpio_arch_toggle_pin(uint32_t gpio){
	// MSS GPIO pins
	if(gpio & MSS_GPIO_MASK){
		uint32_t outputs = MSS_GPIO_get_outputs();

		outputs = (outputs ^ (0x0000001<<(gpio & (~MSS_GPIO_MASK))));
		MSS_GPIO_set_outputs(outputs);

		return RET_OK;
	}

	// CoreGPIO pins
	else{
		if(!flash_freeze_status()){
			uint32_t outputs = GPIO_get_outputs(&hgpio);

			outputs = (outputs ^ (0x0000001<<gpio));
			GPIO_set_outputs(&hgpio, outputs);

			return RET_OK;
		}
		return RET_ERROR;
	}
}

uint16_t gpio_arch_get_pin(uint32_t gpio){

	uint16_t ret = 0;

	// MSS GPIO pins
	if(gpio & MSS_GPIO_MASK){
		uint32_t inputs = MSS_GPIO_get_inputs();

		ret = (uint16_t)((inputs >> (gpio & (~MSS_GPIO_MASK))) & 0x00000001);
	}

	// CoreGPIO pins
	else{
		if(!flash_freeze_status()){
			uint32_t inputs = GPIO_get_inputs(&hgpio);

			ret = (uint16_t)((inputs >> gpio) & 0x00000001);
		}
	}
	return ret;
}

 void GPIO_Pin_Callback(uint32_t GPIO_Pin){
	sysGpioInterruptCallback((gpio_pin_t) GPIO_Pin);
}
