/*
 * Copyright (c) 2019, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * flash_freeze_arch.c
 *
 *  Created on: 22/02/2019
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file flash_freeze_arch.c
 */

#include "system_api.h"
#include "clks_arch.h"
#include "mss_sys_services.h"
#include "flash_freeze_arch.h"

static flash_freeze_satus_t flash_freeze_flag = FF_INACTIVE;
static flash_freeze_cb_t enter_flash_freeze_cb_func = NULL;
static flash_freeze_cb_t exit_flash_freeze_cb_func = NULL;

static uint32_t exit_flash_freeze_semaphore_id;


/**
 *
 * @return
 */
void flash_freeze_init(void){

	exit_flash_freeze_semaphore_id = ytSemaphoreCreate(1);
	ytSemaphoreWait(exit_flash_freeze_semaphore_id, YT_WAIT_FOREVER);

	ytGpioInitPin(FF_CONTROL, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);

	/* Set priority of COMM_BLK interruptions */
	NVIC_DisableIRQ(ComBlk_IRQn);
	if(COMM_BLK_IRQN_PRIORITY >= MAX_PRIORITY_INTERRUPT_SERVICE){
		NVIC_SetPriority(ComBlk_IRQn, COMM_BLK_IRQN_PRIORITY);
	}
	else{
		NVIC_SetPriority(ComBlk_IRQn, MAX_PRIORITY_INTERRUPT_SERVICE);
	}
	NVIC_ClearPendingIRQ(ComBlk_IRQn);
	NVIC_EnableIRQ(ComBlk_IRQn);
}


/**
 *
 * @return
 */
void flash_freeze_deinit(void){
	ytSemaphoreDelete(exit_flash_freeze_semaphore_id);

	ytGpioDeInitPin(FF_CONTROL);

	NVIC_DisableIRQ(ComBlk_IRQn);
}


/**
 *
 * @return
 */
retval_t enter_flash_freeze(void){
	if(flash_freeze_flag == FF_INACTIVE){
		flash_freeze_flag = FF_ACTIVE;
		if(MSS_SYS_flash_freeze(MSS_SYS_FPGA_POWER_DOWN | MSS_SYS_MPLL_POWER_DOWN) == MSS_SYS_SUCCESS){
			return RET_OK;
		}
	}
	return RET_ERROR;
}


/**
 *
 * @return
 */
retval_t exit_flash_freeze(void){

	if(flash_freeze_flag == FF_ACTIVE){
		ytGpioPinToggle(FF_CONTROL);
		ytSemaphoreWait(exit_flash_freeze_semaphore_id, YT_WAIT_FOREVER);
		return RET_OK;
	}
	return RET_ERROR;
}


/**
 *
 * @param cb_func
 * @return
 */
retval_t set_enter_flash_freeze_cb(flash_freeze_cb_t cb_func){
	enter_flash_freeze_cb_func = cb_func;
	return RET_OK;
}


/**
 *
 * @param my_cb_func
 * @return
 */
retval_t set_exit_flash_freeze_cb(flash_freeze_cb_t cb_func){
	exit_flash_freeze_cb_func = cb_func;
	return RET_OK;
}


/**
 *
 * @return
 */
flash_freeze_satus_t flash_freeze_status(void){
	return flash_freeze_flag;
}


/**
 *
 * @param response
 */
void sys_serv_flash_freeze_shutdown_handler(uint8_t response){
	if(enter_flash_freeze_cb_func){
		enter_flash_freeze_cb_func();
	}
}


/**
 *
 * @param response
 */
void sys_serv_flash_freeze_exit_handler(uint8_t response){

	// Power down the MPLL CORE and the MPLL
//	SYSREG->MSSDDR_PLL_STATUS_HIGH_CR = (SYSREG->MSSDDR_PLL_STATUS_HIGH_CR & CONFIG_MPLL_STATE_MASK) | \
			(CONFIG_MPLL_CORE_STATE_OFF | CONFIG_MPLL_STATE_OFF);
	// Power down only the MPLL CORE
//	SYSREG->MSSDDR_PLL_STATUS_HIGH_CR = (SYSREG->MSSDDR_PLL_STATUS_HIGH_CR & CONFIG_MPLL_STATE_MASK) | \
			CONFIG_MPLL_CORE_STATE_OFF;

	flash_freeze_flag = FF_INACTIVE;

	ytSemaphoreRelease(exit_flash_freeze_semaphore_id);

	if(exit_flash_freeze_cb_func){
		exit_flash_freeze_cb_func();
	}
}
