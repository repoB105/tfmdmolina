/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * unique_id.c
 *
 *  Created on: 19/09/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file unique_id.c
 */

#include "system_api.h"
#include "unique_id.h"

#define SF2_SERIAL_NUMBER_LENGTH	16

retval_t get_serial_number_base_addr(uint8_t* p_serial_number){

	if(MSS_SYS_get_serial_number(p_serial_number) != MSS_SYS_SUCCESS){
		return RET_ERROR;
	}
	return RET_OK;
}


#if USE_16_BITS_UNIQUE_ID
/**
 *
 * @return
 */
uint16_t get_unique_16_bits_id(void){

	uint8_t sn_base_addr[SF2_SERIAL_NUMBER_LENGTH];

	if(get_serial_number_base_addr(sn_base_addr) != RET_OK){
		return 0;
	}

	return *((uint16_t*)sn_base_addr);
}
#endif


#if USE_32_BITS_UNIQUE_ID
/**
 *
 * @return
 */
uint32_t get_uinque_32_bits_id(void){

	uint8_t sn_base_addr[SF2_SERIAL_NUMBER_LENGTH];

	if(get_serial_number_base_addr(sn_base_addr) != RET_OK){
		return 0;
	}

	return *((uint32_t*)sn_base_addr);
}
#endif

#if USE_64_BITS_UNIQUE_ID
/**
 *
 * @return
 */
uint64_t get_unique_64_bits_id(void){

	uint8_t sn_base_addr[SF2_SERIAL_NUMBER_LENGTH];

	if(get_serial_number_base_addr(sn_base_addr) != RET_OK){
		return 0;
	}

	return *((uint64_t*)sn_base_addr);
}
#endif

#if USE_96_BITS_UNIQUE_ID
/**
 *
 * @return
 */
uint32_t* get_unique_96_bits_id(void){

	uint8_t sn_base_addr[SF2_SERIAL_NUMBER_LENGTH];

	if(get_serial_number_base_addr(sn_base_addr) != RET_OK){
		return NULL;
	}

	return (uint32_t*)sn_base_addr;
}
#endif

#if USE_128_BITS_UNIQUE_ID
/**
 *
 * @return
 */
uint32_t* get_unique_128_bits_id(void){

	uint8_t sn_base_addr[SF2_SERIAL_NUMBER_LENGTH];

	if(get_serial_number_base_addr(sn_base_addr) != RET_OK){
		return NULL;
	}

	return (uint32_t*)sn_base_addr;
}
#endif
