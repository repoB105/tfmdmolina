/*
 * Copyright (c) 2019, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * rtc_time_arch.c
 *
 *  Created on: 30/01/2019
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file rtc_time_arch.c
 */

#include "mss_rtc.h"
#include "rtc_time.h"
#include "platform-conf.h"
#include <time.h>

#if ENABLE_RTC_TIMESTAMP

#define PRESCALER					32
#define RTC_CLK_HZ					32768	// Hz
#define BASE_YEAR					1900

/* Private functions */
static uint8_t wday_being_monday_the_first(uint8_t old_wday);
static uint8_t wday_being_sunday_the_first(uint8_t old_wday);
static uint8_t is_leap_year(uint16_t year);


retval_t rtc_time_arch_init(void){

	MSS_RTC_init(MSS_RTC_BINARY_MODE, PRESCALER-1);
	MSS_RTC_set_binary_count(INIT_TIMESTAMP);
	MSS_RTC_disable_irq();
	NVIC_ClearPendingIRQ(RTC_Wakeup_IRQn);
	MSS_RTC_start();

	return RET_OK;
}

retval_t rtc_time_arch_deinit(void){

	MSS_RTC_stop();
	MSS_RTC_set_binary_count(0);
	MSS_RTC_disable_irq();
	NVIC_ClearPendingIRQ(RTC_Wakeup_IRQn);

	return RET_OK;
}

/**
 * @brief					Synchronizes the HW RTC with the new_timestamp param. There is a delay setting the new timestamp that should be measured for accurate applications.
 * @param new_timestamp		New timestamp to be set in milliseconds. Type uint64_t which is usually equal to unsigned long long.
 * @return					Return status
 */
retval_t rtc_time_arch_set_timestamp(uint64_t new_timestamp){

	uint64_t nticks = 0;

	nticks = (new_timestamp*RTC_CLK_HZ)/(PRESCALER*1000);
	MSS_RTC_set_binary_count(nticks);

	return RET_OK;
}


/**
 * @brief				Returns the current RTC timestamp. This function has a delay that should be measured for accurate applications.
 * @return				Return the timestamp. Type uint64_t which is usually equal to unsigned long long.
 */
uint64_t rtc_time_arch_get_timestamp(void){

	uint64_t nticks = 0;

	nticks = MSS_RTC_get_binary_count();

	return (nticks*PRESCALER*1000)/RTC_CLK_HZ;
}


/**
 * @brief				Gets the current date stored by the RTC
 * @param rtc_time_date	Return value with the current date
 * @return				Return status
 */
retval_t rtc_time_arch_get_date(rtc_time_date_t* rtc_time_date){

	time_t time_sec = 0;
	struct tm ts;

	time_sec = (time_t)(rtc_time_arch_get_timestamp()/1000); // Timestamp MUST be in seconds
	ts = *gmtime(&time_sec);

	rtc_time_date->year		 = BASE_YEAR + ts.tm_year;					// tm_year: The number of years since 1900
	rtc_time_date->month 	 = ts.tm_mon + 1;							// tm_mon: Month, range 0 to 11
	rtc_time_date->month_day = ts.tm_mday;								// tm_mday: Day of the month, range 1 to 31
	rtc_time_date->week_day	 = wday_being_monday_the_first(ts.tm_wday);	// tm_wday: Day of the week, range 0 to 6, being Sunday 0, Monday 1, and so on.
	rtc_time_date->hour		 = ts.tm_hour;								// tm_hour: Hours, range 0 to 23
	rtc_time_date->minute	 = ts.tm_min;								// tm_min: Minutes, range 0 to 59
	rtc_time_date->second	 = ts.tm_sec;								// tm_sec: Seconds,  range 0 to 59

	return RET_OK;
}


/**
 * @brief				Sets the current date to the RTC timer
 * @param rtc_time_date	New date to be set
 * @return				Return status
 */
retval_t rtc_time_arch_set_date(rtc_time_date_t* rtc_time_date){

	struct tm ts;
	time_t time_sec = 0;


	// Year --------------------------------------------------------------------
	ts.tm_year	= rtc_time_date->year - BASE_YEAR;	// rtc_time_date->year: The number of years


	// Month -------------------------------------------------------------------
	if((rtc_time_date->month < 1) || (rtc_time_date->month > 12)){
		return RET_ERROR;
	}
	ts.tm_mon	= rtc_time_date->month - 1;			// rtc_time_date->month: Month, range 1 to 12


	// Month day ---------------------------------------------------------------
	if((rtc_time_date->month_day < 1) || (rtc_time_date->month_day > 31)){
		return RET_ERROR;
	}

	switch(rtc_time_date->month){
	case 1:
		if(rtc_time_date->month_day > 31){
			return RET_ERROR;
		}
		break;
	case 2:
		if(is_leap_year(rtc_time_date->year)){
			if(rtc_time_date->month_day > 29){
				return RET_ERROR;
			}
		}
		else{
			if(rtc_time_date->month_day > 28){
				return RET_ERROR;
			}
		}
		break;
	case 3:
		if(rtc_time_date->month_day > 31){
			return RET_ERROR;
		}
		break;
	case 4:
		if(rtc_time_date->month_day > 30){
			return RET_ERROR;
		}
		break;
	case 5:
		if(rtc_time_date->month_day > 31){
			return RET_ERROR;
		}
		break;
	case 6:
		if(rtc_time_date->month_day > 30){
			return RET_ERROR;
		}
		break;
	case 7:
		if(rtc_time_date->month_day > 31){
			return RET_ERROR;
		}
		break;
	case 8:
		if(rtc_time_date->month_day > 31){
			return RET_ERROR;
		}
		break;
	case 9:
		if(rtc_time_date->month_day > 30){
			return RET_ERROR;
		}
		break;
	case 10:
		if(rtc_time_date->month_day > 31){
			return RET_ERROR;
		}
		break;
	case 11:
		if(rtc_time_date->month_day > 30){
			return RET_ERROR;
		}
		break;
	case 12:
		if(rtc_time_date->month_day > 31){
			return RET_ERROR;
		}
		break;
	default:
		return RET_ERROR;
		break;
	}
	ts.tm_mday	= rtc_time_date->month_day;			// rtc_time_date->month_day: Day of the month, range 1 to 31


	// Week day ----------------------------------------------------------------
	if((rtc_time_date->week_day < 1) || (rtc_time_date->week_day > 7)){
		return RET_ERROR;
	}
	ts.tm_wday = wday_being_sunday_the_first(rtc_time_date->week_day);	// rtc_time_date->week_day: Day of the week, range 1 to 7, being Sunday 7, Monday 1, and so on.


	// Hour --------------------------------------------------------------------
	if(rtc_time_date->hour > 23){
		return RET_ERROR;
	}
	ts.tm_hour = rtc_time_date->hour;				// rtc_time_date->hour: Hours, range 0 to 23


	// Minute ------------------------------------------------------------------
	if(rtc_time_date->minute > 59){
		return RET_ERROR;
	}
	ts.tm_min = rtc_time_date->minute;				// rtc_time_date->minute: Minutes, range 0 to 59


	// Second ------------------------------------------------------------------
	if(rtc_time_date->second > 59){
		return RET_ERROR;
	}
	ts.tm_sec = rtc_time_date->second;				// rtc_time_date->second: Seconds, range 0 to 59


	time_sec = mktime(&ts);
	rtc_time_arch_set_timestamp((uint64_t)(time_sec * 1000));	// Timestamp MUST be in microseconds

	return RET_OK;
}


/**
 *
 * @param old_wday
 * @return
 */
static uint8_t wday_being_monday_the_first(uint8_t old_wday){

	uint8_t new_wday = 0;

	if(old_wday > 6){
		return 0xFF;
	}

	// Convert Sunday from first day of the week (0) to last (7)
	if (old_wday == 0){
		new_wday = 7;
	}

	else{
		new_wday = old_wday;
	}

	return new_wday;
}


/**
 *
 * @param old_wday
 * @return
 */
static uint8_t wday_being_sunday_the_first(uint8_t old_wday){

	uint8_t new_wday = 0;

	if((old_wday < 1) || (old_wday > 7)){
		return 0xFF;
	}

	// Convert Sunday from last day of the week (7) to first (0)
	if (old_wday == 7){
		new_wday = 0;
	}

	else{
		new_wday = old_wday;
	}

	return new_wday;
}


/**
 * @brief Function to check if a given year is leap year
 * @param year
 * @return
 */
static uint8_t is_leap_year(uint16_t year){
	// A year is leap year if the following conditions are satisfied:
	// 1.- Year is multiple of 400.
	// 2.- Year is multiple of 4 and not multiple of 100.
	return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

#endif // ENABLE_RTC_TIMESTAMP
