/*
 * Copyright (c) 2019, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * hs_timer_arch.c
 *
 *  Created on: 30/01/2019
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file hs_timer_arch.c
 */

#include "hs_timer_arch.h"

#if USE_HIGH_RES_TIMER

TIM_HandleTypeDef        htim5;

/**
 * @brief 	Initializes the Timer 5 as high resolution clock source
 * @return	Return status
 */
retval_t init_hs_timer_arch(void){

	  /* Enable TIM5 clock */
//	  __HAL_RCC_TIM5_CLK_ENABLE();
//
//	/* Initialize TIM5 */
//	  htim5.Instance = TIM5;
//
//	  /*
//	   * Configure the timer
//	  */
//	  htim5.Init.Period = 0xFFFFFFFF;	//MAX count available in 32 bits timer
//	  htim5.Init.Prescaler = (configCPU_CLOCK_HZ/HIGH_SPEED_TIMER_FREQ) - 1;		//HIGH_SPEED_TIMER_FREQ counting
//	  htim5.Init.ClockDivision = 0;
//	  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
//	  if(HAL_TIM_Base_Init(&htim5) == HAL_OK)
//	  {
//	    /* Start the TIM time Base generation*/
//	    HAL_TIM_Base_Start(&htim5);
//	  }

	  return RET_OK;
}

#endif
