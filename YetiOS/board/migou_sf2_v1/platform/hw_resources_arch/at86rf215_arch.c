/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * at86rf215_arch.c
 *
 *  Created on: 11/07/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file at86rf215_arch.c
 */

#include "system_api.h"
#include "at86rf215_core.h"
#include "at86rf215_const.h"
#include "at86rf215_arch.h"
#include "at86rf215_utils.h"
#include "spi_driver.h"

/* Write access command of the transceiver */
#define WRITE_ACCESS_COMMAND	(0x8000)

/* Read access command to the transceiver */
#define READ_ACCESS_COMMAND		(0x0000)

/* The two COMMAND bytes define the SPI access mode (read/write) and the 14-bit address */
#define COMMAND_SIZE			2

/* AT86RF215 memory area that should be saved before enteirng DEEP_SLEEP mode */
#define BACKUP_START_ADDR		0x000A
#define BACKUP_STOP_ADDR		0x0494
#define BACKUP_SIZE				(BACKUP_STOP_ADDR - BACKUP_START_ADDR) + 1

/* SPI configuration parameters */
#define IOCTL_CMD_SPI_CONFIG	((spi_driver_ioctl_cmd_t)	CONFIG_SPI	)
#define TRX_SPI_MODE			((migou_spi_mode_t)			MASTER		)
#define TRX_SPI_CS				((migou_spi_hw_cs_t)		TRANSCEIVER	)
#define TRX_SPI_SPEED			((uint32_t)					25000000	) // 25 MHz
#define TRX_SPI_POL				((uint8_t)					0			)
#define TRX_SPI_PHA				((uint8_t)					0			)

#define TRX_WAKEUP_TIMEOUT		YT_WAIT_FOREVER

typedef struct __packed at86rf215_conf_bk_{
	uint8_t cmd[COMMAND_SIZE];
	uint8_t regs[BACKUP_SIZE];
}at86rf215_conf_bk_t;

at86rf215_conf_bk_t config_backup;


#if ENABLE_NETSTACK_ARCH

static retval_t config_spi_device(at86rf215_data_t* at86rf215_data, char* spi_dev);
static void const at86rf215_wakeup_irq_cb(void const* argument);


/**
 *
 * @param at86rf215_data
 * @param spi_dev
 * @return
 */
retval_t at86rf215_arch_init(at86rf215_data_t* at86rf215_data, char* spi_dev){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// First open and configure the SPI to be used
	if (config_spi_device(at86rf215_data, spi_dev) != RET_OK){
		return RET_ERROR;
	}

	// Configure IRQ pin
	if(ytGpioInitPin(at86rf215_data->irq_pin, GPIO_PIN_INTERRUPT_RISING, GPIO_PIN_NO_PULL) != RET_OK){
		return RET_ERROR;
	}
	if(ytGpioPinSetCallbackInterrupt(at86rf215_data->irq_pin, NULL, NULL) != RET_OK){
		return RET_ERROR;
	}

	// Configure TCXO_EN pin
	ytGpioInitPin(TRX_TCXO_EN, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);
	ytGpioPinSet(TRX_TCXO_EN); // Enabled by default
	ytDelay(2);

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_arch_deInit(at86rf215_data_t* at86rf215_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// Deinit IRQ pin
	if(ytGpioDeInitPin(at86rf215_data->irq_pin)!= RET_OK){
		return RET_ERROR;
	}

	// Close SPI device
	ytClose(at86rf215_data->spi_id);
	at86rf215_data->spi_id = 0;

	// Deinit TCXO_EN pin
	// Disable TCXO before deinit the pin
//	ytGpioPinReset(TRX_TCXO_EN); // TRX_TCXO_EN is a CoreGPIO pin, and it should not be changed during Flash*Freeze mode
	if(ytGpioDeInitPin(TRX_TCXO_EN)!= RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @param reg_addr
 * @param read_val
 * @return
 */
retval_t at86rf215_arch_read_reg(at86rf215_data_t* at86rf215_data, uint16_t reg_addr, uint8_t* read_val){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	uint8_t tx_val[3] = {0x00, 0x00, 0x00};
	uint8_t rx_val[3] = {0x00, 0x00, 0x00};
	spi_read_write_buffs_t rd_wr_buffs;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	tx_val[0] = (READ_ACCESS_COMMAND | reg_addr) >> 8;
	tx_val[1] = (uint8_t)reg_addr;
	tx_val[2] = 0;

	rd_wr_buffs.prx = &rx_val[0];
	rd_wr_buffs.ptx = &tx_val[0];
	rd_wr_buffs.size = 3;

	if(ytIoctl(at86rf215_data->spi_id, SPI_READ_WRITE_OP, (void*) &rd_wr_buffs) != RET_OK){
		return RET_ERROR;
	}

	*read_val = rx_val[2];

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @param reg_addr
 * @param write_val
 * @return
 */
retval_t at86rf215_arch_write_reg(at86rf215_data_t* at86rf215_data, uint16_t reg_addr, uint8_t write_val){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	uint8_t tx_val[3] = {0x00, 0x00, 0x00};

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	tx_val[0] = (WRITE_ACCESS_COMMAND | reg_addr) >> 8;
	tx_val[1] = (uint8_t)reg_addr;
	tx_val[2] = write_val;

	if(ytWrite(at86rf215_data->spi_id, (void*) &tx_val[0], 3) != 3){
		return RET_ERROR;
	}

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @param reg_addr
 * @param read_val
 * @param size
 * @return
 */
retval_t at86rf215_arch_read_multi_reg(at86rf215_data_t* at86rf215_data, uint16_t reg_addr, uint8_t* read_val, uint16_t size){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	spi_read_write_buffs_t rd_wr_buffs;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	rd_wr_buffs.ptx = (uint8_t*) ytMalloc(COMMAND_SIZE + size);
	rd_wr_buffs.prx = (uint8_t*) ytMalloc(COMMAND_SIZE + size);

	rd_wr_buffs.ptx[0] = (READ_ACCESS_COMMAND | reg_addr) >> 8;
	rd_wr_buffs.ptx[1] = (uint8_t)reg_addr;

	rd_wr_buffs.size = COMMAND_SIZE + size;


	if(ytIoctl(at86rf215_data->spi_id, SPI_READ_WRITE_OP, (void*) &rd_wr_buffs) != RET_OK){
		ytFree(rd_wr_buffs.ptx);
		ytFree(rd_wr_buffs.prx);
		return RET_ERROR;
	}

	memcpy(read_val, &rd_wr_buffs.prx[COMMAND_SIZE], size);

	ytFree(rd_wr_buffs.ptx);
	ytFree(rd_wr_buffs.prx);

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @param reg_addr
 * @param write_val
 * @param size
 * @return
 */
retval_t at86rf215_arch_write_multi_reg(at86rf215_data_t* at86rf215_data, uint16_t reg_addr, uint8_t* write_val, uint16_t size){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	uint8_t* tx_val;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	tx_val = (uint8_t*) ytMalloc(COMMAND_SIZE + size);

	tx_val[0] = (WRITE_ACCESS_COMMAND | reg_addr) >> 8;
	tx_val[1] = (uint8_t)reg_addr;

	memcpy(&tx_val[COMMAND_SIZE], write_val, size);

	if(ytWrite(at86rf215_data->spi_id, (void*) tx_val, (COMMAND_SIZE + size)) != (COMMAND_SIZE + size)){
		ytFree(tx_val);
		return RET_ERROR;
	}

	ytFree(tx_val);
	return RET_OK;
#endif
}


/**
 *
 * @param cc2500_data
 * @param rx_data
 * @param size
 * @return
 */
retval_t at86rf215_arch_read_RxFrameBuffer(at86rf215_data_t* at86rf215_data, uint8_t* rx_data, uint16_t size){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	uint16_t rf24_fb_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}
	if(size > MAX_FRAME_BUFFER_SIZE){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_fb_offset = RF24_FB_OFFSET;
	}

	if(at86rf215_arch_read_multi_reg(at86rf215_data, (RG_BBC0_FBRXS + rf24_fb_offset), rx_data, size) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @param tx_data
 * @param size
 * @return
 */
retval_t at86rf215_arch_write_TxFrameBuffer(at86rf215_data_t* at86rf215_data, uint8_t* tx_data, uint16_t size){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	uint16_t rf24_fb_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}
	if(size > MAX_FRAME_BUFFER_SIZE){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_fb_offset = RF24_FB_OFFSET;
	}

	if(at86rf215_arch_write_multi_reg(at86rf215_data, (RG_BBC0_FBTXS + rf24_fb_offset), tx_data, size) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @param status
 * @return
 */
retval_t at86rf215_arch_read_status(at86rf215_data_t* at86rf215_data, rf_cmd_status_t* status){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	uint8_t read_status;
	uint16_t rf24_std_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	if(at86rf215_read_subreg(at86rf215_data, (RG_RF09_STATE + rf24_std_offset), \
			STATE_STATE_MASK, STATE_STATE_SHIFT, &read_status) != RET_OK){
		return RET_ERROR;
	}

	*status = read_status;

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @param cmd
 * @return
 */
retval_t at86rf215_arch_send_cmd(at86rf215_data_t* at86rf215_data, rf_cmd_state_t cmd){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else

	uint16_t rf24_std_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_CMD + rf24_std_offset), \
			CMD_CMD_MASK, CMD_CMD_SHIFT, (uint8_t)cmd) != RET_OK){
		return RET_ERROR;
	}
	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @param cmd
 * @return
 */
retval_t at86rf215_arch_change_state(at86rf215_data_t* at86rf215_data, rf_cmd_state_t cmd){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else

	uint32_t initial_time = 0;
	rf_cmd_status_t status;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// Check if the transceiver is already in the desired state
	if(at86rf215_arch_read_status(at86rf215_data, &status) != RET_OK){
		return RET_ERROR;
	}
	if(status == (rf_cmd_status_t)cmd){
		return RET_OK;
	}


	// If the transceiver is not already in the desired state,
	// the actions to be carried out will depend on each particular command.
	switch(cmd){

	// RF_TX -------------------------------------------------------------------
	// The state TX is reached from the state TXPREP by writing the command TX
	// to the register RFn_CMD.
	case RF_TX:
		if(status != STATUS_RF_TXPREP){
			return RET_ERROR;
		}
		if(at86rf215_arch_send_cmd(at86rf215_data, cmd) != RET_OK){
			return RET_ERROR;
		}
		break;
	// -------------------------------------------------------------------------


	// RF_RX -------------------------------------------------------------------
	// The state RX is reached from the state TXPREP or from the state TRXOFF by
	// writing the command RX to the register RFn_CMD.
	case RF_RX:
		if((status != STATUS_RF_TXPREP) && (status != STATUS_RF_TRXOFF)){
			return RET_ERROR;
		}
		if(at86rf215_arch_send_cmd(at86rf215_data, cmd) != RET_OK){
			return RET_ERROR;
		}
		break;
	// -------------------------------------------------------------------------


	// RF_TXPREP ---------------------------------------------------------------
	// The state TXPREP can be reached from any state, except from the sleep states
	// (i.e. SLEEP and DEEP_SLEEP), by writing the command TXPREP to the register RFn_CMD.
	case RF_TXPREP:
		if(status == STATUS_RF_RESET){	// The transceiver is in state SLEEP
			return RET_ERROR;
		}
		if(at86rf215_arch_send_cmd(at86rf215_data, cmd) != RET_OK){
			return RET_ERROR;
		}
		if(at86rf215_arch_read_status(at86rf215_data, &status) != RET_OK){
			return RET_ERROR;
		}
		// Check if the desired state has been reached.
		initial_time = ytGetSysTickMilliSec();
		while(status != (rf_cmd_status_t)cmd){
			if((ytGetSysTickMilliSec() - initial_time) > CHANGE_STATE_TIMEOUT){
				return RET_ERROR;
			}
			ytDelay(2);
			if(at86rf215_arch_read_status(at86rf215_data, &status) != RET_OK){
				return RET_ERROR;
			}
		}
		break;
	// -------------------------------------------------------------------------


	// RF_TRXOFF ---------------------------------------------------------------
	// The state TRXOFF can be reached from all states by writing the command
	// TRXOFF to the register RFn_CMD.
	case RF_TRXOFF:
		if(at86rf215_arch_send_cmd(at86rf215_data, cmd) != RET_OK){
			return RET_ERROR;
		}
		if(at86rf215_arch_read_status(at86rf215_data, &status) != RET_OK){
			return RET_ERROR;
		}
		// Check if the desired state has been reached.
		initial_time = ytGetSysTickMilliSec();
		while(status != (rf_cmd_status_t)cmd){
			if((ytGetSysTickMilliSec() - initial_time) > CHANGE_STATE_TIMEOUT){
				return RET_ERROR;
			}
			ytDelay(2);
			if(at86rf215_arch_read_status(at86rf215_data, &status) != RET_OK){
				return RET_ERROR;
			}
		}
		break;
	// -------------------------------------------------------------------------


	// RF_SLEEP ----------------------------------------------------------------
	// From state TRXOFF the transceiver is set to state SLEEP by writing the
	// command SLEEP to the register RFn_CMD.
	// -
	// After an RF_SLEEP command, the transceiver may have entered the DEEP_SLEEP state.
	// In that case, the internal state can not be read from the RFn_STATE register,
	// so a margin of time is left to ensure that the transition has been completed successfully.
	case RF_SLEEP:
		if(status != STATUS_RF_TRXOFF){
			return RET_ERROR;
		}
		if(at86rf215_arch_send_cmd(at86rf215_data, cmd) != RET_OK){
			return RET_ERROR;
		}
		ytDelay(CHANGE_STATE_TIMEOUT);
		break;
	// -------------------------------------------------------------------------


	// RF_RESET ----------------------------------------------------------------
	// From each state a Transceiver Reset can be initiated.
	// Once the reset procedure is completed, the state TRXOFF is reached.
	case RF_RESET:
		if(at86rf215_arch_send_cmd(at86rf215_data, cmd) != RET_OK){
			return RET_ERROR;
		}
		if(at86rf215_arch_read_status(at86rf215_data, &status) != RET_OK){
			return RET_ERROR;
		}
		initial_time = ytGetSysTickMilliSec();
		while(status != STATUS_RF_TRXOFF){
			if((ytGetSysTickMilliSec() - initial_time) > CHANGE_STATE_TIMEOUT){
				return RET_ERROR;
			}
			ytDelay(2);
			if(at86rf215_arch_read_status(at86rf215_data, &status) != RET_OK){
				return RET_ERROR;
			}
		}
		break;
	// -------------------------------------------------------------------------


	// RF_NOP ------------------------------------------------------------------
	// No operation
	case RF_NOP:
		break;
	// -------------------------------------------------------------------------


	default:
		return RET_ERROR;
	} // End of switch()

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_arch_clear_interrupt(at86rf215_data_t* at86rf215_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// As long as there is any unmasked/enabled interrupt reason pending, the pin IRQ is kept active.
	// IRQ Status registers are cleared by a read access.

	// Read IRQ events registers. They are four consecutive registers starting from RG_RF09_IRQS.
	if(at86rf215_arch_read_multi_reg(at86rf215_data, RG_RF09_IRQS, NULL, 4) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_arch_disable_interrupt(at86rf215_data_t* at86rf215_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(ytGpioPinSetCallbackInterrupt(at86rf215_data->irq_pin, NULL, NULL) != RET_OK){
		return RET_ERROR;
	}

	if(at86rf215_arch_clear_interrupt(at86rf215_data) != RET_OK){
		return RET_ERROR;
	}
	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_arch_enable_interrupt(at86rf215_data_t* at86rf215_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_arch_clear_interrupt(at86rf215_data) != RET_OK){
		return RET_ERROR;
	}

	if(ytGpioPinSetCallbackInterrupt(at86rf215_data->irq_pin, at86rf215_data->interrupt_cb_func, (void*) at86rf215_data->interrupt_cb_arg) != RET_OK){
		return RET_ERROR;
	}
	return RET_OK;
#endif
}

/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_arch_chip_reset(at86rf215_data_t* at86rf215_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else

	uint32_t initial_time = 0;
	rf_cmd_status_t status = STATUS_RF_RESET;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// To trigger the Chip Reset via the Chip Reset command, the value 0x07
	// needs to be written to the sub-register RF_RST.CMD.
	if(at86rf215_write_subreg(at86rf215_data, SR_RF_RST_CMD, (uint8_t)RF_RESET) != RET_OK){
		return RET_ERROR;
	}

	initial_time = ytGetSysTickMilliSec();

	// After the reset procedure is completed, the state RESET is left
	// and the state TRXOFF is reached.
	while(status != STATUS_RF_TRXOFF){
		if((ytGetSysTickMilliSec() - initial_time) > CHIP_RESET_TIMEOUT){
			return RET_ERROR;
		}

		ytDelay(2);

		if(at86rf215_arch_read_status(at86rf215_data, &status) != RET_OK){
			return RET_ERROR;
		}
	}

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_arch_transceiver_reset(at86rf215_data_t* at86rf215_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// While the Chip Reset procedure resets the entire device,
	// the Transceiver Reset is used to reset only a single transceiver (RF09/BBC0 or RF24/BBC1).
	// Once the reset procedure is completed, the state TRXOFF is reached.
	if(at86rf215_arch_change_state(at86rf215_data, RF_RESET) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 */
void at86rf215_arch_swap_band(at86rf215_data_t* at86rf215_data){

	if(at86rf215_data->band == RF09){
		at86rf215_data->band = RF24;
	}

	else if(at86rf215_data->band == RF24){
		at86rf215_data->band = RF09;
	}
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_arch_transceiver_sleep(at86rf215_data_t* at86rf215_data, at86rf215_band_t band){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(band == RFBOTH){
		return RET_ERROR;
	}

	if(at86rf215_data->band == band){

		// The state TRXOFF can be reached from all states by writing
		// the command TRXOFF to the register RFn_CMD.
		if(at86rf215_arch_change_state(at86rf215_data, RF_TRXOFF) != RET_OK){
			return RET_ERROR;
		}

		// From state TRXOFF the transceiver is set to state SLEEP by writing
		// the command SLEEP to the register RFn_CMD.
		if(at86rf215_arch_change_state(at86rf215_data, RF_SLEEP) != RET_OK){
			return RET_ERROR;
		}
	}

	else{
		at86rf215_arch_swap_band(at86rf215_data);

		// The state TRXOFF can be reached from all states by writing
		// the command TRXOFF to the register RFn_CMD.
		if(at86rf215_arch_change_state(at86rf215_data, RF_TRXOFF) != RET_OK){
			at86rf215_arch_swap_band(at86rf215_data);
			return RET_ERROR;
		}

		// From state TRXOFF the transceiver is set to state SLEEP by writing
		// the command SLEEP to the register RFn_CMD.
		if(at86rf215_arch_change_state(at86rf215_data, RF_SLEEP) != RET_OK){
			at86rf215_arch_swap_band(at86rf215_data);
			return RET_ERROR;
		}

		at86rf215_arch_swap_band(at86rf215_data);
	}

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_arch_deep_sleep(at86rf215_data_t* at86rf215_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// Both transceivers need to be set to SLEEP to automatically enter state DEEP_SLEEP

	// First, the sub-1 GHz transceiver is set to SLEEP.
	at86rf215_arch_write_reg(at86rf215_data, RG_RF09_CMD, RF_TRXOFF);
	at86rf215_arch_write_reg(at86rf215_data, RG_RF09_CMD, RF_SLEEP);

	// Then, the 2.4 GHz transceiver is set to SLEEP. (Interrupt should be enabled for a fastest wake up)
	at86rf215_arch_write_reg(at86rf215_data, RG_RF24_CMD, RF_TRXOFF);
	ytGpioPinSetCallbackInterrupt(at86rf215_data->irq_pin, at86rf215_wakeup_irq_cb, (void*)at86rf215_data->wakeup_semaphore_id);
	at86rf215_arch_write_reg(at86rf215_data, RG_RF24_CMD, RF_SLEEP);

	// Disable the TCXO to preserve consumed power.
//	ytGpioPinReset(TRX_TCXO_EN); // TRX_TCXO_EN is a CoreGPIO pin, and it should not be changed during Flash*Freeze mode

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_arch_transceiver_wakeup(at86rf215_data_t* at86rf215_data, at86rf215_band_t band){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(band == RFBOTH){
		return RET_ERROR;
	}

	// The TCXO is enabled, just in case it were disabled.
//	ytGpioPinSet(TRX_TCXO_EN); // TRX_TCXO_EN is a CoreGPIO pin, and it should not be changed during Flash*Freeze mode

	if(at86rf215_data->band == band){
		// The state SLEEP is left by writing command TRXOFF to the register RFn_CMD.
		if(at86rf215_arch_change_state(at86rf215_data, RF_TRXOFF) != RET_OK){
			return RET_ERROR;
		}
	}

	else{
		at86rf215_arch_swap_band(at86rf215_data);

		// The state SLEEP is left by writing command TRXOFF to the register RFn_CMD.
		if(at86rf215_arch_change_state(at86rf215_data, RF_TRXOFF) != RET_OK){
			at86rf215_arch_swap_band(at86rf215_data);
			return RET_ERROR;
		}

		at86rf215_arch_swap_band(at86rf215_data);
	}

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_arch_deep_sleep_wakeup(at86rf215_data_t* at86rf215_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// The TCXO is enabled.
//	ytGpioPinSet(TRX_TCXO_EN); // TRX_TCXO_EN is a CoreGPIO pin, and it should not be changed during Flash*Freeze mode
//	ytDelay(2);

	// Both transceivers need to be set to SLEEP to enter state DEEP_SLEEP.
	// Waking up any of them while in state DEEP_SLEEP causes the transition
	// from state DEEP_SLEEP to TRXOFF in both transceivers.
	// First, the sub-1 GHz transceiver is set to SLEEP.
	at86rf215_arch_write_reg(at86rf215_data, RG_RF09_CMD, RF_TRXOFF);

	// Wait for the WAKEUP interrupt
	ytSemaphoreWait(at86rf215_data->wakeup_semaphore_id, TRX_WAKEUP_TIMEOUT);

	// Clear WAKEUP interrupt
	at86rf215_arch_read_multi_reg(at86rf215_data, RG_RF09_IRQS, NULL, 2);

	// Disable interrupts
	ytGpioPinSetCallbackInterrupt(at86rf215_data->irq_pin, NULL, NULL);

	return RET_OK;
#endif
}



/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_arch_power_on(at86rf215_data_t* at86rf215_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// Wake the transceiver up from DEEP_SLEEP mode
	if(at86rf215_arch_deep_sleep_wakeup(at86rf215_data) != RET_OK){
		return RET_ERROR;
	}

	// Restore transceiver configuration after waking it up from DEEP_SLEEP mode
	if(at86rf215_arch_restore_config(at86rf215_data) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_arch_power_off(at86rf215_data_t* at86rf215_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// Save transceiver configuration before putting it in DEEP_SLEEP mode
	if(at86rf215_arch_save_config(at86rf215_data) != RET_OK){
		return RET_ERROR;
	}

	// Put the transceiver in DEEP_SLEEP mode
	if(at86rf215_arch_deep_sleep(at86rf215_data) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_arch_save_config(at86rf215_data_t* at86rf215_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	spi_read_write_buffs_t rd_wr_buffs;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	rd_wr_buffs.ptx = (uint8_t*) ytMalloc(COMMAND_SIZE + BACKUP_SIZE);
	rd_wr_buffs.prx = (uint8_t*) &config_backup;

	rd_wr_buffs.ptx[0] = (READ_ACCESS_COMMAND | BACKUP_START_ADDR) >> 8;
	rd_wr_buffs.ptx[1] = (uint8_t)BACKUP_START_ADDR;

	rd_wr_buffs.size = COMMAND_SIZE + BACKUP_SIZE;


	if(ytIoctl(at86rf215_data->spi_id, SPI_READ_WRITE_OP, (void*) &rd_wr_buffs) != RET_OK){
		ytFree(rd_wr_buffs.ptx);
		return RET_ERROR;
	}

	ytFree(rd_wr_buffs.ptx);

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_arch_restore_config(at86rf215_data_t* at86rf215_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	config_backup.cmd[0] = (WRITE_ACCESS_COMMAND | BACKUP_START_ADDR) >> 8;
	config_backup.cmd[1] = (uint8_t)BACKUP_START_ADDR;

	if(ytWrite(at86rf215_data->spi_id, (void*) &config_backup, (COMMAND_SIZE + BACKUP_SIZE)) != (COMMAND_SIZE + BACKUP_SIZE)){
		return RET_ERROR;
	}

	return RET_OK;
#endif
}


/**
 *
 * @param at86rf215_data
 * @param spi_dev
 * @return
 */
static retval_t config_spi_device(at86rf215_data_t* at86rf215_data, char* spi_dev){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else

	spi_config_t spi_at86rf215_config;

	at86rf215_data->spi_id = ytOpen(spi_dev, 0);
	if(!at86rf215_data->spi_id){
		return RET_ERROR;
	}

	// Configure device
	spi_at86rf215_config.mode = TRX_SPI_MODE;
	spi_at86rf215_config.hw_cs = TRX_SPI_CS; // TODO: Versión que utilice at86rf216->cs_pin
	spi_at86rf215_config.speed = TRX_SPI_SPEED;
	spi_at86rf215_config.cpol = TRX_SPI_POL;
	spi_at86rf215_config.cpha = TRX_SPI_PHA;
	if(SystemCoreClock == 50000000u){		// M3_CLK is 50 MHz
		spi_at86rf215_config.pdma_write_adj = PDMA_SPI_WRITE_ADJ_50MHZ;
	}
	else if(SystemCoreClock == 100000000u){	// M3_CLK is 100 MHz
		spi_at86rf215_config.pdma_write_adj = PDMA_SPI_WRITE_ADJ_100MHZ;
	}
	else{
		return RET_ERROR;
	}


	if(ytIoctl(at86rf215_data->spi_id, IOCTL_CMD_SPI_CONFIG, &spi_at86rf215_config) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
#endif
}


/**
 *
 * @param argument
 */
static void at86rf215_wakeup_irq_cb(void const* argument){
    ytSemaphoreRelease((uint32_t)argument);
}
#endif
