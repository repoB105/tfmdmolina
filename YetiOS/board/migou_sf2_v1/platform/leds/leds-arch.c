/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * leds-arch.c
 *
 *  Created on: 17 abr. 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file leds-arch.c
 */

#include "leds.h"
#include "flash_freeze_arch.h"


/**
 * @brief	Init HW for Leds control
 */
void leds_arch_init(void)
{
	if(!flash_freeze_status()){
		GPIO_config(&hgpio, (gpio_id_t) LEDS_BLUE_PIN, GPIO_OUTPUT_MODE);
		GPIO_config(&hgpio, (gpio_id_t) LEDS_GREEN_PIN, GPIO_OUTPUT_MODE);
		GPIO_config(&hgpio, (gpio_id_t) LEDS_RED1_PIN, GPIO_OUTPUT_MODE);
		GPIO_config(&hgpio, (gpio_id_t) LEDS_RED2_PIN, GPIO_OUTPUT_MODE);

		GPIO_set_output(&hgpio, (gpio_id_t) LEDS_BLUE_PIN, 0);
		GPIO_set_output(&hgpio, (gpio_id_t) LEDS_GREEN_PIN, 0);
		GPIO_set_output(&hgpio, (gpio_id_t) LEDS_RED1_PIN, 0);
		GPIO_set_output(&hgpio, (gpio_id_t) LEDS_RED2_PIN, 0);
	}
}

/**
 * @brief	Get leds state
 * @return	Binary leds state
 */
unsigned char leds_arch_get(void)
{
	if(!flash_freeze_status()){
		return((gpio_arch_get_pin(LEDS_BLUE_PIN) ? LEDS_BLUE : 0)
			  |(gpio_arch_get_pin(LEDS_GREEN_PIN) ? LEDS_GREEN: 0)
			  |(gpio_arch_get_pin(LEDS_RED1_PIN) ? LEDS_RED1: 0)
			  |(gpio_arch_get_pin(LEDS_RED2_PIN) ? LEDS_RED2: 0));
	}
	return 0;
}

/**
 * @brief		Turn on selected leds
 * @param leds	Leds to be turned on
 */
void leds_arch_set(unsigned char leds)
{
	if(!flash_freeze_status()){
		if(leds & LEDS_BLUE)  { GPIO_set_output(&hgpio, (gpio_id_t) LEDS_BLUE_PIN, 1); } else {  GPIO_set_output(&hgpio, (gpio_id_t) LEDS_BLUE_PIN, 0);}
		if(leds & LEDS_GREEN) { GPIO_set_output(&hgpio, (gpio_id_t) LEDS_GREEN_PIN, 1); } else {  GPIO_set_output(&hgpio, (gpio_id_t) LEDS_GREEN_PIN, 0);}
		if(leds & LEDS_RED1)  { GPIO_set_output(&hgpio, (gpio_id_t) LEDS_RED1_PIN, 1); } else {  GPIO_set_output(&hgpio, (gpio_id_t) LEDS_RED1_PIN, 0);}
		if(leds & LEDS_RED2)  { GPIO_set_output(&hgpio, (gpio_id_t) LEDS_RED2_PIN, 1); } else {  GPIO_set_output(&hgpio, (gpio_id_t) LEDS_RED2_PIN, 0);}
	}
}
