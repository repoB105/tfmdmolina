/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spi_arch.c
 *
 *  Created on: 04/06/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file spi_arch.c
 */


#include "spi_arch.h"
#include "system_api.h"
#include "mss_pdma.h"

#define DEFAULT_TIMEOUT	500

#define SPI0_RX_PDMA_CH		PDMA_CHANNEL_0
#define SPI0_TX_PDMA_CH		PDMA_CHANNEL_1
#define SPI1_RX_PDMA_CH		PDMA_CHANNEL_2
#define SPI1_TX_PDMA_CH		PDMA_CHANNEL_3

#define TX_FIFO_RESET_MASK		0x00000008u
#define RX_FIFO_RESET_MASK		0x00000004u

static uint32_t spi0_tx_semaphore_id;
static uint32_t spi0_rx_semaphore_id;
static uint32_t spi1_tx_semaphore_id;
static uint32_t spi1_rx_semaphore_id;

static void spi_arch_pdma_ch_config(mss_spi_instance_t * this_spi, uint8_t write_adj);
static mss_spi_slave_t spi_arch_get_smartfusion2_hw_cs(migou_spi_hw_cs_t * migou_spi_hw_cs);
static void spi_arch_TxRx_FIFOs_reset(mss_spi_instance_t * this_spi);
static void spi_arch_0_TxCpltCallback(void);
static void spi_arch_1_TxCpltCallback(void);
static void spi_arch_0_RxCpltCallback(void);
static void spi_arch_1_RxCpltCallback(void);


/**
 *
 * @param spi_hw_number
 * @return
 */
retval_t spi_arch_init(uint16_t spi_hw_number){

	if(spi_hw_number == 0){

		// SPI0 TX Semaphore
		if((spi0_tx_semaphore_id = ytSemaphoreCreate(1)) == 0){
			return RET_ERROR;
		}
		ytSemaphoreWait(spi0_tx_semaphore_id, YT_WAIT_FOREVER);

		// SPI0 RX Semaphore
		if((spi0_rx_semaphore_id = ytSemaphoreCreate(1)) == 0){
			return RET_ERROR;
		}
		ytSemaphoreWait(spi0_rx_semaphore_id, YT_WAIT_FOREVER);

		// SPI0 Initialization
		MSS_SPI_init( &g_mss_spi0 );

		// SPI0 PDMA RX/TX Channels Configuration
		spi_arch_pdma_ch_config(&g_mss_spi0, SPI_PDMA_DEFAULT_WRITE_ADJ);

		// SPI0 PDMA RX/TX Handler Assignment
		PDMA_set_irq_handler(SPI0_TX_PDMA_CH, spi_arch_0_TxCpltCallback);
		PDMA_set_irq_handler(SPI0_RX_PDMA_CH, spi_arch_0_RxCpltCallback);

	}
	else if(spi_hw_number == 1){

		// SPI1 TX Semaphore
		if((spi1_tx_semaphore_id = ytSemaphoreCreate(1)) == 0){
			return RET_ERROR;
		}
		ytSemaphoreWait(spi1_tx_semaphore_id, YT_WAIT_FOREVER);

		// SPI1 RX Semaphore
		if((spi1_rx_semaphore_id = ytSemaphoreCreate(1)) == 0){
			return RET_ERROR;
		}
		ytSemaphoreWait(spi1_rx_semaphore_id, YT_WAIT_FOREVER);

		// SPI1 Initialization
		MSS_SPI_init( &g_mss_spi1 );

		// SPI1 PDMA RX/TX Channels Configuration
		spi_arch_pdma_ch_config(&g_mss_spi1, SPI_PDMA_DEFAULT_WRITE_ADJ);

		// SPI1 PDMA RX/TX Handler Assignment
		PDMA_set_irq_handler(SPI1_TX_PDMA_CH, spi_arch_1_TxCpltCallback);
		PDMA_set_irq_handler(SPI1_RX_PDMA_CH, spi_arch_1_RxCpltCallback);

	}
	else{
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 *
 * @param spi_hw_number
 * @return
 */
retval_t spi_arch_deinit(uint16_t spi_hw_number){

	if(spi_hw_number == 0){
		ytSemaphoreDelete(spi0_tx_semaphore_id);
		ytSemaphoreDelete(spi0_rx_semaphore_id);
		PDMA_set_irq_handler(SPI0_TX_PDMA_CH, NULL);
		PDMA_set_irq_handler(SPI0_RX_PDMA_CH, NULL);
	}
	else if(spi_hw_number == 1){
		ytSemaphoreDelete(spi1_tx_semaphore_id);
		ytSemaphoreDelete(spi1_rx_semaphore_id);
		PDMA_set_irq_handler(SPI1_TX_PDMA_CH, NULL);
		PDMA_set_irq_handler(SPI1_RX_PDMA_CH, NULL);
	}
	else{
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 *
 * @param spi_data
 * @param txData
 * @param size
 * @return
 */
retval_t spi_arch_write(spi_data_t* spi_data, uint8_t* txData, uint16_t size){

	mss_spi_slave_t slave = spi_arch_get_smartfusion2_hw_cs(&spi_data->config.hw_cs);

	if(spi_data->spi_hw_num == 0){
		MSS_SPI_set_slave_select(&g_mss_spi0, slave);
		MSS_SPI_set_transfer_byte_count(&g_mss_spi0, size);
		spi_arch_TxRx_FIFOs_reset(&g_mss_spi0); // Flush the Tx and Rx FIFOs
		PDMA_enable_irq(SPI0_TX_PDMA_CH);
		PDMA_start
			(
				SPI0_TX_PDMA_CH,
				(uint32_t)txData,
				PDMA_SPI0_TX_REGISTER,
				size
			);

		if(ytSemaphoreWait(spi0_tx_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			PDMA_disable_irq(SPI0_TX_PDMA_CH);
			MSS_SPI_clear_slave_select(&g_mss_spi0, slave);
			return RET_ERROR;
		}
		PDMA_disable_irq(SPI0_TX_PDMA_CH);
		MSS_SPI_clear_slave_select(&g_mss_spi0, slave);
	}
	else if(spi_data->spi_hw_num == 1){
		MSS_SPI_set_slave_select(&g_mss_spi1, slave);
		MSS_SPI_set_transfer_byte_count(&g_mss_spi1, size);
		spi_arch_TxRx_FIFOs_reset(&g_mss_spi1); // Flush the Tx and Rx FIFOs
		PDMA_enable_irq(SPI1_TX_PDMA_CH);
		PDMA_start
			(
				SPI1_TX_PDMA_CH,
				(uint32_t)txData,
				PDMA_SPI1_TX_REGISTER,
				size
			);

		if(ytSemaphoreWait(spi1_tx_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			PDMA_disable_irq(SPI1_TX_PDMA_CH);
			MSS_SPI_clear_slave_select(&g_mss_spi1, slave);
			return RET_ERROR;
		}
		PDMA_disable_irq(SPI1_TX_PDMA_CH);
		MSS_SPI_clear_slave_select(&g_mss_spi1, slave);
	}
	else{
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 *
 * @param spi_data
 * @param rxData
 * @param size
 * @return
 */
retval_t spi_arch_read(spi_data_t* spi_data, uint8_t* rxData, uint16_t size){

	mss_spi_slave_t slave = spi_arch_get_smartfusion2_hw_cs(&spi_data->config.hw_cs);

	if(spi_data->spi_hw_num == 0){
		MSS_SPI_set_slave_select(&g_mss_spi0, slave);
		MSS_SPI_set_transfer_byte_count(&g_mss_spi0, size);
		spi_arch_TxRx_FIFOs_reset(&g_mss_spi0); // Flush the Tx and Rx FIFOs
		PDMA_enable_irq(SPI0_RX_PDMA_CH);
		PDMA_start
			(
				SPI0_RX_PDMA_CH,
				PDMA_SPI0_RX_REGISTER,
				(uint32_t)rxData,
				size
			);

		if(ytSemaphoreWait(spi0_rx_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			PDMA_disable_irq(SPI0_RX_PDMA_CH);
			MSS_SPI_clear_slave_select(&g_mss_spi0, slave);
			return RET_ERROR;
		}
		PDMA_disable_irq(SPI0_RX_PDMA_CH);
		MSS_SPI_clear_slave_select(&g_mss_spi0, slave);
	}
	else if(spi_data->spi_hw_num == 1){
		MSS_SPI_set_slave_select(&g_mss_spi1, slave);
		MSS_SPI_set_transfer_byte_count(&g_mss_spi1, size);
		spi_arch_TxRx_FIFOs_reset(&g_mss_spi1); // Flush the Tx and Rx FIFOs
		PDMA_enable_irq(SPI1_RX_PDMA_CH);
		PDMA_start
			(
				SPI1_RX_PDMA_CH,
				PDMA_SPI1_RX_REGISTER,
				(uint32_t)rxData,
				size
			);

		if(ytSemaphoreWait(spi1_rx_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			PDMA_disable_irq(SPI1_RX_PDMA_CH);
			MSS_SPI_clear_slave_select(&g_mss_spi1, slave);
			return RET_ERROR;
		}
		PDMA_disable_irq(SPI1_RX_PDMA_CH);
		MSS_SPI_clear_slave_select(&g_mss_spi1, slave);
	}
	else{
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 *
 * @param spi_data
 * @param txData
 * @param rxData
 * @param size
 * @return
 */
retval_t spi_arch_read_write(spi_data_t* spi_data, uint8_t* txData, uint8_t* rxData, uint16_t size){

	mss_spi_slave_t slave = spi_arch_get_smartfusion2_hw_cs(&spi_data->config.hw_cs);

	if(spi_data->spi_hw_num == 0){
		MSS_SPI_set_slave_select(&g_mss_spi0, slave);
		MSS_SPI_set_transfer_byte_count(&g_mss_spi0, size);
		spi_arch_TxRx_FIFOs_reset(&g_mss_spi0);	// Flush the Tx and Rx FIFOs
		PDMA_enable_irq(SPI0_RX_PDMA_CH);
		PDMA_start
			(
				SPI0_RX_PDMA_CH,
				PDMA_SPI0_RX_REGISTER,
				(uint32_t)rxData,
				size
			);
		PDMA_start
			(
				SPI0_TX_PDMA_CH,
				(uint32_t)txData,
				PDMA_SPI0_TX_REGISTER,
				size
			);

		if(ytSemaphoreWait(spi0_rx_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			PDMA_disable_irq(SPI0_RX_PDMA_CH);
			MSS_SPI_clear_slave_select(&g_mss_spi0, slave);
			return RET_ERROR;
		}

		if(ytSemaphoreWait(spi0_tx_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			PDMA_disable_irq(SPI0_RX_PDMA_CH);
			MSS_SPI_clear_slave_select(&g_mss_spi0, slave);
			return RET_ERROR;
		}

		PDMA_disable_irq(SPI0_RX_PDMA_CH);
		MSS_SPI_clear_slave_select(&g_mss_spi0, slave);
	}
	else if(spi_data->spi_hw_num == 1){
		MSS_SPI_set_slave_select(&g_mss_spi1, slave);
		MSS_SPI_set_transfer_byte_count(&g_mss_spi1, size);
		spi_arch_TxRx_FIFOs_reset(&g_mss_spi1); // Flush the Tx and Rx FIFOs
		PDMA_enable_irq(SPI1_RX_PDMA_CH);
		PDMA_start
			(
				SPI1_RX_PDMA_CH,
				PDMA_SPI1_RX_REGISTER,
				(uint32_t)rxData,
				size
			);
		PDMA_start
			(
				SPI1_TX_PDMA_CH,
				(uint32_t)txData,
				PDMA_SPI1_TX_REGISTER,
				size
			);

		if(ytSemaphoreWait(spi1_rx_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			PDMA_disable_irq(SPI1_RX_PDMA_CH);
			MSS_SPI_clear_slave_select(&g_mss_spi1, slave);
			return RET_ERROR;
		}

		if(ytSemaphoreWait(spi1_tx_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			PDMA_disable_irq(SPI1_RX_PDMA_CH);
			MSS_SPI_clear_slave_select(&g_mss_spi1, slave);
			return RET_ERROR;
		}

		PDMA_disable_irq(SPI1_RX_PDMA_CH);
		MSS_SPI_clear_slave_select(&g_mss_spi1, slave);
	}
	else{
		return RET_ERROR;
	}


	return RET_OK;
}


/**
 *
 * @param spi_data
 * @return
 */
retval_t spi_arch_set_config(spi_data_t* spi_data){

	mss_spi_instance_t* selected_spi;
	mss_spi_protocol_mode_t protocol_mode;
	mss_spi_slave_t	slave;
	uint32_t clk_div;
	uint32_t write_adj;


	/* MSS SPI to be configured */
	if (spi_data->spi_hw_num == 0){
		selected_spi = &g_mss_spi0;
	}
	else if (spi_data->spi_hw_num == 1){
		selected_spi = &g_mss_spi1;
	}
	else{
		return RET_ERROR;
	}


	/* Protocol mode */
	if (spi_data->config.cpol == 0){
		if (spi_data->config.cpha == 0){
			protocol_mode = MSS_SPI_MODE0;
		}
		else if (spi_data->config.cpha == 1){
			protocol_mode = MSS_SPI_MODE1;
		}
		else{
			return RET_ERROR;
		}
	}
	else if (spi_data->config.cpol == 1){
		if (spi_data->config.cpha == 0){
			protocol_mode = MSS_SPI_MODE2;
		}
		else if (spi_data->config.cpha == 1){
			protocol_mode = MSS_SPI_MODE3;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}


	/* MASTER CONFIGURATION */
	if(spi_data->config.mode == MASTER){

		/* SPI HW chip selects */
		slave = spi_arch_get_smartfusion2_hw_cs(&spi_data->config.hw_cs);
		if (slave == RET_ERROR){
			return RET_ERROR;
		}


		/* SPI Speed */
		/* clk_div: SPI clock divider value used to generate the serial
		 * interface clock signal from PCLK. Allowed values are even numbers in
		 * the range from 2 to 512. The PCLK frequency is divided by the
		 * specified value to give the serial interface clock frequency. */

		if (spi_data->config.speed < (g_FrequencyPCLK0/512)){
			clk_div = 512;
		}
		else if	(spi_data->config.speed > (g_FrequencyPCLK0/2)){
			clk_div = 2;
		}
		else{
			clk_div = (uint32_t)(g_FrequencyPCLK0/spi_data->config.speed);
			if((uint32_t)(g_FrequencyPCLK0%spi_data->config.speed) != 0){
				clk_div = clk_div + 2;
			}
		}


		/* SPI configuration in master mode */
		MSS_SPI_configure_master_mode
			(
				selected_spi,
				slave,
				protocol_mode,
				clk_div,
				MSS_SPI_BLOCK_TRANSFER_FRAME_SIZE
			);
	}

	/* SLAVE CONFIGURATION */
//	else if(spi_data->config->mode == SLAVE){
//		MSS_SPI_configure_slave_mode
//			(
//				selected_spi,
//				protocol_mode,
//				MSS_SPI_BLOCK_TRANSFER_FRAME_SIZE
//			);
//	}

	else{
		return RET_ERROR;
	}

	/* PDMA RX/TX Channels Configuration*/
	spi_arch_pdma_ch_config(selected_spi, spi_data->config.pdma_write_adj);

	return RET_OK;
}


/**
 *
 * @param this_spi
 * @return
 */
static mss_spi_slave_t spi_arch_get_smartfusion2_hw_cs
(
	migou_spi_hw_cs_t * migou_spi_hw_cs
)
{
	switch(*migou_spi_hw_cs){
	case SSN0:
		return MSS_SPI_SLAVE_0;
		break;
	case SSN1:
		return  MSS_SPI_SLAVE_1;
		break;
	case SSN2:
		return  MSS_SPI_SLAVE_2;
		break;
	case TRANSCEIVER:
		return  MSS_SPI_SLAVE_0;
		break;
	default:
		return RET_ERROR;
		break;
	}
}


/**
 *
 * @param this_spi
 * @param write_adj
 */
static void spi_arch_pdma_ch_config(mss_spi_instance_t * this_spi, uint8_t write_adj){

	/* SPI0 PDMA Channels configuration */
	if (this_spi == &g_mss_spi0){

		/* SPI0 RX PDMA Channel */
		PDMA_configure
			(
				SPI0_RX_PDMA_CH,
				PDMA_FROM_SPI_0,
				PDMA_LOW_PRIORITY | PDMA_BYTE_TRANSFER | PDMA_INC_DEST_ONE_BYTE,
				PDMA_DEFAULT_WRITE_ADJ
			);

		/* SPI0 TX PDMA Channel */
		PDMA_configure
			(
				SPI0_TX_PDMA_CH,
				PDMA_TO_SPI_0,
				PDMA_LOW_PRIORITY | PDMA_BYTE_TRANSFER | PDMA_INC_SRC_ONE_BYTE,
				write_adj
			);
	}

	/* SPI1 PDMA Channels configuration */
	else if (this_spi == &g_mss_spi1){

		/* SPI1 RX PDMA Channel */
		PDMA_configure
			(
				SPI1_RX_PDMA_CH,
				PDMA_FROM_SPI_1,
				PDMA_LOW_PRIORITY | PDMA_BYTE_TRANSFER | PDMA_INC_DEST_ONE_BYTE,
				PDMA_DEFAULT_WRITE_ADJ
			);

		/* SPI1 TX PDMA Channel */
		PDMA_configure
			(
				SPI1_TX_PDMA_CH,
				PDMA_TO_SPI_1,
				PDMA_LOW_PRIORITY | PDMA_BYTE_TRANSFER | PDMA_INC_SRC_ONE_BYTE,
				write_adj
			);
	}
}


/**
 *
 * @param this_spi
 * @return
 */
static void spi_arch_TxRx_FIFOs_reset(mss_spi_instance_t * this_spi){
    /* Flush the Tx and Rx FIFOs. */
    this_spi->hw_reg->COMMAND |= (TX_FIFO_RESET_MASK | RX_FIFO_RESET_MASK);
}


/**
 *
 *
 */
void spi_arch_0_TxCpltCallback(void){
	ytSemaphoreRelease(spi0_tx_semaphore_id);
	PDMA_clear_irq(SPI0_TX_PDMA_CH);
}


/**
 *
 *
 */
void spi_arch_1_TxCpltCallback(void){
	ytSemaphoreRelease(spi1_tx_semaphore_id);
	PDMA_clear_irq(SPI1_TX_PDMA_CH);
}


/**
 *
 *
 */
void spi_arch_0_RxCpltCallback(void){
	ytSemaphoreRelease(spi0_rx_semaphore_id);
	PDMA_clear_irq(SPI0_RX_PDMA_CH);
}


/**
 *
 *
 */
void spi_arch_1_RxCpltCallback(void){
	ytSemaphoreRelease(spi1_rx_semaphore_id);
	PDMA_clear_irq(SPI1_RX_PDMA_CH);
}
