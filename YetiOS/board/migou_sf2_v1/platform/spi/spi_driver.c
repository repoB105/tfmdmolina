/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spi_driver.c
 *
 *  Created on: 04/06/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file spi_driver.c
 */

#include "spi_arch.h"
#include "device.h"
#include "platform-conf.h"
#include "yetimote_stdio.h"
#include "sys_gpio.h"
#include "low_power.h"
#include "spi_driver.h"
#include "process.h"
#include "system_api.h"


#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#if ENABLE_SPI_DRIVER

/* Device name and Id */
#define SPI0_DEVICE_NAME 			SPI_DEV_0
#define SPI0_HW_NUMBER				0

/* Device name and Id */
#define SPI1_DEVICE_NAME 			SPI_DEV_1
#define SPI1_HW_NUMBER				1

static uint32_t spi0_mutex_id;

static uint32_t spi1_mutex_id;

/* Device Init functions */
static retval_t spi_init(void);
static retval_t spi_exit(void);


/* Device driver operation functions declaration */
static retval_t spi_open(device_t* device, dev_file_t* filep);
static retval_t spi_close(dev_file_t* filep);
static size_t spi_read(dev_file_t* filep, uint8_t* prx, size_t size);
static size_t spi_write(dev_file_t* filep, uint8_t* ptx, size_t size);
static retval_t spi_ioctl(dev_file_t* filep, uint16_t request, void* args);

static size_t spi_read_write(dev_file_t* filep, uint8_t* ptx, uint8_t* prx, size_t size);

/* Define driver operations */
static driver_ops_t spi_driver_ops = {
	.open = spi_open,
	.close = spi_close,
	.read = spi_read,
	.write = spi_write,
	.ioctl = spi_ioctl,
};


/**
 *
 * @return
 */
static retval_t spi_init(void){
	if(spi_arch_init(SPI0_HW_NUMBER) != RET_OK){	// SPI0 HW Initialization
		return RET_ERROR;
	}
	if(registerDevice(SPI0_DEVICE_ID, &spi_driver_ops, SPI0_DEVICE_NAME) != RET_OK){
		return RET_ERROR;
	}
	spi0_mutex_id = ytMutexCreate();				// Initialize blocking mutex

	if(spi_arch_init(SPI1_HW_NUMBER) != RET_OK){	// SPI1 HW Initialization
		return RET_ERROR;
	}
	if(registerDevice(SPI1_DEVICE_ID, &spi_driver_ops, SPI1_DEVICE_NAME) != RET_OK){
		return RET_ERROR;
	}
	spi1_mutex_id = ytMutexCreate();				// Initialize blocking mutex

	return RET_OK;
}

/**
 *
 * @return
 */
static retval_t spi_exit(void){
	if(spi_arch_deinit(SPI0_HW_NUMBER) != RET_OK){	// SPI0 HW Deinitialization
		return RET_ERROR;
	}
	unregisterDevice(SPI0_DEVICE_ID, SPI0_DEVICE_NAME);
	ytMutexDelete(spi0_mutex_id);

	if(spi_arch_deinit(SPI1_HW_NUMBER) != RET_OK){	// SPI1 HW Deinitialization
		return RET_ERROR;
	}
	unregisterDevice(SPI1_DEVICE_ID, SPI1_DEVICE_NAME);
	ytMutexDelete(spi1_mutex_id);

	return RET_OK;
}

/**
 *
 * @param device
 * @param filep
 * @return
 */
static retval_t spi_open(device_t* device, dev_file_t* filep){
	spi_data_t* spi_data;

	if(device->device_state != DEV_STATE_INIT){
		return RET_ERROR;
	}

	spi_data = (spi_data_t*) ytMalloc(sizeof(spi_data_t));
	if(spi_data == NULL){
		return RET_ERROR;
	}

	if((strcmp(device->device_name, SPI0_DEVICE_NAME)) == 0){

		ytMutexWait(spi0_mutex_id, YT_WAIT_FOREVER);

		spi_data->spi_hw_num = SPI0_HW_NUMBER;
		spi_data->config.mode = MASTER;
		spi_data->config.hw_cs = SSN0;
		spi_data->config.speed = 25000000;
		spi_data->config.cpol = 0;
		spi_data->config.cpha = 0;
		spi_data->config.pdma_write_adj = SPI_PDMA_DEFAULT_WRITE_ADJ;
		filep->private_data = (void*) spi_data;

		ytMutexRelease(spi0_mutex_id);
	}

	else if((strcmp(device->device_name, SPI1_DEVICE_NAME)) == 0){

		ytMutexWait(spi1_mutex_id, YT_WAIT_FOREVER);

		spi_data->spi_hw_num = SPI1_HW_NUMBER;
		spi_data->config.mode = MASTER;
		spi_data->config.hw_cs = TRANSCEIVER;
		spi_data->config.speed = 25000000;
		spi_data->config.cpol = 0;
		spi_data->config.cpha = 0;
		spi_data->config.pdma_write_adj = SPI_PDMA_DEFAULT_WRITE_ADJ;
		filep->private_data = (void*) spi_data;

		ytMutexRelease(spi1_mutex_id);
	}

	else{
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 *
 * @param filep
 * @return
 */
static retval_t spi_close(dev_file_t* filep){

	if(filep->private_data != NULL){
		ytFree(filep->private_data);
		return RET_OK;
	}
	return RET_ERROR;
}

/**
 *
 * @param filep
 * @param ptx
 * @param size
 * @return
 */
static size_t spi_write(dev_file_t* filep, uint8_t* ptx, size_t size){

	spi_data_t* spi_data =  (spi_data_t*) filep->private_data;
	retval_t ret = RET_OK;

	if(spi_data->spi_hw_num == 0){
		ytMutexWait(spi0_mutex_id, YT_WAIT_FOREVER);
	}
	else if(spi_data->spi_hw_num == 1){
		ytMutexWait(spi1_mutex_id, YT_WAIT_FOREVER);
	}
	else{
		return 0;
	}

	disable_low_power_mode();

	ret = spi_arch_write(spi_data, ptx, (uint16_t) size);

	enable_low_power_mode();

	if(spi_data->spi_hw_num == 0){
		ytMutexRelease(spi0_mutex_id);
	}
	else if(spi_data->spi_hw_num == 1){
		ytMutexRelease(spi1_mutex_id);
	}
	else{
		return 0;
	}


	if (ret != RET_OK){
		return 0;
	}
	else{
		return size;
	}
}


/**
 *
 * @param filep
 * @param prx
 * @param size
 * @return
 */
static size_t spi_read(dev_file_t* filep, uint8_t* prx, size_t size){

	spi_data_t* spi_data =  (spi_data_t*) filep->private_data;
	retval_t ret = RET_OK;

	if(spi_data->spi_hw_num == 0){
		ytMutexWait(spi0_mutex_id, YT_WAIT_FOREVER);
	}
	else if(spi_data->spi_hw_num == 1){
		ytMutexWait(spi1_mutex_id, YT_WAIT_FOREVER);
	}
	else{
		return 0;
	}

	disable_low_power_mode();

	ret = spi_arch_read(spi_data, prx, (uint16_t) size);

	enable_low_power_mode();

	if(spi_data->spi_hw_num == 0){
		ytMutexRelease(spi0_mutex_id);
	}
	else if(spi_data->spi_hw_num == 1){
		ytMutexRelease(spi1_mutex_id);
	}
	else{
		return 0;
	}


	if (ret != RET_OK){
		return 0;
	}
	else{
		return size;
	}
}

/**
 *
 * @param filep
 * @param request
 * @param args
 * @return
 */
static retval_t spi_ioctl(dev_file_t* filep, uint16_t request, void* args){

	spi_data_t* spi_data;
	spi_driver_ioctl_cmd_t cmd;
	spi_read_write_buffs_t* read_write_buffs;
	retval_t ret = RET_OK;

	if(filep == NULL){
		return RET_ERROR;
	}
	spi_data = (spi_data_t*) filep->private_data;
	cmd = (spi_driver_ioctl_cmd_t) request;

	if(spi_data->spi_hw_num == 0){
		ytMutexWait(spi0_mutex_id, YT_WAIT_FOREVER);
	}
	else if(spi_data->spi_hw_num == 1){
		ytMutexWait(spi1_mutex_id, YT_WAIT_FOREVER);
	}
	else{
		return RET_ERROR;
	}

	switch(cmd){
	case CONFIG_SPI:
		memcpy(&(spi_data->config), args, sizeof(spi_config_t));
		ret = spi_arch_set_config(spi_data);
		break;
	case SPI_READ_WRITE_OP:
		read_write_buffs = (spi_read_write_buffs_t*) args;
		if(!spi_read_write(filep, read_write_buffs->ptx, read_write_buffs->prx, read_write_buffs->size)){
			ret = RET_ERROR;
			break;
		}
		break;
	default:
		ret = RET_ERROR;
		break;
	}

	if(spi_data->spi_hw_num == 0){
		ytMutexRelease(spi0_mutex_id);
	}
	else if(spi_data->spi_hw_num == 1){
		ytMutexRelease(spi1_mutex_id);
	}
	else{
		return RET_ERROR;
	}

	return ret;
}


/**
 *
 * @param filep
 * @param ptx
 * @param prx
 * @param size
 * @return
 */
static size_t spi_read_write(dev_file_t* filep, uint8_t* ptx, uint8_t* prx, size_t size){

	spi_data_t* spi_data =  (spi_data_t*) filep->private_data;
	retval_t ret = RET_OK;

	disable_low_power_mode();

	ret = spi_arch_read_write(spi_data, ptx, prx, (uint16_t) size);

	enable_low_power_mode();

	if (ret != RET_OK){
		return 0;
	}
	else{
		return size;
	}
}

/* Register the init functions in the kernel Init system */
init_device(spi_init);
exit_device(spi_exit);

#endif
