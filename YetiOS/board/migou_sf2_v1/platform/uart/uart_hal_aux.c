/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * uart_hal_aux.c
 *
 *  Created on: 05/12/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file uart_hal_aux.c
 */

#include "uart_hal_aux.h"
#include "mss_uart_regs.h"
#include "../../CMSIS/mss_assert.h"
#include "../../CMSIS/hw_reg_io.h"
#include "../../CMSIS/system_m2sxxx.h"


// This function does practically the same as config_baud_divisors(). However,
// that function, which is part of the firmware provided by Microsemi, is defined
// as static in the mss_uart.c file, so it was not possible to use it here.
/**
 *
 * @param this_uart
 * @param baudrate
 */
void MSS_UART_set_baud_rate(mss_uart_instance_t * this_uart, uint32_t baudrate){

	uint32_t baud_value;
	uint32_t baud_value_by_64;
	uint32_t baud_value_by_128;
	uint32_t fractional_baud_value;
	uint32_t pclk_freq;

	this_uart->baudrate = baudrate;

	/* Force the value of the CMSIS global variables holding the various system
	  * clock frequencies to be updated. */
	SystemCoreClockUpdate();
	if(this_uart == &g_mss_uart0)
	{
		pclk_freq = g_FrequencyPCLK0;
	}
	else
	{
		pclk_freq = g_FrequencyPCLK1;
	}

	/*
	 * Compute baud value based on requested baud rate and PCLK frequency.
	 * The baud value is computed using the following equation:
	 *      baud_value = PCLK_Frequency / (baud_rate * 16)
	 */
	baud_value_by_128 = (8u * pclk_freq) / baudrate;
	baud_value_by_64 = baud_value_by_128 / 2u;
	baud_value = baud_value_by_64 / 64u;
	fractional_baud_value = baud_value_by_64 - (baud_value * 64u);
	fractional_baud_value += (baud_value_by_128 - (baud_value * 128u)) - (fractional_baud_value * 2u);


	if(baud_value > 1u)
	{
		/*
		 * Use Frational baud rate divisors
		 */
		/* set divisor latch */
		set_bit_reg8(&this_uart->hw_reg->LCR,DLAB);

		/* msb of baud value */
		this_uart->hw_reg->DMR = (uint8_t)(baud_value >> 8);
		/* lsb of baud value */
		this_uart->hw_reg->DLR = (uint8_t)baud_value;

		/* reset divisor latch */
		clear_bit_reg8(&this_uart->hw_reg->LCR,DLAB);

		/* Enable Fractional baud rate */
		set_bit_reg8(&this_uart->hw_reg->MM0,EFBR);

		/* Load the fractional baud rate register */
		ASSERT(fractional_baud_value <= (uint32_t)UINT8_MAX);
		this_uart->hw_reg->DFR = (uint8_t)fractional_baud_value;
	}
	else
	{
		/*
		 * Do NOT use Frational baud rate divisors.
		 */
		/* set divisor latch */
		set_bit_reg8(&this_uart->hw_reg->LCR,DLAB);

		/* msb of baud value */
		this_uart->hw_reg->DMR = (uint8_t)(baud_value >> 8u);
		/* lsb of baud value */
		this_uart->hw_reg->DLR = (uint8_t)baud_value;

		/* reset divisor latch */
		clear_bit_reg8(&this_uart->hw_reg->LCR,DLAB);

		/* Disable Fractional baud rate */
		clear_bit_reg8(&this_uart->hw_reg->MM0,EFBR);
	}
}


/**
 *
 * @param this_uart
 * @param parity
 */
void MSS_UART_set_parity(mss_uart_instance_t * this_uart, uint32_t parity){

	uint8_t line_config;

    /* Get the current line control register (bit length, stop bits, parity) */
    line_config = this_uart->hw_reg->LCR;

    /* Clear the previous values */
    line_config &= ~UART_PARITY_MASK;

    /* Insert the new parity value */
    line_config |= parity;

    /* Set the new line control register */
    this_uart->hw_reg->LCR = line_config;
	this_uart->lineconfig = line_config;
}


/**
 *
 * @param this_uart
 * @param length
 */
void MSS_UART_set_data_bits_length(mss_uart_instance_t * this_uart, uint32_t length){

	uint8_t line_config;

    /* Get the current line control register (bit length, stop bits, parity) */
    line_config = this_uart->hw_reg->LCR;

    /* Clear the previous values */
    line_config &= ~UART_DATA_LENGTH_MASK;

    /* Insert the new length value */
    line_config |= length;

    /* Set the new line control register */
    this_uart->hw_reg->LCR = line_config;
	this_uart->lineconfig = line_config;
}


/**
 *
 * @param this_uart
 * @param nstopbits
 */
void MSS_UART_set_number_stop_bits(mss_uart_instance_t * this_uart, uint32_t nstopbits){

	uint8_t line_config;

    /* Get the current line control register (bit length, stop bits, parity) */
    line_config = this_uart->hw_reg->LCR;

    /* Clear the previous values */
    line_config &= ~UART_STOP_BITS_MASK;

    /* Insert the new parity value */
    line_config |= nstopbits;

    /* Set the new line control register */
    this_uart->hw_reg->LCR = line_config;
	this_uart->lineconfig = line_config;
}
