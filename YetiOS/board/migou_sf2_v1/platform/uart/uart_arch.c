/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * uart_arch.c
 *
 *  Created on: 30/10/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file uart_arch.c
 */


#include "uart_arch.h"
#include "mss_uart.h"
#include "system_api.h"
#include "mss_pdma.h"
#include "low_power.h"
#include "uart_hal_aux.h"
#define UART_RX_BUFFER_SIZE		256
#define UART_RX_TIMER_TIMEOUT	8		// An overflow may occur in the timer if the UART speed and data flow are very high.
										// 4 ms timeout prevents overflow with a 256 buffer and up to 250 kbps of continuous data rate.
										// Higher speed require a larger buffer. When overflow occurs old data is discarded and replaced by new one
#define UART_RX_POLL_TIME		10

#define UART_RX_MAX_READ_SIZE	192

#define UART_DEFAULT_ECHO		0

#define DEFAULT_BAUD_RATE		MSS_UART_230400_BAUD
#define DEFAULT_LINE_CONF		MSS_UART_DATA_8_BITS | MSS_UART_NO_PARITY | MSS_UART_ONE_STOP_BIT

#define UART0_RX_PDMA_CH		PDMA_CHANNEL_4
#define UART0_TX_PDMA_CH		PDMA_CHANNEL_5
#define UART1_RX_PDMA_CH		PDMA_CHANNEL_6
#define UART1_TX_PDMA_CH		PDMA_CHANNEL_7

#define UART0_FLAG				0x01
#define UART1_FLAG				0x02

static uint8_t uart_n_flag = 0x00;

static uint32_t uart0_tx_semaphore_id;
static uint32_t uart0_rx_timer_id;
static uint32_t uart0_tx_mutex_id;
static uint32_t uart0_rx_mutex_id;
static uint8_t 	uart0_rx_buffer[UART_RX_BUFFER_SIZE];
static uint8_t* uart0_rx_ptr_head;			// Last byte stored in the buffer
static uint8_t* uart0_rx_ptr_tail;			// First byte stored in the buffer
static uint16_t uart0_num_stored_bytes;

static uint32_t	uart1_tx_semaphore_id;
static uint32_t uart1_rx_timer_id;
static uint32_t uart1_tx_mutex_id;
static uint32_t uart1_rx_mutex_id;
static uint8_t 	uart1_rx_buffer[UART_RX_BUFFER_SIZE];
static uint8_t* uart1_rx_ptr_head;			// Last byte stored in the buffer
static uint8_t* uart1_rx_ptr_tail;			// First byte stored in the buffer
static uint16_t uart1_num_stored_bytes;

static uint16_t enable_local_echo = UART_DEFAULT_ECHO;

static uint16_t current_pdma_0_buffer = 0;
static uint16_t current_pdma_1_buffer = 0;

static void uart_arch_pdma_ch_config(mss_uart_instance_t * this_uart);
static void uart_rx_timer_cb(void const * argument);
static void update_stored_bytes(uint8_t uart_hw_number);
static void uart_arch_0_TxCpltCallback(void);
static void uart_arch_1_TxCpltCallback(void);
static void uart_arch_0_RxCpltCallback(void);
static void uart_arch_1_RxCpltCallback(void);


/**
 *
 * @param uart_hw_number
 * @return
 */
retval_t uart_arch_init(uint8_t uart_hw_number){

	mss_uart_instance_t* selected_uart;

	/* MSS UART to be configured */
	if(uart_hw_number == 0){
		selected_uart = &g_mss_uart0;
		if((uart0_tx_semaphore_id = ytSemaphoreCreate(1)) == 0){
			return RET_ERROR;
		}
		uart0_tx_mutex_id = ytMutexCreate();	// Initialize blocking mutex
		uart0_rx_mutex_id = ytMutexCreate();	// Initialize blocking mutex
		ytSemaphoreWait(uart0_tx_semaphore_id, YT_WAIT_FOREVER);
		uart0_rx_timer_id = ytTimerCreate(ytTimerPeriodic, uart_rx_timer_cb, NULL);
		uart0_num_stored_bytes = 0;
		MSS_UART_init(selected_uart, DEFAULT_BAUD_RATE, DEFAULT_LINE_CONF);
		uart_arch_pdma_ch_config(selected_uart);	// PDMA RX/TX Channels Configuration
		PDMA_set_irq_handler(UART0_RX_PDMA_CH, uart_arch_0_RxCpltCallback);
		PDMA_start
			(
				UART0_RX_PDMA_CH,
				PDMA_UART0_RX_REGISTER,
				(uint32_t)uart0_rx_buffer,
				UART_RX_BUFFER_SIZE
			);
		PDMA_load_next_buffer
			(
				UART0_RX_PDMA_CH,
				PDMA_UART0_RX_REGISTER,
				(uint32_t)uart0_rx_buffer,
				UART_RX_BUFFER_SIZE
			);
	}

	else if(uart_hw_number == 1){
		selected_uart = &g_mss_uart1;
		if((uart1_tx_semaphore_id = ytSemaphoreCreate(1)) == 0){
			return RET_ERROR;
		}
		uart1_tx_mutex_id = ytMutexCreate();	// Initialize blocking mutex
		uart1_rx_mutex_id = ytMutexCreate();	// Initialize blocking mutex
		ytSemaphoreWait(uart1_tx_semaphore_id, YT_WAIT_FOREVER);
		uart1_rx_timer_id = ytTimerCreate(ytTimerPeriodic, uart_rx_timer_cb, NULL);
		uart1_num_stored_bytes = 0;
		MSS_UART_init(selected_uart, DEFAULT_BAUD_RATE, DEFAULT_LINE_CONF);
		uart_arch_pdma_ch_config(selected_uart);	// PDMA RX/TX Channels Configuration
		PDMA_set_irq_handler(UART1_RX_PDMA_CH, uart_arch_1_RxCpltCallback);
		PDMA_start
			(
				UART1_RX_PDMA_CH,
				PDMA_UART1_RX_REGISTER,
				(uint32_t)uart1_rx_buffer,
				UART_RX_BUFFER_SIZE
			);
		PDMA_load_next_buffer
			(
				UART1_RX_PDMA_CH,
				PDMA_UART1_RX_REGISTER,
				(uint32_t)uart1_rx_buffer,
				UART_RX_BUFFER_SIZE
			);
	}

	else{
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param uart_hw_number
 * @return
 */
retval_t uart_arch_deinit(uint8_t uart_hw_number){

	if(uart_hw_number == 0){
		ytSemaphoreDelete(uart0_tx_semaphore_id);
		ytMutexDelete(uart0_tx_mutex_id);
		ytMutexDelete(uart0_rx_mutex_id);
		ytTimerStop(uart0_rx_timer_id);
		ytTimerDelete(uart0_rx_timer_id);
	}

	else if(uart_hw_number == 1){
		ytSemaphoreDelete(uart1_tx_semaphore_id);
		ytMutexDelete(uart1_tx_mutex_id);
		ytMutexDelete(uart1_rx_mutex_id);
		ytTimerStop(uart1_rx_timer_id);
		ytTimerDelete(uart1_rx_timer_id);
	}

	else{
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param uart_hw_number
 * @return
 */
retval_t uart_arch_start_storing_data(uint8_t uart_hw_number){

	if(uart_hw_number == 0){

		if((current_pdma_0_buffer%2) == 0){
			uart0_rx_ptr_head = uart0_rx_buffer + UART_RX_BUFFER_SIZE - PDMA->CHANNEL[UART0_RX_PDMA_CH].BUFFER_A_TRANSFER_COUNT;	// Start storing the samples
		}
		else{
			uart0_rx_ptr_head = uart0_rx_buffer + UART_RX_BUFFER_SIZE - PDMA->CHANNEL[UART0_RX_PDMA_CH].BUFFER_B_TRANSFER_COUNT;	// Start storing the samples
		}

		uart0_rx_ptr_tail = uart0_rx_ptr_head;
		uart0_num_stored_bytes = 0;

		uart_n_flag |= UART0_FLAG;
		return ytTimerStart(uart0_rx_timer_id, UART_RX_TIMER_TIMEOUT);
	}

	else if(uart_hw_number == 1){

		if((current_pdma_1_buffer%2) == 0){
			uart1_rx_ptr_head = uart1_rx_buffer + UART_RX_BUFFER_SIZE - PDMA->CHANNEL[UART1_RX_PDMA_CH].BUFFER_A_TRANSFER_COUNT;	// Start storing the samples
		}
		else{
			uart1_rx_ptr_head = uart1_rx_buffer + UART_RX_BUFFER_SIZE - PDMA->CHANNEL[UART1_RX_PDMA_CH].BUFFER_B_TRANSFER_COUNT;	// Start storing the samples
		}

		uart1_rx_ptr_tail = uart1_rx_ptr_head;
		uart1_num_stored_bytes = 0;

		uart_n_flag |= UART1_FLAG;
		return ytTimerStart(uart1_rx_timer_id, UART_RX_TIMER_TIMEOUT);
	}

	else{
		return RET_ERROR;
	}
}


/**
 *
 * @param uart_hw_number
 * @return
 */
retval_t uart_arch_stop_storing_data(uint8_t uart_hw_number){

	if(uart_hw_number == 0){

		if((current_pdma_0_buffer%2) == 0){
			uart0_rx_ptr_head = uart0_rx_buffer + UART_RX_BUFFER_SIZE - PDMA->CHANNEL[UART0_RX_PDMA_CH].BUFFER_A_TRANSFER_COUNT;	// Start storing the samples
		}
		else{
			uart0_rx_ptr_head = uart0_rx_buffer + UART_RX_BUFFER_SIZE - PDMA->CHANNEL[UART0_RX_PDMA_CH].BUFFER_B_TRANSFER_COUNT;	// Start storing the samples
		}

		uart0_rx_ptr_tail = uart0_rx_ptr_head;
		uart0_num_stored_bytes = 0;

		uart_n_flag &= ~UART0_FLAG;
		return ytTimerStop(uart0_rx_timer_id);
	}

	else if(uart_hw_number == 1){

		if((current_pdma_1_buffer%2) == 0){
			uart1_rx_ptr_head = uart1_rx_buffer + UART_RX_BUFFER_SIZE - PDMA->CHANNEL[UART1_RX_PDMA_CH].BUFFER_A_TRANSFER_COUNT;	// Start storing the samples
		}
		else{
			uart1_rx_ptr_head = uart1_rx_buffer + UART_RX_BUFFER_SIZE - PDMA->CHANNEL[UART1_RX_PDMA_CH].BUFFER_B_TRANSFER_COUNT;	// Start storing the samples
		}

		uart1_rx_ptr_tail = uart1_rx_ptr_head;
		uart1_num_stored_bytes = 0;

		uart_n_flag &= ~UART0_FLAG;
		return ytTimerStop(uart1_rx_timer_id);
	}

	else{
		return RET_ERROR;
	}
}


/**
 *
 * @param uart_hw_number
 * @param txData
 * @param size
 * @param timeout
 * @return
 */
retval_t uart_arch_write(uint8_t uart_hw_number, uint8_t* txData, uint16_t size, uint32_t timeout){

	if(!size){
		return RET_ERROR;
	}

	if(uart_hw_number == 0){
		ytMutexWait(uart0_tx_mutex_id, YT_WAIT_FOREVER);
		disable_low_power_mode();
		PDMA_set_irq_handler(UART0_TX_PDMA_CH, uart_arch_0_TxCpltCallback);
		PDMA_start
			(
				UART0_TX_PDMA_CH,
				(uint32_t)txData,
				PDMA_UART0_TX_REGISTER,
				size
			);
		if(ytSemaphoreWait(uart0_tx_semaphore_id, timeout) != RET_OK){
			enable_low_power_mode();
			ytMutexRelease(uart0_tx_mutex_id);
			return RET_ERROR;
		}
		enable_low_power_mode();
		ytMutexRelease(uart0_tx_mutex_id);
	}

	else if(uart_hw_number == 1){
		ytMutexWait(uart1_tx_mutex_id, YT_WAIT_FOREVER);
		disable_low_power_mode();
		PDMA_set_irq_handler(UART1_TX_PDMA_CH, uart_arch_1_TxCpltCallback);
		PDMA_start
			(
				UART1_TX_PDMA_CH,
				(uint32_t)txData,
				PDMA_UART1_TX_REGISTER,
				size
			);
		if(ytSemaphoreWait(uart1_tx_semaphore_id, timeout) != RET_OK){
			return RET_ERROR;
			enable_low_power_mode();
			ytMutexRelease(uart1_tx_mutex_id);
		}
		enable_low_power_mode();
		ytMutexRelease(uart1_tx_mutex_id);
	}

	else{
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param uart_hw_number
 * @param rxData
 * @param size
 * @param timeout
 * @return
 */
retval_t uart_arch_read(uint8_t uart_hw_number, uint8_t* rxData, uint16_t size, uint32_t timeout){

	uint32_t start_time;
	uint16_t i;
	uint16_t read_bytes = 0;

	if(uart_hw_number == 0){
		ytMutexWait(uart0_rx_mutex_id, YT_WAIT_FOREVER);
//		disable_low_power_mode();
		update_stored_bytes(uart_hw_number);
		start_time = ytGetSysTickMilliSec();
		// Now read the buffer depending on the parameter size
		if(size <= UART_RX_MAX_READ_SIZE){
			while(uart0_num_stored_bytes < size){		// Wait till the required bytes are available
				ytDelay(UART_RX_POLL_TIME);
				if((ytGetSysTickMilliSec() - start_time) > timeout){
//					enable_low_power_mode();
					ytMutexRelease(uart0_rx_mutex_id);
					return RET_ERROR;
				}
			}
			while (read_bytes < size){
				rxData[read_bytes] = *uart0_rx_ptr_tail;
				uart0_rx_ptr_tail++;
				uart0_num_stored_bytes--;
				read_bytes++;
				if (uart0_rx_ptr_tail >= &uart0_rx_buffer[UART_RX_BUFFER_SIZE]){
					uart0_rx_ptr_tail = uart0_rx_buffer;
				}
				if(enable_local_echo){
					uart_arch_write(uart_hw_number, &rxData[read_bytes-1], 1, timeout);
				}
			}
		}

		else{
			while(read_bytes < size){

				if((size-read_bytes) > UART_RX_MAX_READ_SIZE){	// If there is no room in the buffer for the remaining bytes
					while(uart0_num_stored_bytes < UART_RX_MAX_READ_SIZE){		// Wait till the required bytes are available
						ytDelay(UART_RX_POLL_TIME);
						if((ytGetSysTickMilliSec() - start_time) > timeout){
//							enable_low_power_mode();
							ytMutexRelease(uart0_rx_mutex_id);
							return RET_ERROR;
						}
					}
					for(i=0; i<UART_RX_MAX_READ_SIZE; i++){
						rxData[read_bytes] = *uart0_rx_ptr_tail;
						uart0_rx_ptr_tail++;
						uart0_num_stored_bytes--;
						read_bytes++;
						if (uart0_rx_ptr_tail >= &uart0_rx_buffer[UART_RX_BUFFER_SIZE]){
							uart0_rx_ptr_tail = uart0_rx_buffer;
						}
						if(enable_local_echo){
							uart_arch_write(uart_hw_number, &rxData[read_bytes-1], 1, timeout);
						}
					}
				}

				else{											// There is room in the buffer for the remaining bytes
					while(uart0_num_stored_bytes < (size-read_bytes)){			// Wait till the required bytes are available
						ytDelay(UART_RX_POLL_TIME);
						if((ytGetSysTickMilliSec() - start_time) > timeout){
//							enable_low_power_mode();
							ytMutexRelease(uart0_rx_mutex_id);
							return RET_ERROR;
						}
					}
					while(read_bytes < size){	// Read the last bytes
						rxData[read_bytes] = *uart0_rx_ptr_tail;
						uart0_rx_ptr_tail++;
						uart0_num_stored_bytes--;
						read_bytes++;
						if (uart0_rx_ptr_tail >= &uart0_rx_buffer[UART_RX_BUFFER_SIZE]){
							uart0_rx_ptr_tail = uart0_rx_buffer;
						}
						if(enable_local_echo){
							uart_arch_write(uart_hw_number, &rxData[read_bytes-1], 1, timeout);
						}
					}
				}
			}
		}
//		enable_low_power_mode();
		ytMutexRelease(uart0_rx_mutex_id);
	}

	else if(uart_hw_number == 1){
		ytMutexWait(uart1_rx_mutex_id, YT_WAIT_FOREVER);
//		disable_low_power_mode();
		update_stored_bytes(uart_hw_number);
		start_time = ytGetSysTickMilliSec();
		// Now read the buffer depending on the parameter size
		if(size <= UART_RX_MAX_READ_SIZE){
			while(uart1_num_stored_bytes < size){		// Wait till the required bytes are available
				ytDelay(UART_RX_POLL_TIME);
				if((ytGetSysTickMilliSec() - start_time) > timeout){
//					enable_low_power_mode();
					ytMutexRelease(uart1_rx_mutex_id);
					return RET_ERROR;
				}
			}
			while (read_bytes < size){
				rxData[read_bytes] = *uart1_rx_ptr_tail;
				uart1_rx_ptr_tail++;
				uart1_num_stored_bytes--;
				read_bytes++;
				if (uart1_rx_ptr_tail >= &uart1_rx_buffer[UART_RX_BUFFER_SIZE]){
					uart1_rx_ptr_tail = uart1_rx_buffer;
				}
				if(enable_local_echo){
					uart_arch_write(uart_hw_number, &rxData[read_bytes-1], 1, timeout);
				}
			}

		}
		else{
			while(read_bytes < size){

				if((size-read_bytes) > UART_RX_MAX_READ_SIZE){	// If there is no room in the buffer for the remaining bytes
					while(uart1_num_stored_bytes < UART_RX_MAX_READ_SIZE){		// Wait till the required bytes are available
						ytDelay(UART_RX_POLL_TIME);
						if((ytGetSysTickMilliSec() - start_time) > timeout){
//							enable_low_power_mode();
							ytMutexRelease(uart1_rx_mutex_id);
							return RET_ERROR;
						}
					}
					for(i=0; i<UART_RX_MAX_READ_SIZE; i++){
						rxData[read_bytes] = *uart1_rx_ptr_tail;
						uart1_rx_ptr_tail++;
						uart1_num_stored_bytes--;
						read_bytes++;
						if (uart1_rx_ptr_tail >= &uart1_rx_buffer[UART_RX_BUFFER_SIZE]){
							uart1_rx_ptr_tail = uart1_rx_buffer;
						}
						if(enable_local_echo){
							uart_arch_write(uart_hw_number, &rxData[read_bytes-1], 1, timeout);
						}
					}

				}
				else{											// There is room in the buffer for the remaining bytes
					while(uart1_num_stored_bytes < (size-read_bytes)){				// Wait till the required bytes are available
						ytDelay(UART_RX_POLL_TIME);
						if((ytGetSysTickMilliSec() - start_time) > timeout){
//							enable_low_power_mode();
							ytMutexRelease(uart1_rx_mutex_id);
							return RET_ERROR;
						}
					}
					while(read_bytes < size){	// Read the last bytes
						rxData[read_bytes] = *uart1_rx_ptr_tail;
						uart1_rx_ptr_tail++;
						uart1_num_stored_bytes--;
						read_bytes++;
						if (uart1_rx_ptr_tail >= &uart1_rx_buffer[UART_RX_BUFFER_SIZE]){
							uart1_rx_ptr_tail = uart1_rx_buffer;
						}
						if(enable_local_echo){
							uart_arch_write(uart_hw_number, &rxData[read_bytes-1], 1, timeout);
						}
					}
				}
			}
		}
//		enable_low_power_mode();
		ytMutexRelease(uart1_rx_mutex_id);
	}

	else{
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param uart_hw_number
 * @param rxData
 * @param size
 * @param timeout
 * @return
 */
retval_t uart_arch_read_line(uint8_t uart_hw_number, uint8_t* rxData, uint16_t size, uint32_t timeout){

	uint32_t start_time;
	uint16_t read_bytes = 0;
	uint8_t backw[3] = "\b \b";
	uint8_t cr_lf[2] = "\r\n";

	if(uart_hw_number == 0){
		ytMutexWait(uart0_rx_mutex_id, YT_WAIT_FOREVER);
//		disable_low_power_mode();
		update_stored_bytes(uart_hw_number);

		start_time = ytGetSysTickMilliSec();
		while(read_bytes < size){
			while(uart0_num_stored_bytes){
				if(((*uart0_rx_ptr_tail) != '\r') && ((*uart0_rx_ptr_tail) != '\n')){	// If not line end, store the character

					if((*uart0_rx_ptr_tail) != '\b'){
						rxData[read_bytes] = *uart0_rx_ptr_tail;
						read_bytes++;
						uart0_rx_ptr_tail++;
						uart0_num_stored_bytes--;
						if (uart0_rx_ptr_tail >= &uart0_rx_buffer[UART_RX_BUFFER_SIZE]){
							uart0_rx_ptr_tail = uart0_rx_buffer;
						}
						if(read_bytes >= size){
//							enable_low_power_mode();
							ytMutexRelease(uart0_rx_mutex_id);
							return RET_ERROR;
						}
						if(enable_local_echo){
							uart_arch_write(uart_hw_number, &rxData[read_bytes-1], 1, timeout);
						}
					}

					else{	// Move Backwards. Don't store the character and reduce one
						if(read_bytes){
							read_bytes--;
						}
						uart0_rx_ptr_tail++;
						uart0_num_stored_bytes--;
						if (uart0_rx_ptr_tail >= &uart0_rx_buffer[UART_RX_BUFFER_SIZE]){
							uart0_rx_ptr_tail = uart0_rx_buffer;
						}
						if(enable_local_echo){
							uart_arch_write(uart_hw_number, backw, 3, timeout);
						}
					}
				}

				else{	// Line end detected.
					rxData[read_bytes] = '\0';
					uart0_rx_ptr_tail++;
					uart0_num_stored_bytes--;
					read_bytes++;
					if (uart0_rx_ptr_tail >= &uart0_rx_buffer[UART_RX_BUFFER_SIZE]){
						uart0_rx_ptr_tail = uart0_rx_buffer;
					}

					// Check if the next element is a '\r' or a '\n' which is very feasible when the line end is "\r\n"
					if(uart0_num_stored_bytes){
						if(((*uart0_rx_ptr_tail) == '\r') || ((*uart0_rx_ptr_tail) == '\n')){
							uart0_rx_ptr_tail++;
							uart0_num_stored_bytes--;
							if (uart0_rx_ptr_tail >= &uart0_rx_buffer[UART_RX_BUFFER_SIZE]){
								uart0_rx_ptr_tail = uart0_rx_buffer;
							}
						}
					}

					if(enable_local_echo){
						uart_arch_write(uart_hw_number, cr_lf, 2, timeout);
					}
//					enable_low_power_mode();
					ytMutexRelease(uart0_rx_mutex_id);
					return RET_OK;
				}
			}

			ytDelay(UART_RX_POLL_TIME);

			if((ytGetSysTickMilliSec() - start_time) > timeout){
//				enable_low_power_mode();
				ytMutexRelease(uart0_rx_mutex_id);
				return RET_ERROR;
			}
		}
//		enable_low_power_mode();
		ytMutexRelease(uart0_rx_mutex_id);
	}

	else if(uart_hw_number == 1){
		ytMutexWait(uart1_rx_mutex_id, YT_WAIT_FOREVER);
//		disable_low_power_mode();
		update_stored_bytes(uart_hw_number);

		start_time = ytGetSysTickMilliSec();
		while(read_bytes < size){
			while(uart1_num_stored_bytes){
				if(((*uart1_rx_ptr_tail) != '\r') && ((*uart1_rx_ptr_tail) != '\n')){	// If not line end, store the character

					if((*uart1_rx_ptr_tail) != '\b'){
						rxData[read_bytes] = *uart1_rx_ptr_tail;
						read_bytes++;
						uart1_rx_ptr_tail++;
						uart1_num_stored_bytes--;
						if (uart1_rx_ptr_tail >= &uart1_rx_buffer[UART_RX_BUFFER_SIZE]){
							uart1_rx_ptr_tail = uart1_rx_buffer;
						}
						if(read_bytes >= size){
//							enable_low_power_mode();
							ytMutexRelease(uart1_rx_mutex_id);
							return RET_ERROR;
						}
						if(enable_local_echo){
							uart_arch_write(uart_hw_number, &rxData[read_bytes-1], 1, timeout);
						}
					}

					else{	// Move Backwards. Don't store the character and reduce one
						if(read_bytes){
							read_bytes--;
						}
						uart1_rx_ptr_tail++;
						uart1_num_stored_bytes--;
						if (uart1_rx_ptr_tail >= &uart1_rx_buffer[UART_RX_BUFFER_SIZE]){
							uart1_rx_ptr_tail = uart1_rx_buffer;
						}
						if(enable_local_echo){
							uart_arch_write(uart_hw_number, backw, 3, timeout);
						}
					}
				}

				else{	// Line end detected.
					rxData[read_bytes] = '\0';
					uart1_rx_ptr_tail++;
					uart1_num_stored_bytes--;
					read_bytes++;
					if (uart1_rx_ptr_tail >= &uart1_rx_buffer[UART_RX_BUFFER_SIZE]){
						uart1_rx_ptr_tail = uart1_rx_buffer;
					}

					// Check if the next element is a '\r' or a '\n' which is very feasible when the line end is "\r\n"
					if(uart1_num_stored_bytes){
						if(((*uart1_rx_ptr_tail) == '\r') || ((*uart1_rx_ptr_tail) == '\n')){
							uart1_rx_ptr_tail++;
							uart1_num_stored_bytes--;
							if (uart1_rx_ptr_tail >= &uart1_rx_buffer[UART_RX_BUFFER_SIZE]){
								uart1_rx_ptr_tail = uart1_rx_buffer;
							}
						}
					}

					if(enable_local_echo){
						uart_arch_write(uart_hw_number, cr_lf, 2, timeout);
					}
//					enable_low_power_mode();
					ytMutexRelease(uart1_rx_mutex_id);
					return RET_OK;
				}
			}

			ytDelay(UART_RX_POLL_TIME);

			if((ytGetSysTickMilliSec() - start_time) > timeout){
//				enable_low_power_mode();
				ytMutexRelease(uart1_rx_mutex_id);
				return RET_ERROR;
			}
		}
//		enable_low_power_mode();
		ytMutexRelease(uart1_rx_mutex_id);
	}

	else{
		return RET_ERROR;
	}

	return RET_ERROR;
}


/**
 *
 * @param uart_hw_number
 * @param speed
 * @return
 */
retval_t uart_arch_set_speed(uint8_t uart_hw_number, uint32_t speed){

	mss_uart_instance_t * this_uart;

	if(uart_hw_number == 0){
		this_uart = &g_mss_uart0;
	}
	else if(uart_hw_number == 1){
		this_uart = &g_mss_uart1;
	}
	else{
		return RET_ERROR;
	}

	// Check if the speed value is supported
	if ((speed != MSS_UART_110_BAUD)	&&
		(speed != MSS_UART_300_BAUD)	&&
		(speed != MSS_UART_1200_BAUD)	&&
		(speed != MSS_UART_2400_BAUD)	&&
		(speed != MSS_UART_4800_BAUD)	&&
		(speed != MSS_UART_9600_BAUD)	&&
		(speed != MSS_UART_19200_BAUD)	&&
		(speed != MSS_UART_38400_BAUD)	&&
		(speed != MSS_UART_57600_BAUD)	&&
		(speed != MSS_UART_115200_BAUD)	&&
		(speed != MSS_UART_230400_BAUD) &&
		(speed != MSS_UART_460800_BAUD) &&
		(speed != MSS_UART_921600_BAUD) ){

		return RET_ERROR;
	}

	MSS_UART_set_baud_rate(this_uart, speed);

	return RET_OK;
}


/**
 *
 * @param uart_hw_number
 * @param parity
 * @return
 */
retval_t uart_arch_set_parity(uint8_t uart_hw_number, uint32_t parity){

	mss_uart_instance_t * this_uart;

	if(uart_hw_number == 0){
		this_uart = &g_mss_uart0;
	}
	else if(uart_hw_number == 1){
		this_uart = &g_mss_uart1;
	}
	else{
		return RET_ERROR;
	}

	// Check if the parity value is supported
	if ((parity != MSS_UART_NO_PARITY)		&&
		(parity != MSS_UART_ODD_PARITY)		&&
		(parity != MSS_UART_EVEN_PARITY)	&&
		(parity != MSS_UART_STICK_PARITY_0)	&&
		(parity != MSS_UART_STICK_PARITY_1)	){

		return RET_ERROR;
	}

	MSS_UART_set_parity(this_uart, parity);

	return RET_OK;
}


/**
 *
 * @param uart_hw_number
 * @param length
 * @return
 */
retval_t uart_arch_set_data_bits_length(uint8_t uart_hw_number, uint32_t length){

	mss_uart_instance_t * this_uart;

	if(uart_hw_number == 0){
		this_uart = &g_mss_uart0;
	}
	else if(uart_hw_number == 1){
		this_uart = &g_mss_uart1;
	}
	else{
		return RET_ERROR;
	}

	// Check if the length value is supported
	if ((length != MSS_UART_DATA_5_BITS)	&&
		(length != MSS_UART_DATA_6_BITS)	&&
		(length != MSS_UART_DATA_7_BITS)	&&
		(length != MSS_UART_DATA_8_BITS)	){

		return RET_ERROR;
	}

	MSS_UART_set_data_bits_length(this_uart, length);

	return RET_OK;
}


/**
 *
 * @param uart_hw_number
 * @param nstopbits
 * @return
 */
retval_t uart_arch_set_number_stop_bits(uint8_t uart_hw_number, uint32_t nstopbits){

	mss_uart_instance_t * this_uart;

	if(uart_hw_number == 0){
		this_uart = &g_mss_uart0;
	}
	else if(uart_hw_number == 1){
		this_uart = &g_mss_uart1;
	}
	else{
		return RET_ERROR;
	}

	// Check if the nstopbits value is supported
	if ((nstopbits != MSS_UART_ONE_STOP_BIT)	&&
		(nstopbits != MSS_UART_ONEHALF_STOP_BIT)&&
		(nstopbits != MSS_UART_TWO_STOP_BITS)	){

		return RET_ERROR;
	}

	MSS_UART_set_number_stop_bits(this_uart, nstopbits);

	return RET_OK;
}


/**
 *
 * @return
 */
retval_t uart_arch_enable_echo(void){
	enable_local_echo = 1;
	return RET_OK;
}

/**
 *
 * @return
 */
retval_t uart_arch_disable_echo(void){
	enable_local_echo = 0;
	return RET_OK;
}


/**
 *
 * @param this_uart
 */
static void uart_arch_pdma_ch_config(mss_uart_instance_t * this_uart){

	/* UART0 PDMA Channels configuration */
	if (this_uart == &g_mss_uart0){

		/* UART0 RX PDMA Channel */
		PDMA_configure
			(
				UART0_RX_PDMA_CH,
				PDMA_FROM_UART_0,
				PDMA_LOW_PRIORITY | PDMA_BYTE_TRANSFER | PDMA_INC_DEST_ONE_BYTE,
				PDMA_DEFAULT_WRITE_ADJ
			);

		/* UART0 TX PDMA Channel */
		PDMA_configure
			(
				UART0_TX_PDMA_CH,
				PDMA_TO_UART_0,
				PDMA_LOW_PRIORITY | PDMA_BYTE_TRANSFER | PDMA_INC_SRC_ONE_BYTE,
				PDMA_DEFAULT_WRITE_ADJ
			);
	}

	/* UART1 PDMA Channels configuration */
	else if (this_uart == &g_mss_uart1){

		/* UART1 RX PDMA Channel */
		PDMA_configure
			(
				UART1_RX_PDMA_CH,
				PDMA_FROM_UART_1,
				PDMA_LOW_PRIORITY | PDMA_BYTE_TRANSFER | PDMA_INC_DEST_ONE_BYTE,
				PDMA_DEFAULT_WRITE_ADJ
			);

		/* UART1 TX PDMA Channel */
		PDMA_configure
			(
				UART1_TX_PDMA_CH,
				PDMA_TO_UART_1,
				PDMA_LOW_PRIORITY | PDMA_BYTE_TRANSFER | PDMA_INC_SRC_ONE_BYTE,
				PDMA_DEFAULT_WRITE_ADJ
			);
	}
}


/**
 *
 * @param argument
 */
static void uart_rx_timer_cb(void const * argument){

	if(uart_n_flag & UART0_FLAG){
		update_stored_bytes(0);
	}

	if(uart_n_flag & UART1_FLAG){
		update_stored_bytes(1);
	}
}


/**
 *
 */
static void update_stored_bytes(uint8_t uart_hw_number){

	uint8_t* new_uart_rx_ptr_head;
	uint32_t pdma_counter;
	uint16_t new_bytes = 0;

	if(uart_hw_number == 0){

		if((current_pdma_0_buffer%2) == 0){
			pdma_counter = PDMA->CHANNEL[UART0_RX_PDMA_CH].BUFFER_A_TRANSFER_COUNT;	// PDMA remaining downcounter
		}
		else{
			pdma_counter = PDMA->CHANNEL[UART0_RX_PDMA_CH].BUFFER_B_TRANSFER_COUNT;	// PDMA remaining downcounter
		}

		// Prevent error if the PDMA downcounter is 0 (the UART pointer will overflow)
		if(!pdma_counter){
			pdma_counter = UART_RX_BUFFER_SIZE;
		}

		new_uart_rx_ptr_head = uart0_rx_buffer + UART_RX_BUFFER_SIZE - pdma_counter;

		if (new_uart_rx_ptr_head >= uart0_rx_ptr_head){
			new_bytes = (uint16_t)(new_uart_rx_ptr_head - uart0_rx_ptr_head);
			uart0_num_stored_bytes += new_bytes;
		}
		else{
			new_bytes = UART_RX_BUFFER_SIZE - ((uint16_t)(uart0_rx_ptr_head - new_uart_rx_ptr_head));
			uart0_num_stored_bytes += new_bytes;
		}

		uart0_rx_ptr_head = new_uart_rx_ptr_head; 			// Update head pointer

		if(uart0_num_stored_bytes >= UART_RX_BUFFER_SIZE){	// The buffer is full. Move the tail pointer
			uart0_num_stored_bytes = UART_RX_BUFFER_SIZE;
			uart0_rx_ptr_tail = uart0_rx_ptr_head;			// The first written byte is the last unwritten byte when the buffer is full
		}
	}

	else if(uart_hw_number == 1){

		if((current_pdma_1_buffer%2) == 0){
			pdma_counter = PDMA->CHANNEL[UART1_RX_PDMA_CH].BUFFER_A_TRANSFER_COUNT;	// PDMA remaining downcounter
		}
		else{
			pdma_counter = PDMA->CHANNEL[UART1_RX_PDMA_CH].BUFFER_B_TRANSFER_COUNT;	// PDMA remaining downcounter
		}

		// Prevent error if the PDMA downcounter is 0 (the UART pointer will overflow)
		if(!pdma_counter){
			pdma_counter = UART_RX_BUFFER_SIZE;
		}

		new_uart_rx_ptr_head = uart1_rx_buffer + UART_RX_BUFFER_SIZE - pdma_counter;

		if (new_uart_rx_ptr_head >= uart1_rx_ptr_head){
			new_bytes = (uint16_t)(new_uart_rx_ptr_head - uart1_rx_ptr_head);
			uart1_num_stored_bytes += new_bytes;
		}
		else{
			new_bytes = UART_RX_BUFFER_SIZE - ((uint16_t)(uart1_rx_ptr_head - new_uart_rx_ptr_head));
			uart1_num_stored_bytes += new_bytes;
		}

		uart1_rx_ptr_head = new_uart_rx_ptr_head; 			// Update head pointer

		if(uart1_num_stored_bytes >= UART_RX_BUFFER_SIZE){	// The buffer is full. Move the tail pointer
			uart1_num_stored_bytes = UART_RX_BUFFER_SIZE;
			uart1_rx_ptr_tail = uart1_rx_ptr_head;			// The first written byte is the last unwritten byte when the buffer is full
		}
	}
}


/**
 *
 *
 */
static void uart_arch_0_TxCpltCallback(void){
	ytSemaphoreRelease(uart0_tx_semaphore_id);
	PDMA_clear_irq(UART0_TX_PDMA_CH);
}


/**
 *
 *
 */
static void uart_arch_1_TxCpltCallback(void){
	ytSemaphoreRelease(uart1_tx_semaphore_id);
	PDMA_clear_irq(UART1_TX_PDMA_CH);
}


/**
 *
 *
 */
static void uart_arch_0_RxCpltCallback(void){
	current_pdma_0_buffer++;
	PDMA_clear_irq(UART0_RX_PDMA_CH);
}


/**
 *
 *
 */
static void uart_arch_1_RxCpltCallback(void){
	current_pdma_1_buffer++;
	PDMA_clear_irq(UART1_RX_PDMA_CH);
}
