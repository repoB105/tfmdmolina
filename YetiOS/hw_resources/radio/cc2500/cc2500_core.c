/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * cc2500_core.c
 *
 *  Created on: 3 de may. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file cc2500_core.c
 */

#include "system_api.h"
#include "cc2500_core.h"
#include "cc2500_const.h"
#include "cc2500_arch.h"
#include "cc2500_config.h"

#define CC2500_MAX_FIFO_LENGTH	64

#define CC2500_SEND_TIMEOUT 	50
#define CC2500_RCV_TIMEOUT 		50

/**
 *
 * @param cs_pin
 * @param gdo2_int_pin
 * @param interrupt_cb_func
 * @param args
 * @return
 */
cc2500_data_t* new_cc2500_data(gpio_pin_t cs_pin, gpio_pin_t gdo2_int_pin, ytProcessFunc_t interrupt_cb_func, void* args){
	cc2500_data_t* new_cc2500_data;
	if((new_cc2500_data = (cc2500_data_t*) ytMalloc(sizeof(cc2500_data_t))) == NULL){
		return NULL;
	}
	new_cc2500_data->cs_pin = cs_pin;
	new_cc2500_data->gdo2_int_pin = gdo2_int_pin;
	new_cc2500_data->spi_id = 0;
	new_cc2500_data->interrupt_cb_func = interrupt_cb_func;
	new_cc2500_data->interrupt_cb_arg = args;
	new_cc2500_data->transmitting_packet = 0;
	new_cc2500_data->receiving_packet = 0;

	new_cc2500_data->cc2500_rx_time = 0;
	new_cc2500_data->cc2500_tx_time = 0;

	new_cc2500_data->cc2500_mutex_id = ytMutexCreate();

	return new_cc2500_data;
}

/**
 *
 * @param cc2500_data
 * @return
 */
retval_t delete_cc2500_data(cc2500_data_t* cc2500_data){
	if(cc2500_data == NULL){
		return RET_ERROR;
	}
	ytMutexDelete(cc2500_data->cc2500_mutex_id);
	ytFree(cc2500_data);
	return RET_OK;

}

/**
 *
 * @param cc2500_data
 * @param cc2500_init_config
 * @param spi_dev
 * @return
 */
retval_t cc2500_hw_init(cc2500_data_t* cc2500_data, cc2500_config_t* cc2500_init_config, char* spi_dev){
	cc2500_status_t status;
	uint8_t read_val;

	if(cc2500_arch_init(cc2500_data, spi_dev) != RET_OK){	//Lo primero es inicializar todos el HW necesario del uC. SPI y GPIOs
		return RET_ERROR;
	}
	cc2500_arch_power_on(cc2500_data);	//Me enciendo y espero
	ytDelay(2);

	cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SRES);
	ytDelay(100);

	status = cc2500_arch_get_status(cc2500_data);	//Despues de reiniciar y esperar este tiempo, ya deber�a estar activo
	if(status.CHIP_RDYn){
		cc2500_arch_power_off(cc2500_data);
		cc2500_arch_deInit(cc2500_data);
		return RET_ERROR;
	}

	/* Check partnum */
	if(cc2500_arch_read_status_reg(cc2500_data, CC2500_PARTNUM, &read_val) != RET_OK){
		cc2500_arch_power_off(cc2500_data);
		cc2500_arch_deInit(cc2500_data);
		return RET_ERROR;
	}
	if(read_val != 0x80){
		cc2500_arch_power_off(cc2500_data);
		cc2500_arch_deInit(cc2500_data);
		return RET_ERROR;
	}

	cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SIDLE);

	if(cc2500_config_gdo2_pin(cc2500_data, CC2500_SENT_RECEIVED) != RET_OK){
		cc2500_arch_power_off(cc2500_data);
		cc2500_arch_deInit(cc2500_data);
		return RET_ERROR;
	}
	if(cc2500_disable_fixed_packet_length(cc2500_data) != RET_OK){
		cc2500_arch_power_off(cc2500_data);
		cc2500_arch_deInit(cc2500_data);
		return RET_ERROR;
	}

	if(cc2500_set_channel_num(cc2500_data, cc2500_init_config->channel_num) != RET_OK){
		cc2500_arch_power_off(cc2500_data);
		cc2500_arch_deInit(cc2500_data);
		return RET_ERROR;
	}
	if(cc2500_set_base_freq(cc2500_data, cc2500_init_config->modulation_freq) != RET_OK){
		cc2500_arch_power_off(cc2500_data);
		cc2500_arch_deInit(cc2500_data);
		return RET_ERROR;
	}
	if(cc2500_set_rx_bw(cc2500_data, cc2500_init_config->rx_bandwidth) != RET_OK){
		cc2500_arch_power_off(cc2500_data);
		cc2500_arch_deInit(cc2500_data);
		return RET_ERROR;
	}
	if(cc2500_set_datarate(cc2500_data, cc2500_init_config->baud_rate) != RET_OK){
		cc2500_arch_power_off(cc2500_data);
		cc2500_arch_deInit(cc2500_data);
		return RET_ERROR;
	}
	if(cc2500_set_modulation_format(cc2500_data, cc2500_init_config->modulation) != RET_OK){
		cc2500_arch_power_off(cc2500_data);
		cc2500_arch_deInit(cc2500_data);
		return RET_ERROR;
	}
	if(cc2500_set_channel_spacing(cc2500_data, cc2500_init_config->channel_spacing) != RET_OK){
		cc2500_arch_power_off(cc2500_data);
		cc2500_arch_deInit(cc2500_data);
		return RET_ERROR;
	}
	if(cc2500_init_config->modulation != CC2500_MSK){
		if(cc2500_set_freq_dev(cc2500_data, cc2500_init_config->freq_deviation) != RET_OK){
			cc2500_arch_power_off(cc2500_data);
			cc2500_arch_deInit(cc2500_data);
			return RET_ERROR;
		}
	}
	if(cc2500_set_output_pwr(cc2500_data, cc2500_init_config->output_power) != RET_OK){
		cc2500_arch_power_off(cc2500_data);
		cc2500_arch_deInit(cc2500_data);
		return RET_ERROR;
	}

	cc2500_arch_write_reg(cc2500_data, CC2500_PKTCTRL1, 0x00);	//Disable status bytes

	cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SFRX);
	cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SFTX);

	cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SPWD);
	cc2500_arch_disable_interrupt(cc2500_data);

	return RET_OK;
}

/**
 *
 * @param cc2500_data
 * @return
 */
retval_t cc2500_hw_deinit(cc2500_data_t* cc2500_data){

	if(cc2500_data == NULL){
		return RET_ERROR;
	}
	cc2500_arch_power_off(cc2500_data);
	cc2500_arch_deInit(cc2500_data);

	return RET_OK;
}

/**
 *
 * @param cc2500_data
 * @param data
 * @param size
 * @return
 */
retval_t cc2500_send_data(cc2500_data_t* cc2500_data, uint8_t* data, uint16_t size){
	cc2500_status_t status;
	if(cc2500_data == NULL){
		return RET_ERROR;
	}
	if(size > CC2500_MAX_FIFO_LENGTH-1) {		//Alloctae one byte for size
		return RET_ERROR;
	}
	ytMutexWait(cc2500_data->cc2500_mutex_id, YT_WAIT_FOREVER);

	if(cc2500_data->transmitting_packet){
		if((ytGetSysTickMilliSec() - cc2500_data->cc2500_tx_time) > CC2500_SEND_TIMEOUT){
			cc2500_data->transmitting_packet = 0;
		}
		else{
			ytMutexRelease(cc2500_data->cc2500_mutex_id);
			return RET_ERROR;
		}
	}

	cc2500_data->cc2500_tx_time = ytGetSysTickMilliSec();
	cc2500_data->transmitting_packet = 1;

	if (cc2500_data->receiving_packet){
		if((ytGetSysTickMilliSec() - cc2500_data->cc2500_rx_time) > CC2500_RCV_TIMEOUT){
			cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SFRX);
			cc2500_data->receiving_packet = 0;
		}
		cc2500_data->transmitting_packet = 0;
		ytMutexRelease(cc2500_data->cc2500_mutex_id);
		return RET_ERROR;
	}

	cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SIDLE);	//Voy a idle
	do{
		status =  cc2500_arch_get_status(cc2500_data);
		cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SIDLE);
	}while(status.CC2500_STATE != CC2500_STATE_IDLE);

	cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SFTX);	//Primero limpio la tx fifo
	cc2500_arch_write_txfifo(cc2500_data, (uint8_t*) &size, 1);	//Fill the size byte
	cc2500_arch_write_txfifo(cc2500_data, data, size);			//Fill the tx fifo

	cc2500_arch_enable_interrupt(cc2500_data);

	cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_STX);	//Voy al estado tx

	ytMutexRelease(cc2500_data->cc2500_mutex_id);

	return RET_OK;
}

/**
 *
 * @param cc2500_data
 * @return
 */
retval_t cc2500_set_mode_rx(cc2500_data_t* cc2500_data){
	cc2500_status_t status;
	if(cc2500_data == NULL){
		return RET_ERROR;
	}
	ytMutexWait(cc2500_data->cc2500_mutex_id, YT_WAIT_FOREVER);
	if(cc2500_data->transmitting_packet){
		if((ytGetSysTickMilliSec() - cc2500_data->cc2500_tx_time) > CC2500_SEND_TIMEOUT){
			cc2500_data->transmitting_packet = 0;
		}
		else{
			ytMutexRelease(cc2500_data->cc2500_mutex_id);
			return RET_ERROR;
		}
	}

	if(cc2500_data->receiving_packet){
		if((ytGetSysTickMilliSec() - cc2500_data->cc2500_rx_time) > CC2500_RCV_TIMEOUT){
			cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SFRX);
			cc2500_data->receiving_packet = 0;
		}
		else{
			ytMutexRelease(cc2500_data->cc2500_mutex_id);
			return RET_ERROR;
		}
	}


	status =  cc2500_arch_get_status(cc2500_data);

	if(status.CC2500_STATE != CC2500_STATE_RX){
		do{
			status =  cc2500_arch_get_status(cc2500_data);
			cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SIDLE);
		}while(status.CC2500_STATE != CC2500_STATE_IDLE);


		cc2500_arch_enable_interrupt(cc2500_data);

		cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SRX);	//Me pongo en modo RX este donde este
	}
	ytMutexRelease(cc2500_data->cc2500_mutex_id);
	return RET_OK;
}

/**
 *
 * @param cc2500_data
 * @return
 */
retval_t cc2500_set_mode_sleep(cc2500_data_t* cc2500_data){
	if(cc2500_data == NULL){
		return RET_ERROR;
	}

	ytMutexWait(cc2500_data->cc2500_mutex_id, YT_WAIT_FOREVER);


	if(cc2500_data->transmitting_packet){
		if((ytGetSysTickMilliSec() - cc2500_data->cc2500_tx_time) > CC2500_SEND_TIMEOUT){
			cc2500_data->transmitting_packet = 0;
		}
		else{
			ytMutexRelease(cc2500_data->cc2500_mutex_id);
			return RET_ERROR;
		}
	}

	if(cc2500_data->receiving_packet){
		if((ytGetSysTickMilliSec() - cc2500_data->cc2500_rx_time) > CC2500_RCV_TIMEOUT){
			cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SFRX);
			cc2500_data->receiving_packet = 0;
		}
		else{
			ytMutexRelease(cc2500_data->cc2500_mutex_id);
			return RET_ERROR;
		}
	}

	cc2500_data->transmitting_packet = 0;
	cc2500_data->receiving_packet = 0;

	cc2500_arch_disable_interrupt(cc2500_data);

	cc2500_arch_power_off(cc2500_data);

	ytMutexRelease(cc2500_data->cc2500_mutex_id);

	return RET_OK;
}

/**
 *
 * @param cc2500_data
 * @return
 */
retval_t cc2500_set_mode_idle(cc2500_data_t* cc2500_data){
	if(cc2500_data == NULL){
		return RET_ERROR;
	}

	ytMutexWait(cc2500_data->cc2500_mutex_id, YT_WAIT_FOREVER);

	if(cc2500_data->transmitting_packet){
		if((ytGetSysTickMilliSec() - cc2500_data->cc2500_tx_time) > CC2500_SEND_TIMEOUT){
			cc2500_data->transmitting_packet = 0;
		}
		else{
			ytMutexRelease(cc2500_data->cc2500_mutex_id);
			return RET_ERROR;
		}
	}

	if(cc2500_data->receiving_packet){
		if((ytGetSysTickMilliSec() - cc2500_data->cc2500_rx_time) > CC2500_RCV_TIMEOUT){
			cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SFRX);
			cc2500_data->receiving_packet = 0;
		}
		else{
			ytMutexRelease(cc2500_data->cc2500_mutex_id);
			return RET_ERROR;
		}
	}

	cc2500_data->transmitting_packet = 0;
	cc2500_data->receiving_packet = 0;

	cc2500_arch_power_on(cc2500_data);

	cc2500_arch_disable_interrupt(cc2500_data);


	cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SFRX);
	cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SFTX);

	cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SIDLE);

	ytMutexRelease(cc2500_data->cc2500_mutex_id);

	return RET_OK;
}

/**
 *
 * @param cc2500_data
 * @return
 */
retval_t cc2500_power_off(cc2500_data_t* cc2500_data){
	cc2500_arch_disable_interrupt(cc2500_data);
	return cc2500_arch_power_off(cc2500_data);
}

/**
 *
 * @param cc2500_data
 * @return
 */
retval_t cc2500_power_on(cc2500_data_t* cc2500_data){
	return cc2500_arch_power_on(cc2500_data);
}

/**
 *
 * @param cc2500_data
 * @return
 */
float32_t cc2500_get_last_rssi(cc2500_data_t* cc2500_data){
	float32_t rssi;
	int8_t rssi_reg_val;
	cc2500_arch_read_status_reg(cc2500_data, CC2500_RSSI, (uint8_t*) &rssi_reg_val);
	rssi = ((float32_t)rssi_reg_val)/2 - 72;
	return rssi;
}



//Esta rutina es llamada al saltar una interrupcion del cc2500. OJO! No es llamada por la interrupcion sino por una hebra. Es decir por la capa phy
//Si se da el caso de que la interrupcion recibe un paquete, este packete se guarda en la variable pasada por parametro

uint16_t cc2500_irq_routine(cc2500_data_t* cc2500_data){

	uint16_t ret = 0;
	uint8_t rx_fifo_bytes;

	ytMutexWait(cc2500_data->cc2500_mutex_id, YT_WAIT_FOREVER);

	if(ytGpioPinGet(cc2500_data->gdo2_int_pin)){	//High level
		if(!cc2500_data->transmitting_packet){
			cc2500_data->receiving_packet = 1;
			cc2500_data->cc2500_rx_time = ytGetSysTickMilliSec();
		}

	}
	else{	//Low level
		if(cc2500_data->transmitting_packet){
			cc2500_data->transmitting_packet = 0;
			cc2500_arch_disable_interrupt(cc2500_data);
			ret |= RET_PCKT_SENT;
		}
		else if (cc2500_data->receiving_packet){
			cc2500_arch_read_status_reg(cc2500_data, CC2500_RXBYTES, &rx_fifo_bytes);
			if(rx_fifo_bytes & 0x80){	//FIFO OVERFLOW
				cc2500_data->receiving_packet = 0;
				cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SFRX);
				cc2500_arch_send_cmd_strobe(cc2500_data, CC2500_STROBE_SRX);
				ytMutexRelease(cc2500_data->cc2500_mutex_id);
				return ret;
			}

			else{
				ret |= RET_PCKT_RCV;
			}

		}
	}

	ytMutexRelease(cc2500_data->cc2500_mutex_id);
	return ret;
}

/**
 *
 * @param cc2500_data
 * @param num_recv_bytes
 * @return
 */
retval_t cc2500_read_num_rcv_bytes(cc2500_data_t* cc2500_data, uint8_t* num_recv_bytes){
	ytMutexWait(cc2500_data->cc2500_mutex_id, YT_WAIT_FOREVER);

	cc2500_arch_read_rxfifo(cc2500_data, num_recv_bytes, 1);

	ytMutexRelease(cc2500_data->cc2500_mutex_id);

	return RET_OK;
}
/**
 *
 * @param cc2500_data
 * @param packet_data
 * @param size
 * @return
 */
retval_t cc2500_read_rcv_data(cc2500_data_t* cc2500_data, uint8_t* packet_data, uint16_t size){
	if(size > CC2500_MAX_FIFO_LENGTH-1){
		return RET_ERROR;
	}
	ytMutexWait(cc2500_data->cc2500_mutex_id, YT_WAIT_FOREVER);

	cc2500_arch_read_rxfifo(cc2500_data, packet_data, size);
	cc2500_data->receiving_packet = 0;

	ytMutexRelease(cc2500_data->cc2500_mutex_id);
	return RET_OK;
}

/**
 *
 * @param cc2500_data
 * @param size
 * @return
 */
retval_t cc2500_flush_last_rcv_data(cc2500_data_t* cc2500_data, uint16_t size){
	uint8_t* null_read;
	if(size > CC2500_MAX_FIFO_LENGTH){
		return RET_ERROR;
	}
	ytMutexWait(cc2500_data->cc2500_mutex_id, YT_WAIT_FOREVER);
	null_read = (uint8_t*) ytMalloc(size);
	cc2500_arch_read_rxfifo(cc2500_data, null_read, size);
	cc2500_data->receiving_packet = 0;
	ytFree(null_read);
	ytMutexRelease(cc2500_data->cc2500_mutex_id);
	return RET_OK;

}
