/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * at86rf215_config_mrfsk.c
 *
 *  Created on: 10/09/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file at86rf215_config_mrfsk.c
 */

#include "system_api.h"
#include "at86rf215_arch.h"
#include "at86rf215_core.h"
#include "at86rf215_const.h"
#include "at86rf215_utils.h"
#include "at86rf215_config.h"
#include "at86rf215_config_mrfsk.h"

#define SR_COL		0		// Symbol Rate Column in Settings Matrices

// Preemphasis settings for FSK direct modulation
// Values extracted from Table 6-57 of AT86RF215 Datasheet
// Columns: Symbol Rate | FSKPE0 | FSKPE1 | FSKPE2
#define PE_ROWS	6
#define PE_COLS	4
#define FSKPE0_COL	1		// FSKPE0 column in preemphasis_settings matrix
#define FSKPE1_COL	2		// FSKPE1 column in preemphasis_settings matrix
#define FSKPE2_COL	3		// FSKPE2 column in preemphasis_settings matrix
static const uint16_t preemphasis_settings[PE_ROWS][PE_COLS] = {
{ 50,  0x02, 0x03, 0xFC }, // Symbol Rate 50  kHz
{ 100, 0x0E, 0x0F, 0xF0 }, // Symbol Rate 100 kHz
{ 150, 0x3C, 0x3F, 0xC0 }, // Symbol Rate 150 kHz
{ 200, 0x74, 0x7F, 0x80 }, // Symbol Rate 200 kHz
{ 300, 0x05, 0x3C, 0xC3 }, // Symbol Rate 300 kHz
{ 400, 0x13, 0x29, 0xC7 }, // Symbol Rate 400 kHz
};


// Symbol Rate Settings for FSK direct modulation (v.3)
// Values extracted from Table 6-51 of AT86RF215 Datasheet
// Columns: Symbol Rate | FSKC1.SRATE | TXDFE.SR | RXDFE.SR
#define SR_ROWS	6
#define SR_COLS	4
#define SRATE_COL		1	// FSKC1.SRATE column in sr_mrfsk_settings matrix
#define TXDFE_SR_COL	2	// TXDFE.SR column in sr_mrfsk_settings matrix
#define RXDFE_SR_COL	3	// RXDFE.SR column in sr_mrfsk_settings matrix
static const uint16_t sr_mrfsk_settings[SR_ROWS][SR_COLS] = {
{ 50,  0x00, 0x08, 0x0A }, // Symbol Rate 50  kHz
{ 100, 0x01, 0x04, 0x05 }, // Symbol Rate 100 kHz
{ 150, 0x02, 0x02, 0x04 }, // Symbol Rate 150 kHz
{ 200, 0x03, 0x02, 0x04 }, // Symbol Rate 200 kHz
{ 300, 0x04, 0x01, 0x02 }, // Symbol Rate 300 kHz
{ 400, 0x05, 0x01, 0x02 }, // Symbol Rate 400 kHz
};


// Recommended Configuration of the Transmitter Front-End for MRFSK
// Values extracted from Tables 6-53 and 6-54 of AT86RF215 Datasheet
// Columns 1 to 3 correspond to Table 6-53 (Modulation Index 0.5)
// Columns 4 to 6 correspond to Table 6-54 (Modulation Index 1  )
// Columns: Symbol Rate | TXCUTC.PARAMP | TXCUTC.LPFCUT | TXDFE.RCUT | TXCUTC.PARAMP | TXCUTC.LPFCUT | TXDFE.RCUT
#define TXFE_ROWS				6
#define TXFE_COLS				7
#define TXFE_MOD_INDEX_OFFSET	3	// This offset should be added to the column index when the modulation index is 1.
#define PARAMP_COL				1	// TXCUTC.PARAMP column in txfe_mrfsk_settings matrix
#define LPFCUT_COL				2	// TXCUTC.LPFCUT column in txfe_mrfsk_settings matrix
#define TXDFE_RCUT_COL			3	// TXDFE.RCUT column in txfe_mrfsk_settings matrix
static const uint16_t txfe_mrfsk_settings[TXFE_ROWS][TXFE_COLS] = {
{ 50,  0x03, 0x00, 0x00, 0x03, 0x00, 0x04 }, // Symbol Rate 50  kHz
{ 100, 0x02, 0x01, 0x00, 0x02, 0x03, 0x04 }, // Symbol Rate 100 kHz
{ 150, 0x02, 0x03, 0x00, 0x02, 0x05, 0x04 }, // Symbol Rate 150 kHz
{ 200, 0x02, 0x04, 0x00, 0x02, 0x06, 0x04 }, // Symbol Rate 200 kHz
{ 300, 0x01, 0x06, 0x00, 0x01, 0x08, 0x04 }, // Symbol Rate 300 kHz
{ 400, 0x01, 0x07, 0x00, 0x01, 0x09, 0x04 }, // Symbol Rate 400 kHz
};


// Recommended Configuration of the Sub-1 GHz Receiver Front-End for MRFSK
// Values extracted from Tables 6-60 and 6-62 of AT86RF215 Datasheet
// Columns 1 to 5 correspond to Table 6-60 (Modulation Index 0.5)
// Columns 6 to 10 correspond to Table 6-62 (Modulation Index 1  )
// Columns: Symbol Rate | RXBWC.BW | RXBWC.IFS | RXDFE.RCUT | AGCC.AVGS | AGCS.TGT | RXBWC.BW | RXBWC.IFS | RXDFE.RCUT | AGCC.AVGS | AGCS.TGT |
#define RXFE_ROWS				6
#define RXFE_COLS				11
#define RXFE_MOD_INDEX_OFFSET	5	// This offset should be added to the column index when the modulation index is 1.
#define BW_COL					1
#define IFS_COL					2
#define RXDFE_RCUT_COL			3
#define AVGS_COL				4
#define TGT_COL					5
static const uint16_t rxfe_09_mrfsk_settings[RXFE_ROWS][RXFE_COLS] = {
{ 50,  RF_BW160KHZ_IF250KHZ,  0x00, 0x00, 0x00, 0x01, RF_BW160KHZ_IF250KHZ,   0x00, 0x01, 0x00, 0x01 }, // Symbol Rate 50  kHz
{ 100, RF_BW200KHZ_IF250KHZ,  0x00, 0x00, 0x00, 0x01, RF_BW320KHZ_IF500KHZ,   0x00, 0x01, 0x00, 0x01 }, // Symbol Rate 100 kHz
{ 150, RF_BW320KHZ_IF500KHZ,  0x00, 0x00, 0x00, 0x01, RF_BW400KHZ_IF500KHZ,   0x00, 0x01, 0x00, 0x01 }, // Symbol Rate 150 kHz
{ 200, RF_BW320KHZ_IF500KHZ,  0x00, 0x01, 0x00, 0x01, RF_BW500KHZ_IF500KHZ,   0x01, 0x02, 0x00, 0x01 }, // Symbol Rate 200 kHz
{ 300, RF_BW500KHZ_IF500KHZ,  0x01, 0x00, 0x00, 0x01, RF_BW630KHZ_IF1000KHZ,  0x00, 0x01, 0x00, 0x01 }, // Symbol Rate 300 kHz
{ 400, RF_BW630KHZ_IF1000KHZ, 0x00, 0x00, 0x00, 0x01, RF_BW1000KHZ_IF1000KHZ, 0x01, 0x01, 0x00, 0x01 }, // Symbol Rate 400 kHz
};


// Recommended Configuration of the 2.4 GHz Receiver Front-End for MRFSK
// Values extracted from Tables 6-61 and 6-63 of AT86RF215 Datasheet
// Columns 1 to 5 correspond to Table 6-61 (Modulation Index 0.5)
// Columns 6 to 10 correspond to Table 6-63 (Modulation Index 1  )
// Columns: Symbol Rate | RXBWC.BW | RXBWC.IFS | RXDFE.RCUT | AGCC.AVGS | AGCS.TGT | RXBWC.BW | RXBWC.IFS | RXDFE.RCUT | AGCC.AVGS | AGCS.TGT |
static const uint16_t rxfe_24_mrfsk_settings[RXFE_ROWS][RXFE_COLS] = {
{ 50,  RF_BW160KHZ_IF250KHZ,  0x00, 0x00, 0x00, 0x01, RF_BW200KHZ_IF250KHZ,   0x00, 0x01, 0x00, 0x01 }, // Symbol Rate 50  kHz
{ 100, RF_BW200KHZ_IF250KHZ,  0x00, 0x00, 0x00, 0x01, RF_BW400KHZ_IF500KHZ,   0x00, 0x01, 0x00, 0x01 }, // Symbol Rate 100 kHz
{ 150, RF_BW320KHZ_IF500KHZ,  0x00, 0x00, 0x00, 0x01, RF_BW630KHZ_IF1000KHZ,  0x00, 0x02, 0x00, 0x01 }, // Symbol Rate 150 kHz
{ 200, RF_BW400KHZ_IF500KHZ,  0x00, 0x01, 0x00, 0x01, RF_BW630KHZ_IF1000KHZ,  0x00, 0x03, 0x00, 0x01 }, // Symbol Rate 200 kHz
{ 300, RF_BW630KHZ_IF1000KHZ, 0x00, 0x00, 0x00, 0x01, RF_BW800KHZ_IF1000KHZ,  0x00, 0x01, 0x00, 0x01 }, // Symbol Rate 300 kHz
{ 400, RF_BW800KHZ_IF1000KHZ, 0x00, 0x01, 0x00, 0x01, RF_BW1000KHZ_IF1000KHZ, 0x01, 0x02, 0x00, 0x01 }, // Symbol Rate 400 kHz
};



/**
 *
 * @param at86rf215_data
 * @param at86rf215_init_config
 * @return
 */
retval_t at86rf215_set_mrfsk_datarate(at86rf215_data_t* at86rf215_data){

	uint16_t rf24_std_offset = 0x0000;
	uint32_t symbol_rate = 0;
	uint8_t i;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}


	if(at86rf215_data->config.modulation == BFSK){

		if(at86rf215_data->config.ch0_center_freq >= 2400000){		// Frequency Band 2400-2483.5 MHz

			// As indicated in Table 134 of IEEE 802.15.4-2012, the only valid
			// data rates in this band are 50, 150 and 200 kbps
			if(at86rf215_data->config.baud_rate !=  50000 &&
			   at86rf215_data->config.baud_rate != 150000 &&
			   at86rf215_data->config.baud_rate != 200000 ){
				return RET_ERROR;
			}

			// Get the index of the corresponding Symbol Rate settings
			symbol_rate = at86rf215_data->config.baud_rate; 			// In BFSK, each symbol is composed of 1 bit.
			for(i = 0; i < SR_ROWS; i++){
				if(sr_mrfsk_settings[i][SR_COL] == (symbol_rate/1000)){
					break;
				}
			}
			if(i == SR_ROWS){
				return RET_ERROR;
			}


			// MR-FSK Symbol Rate
			if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC1 + rf24_std_offset), FSKC1_SRATE_MASK, \
					FSKC1_SRATE_SHIFT, (uint8_t)sr_mrfsk_settings[i][SRATE_COL]) != RET_OK){
				return RET_ERROR;
			}

			// TX Sample Rate
			if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_TXDFE + rf24_std_offset), TXDFE_SR_MASK, \
					TXDFE_SR_SHIFT, (uint8_t)sr_mrfsk_settings[i][TXDFE_SR_COL]) != RET_OK){
				return RET_ERROR;
			}

			// RX Sample Rate
			if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_RXDFE + rf24_std_offset), RXDFE_SR_MASK, \
					RXDFE_SR_SHIFT, (uint8_t)sr_mrfsk_settings[i][RXDFE_SR_COL]) != RET_OK){
				return RET_ERROR;
			}
		}

		else{
			return RET_ERROR;	// TODO
		}
	}

	else if(at86rf215_data->config.modulation == QFSK){
		return RET_ERROR; 		// TODO
	}

	else{
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_default_mrfsk_phy_config(at86rf215_data_t* at86rf215_data){

	uint16_t rf24_std_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->config.modulation != BFSK &&
	   at86rf215_data->config.modulation != QFSK ){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	// *************************************************************************
	// *************************************************************************
	// IEEE 802.15.4-2012 PHY: MR-FSK Default Configuration according to
	// Tables 68d, 131, 134 of the standard
	// *************************************************************************
	// *************************************************************************


	// FSK Configuration Byte 0 ------------------------------------------------

	// FSK Bandwidth Time Product: Without effect when direct modulation is enabled.
	// When the direct modulation (TXDFE.DM = 1, FSKDM.EN = 1) is enabled together with pre-emphasis (FSKDM.PE=1),
	// adjusting the bandwidth time product BT by register FSKC0.BT is without effect.

	// FSK Modulation Index Scale (MIDXS): Configured in at86rf215_set_modulation.

	// FSK Modulation Index (MIDX): Configured in at86rf215_set_modulation.

	// FSK Modulation Order (MORD): Configured in at86rf215_set_modulation.

	// -------------------------------------------------------------------------


	// FSK Configuration Byte 1 ------------------------------------------------

	// Frequency Inversion: Disabled
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC1 + rf24_std_offset), FSKC1_FI_MASK, FSKC1_FI_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// FSK Preamble Length (10-bit): 16 octets (00.0001.0000 -> FSKPLH = 00 and FSKPLL = 0001.0000)
	// If you want to reduce this length, check the Table 6-64 of the AT86RF215 Datasheet.
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC1 + rf24_std_offset), FSKC1_FSKPLH_MASK, FSKC1_FSKPLH_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKPLL + rf24_std_offset), FSKPLL_FSKPLL_MASK, FSKPLL_FSKPLL_SHIFT, (uint8_t)0x10) != RET_OK){
		return RET_ERROR;
	}

	// MR-FSK Symbol Rate (SRATE): Configured in at86rf215_set_mrfsk_datarate, which is called from at86rf215_set_datarate.

	// -------------------------------------------------------------------------


	// FSK Configuration Byte 2 ------------------------------------------------

	// Preamble Detection Mode: PD does not take RSSI values into account
	//  It is recommended to enable this sub-register only if the preamble length is less than 8 octets.
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC2 + rf24_std_offset), FSKC2_PDTM_MASK, FSKC2_PDTM_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// Receiver Override: Receiver restarted by >18dB stronger frame
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC2 + rf24_std_offset), FSKC2_RXO_MASK, FSKC2_RXO_SHIFT, (uint8_t)0x02) != RET_OK){
		return RET_ERROR;
	}

	// Receiver Preamble Time Out: Disabled
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC2 + rf24_std_offset), FSKC2_RXPTO_MASK, FSKC2_RXPTO_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// Mode Switch Enable: Disable
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC2 + rf24_std_offset), FSKC2_MSE_MASK, FSKC2_MSE_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// FEC Scheme: NRNSC
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC2 + rf24_std_offset), FSKC2_FECS_MASK, FSKC2_FECS_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// FEC Interleaving Enable: Enabled
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC2 + rf24_std_offset), FSKC2_FECIE_MASK, FSKC2_FECIE_SHIFT, (uint8_t)0x01) != RET_OK){
		return RET_ERROR;
	}

	// -------------------------------------------------------------------------


	// FSK Configuration Byte 3 ------------------------------------------------

	// Start-of-Frame Delimiter (SFD) Detection Threshold: 8 (Default)
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC3 + rf24_std_offset), FSKC3_SFDT_MASK, FSKC3_SFDT_SHIFT, (uint8_t)0x08) != RET_OK){
		return RET_ERROR;
	}

	// Preamble Detection Threshold (PDT): 5 (Default)
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC3 + rf24_std_offset), FSKC3_PDT_MASK, FSKC3_PDT_SHIFT, (uint8_t)0x05) != RET_OK){
		return RET_ERROR;
	}

	// -------------------------------------------------------------------------


	// FSK Configuration Byte 4 ------------------------------------------------

	// SFD Quantization: 0 (Default)
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC4 + rf24_std_offset), FSKC4_SFDQ_MASK, FSKC4_SFDQ_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// SFD 32Bit: Search for two 16-bit SFD instead of a single 32-bit SFD
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC4 + rf24_std_offset), FSKC4_SFD32_MASK, FSKC4_SFD32_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// RAW mode Reversal Bit: MSB first (Default)
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC4 + rf24_std_offset), FSKC4_RAWRBIT_MASK, FSKC4_RAWRBIT_SHIFT, (uint8_t)0x01) != RET_OK){
		return RET_ERROR;
	}

	// Configuration of PPDU with SFD1: Coded IEEE mode
	// The term “coded” means that FEC is enabled, whereas “uncoded” means that FEC is disabled.
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC4 + rf24_std_offset), FSKC4_CSFD1_MASK, FSKC4_CSFD1_SHIFT, (uint8_t)0x02) != RET_OK){
		return RET_ERROR;
	}

	// Configuration of PPDU with SFD0: Coded IEEE mode
	// The term “coded” means that FEC is enabled, whereas “uncoded” means that FEC is disabled.
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC4 + rf24_std_offset), FSKC4_CSFD0_MASK, FSKC4_CSFD0_SHIFT, (uint8_t)0x02) != RET_OK){
		return RET_ERROR;
	}

	// -------------------------------------------------------------------------


	// FSK SFD0 for coded (b15-b0): 0111.0010.1111.0110 (Table 131) ------------
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKSFD0L + rf24_std_offset), FSKSFD0L_FSKSFD0L_MASK, FSKSFD0L_FSKSFD0L_SHIFT, (uint8_t)0xF6) != RET_OK){
		return RET_ERROR;
	}
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKSFD0H + rf24_std_offset), FSKSFD0H_FSKSFD0H_MASK, FSKSFD0H_FSKSFD0H_SHIFT, (uint8_t)0x72) != RET_OK){
		return RET_ERROR;
	}
	// -------------------------------------------------------------------------


	// FSK SFD1 for coded (b15-b0): 1011.0100.1100.0110 (Table 131) ------------
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKSFD1L + rf24_std_offset), FSKSFD1L_FSKSFD1L_MASK, FSKSFD1L_FSKSFD1L_SHIFT, (uint8_t)0xC6) != RET_OK){
		return RET_ERROR;
	}
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKSFD1H + rf24_std_offset), FSKSFD1H_FSKSFD1H_MASK, FSKSFD1H_FSKSFD1H_SHIFT, (uint8_t)0xB4) != RET_OK){
		return RET_ERROR;
	}


	// FSK PHR TX Information --------------------------------------------------

	// SFD type: SFD0 is used
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKPHRTX + rf24_std_offset), FSKPHRTX_SFD_MASK, FSKPHRTX_SFD_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// Data Whitening: PSDU data whitening enabled
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKPHRTX + rf24_std_offset), FSKPHRTX_DW_MASK, FSKPHRTX_DW_SHIFT, (uint8_t)0x01) != RET_OK){
		return RET_ERROR;
	}

	// Reserved PHR Bit 2: 0
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKPHRTX + rf24_std_offset), FSKPHRTX_RB2_MASK, FSKPHRTX_RB2_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// Reserved PHR Bit 1: 0
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKPHRTX + rf24_std_offset), FSKPHRTX_RB1_MASK, FSKPHRTX_RB1_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// -------------------------------------------------------------------------


	// FSK Direct Modulation ---------------------------------------------------

	// FSK Direct Modulation Enable: Enabled
	// If this sub-register and sub-register RFxx_TXDFE.DM are set to 1, the FSK direct modulation is enabled.
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKDM + rf24_std_offset), FSKDM_EN_MASK, FSKDM_EN_SHIFT, (uint8_t)0x01) != RET_OK){
		return RET_ERROR;
	}

	// Transmitter TX Digital Front-End - Direct Modulation: Enabled
	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_TXDFE + rf24_std_offset), TXDFE_DM_MASK, TXDFE_DM_SHIFT, (uint8_t)0x01) != RET_OK){
		return RET_ERROR;
	}

	// FSK Preemphasis Configuration
	if(at86rf215_set_mrfsk_preemphasis(at86rf215_data) != RET_OK){
		return RET_ERROR;
	}

	// -------------------------------------------------------------------------


	// FSK Reduced Power Consumption -------------------------------------------

	// FSK Reduced Power Consumption Enable: Disabled
	// When ENABLED, TX and RX device have different settings for the preamble length.
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKRPC + rf24_std_offset), FSKRPC_EN_MASK, FSKRPC_EN_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// FSK Reduced Power Consumption Base Time: 32.0 us
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKRPC + rf24_std_offset), FSKRPC_BASET_MASK, FSKRPC_BASET_SHIFT, (uint8_t)0x06) != RET_OK){
		return RET_ERROR;
	}

	// FSK Reduced Power Consumption On Time (Figure 6-23): 320 us / BASET = 10 (0x0A)
	// Min. Preamble Length at 200 kHz symbol rate is 8 octets (Table 6-64)
	// For BFSK, symbol and data rates are equal, so the data rate is 200 kbps.
	// ton = tmin = (1/200 kbps) * (8 octets * 8 bit-per-octet) = 320 us
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKRPCONT + rf24_std_offset), FSKRPCONT_FSKRPCONT_MASK, FSKRPCONT_FSKRPCONT_SHIFT, (uint8_t)0x0A) != RET_OK){
		return RET_ERROR;
	}

	// FSK Reduced Power Consumption Off Time: 5120 us / BASET = 160 (0xA0)
	// RPC with on-off time 1:16. For ton = 320 us, toff = 16 * 320 us = 5120 us
	// According to section 6.10.5.7, 2ton + toff <= tTX-Preamble
	// To meet that condition, for a minimal RX preamble length,
	// the TX preamble length should be 144 octets.
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKRPCOFFT + rf24_std_offset), FSKRPCOFFT_FSKRPCOFFT_MASK, FSKRPCOFFT_FSKRPCOFFT_SHIFT, (uint8_t)0xA0) != RET_OK){
		return RET_ERROR;
	}

	// -------------------------------------------------------------------------

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param at86rf215_config
 * @return
 */
retval_t at86rf215_set_mrfsk_preemphasis(at86rf215_data_t* at86rf215_data){

	uint16_t rf24_std_offset = 0x0000;
	uint32_t symbol_rate = 0;
	uint8_t i = 0;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	if(at86rf215_data->config.modulation == BFSK){
		// It is recommended to apply preemphasis only for symbol rates above 100kHz.
		symbol_rate = at86rf215_data->config.baud_rate; 			// In BFSK, each symbol is composed of 1 bit.
		if(symbol_rate < 100000){
			// FSK Preemphasis: FSK direct modulation NOT preemphasized
			if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKDM + rf24_std_offset), FSKDM_PE_MASK, FSKDM_PE_SHIFT, (uint8_t)0x00) != RET_OK){
				return RET_ERROR;
			}
			return RET_OK;
		}
	}

	else if(at86rf215_data->config.modulation == QFSK){
		// It is recommended to apply preemphasis only for symbol rates above 100kHz.
		symbol_rate = (at86rf215_data->config.baud_rate / 2);	// In QFSK, each symbol is composed of 2 bits.
		if(symbol_rate < 100000){
			// FSK Preemphasis: FSK direct modulation NOT preemphasized
			if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKDM + rf24_std_offset), FSKDM_PE_MASK, FSKDM_PE_SHIFT, (uint8_t)0x00) != RET_OK){
				return RET_ERROR;
			}
			return RET_OK;
		}
	}

	else{
		return RET_ERROR;
	}


	// FSK Preemphasis: FSK direct modulation preemphasized
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKDM + rf24_std_offset), FSKDM_PE_MASK, FSKDM_PE_SHIFT, (uint8_t)0x01) != RET_OK){
		return RET_ERROR;
	}

	// Get the index of the corresponding preemphasis settings
	for(i = 0; i < PE_ROWS; i++){
		if(preemphasis_settings[i][SR_COL] == (symbol_rate/1000)){
			break;
		}
	}
	if(i == PE_ROWS){
		return RET_ERROR;
	}


	// FSK Preemphasis 0 -------------------------------------------------------
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKPE0 + rf24_std_offset), FSKPE0_FSKPE0_MASK, \
			FSKPE0_FSKPE0_SHIFT, (uint8_t)preemphasis_settings[i][FSKPE0_COL]) != RET_OK){
		return RET_ERROR;
	}
	// -------------------------------------------------------------------------

	// FSK Preemphasis 1 -------------------------------------------------------
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKPE1 + rf24_std_offset), FSKPE1_FSKPE1_MASK, \
			FSKPE1_FSKPE1_SHIFT, (uint8_t)preemphasis_settings[i][FSKPE1_COL]) != RET_OK){
		return RET_ERROR;
	}
	// -------------------------------------------------------------------------

	// FSK Preemphasis 2 -------------------------------------------------------
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKPE2 + rf24_std_offset), FSKPE2_FSKPE2_MASK, \
			FSKPE2_FSKPE2_SHIFT, (uint8_t)preemphasis_settings[i][FSKPE2_COL]) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param at86rf215_config
 * @return
 */
retval_t at86rf215_set_mrfsk_output_power(at86rf215_data_t* at86rf215_data){

	uint16_t rf24_std_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->config.modulation != BFSK &&
	   at86rf215_data->config.modulation != QFSK ){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	// Power Amplifier Current Control: Power Amplifier Current Reduction by about 22 mA (3dB reduction of max. small signal gain)
	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_PAC + rf24_std_offset), PAC_PACUR_MASK, \
			PAC_PACUR_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// Transmitter Output Power
	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_PAC + rf24_std_offset), PAC_TXPWR_MASK, \
			PAC_TXPWR_SHIFT, (uint8_t)at86rf215_data->config.output_pwr) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param at86rf215_config
 * @return
 */
retval_t at86rf215_default_mrfsk_txfe_config(at86rf215_data_t* at86rf215_data){

	uint16_t rf24_std_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->config.modulation != BFSK &&
	   at86rf215_data->config.modulation != QFSK ){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	// Recommended Configuration of the Transmitter Front-End (TXFE) for MRFSK
	// Values extracted from Tables 6-53 and 6-54 of AT86RF215 Datasheet
	if(at86rf215_recommended_mrfsk_txfe_config(at86rf215_data) != RET_OK){
		return RET_ERROR;
	}

	// Transmitter Filter Cutoff Control and PA Ramp Time and ------------------

	// Power Amplifier Ramp Time (TXCUTC.PARAMP): Configured in at86rf215_recommended_mrfsk_txfe_config.

	// Transmitter cut-off frequency (TXCUTC.LPFCUT): Configured in at86rf215_recommended_mrfsk_txfe_config.

	// -------------------------------------------------------------------------


	// Transmitter TX Digital Front-End -----------------------------------------

	// TX filter relative to the cut-off frequency (TXDFE.RCUT): Configured in at86rf215_recommended_mrfsk_txfe_config.

	// Direct Modulation: Enabled in at86rf215_default_mrfsk_phy_config();

	// TX Sample Rate: Configured in at86rf215_set_mrfsk_datarate.

	// -------------------------------------------------------------------------


	// Transmitter Power Amplifier Control -------------------------------------

	// Power Amplifier Current Control (RFn_PAC.PACUR): Configured in at86rf215_set_mrfsk_output_power.

	// Transmitter Output Power (RFn_PAC.TXPWR): Configured in at86rf215_set_mrfsk_output_power.

	// -------------------------------------------------------------------------


	// Transceiver Auxiliary Settings ------------------------------------------

	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_AUXS + rf24_std_offset), AUXS_PAVC_MASK, AUXS_PAVC_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// -------------------------------------------------------------------------


	// Transceiver I/Q Data Interface Configuration Register 0 -----------------

	if(at86rf215_write_subreg(at86rf215_data, SR_RF_IQIFC0_EEC, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// -------------------------------------------------------------------------

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param at86rf215_config
 * @return
 */
retval_t at86rf215_recommended_mrfsk_txfe_config(at86rf215_data_t* at86rf215_data){

	uint16_t rf24_std_offset = 0x0000;
	uint32_t symbol_rate = 0;
	uint8_t i;
	uint8_t modulation_index = 0;
	uint8_t col_offset = 0;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->config.modulation != BFSK &&
	   at86rf215_data->config.modulation != QFSK ){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	if(at86rf215_data->config.ch0_center_freq >= 2400000){		// Frequency Band 2400-2483.5 MHz

		// As indicated in Table 134 of IEEE 802.15.4-2012, the only valid
		// data rates in this band are 50, 150 and 200 kbps
		if(at86rf215_data->config.baud_rate !=  50000 &&
		   at86rf215_data->config.baud_rate != 150000 &&
		   at86rf215_data->config.baud_rate != 200000 ){
			return RET_ERROR;
		}

		// Determine the Symbol Rate base on the modulation order.
		if(at86rf215_data->config.modulation == BFSK){
			symbol_rate = at86rf215_data->config.baud_rate; 			// In BFSK, each symbol is composed of 1 bit.
		}
		else{
			symbol_rate = (at86rf215_data->config.baud_rate / 2);	// In QFSK, each symbol is composed of 2 bits.
		}

		// Get the index of the corresponding Symbol Rate settings
		for(i = 0; i < TXFE_ROWS; i++){
			if(txfe_mrfsk_settings[i][SR_COL] == (symbol_rate/1000)){
				break;
			}
		}
		if(i == TXFE_ROWS){
			return RET_ERROR;
		}

		// Select the Column Offset of the txfe_mrfsk_settings matrix based on the modulation index.
		if(at86rf215_read_subreg(at86rf215_data, (RG_BBC0_FSKC0 + rf24_std_offset), FSKC0_MIDX_MASK, FSKC0_MIDX_SHIFT, &modulation_index) != RET_OK){
			return RET_ERROR;
		}
		if (modulation_index == 0x01){		// FSK Modulation Index: 0.5
			col_offset = 0;
		}
		else if (modulation_index == 0x03){	// FSK Modulation Index: 1.0
			col_offset = TXFE_MOD_INDEX_OFFSET;
		}
		else{
			return RET_ERROR;
		}


		// Power Amplifier Ramp Time
		if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_TXCUTC + rf24_std_offset), TXCUTC_PARAMP_MASK, \
				TXCUTC_PARAMP_SHIFT, (uint8_t)txfe_mrfsk_settings[i][PARAMP_COL + col_offset]) != RET_OK){
			return RET_ERROR;
		}

		// Transmitter cut-off frequency
		if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_TXCUTC + rf24_std_offset), TXCUTC_LPFCUT_MASK, \
				TXCUTC_LPFCUT_SHIFT, (uint8_t)txfe_mrfsk_settings[i][LPFCUT_COL + col_offset]) != RET_OK){
			return RET_ERROR;
		}

		// TX filter relative to the cut-off frequency
		if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_TXDFE + rf24_std_offset), TXDFE_RCUT_MASK, \
				TXDFE_RCUT_SHIFT, (uint8_t)txfe_mrfsk_settings[i][TXDFE_RCUT_COL + col_offset]) != RET_OK){
			return RET_ERROR;
		}
	}

	else{
		return RET_ERROR;	// TODO
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param at86rf215_config
 * @return
 */
retval_t at86rf215_default_mrfsk_rxfe_config(at86rf215_data_t* at86rf215_data){

	uint16_t rf24_std_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->config.modulation != BFSK &&
	   at86rf215_data->config.modulation != QFSK ){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	// Recommended Configuration of the Transmitter Front-End (TXFE) for MRFSK
	// Values extracted from Tables 6-53 and 6-54 of AT86RF215 Datasheet
	if(at86rf215_recommended_mrfsk_rxfe_config(at86rf215_data) != RET_OK){
		return RET_ERROR;
	}


	// Receiver Filter Bandwidth Control ---------------------------------------

	// IF Inversion: Default setting for normal operation
	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_RXBWC + rf24_std_offset), RXBWC_IFI_MASK, RXBWC_IFI_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// IF Shift (RXBWC.IFS): Configured in at86rf215_recommended_mrfsk_rxfe_config.

	// Receiver Bandwidth (RXBWC.BW): Configured in at86rf215_recommended_mrfsk_rxfe_config.

	// -------------------------------------------------------------------------


	// Receiver Filter Bandwidth Control ---------------------------------------

	// RX Filter Relative Cut-Off Frequency (RFn_RXDFE.RCUT): Configured in at86rf215_recommended_mrfsk_rxfe_config.

	// RX Sample Rate (RFn_RXDFE.SR): Configured in at86rf215_set_mrfsk_datarate.

	// -------------------------------------------------------------------------


	// Receiver AGC Control 0 --------------------------------------------------

	// AGC Input: The filtered front end signal is used (Default)
	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_AGCC + rf24_std_offset), AGCC_AGCI_MASK, AGCC_AGCI_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// AGC Average Time in Number of Samples (RFn_AGCC.AVGS): Configured in at86rf215_recommended_mrfsk_rxfe_config.

	// AGC Reset (RFn_AGCC.RST): A value of one resets the AGC and sets the maximum receiver gain.
	// 			  				 After command execution, the bit is reset automatically.

	// AGC Freeze Status (RFn_AGCC.FRZS): AGC released (Default)
	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_AGCC + rf24_std_offset), AGCC_FRZS_MASK, AGCC_FRZS_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// AGC Freeze Control (RFn_AGCC.FRZC): AGC released (Default)
	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_AGCC + rf24_std_offset), AGCC_FRZC_MASK, AGCC_FRZC_SHIFT, (uint8_t)0x00) != RET_OK){
		return RET_ERROR;
	}

	// AGC Enable (RFn_AGCC.EN): Enabled.
	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_AGCC + rf24_std_offset), AGCC_EN_MASK, AGCC_EN_SHIFT, (uint8_t)0x01) != RET_OK){
		return RET_ERROR;
	}

	// -------------------------------------------------------------------------


	// Receiver AGCG -----------------------------------------------------------

	// AGC Target Level (RFn_AGCS.TGT): Configured in at86rf215_recommended_mrfsk_rxfe_config.

	// RX Gain Control Word (RFn_AGCS.GCW): Not used as AGC is enabled (RFn_AGCC.EN == 1).

	// -------------------------------------------------------------------------


	// Energy Detection Configuration ------------------------------------------

	// Energy Detection Mode (RFn_EDC.EDM): Energy detection measurement is automatically
	//										triggered if the AGC is held by the internal
	//										baseband or by setting bit FRZC.
	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_EDC + rf24_std_offset), EDC_EDM_MASK, EDC_EDM_SHIFT, (uint8_t)RF_EDAUTO) != RET_OK){
		return RET_ERROR;
	}

	// -------------------------------------------------------------------------


	// Receiver Energy Detection Averaging Duration ----------------------------
	// The averaging duration should be greater than the minimum AGC
	// update time, see Table 6-12 on page 55 for further information.

	// If AGCC.AVGS is always 0 in rxfe_09_mrfsk_settings and rxfe_24_mrfsk_settings
	// and AGCC.AGCI is 0 in this default configuration, then AGC Update Time (Tu)
	// is always smaller than 66 us for all the RXDFE.SR values.

	// Averaging time (T) should be always greater than the AGC Update Time (Tu),
	// that for this configuration is always smaller than 66 us, then:
	// T = DF * DTB = 10 * 8us = 80 us > Tu = 65 us -> Valid configuration

	// Receiver Energy Detection Duration Factor (RFn_EDD.DF): 10 (0x0A)
	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_EDD + rf24_std_offset), EDD_DF_MASK, EDD_DF_SHIFT, (uint8_t)0x0A) != RET_OK){
		return RET_ERROR;
	}

	// Receiver Energy Detection Average Duration Time Basis (RFn_EDD.DTB):  8us (0x01) (Default)
	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_EDD + rf24_std_offset), EDD_DTB_MASK, EDD_DTB_SHIFT, (uint8_t)0x01) != RET_OK){
		return RET_ERROR;
	}

	// -------------------------------------------------------------------------

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param at86rf215_config
 * @return
 */
retval_t at86rf215_recommended_mrfsk_rxfe_config(at86rf215_data_t* at86rf215_data){

	uint16_t rf24_std_offset = 0x0000;
	uint32_t symbol_rate = 0;
	uint8_t i;
	uint8_t modulation_index = 0;
	uint8_t col_offset = 0;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->config.modulation != BFSK &&
	   at86rf215_data->config.modulation != QFSK ){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	if(at86rf215_data->config.ch0_center_freq >= 2400000){		// Frequency Band 2400-2483.5 MHz

		// As indicated in Table 134 of IEEE 802.15.4-2012, the only valid
		// data rates in this band are 50, 150 and 200 kbps
		if(at86rf215_data->config.baud_rate !=  50000 &&
		   at86rf215_data->config.baud_rate != 150000 &&
		   at86rf215_data->config.baud_rate != 200000 ){
			return RET_ERROR;
		}

		// Determine the Symbol Rate base on the modulation order.
		if(at86rf215_data->config.modulation == BFSK){
			symbol_rate = at86rf215_data->config.baud_rate; 			// In BFSK, each symbol is composed of 1 bit.
		}
		else{
			symbol_rate = (at86rf215_data->config.baud_rate / 2);	// In QFSK, each symbol is composed of 2 bits.
		}

		// Get the index of the corresponding Symbol Rate settings
		for(i = 0; i < RXFE_ROWS; i++){
			if(rxfe_09_mrfsk_settings[i][SR_COL] == (symbol_rate/1000)){
				break;
			}
		}
		if(i == RXFE_ROWS){
			return RET_ERROR;
		}

		// Select the Column Offset of the rxfe_XX_mrfsk_settings matrix based on the modulation index.
		if(at86rf215_read_subreg(at86rf215_data, (RG_BBC0_FSKC0 + rf24_std_offset), FSKC0_MIDX_MASK, FSKC0_MIDX_SHIFT, &modulation_index) != RET_OK){
			return RET_ERROR;
		}
		if (modulation_index == 0x01){		// FSK Modulation Index: 0.5
			col_offset = 0;
		}
		else if (modulation_index == 0x03){	// FSK Modulation Index: 1.0
			col_offset = RXFE_MOD_INDEX_OFFSET;
		}
		else{
			return RET_ERROR;
		}


		// Select the corresponding transceiver settings
		if(at86rf215_data->band == RF09){

			// Receiver Bandwidth
			if(at86rf215_write_subreg(at86rf215_data, SR_RF09_RXBWC_BW, (uint8_t)rxfe_09_mrfsk_settings[i][BW_COL + col_offset]) != RET_OK){
				return RET_ERROR;
			}

			// IF Shift
			if(at86rf215_write_subreg(at86rf215_data, SR_RF09_RXBWC_IFS, (uint8_t)rxfe_09_mrfsk_settings[i][IFS_COL + col_offset]) != RET_OK){
				return RET_ERROR;
			}

			// RX Filter Relative Cut-Off Frequency
			if(at86rf215_write_subreg(at86rf215_data, SR_RF09_RXDFE_RCUT, (uint8_t)rxfe_09_mrfsk_settings[i][RXDFE_RCUT_COL + col_offset]) != RET_OK){
				return RET_ERROR;
			}

			// AGC Average Time in Number of Samples
			if(at86rf215_write_subreg(at86rf215_data, SR_RF09_AGCC_AVGS, (uint8_t)rxfe_09_mrfsk_settings[i][AVGS_COL + col_offset]) != RET_OK){
				return RET_ERROR;
			}

			// AGC Average Time in Number of Samples
			if(at86rf215_write_subreg(at86rf215_data, SR_RF09_AGCS_TGT, (uint8_t)rxfe_09_mrfsk_settings[i][TGT_COL + col_offset]) != RET_OK){
				return RET_ERROR;
			}
		}

		else if(at86rf215_data->band == RF24){

			// Receiver Bandwidth
			if(at86rf215_write_subreg(at86rf215_data, SR_RF24_RXBWC_BW, (uint8_t)rxfe_24_mrfsk_settings[i][BW_COL + col_offset]) != RET_OK){
				return RET_ERROR;
			}

			// IF Shift
			if(at86rf215_write_subreg(at86rf215_data, SR_RF24_RXBWC_IFS, (uint8_t)rxfe_24_mrfsk_settings[i][IFS_COL + col_offset]) != RET_OK){
				return RET_ERROR;
			}

			// RX Filter Relative Cut-Off Frequency
			if(at86rf215_write_subreg(at86rf215_data, SR_RF24_RXDFE_RCUT, (uint8_t)rxfe_24_mrfsk_settings[i][RXDFE_RCUT_COL + col_offset]) != RET_OK){
				return RET_ERROR;
			}

			// AGC Average Time in Number of Samples
			if(at86rf215_write_subreg(at86rf215_data, SR_RF24_AGCC_AVGS, (uint8_t)rxfe_24_mrfsk_settings[i][AVGS_COL + col_offset]) != RET_OK){
				return RET_ERROR;
			}

			// AGC Target Level
			if(at86rf215_write_subreg(at86rf215_data, SR_RF24_AGCS_TGT, (uint8_t)rxfe_24_mrfsk_settings[i][TGT_COL + col_offset]) != RET_OK){
				return RET_ERROR;
			}
		}

		else{
			return RET_ERROR;
		}
	}

	else{
		return RET_ERROR;	// TODO
	}

	return RET_OK;
}
