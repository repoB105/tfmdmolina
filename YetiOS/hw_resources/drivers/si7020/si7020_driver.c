/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * at30ts75_driver.c
 *
 *  Created on: 1 de jun. de 2018
 *      Author: Ildefonso Aspera Olmo  <iaspera@b105.upm.es>
 *
 */
/*
 * si7020_driver.c
 *
 *  Created on: 1 jun. 2018
 *      Author: iaspera
 */


#include "device.h"
#include "platform-conf.h"
#include "system_api.h"
#include "arm_math.h"



#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#if ENABLE_SI7020_DRIVER

#include "si7020_platform.h"


/* Device name and Id */
#define SI7020_DEVICE_NAME SI7020_DEV

//static uint32_t i2c_fd = 0;
static uint8_t dev_opened = 0;

/* Device Init functions */
static retval_t si7020_init(void);
static retval_t si7020_exit(void);


/* Device driver operation functions declaration */
static retval_t si7020_open(device_t* device, dev_file_t* filep);
static retval_t si7020_close(dev_file_t* filep);
static size_t si7020_read(dev_file_t* filep, uint8_t* prx, size_t size);
static size_t si7020_write(dev_file_t* filep, uint8_t* ptx, size_t size);
static retval_t si7020_ioctl(dev_file_t* filep, uint16_t request, void* args);


/* Define driver operations */
static driver_ops_t si7020_driver_ops = {
	.open = si7020_open,
	.close = si7020_close,
	.read = si7020_read,
	.write = si7020_write,
	.ioctl = si7020_ioctl,
};


/**
 *
 * @return
 */
static retval_t si7020_init(void){
	uint8_t write_data[2];

	uint32_t i2c_fd = ytOpen(SI7020_I2C_DEV, 0);
	ytIoctl(i2c_fd, SET_SLAVE_ADDR, (void*) SI7020_I2C_ADDR);

	write_data[0] = 0xE6;		// Config register
	write_data[1] = 0xBB;		// 11 bit resolution and heater off
	ytWrite(i2c_fd, write_data, 2);

	ytClose(i2c_fd);

	dev_opened = 0;
	if(registerDevice(SI7020_DEVICE_ID, &si7020_driver_ops, SI7020_DEVICE_NAME) != RET_OK){
		return RET_ERROR;
	}

	PRINTF(">SI7020 Init Done\r\n");

	return RET_OK;
}


/**
 *
 * @return
 */
static retval_t si7020_exit(void){
	uint32_t i2c_fd;
	uint8_t write_data[2];
	while(dev_opened);

	unregisterDevice(SI7020_DEVICE_ID, SI7020_DEVICE_NAME);

	i2c_fd = 0;
	dev_opened = 0;


	PRINTF(">SI7020 Exit Done\r\n");

	return RET_OK;
}





/**
 *
 * @param device
 * @param filep
 * @return
 */
static retval_t si7020_open(device_t* device, dev_file_t* filep){
	uint32_t i2c_fd;
	uint8_t data[2];
	if(device->device_state != DEV_STATE_INIT){
		return RET_ERROR;
	}
	if(dev_opened){
		return RET_ERROR;
	}
	dev_opened++;

   return RET_OK;
}


/**
 *
 * @param filep
 * @return
 */
static retval_t si7020_close(dev_file_t* filep){
	uint32_t i2c_fd;
	uint8_t data[2];
	if(dev_opened){
		dev_opened--;

		i2c_fd = ytOpen(SI7020_I2C_DEV, 0);
		ytIoctl(i2c_fd, SET_SLAVE_ADDR, (void*) SI7020_I2C_ADDR);

		data[0] = 0xE6;		// Write in config reg
		data[1] = 0xBB;		// 11bit res, heater disabled

		ytWrite(i2c_fd, data, 2);

		ytClose(i2c_fd);
		return RET_OK;
	}
	return RET_ERROR;
}


/**
 *
 * @param filep
 * @param ptx
 * @param size
 * @return
 */
static size_t si7020_write(dev_file_t* filep, uint8_t* ptx, size_t size){
	return 0;
}


/**
 *
 * @param filep
 * @param prx
 * @param size
 * @return
 */
static size_t si7020_read(dev_file_t* filep, uint8_t* prx, size_t size){	// Only 1 temperature & humidity for each function call
	uint32_t i2c_fd;
	float32_t* out_val_temp = (float32_t*) prx;
	float32_t* out_val_hum  = (float32_t*) prx+1;
	uint8_t data[4];
	uint16_t read_temp;
	uint16_t read_hum;

	if(size != 2){
		return 0;
	}
	if(!dev_opened){
		return 0;
	}

	i2c_fd = ytOpen(SI7020_I2C_DEV, 0);
	ytIoctl(i2c_fd, SET_SLAVE_ADDR, (void*) SI7020_I2C_ADDR);

	data[0] = 0xF5;
	ytWrite(i2c_fd, data, 1);		// Start humidity conversion
	ytDelay(10);					// Delay for 11bit conversion of hum+temp


	ytRead(i2c_fd, data, 2);		// Read humidity register
	data[2] = 0xE0;
	ytWrite(i2c_fd, data+2, 2);
	ytRead(i2c_fd, data+2, 2);		// Read temperature register from previous RH measurement



	read_hum  = (data[0]<<8) + data[1];
	read_temp = (data[2]<<8) + data[3];

	(*out_val_hum) =  125 * (float32_t)read_hum / 65536  -  6;
	(*out_val_temp) = 175.72 * (float32_t)read_temp / 65536  -  46.85;


	ytClose(i2c_fd);
	return size;
}


/**
 *
 * @param filep
 * @param request
 * @param args
 * @return
 */
static retval_t si7020_ioctl(dev_file_t* filep, uint16_t request, void* args){
	return RET_ERROR;
}

/* Register the init functions in the kernel Init system */
init_device_1(si7020_init);
exit_device_1(si7020_exit);

#endif
