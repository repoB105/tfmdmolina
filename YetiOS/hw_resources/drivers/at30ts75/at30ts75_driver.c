/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * at30ts75_driver.c
 *
 *  Created on: 12 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file at30ts75_driver.c
 */


#include "device.h"
#include "platform-conf.h"
#include "system_api.h"
#include "arm_math.h"


#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#if ENABLE_AT30TS75_DRIVER

#include "at30ts75_platform.h"

#define TEMP_RESOLUTION		0.0039f
/* Device name and Id */
#define AT30TS75_DEVICE_NAME AT30TS75_DEV

//static uint32_t i2c_fd = 0;
static uint8_t dev_opened = 0;

/* Device Init functions */
static retval_t at30ts75_init(void);
static retval_t at30ts75_exit(void);


/* Device driver operation functions declaration */
static retval_t at30ts75_open(device_t* device, dev_file_t* filep);
static retval_t at30ts75_close(dev_file_t* filep);
static size_t at30ts75_read(dev_file_t* filep, uint8_t* prx, size_t size);
static size_t at30ts75_write(dev_file_t* filep, uint8_t* ptx, size_t size);
static retval_t at30ts75_ioctl(dev_file_t* filep, uint16_t request, void* args);


/* Define driver operations */
static driver_ops_t at30ts75_driver_ops = {
	.open = at30ts75_open,
	.close = at30ts75_close,
	.read = at30ts75_read,
	.write = at30ts75_write,
	.ioctl = at30ts75_ioctl,
};


/**
 *
 * @return
 */
static retval_t at30ts75_init(void){
	uint8_t write_data[2];

	uint32_t i2c_fd = ytOpen(AT30TS75_I2C_DEV, 0);
	ytIoctl(i2c_fd, SET_SLAVE_ADDR, (void*) AT30TS75_I2C_ADDR);

	write_data[0] = 0x01;
	write_data[1] = 0x31;		//High resolution and Shutdown mode
	ytWrite(i2c_fd, write_data, 2);

	ytClose(i2c_fd);

	dev_opened = 0;
	if(registerDevice(AT30TS75_DEVICE_ID, &at30ts75_driver_ops, AT30TS75_DEVICE_NAME) != RET_OK){
		return RET_ERROR;
	}

	PRINTF(">AT30TS75 Init Done\r\n");

	return RET_OK;
}

/**
 *
 * @return
 */
static retval_t at30ts75_exit(void){
	uint32_t i2c_fd;
	uint8_t write_data[2];
	while(dev_opened);

	unregisterDevice(AT30TS75_DEVICE_ID, AT30TS75_DEVICE_NAME);

	i2c_fd = ytOpen(AT30TS75_I2C_DEV, 0);
	ytIoctl(i2c_fd, SET_SLAVE_ADDR, (void*) AT30TS75_I2C_ADDR);

	write_data[0] = 0x01;
	write_data[1] = 0x01;		//Shutdown mode
	ytWrite(i2c_fd, write_data, 2);

	ytClose(i2c_fd);

	i2c_fd = 0;
	dev_opened = 0;


	PRINTF(">AT30TS75 Exit Done\r\n");

	return RET_OK;
}

/**
 *
 * @param device
 * @param filep
 * @return
 */
static retval_t at30ts75_open(device_t* device, dev_file_t* filep){
	uint32_t i2c_fd;
	uint8_t data[2];
	if(device->device_state != DEV_STATE_INIT){
		return RET_ERROR;
	}
	if(dev_opened){
		return RET_ERROR;
	}
	i2c_fd = ytOpen(AT30TS75_I2C_DEV, 0);
	ytIoctl(i2c_fd, SET_SLAVE_ADDR, (void*) AT30TS75_I2C_ADDR);

	dev_opened++;
	data[0] = 0x01;
	ytWrite(i2c_fd, data, 1);			//Set pointer to register 0x01

	ytRead(i2c_fd, &data[1], 1);		//Read register value

	data[1] = data[1] & 0xFE;
	ytWrite(i2c_fd, data, 2);			//Enable sensor

	data[0] = 0x00;
	ytWrite(i2c_fd, data, 1);			//Set pointer to register 0x00


	ytClose(i2c_fd);
	return RET_OK;
}

/**
 *
 * @param filep
 * @return
 */
static retval_t at30ts75_close(dev_file_t* filep){
	uint32_t i2c_fd;
	uint8_t data[2];
	if(dev_opened){
		dev_opened--;

		i2c_fd = ytOpen(AT30TS75_I2C_DEV, 0);
		ytIoctl(i2c_fd, SET_SLAVE_ADDR, (void*) AT30TS75_I2C_ADDR);

		data[0] = 0x01;
		ytWrite(i2c_fd, data, 1);			//Set pointer to register 0x01

		ytRead(i2c_fd, &data[1], 1);		//Read register value

		data[1] = data[1] | 0x01;
		ytWrite(i2c_fd, data, 2);			//Disable sensor

		ytClose(i2c_fd);
		return RET_OK;
	}
	return RET_ERROR;
}

/**
 *
 * @param filep
 * @param ptx
 * @param size
 * @return
 */
static size_t at30ts75_write(dev_file_t* filep, uint8_t* ptx, size_t size){
	return 0;
}


/**
 *
 * @param filep
 * @param prx
 * @param size
 * @return
 */
static size_t at30ts75_read(dev_file_t* filep, uint8_t* prx, size_t size){	//Only 1 temperature read each time allowed
	uint32_t i2c_fd;
	float32_t* out_val = (float32_t*) prx;
	uint8_t data[4];
	int16_t* read_temp;
	if(size != 1){
		return 0;
	}
	if(!dev_opened){
		return 0;
	}

	i2c_fd = ytOpen(AT30TS75_I2C_DEV, 0);
	ytIoctl(i2c_fd, SET_SLAVE_ADDR, (void*) AT30TS75_I2C_ADDR);

	ytRead(i2c_fd, data, 2);		//Read temp register

	data[2] = data[1];
	data[3] = data[0];
	read_temp = (int16_t*) &data[2];


	(*out_val) = TEMP_RESOLUTION * ((float32_t) (*read_temp));
	ytClose(i2c_fd);
	return size;
}


/**
 *
 * @param filep
 * @param request
 * @param args
 * @return
 */
static retval_t at30ts75_ioctl(dev_file_t* filep, uint16_t request, void* args){
	return RET_ERROR;
}

/* Register the init functions in the kernel Init system */
init_device_1(at30ts75_init);
exit_device_1(at30ts75_exit);

#endif
