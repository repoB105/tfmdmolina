/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * cs5490_driver.c
 *
 *  Created on: 12 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file cs5490_driver.c
 */


#include "device.h"
#include "platform-conf.h"
#include "system_api.h"
#include "arm_math.h"


#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#if ENABLE_CS5490_DRIVER

#include "cs5490_platform.h"
#include "uart_driver.h"

/* Device name and Id */
#define CS5490_DEVICE_NAME CS5490_DEV

// Define command types
#define CS5490_CMD_FUNC_READ_REG		0x00
#define CS5490_CMD_FUNC_WRITE_REG		0x40
#define CS5490_CMD_FUNC_PAGE_SEL		0x80
#define CS5490_CMD_FUNC_INSTRUCTION		0xC0
#define UART_FAST						14746	 //115200 * 524288/4096000;
#define FACTORY_PROGRAM_SPEED			0
#define CALIBRATE_I_RMS					0


static uint8_t dev_opened = 0;
uint16_t waitUartRxProcId;
struct cs5490_sharedData_t {
	uint32_t uartRXSyncSemph_id;
	uint32_t uartRXReadySemph_id;
	uint32_t filedes;
	uint8_t nBytes;
	uint8_t bufferRX[4];
}cs5490_sharedData;




/* Device Init functions */
static retval_t cs5490_init(void);
static retval_t cs5490_exit(void);


/* Device driver operation functions declaration */
static retval_t cs5490_open(device_t* device, dev_file_t* filep);
static retval_t cs5490_close(dev_file_t* filep);
static size_t cs5490_read(dev_file_t* filep, uint8_t* prx, size_t size);
static size_t cs5490_write(dev_file_t* filep, uint8_t* ptx, size_t size);
static retval_t cs5490_ioctl(dev_file_t* filep, uint16_t request, void* args);


/* Define driver operations */
static driver_ops_t cs5490_driver_ops = {
		.open = cs5490_open,
		.close = cs5490_close,
		.read = cs5490_read,
		.write = cs5490_write,
		.ioctl = cs5490_ioctl,
};


/**
 *
 * @return
 */
static retval_t cs5490_init(void){
	uint8_t write_data[4];
	//uint32_t temp = 2516582; 						// 0.6 * 2^22;
	cs5490_sharedData.filedes = ytOpen(UART_DEV_2, 0);
	//cs5490_sharedData.uartRXSyncSemph_id = ytSemaphoreCreate(1);
	//ytSemaphoreWait(cs5490_sharedData.uartRXSyncSemph_id, YT_WAIT_FOREVER);
	//cs5490_sharedData.uartRXReadySemph_id = ytSemaphoreCreate(1);
	//ytSemaphoreWait(cs5490_sharedData.uartRXReadySemph_id, YT_WAIT_FOREVER);

	ytIoctl(cs5490_sharedData.filedes, SET_UART_SPEED, (void*) 600);

#if FACTORY_PROGRAM_SPEED
	UART_HandleTypeDef huart2;

	huart2.Instance = USART2;
	huart2.Init.BaudRate = 600;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}


	write_data[0] = CS5490_CMD_FUNC_PAGE_SEL + 0;		// Select page 0
	ytWrite(uart_fd, write_data, 1);

	write_data[0] = CS5490_CMD_FUNC_WRITE_REG + 7;
	write_data[3] = 0x02;								// Default
	write_data[2] = (UART_FAST>>8)&0xFF;				// Baud rate MSB
	write_data[1] = UART_FAST&0xFF;						// Baud rate LSB
	ytWrite(uart_fd, write_data, 4);


	huart2.Instance = USART2;
	huart2.Init.BaudRate = 115200;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

#endif


	write_data[0] = CS5490_CMD_FUNC_INSTRUCTION + 0x01;	// SW reset
	ytWrite(cs5490_sharedData.filedes, write_data, 1);
	ytDelay(10);

	write_data[0] = CS5490_CMD_FUNC_PAGE_SEL + 0;		// Select page 0
	ytWrite(cs5490_sharedData.filedes, write_data, 1);
	ytDelay(10);

	write_data[0] = CS5490_CMD_FUNC_WRITE_REG + 0;
	write_data[3] = 0xC0;								// Default
	write_data[2] = 0x20;								// Default
	write_data[1] = 0x20;								// I Gain -> 50
	ytWrite(cs5490_sharedData.filedes, write_data, 4);

	write_data[0] = CS5490_CMD_FUNC_WRITE_REG + 1;
	write_data[3] = 0x11;								// Enable EPG block, open drain DO
	write_data[2] = 0xEE;								// Default
	write_data[1] = 0xE0;								// EPG block output
	ytWrite(cs5490_sharedData.filedes, write_data, 4);


	write_data[0] = CS5490_CMD_FUNC_PAGE_SEL + 16;		// Select page 16
	ytWrite(cs5490_sharedData.filedes, write_data, 1);

	write_data[0] = CS5490_CMD_FUNC_WRITE_REG + 0;
	write_data[3] = 0x50;								// + energy only
	write_data[2] = 0x02;								// Default
	write_data[1] = 0x00;								//
	ytWrite(cs5490_sharedData.filedes, write_data, 4);

	write_data[0] = CS5490_CMD_FUNC_INSTRUCTION + 0x15;	// Begin continuous conversion
	ytWrite(cs5490_sharedData.filedes, write_data, 1);
	ytDelay(10);

	ytClose(cs5490_sharedData.filedes);

	dev_opened = 0;
	if(registerDevice(CS5490_DEVICE_ID, &cs5490_driver_ops, CS5490_DEVICE_NAME) != RET_OK){
		return RET_ERROR;
	}

	PRINTF(">CS5490 Init Done\r\n");

#if CALIBRATE_I_RMS

	/* Gain Calibration of I */
	// Set Igain to Ical/Imax

	temp = 2516582; 						// Igain = 0.6 * 2^22;

	write_data[0] = CS5490_CMD_FUNC_PAGE_SEL + 16;		// Select page 16
	ytWrite(uart_fd, write_data, 1);

	write_data[0] = CS5490_CMD_FUNC_WRITE_REG + 33;
	write_data[3] = (temp>>16)&0xFF;
	write_data[2] = (temp>>8) &0xFF;
	write_data[1] = temp&0xFF;
	ytWrite(uart_fd, write_data, 4);


	// Set Tsettle to 2000ms
	temp = 2516582; 						// Igain = 0.6 * 2^22;

	// Apply ref signal to input

	// During calibration, Irms is divided by the Scale register




#endif


	return RET_OK;
}


/**
 *
 * @return
 */
static retval_t cs5490_exit(void){

	while(dev_opened);

	unregisterDevice(CS5490_DEVICE_ID, CS5490_DEVICE_NAME);

	dev_opened = 0;
	//ytSemaphoreDelete(cs5490_sharedData.uartRXSyncSemph_id);
	//ytSemaphoreDelete(cs5490_sharedData.uartRXReadySemph_id);
	PRINTF(">CS5490 Exit Done\r\n");

	return RET_OK;
}


/**
 *
 * @param device
 * @param filep
 * @return
 */
static retval_t cs5490_open(device_t* device, dev_file_t* filep){

	if(device->device_state != DEV_STATE_INIT){
		return RET_ERROR;
	}
	if(dev_opened){
		return RET_ERROR;
	}

	dev_opened++;

	return RET_OK;
}


/**
 *
 * @param filep
 * @return
 */
static retval_t cs5490_close(dev_file_t* filep){

	if(dev_opened){
		dev_opened--;
		return RET_OK;
	}
	return RET_ERROR;
}


/**
 *
 * @param filep
 * @param ptx
 * @param size
 * @return
 */
static size_t cs5490_write(dev_file_t* filep, uint8_t* ptx, size_t size){
	return 0;
}



/**
 *
 * @param filep
 * @param prx
 * @param size
 * @return
 */
static size_t cs5490_read(dev_file_t* filep, uint8_t* prx, size_t size){	

	int32_t powerAvg;
	uint32_t currentRMS;
	uint32_t voltageRMS;
	int32_t reactiveAvg;
	int32_t currentPeak;
	int32_t voltagePeak;
	uint32_t aparentPower;
	int32_t powerFactor;
	struct cs5490_data_t* cs5490Data = (struct cs5490_data_t*)prx;
	cs5490_sharedData.filedes = ytOpen(CS5490_DEV, 0);
	cs5490_sharedData.nBytes = 3;


	if(size != sizeof(*cs5490Data)){
		return 0;
	}
	if(!dev_opened){
		return 0;
	}


	cs5490_sharedData.filedes = ytOpen(UART_DEV_2, 0);

	cs5490_sharedData.bufferRX[0] = CS5490_CMD_FUNC_PAGE_SEL + 16;		// Select page 16
	ytWrite(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, 1);
	ytDelay(10);
	// Power Average
	cs5490_sharedData.bufferRX[0] = CS5490_CMD_FUNC_READ_REG + 5;		// Select addr 5
	ytWrite(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, 1);
	ytRead(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, cs5490_sharedData.nBytes);
	// OLD ytRead(uart_fd, data, 3);							//Read Pavg
	powerAvg = ((cs5490_sharedData.bufferRX[2]<<16) + (cs5490_sharedData.bufferRX[1]<<8) + (cs5490_sharedData.bufferRX[0]));
	powerAvg |= (powerAvg & 0x800000) ? 0xFF000000 : 0;	// Sign extension
	cs5490Data->powerAvg = ((float)powerAvg * 0.25 * 1498 * 0.05 / 0.003) / 8388608;

	// Current RMS
	cs5490_sharedData.bufferRX[0] = CS5490_CMD_FUNC_READ_REG + 6;		// Select addr 6
	ytWrite(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, 1);
	ytRead(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, cs5490_sharedData.nBytes);
	// OLD ytRead(uart_fd, data, 3);
	currentRMS = ((cs5490_sharedData.bufferRX[2]<<16) + (cs5490_sharedData.bufferRX[1]<<8) + (cs5490_sharedData.bufferRX[0]));
	cs5490Data->currentRMS = ((float)currentRMS * 0.05 / 0.003) / 16777216;

	// Voltage RMS
	cs5490_sharedData.bufferRX[0] = CS5490_CMD_FUNC_READ_REG + 7;		// Select addr 7
	ytWrite(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, 1);
	ytRead(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, cs5490_sharedData.nBytes);
	// OLD ytRead(uart_fd, data, 3);
	voltageRMS = ((cs5490_sharedData.bufferRX[2]<<16) + (cs5490_sharedData.bufferRX[1]<<8) + (cs5490_sharedData.bufferRX[0]));
	cs5490Data->voltageRMS  = (float)voltageRMS * 0.25 * 1498 / 16777216;

	// Reactive Average
	cs5490_sharedData.bufferRX[0] = CS5490_CMD_FUNC_READ_REG + 14;		// Select addr 14
	ytWrite(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, 1);
	ytRead(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, cs5490_sharedData.nBytes);
	ytDelay(100);													// La transmision de 3 bytes deberia tardar sobre 50ms
	// OLD ytRead(uart_fd, data, 3);
	reactiveAvg = ((cs5490_sharedData.bufferRX[2]<<16) + (cs5490_sharedData.bufferRX[1]<<8) + (cs5490_sharedData.bufferRX[0]));
	reactiveAvg |= (reactiveAvg & 0x800000) ? 0xFF000000 : 0;	// Sign extension
	cs5490Data->reactiveAvg = ((float)reactiveAvg * 0.25 * 1498 * 0.05 / 0.003) / 8388608;

	// Aparent Power
	cs5490_sharedData.bufferRX[0] = CS5490_CMD_FUNC_READ_REG + 20;		// Select addr 20
	ytWrite(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, 1);
	ytRead(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, cs5490_sharedData.nBytes);
	// OLD ytRead(uart_fd, data, 3);
	aparentPower = ((cs5490_sharedData.bufferRX[2]<<16) + (cs5490_sharedData.bufferRX[1]<<8) + (cs5490_sharedData.bufferRX[0]));
	cs5490Data->aparentPower = (float)0;					// Not currently used in the library

	// Power Factor
	cs5490_sharedData.bufferRX[0] = CS5490_CMD_FUNC_READ_REG + 21;		// Select addr 21
	ytWrite(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, 1);
	ytRead(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, cs5490_sharedData.nBytes);
	// OLD ytRead(uart_fd, data, 3);
	powerFactor = ((cs5490_sharedData.bufferRX[2]<<16) + (cs5490_sharedData.bufferRX[1]<<8) + (cs5490_sharedData.bufferRX[0]));
	powerFactor |= (powerFactor & 0x800000) ? 0xFF000000 : 0;	// Sign extension
	cs5490Data->powerFactor = (float)powerFactor / 8388608;

	cs5490_sharedData.bufferRX[0] = CS5490_CMD_FUNC_PAGE_SEL + 0;		// Select page 0
	ytWrite(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, 1);

	// Current Peak
	cs5490_sharedData.bufferRX[0] = CS5490_CMD_FUNC_READ_REG + 37;		// Select addr 37
	ytWrite(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, 1);
	ytRead(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, cs5490_sharedData.nBytes);
	// OLD ytRead(uart_fd, data, 3);
	currentPeak = ((cs5490_sharedData.bufferRX[2]<<16) + (cs5490_sharedData.bufferRX[1]<<8) + (cs5490_sharedData.bufferRX[0]));
	currentPeak |= (currentPeak & 0x800000) ? 0xFF000000 : 0;	// Sign extension
	cs5490Data->currentPeak = ((float)currentPeak * 0.05 / 0.003) / 8388608;

	// Voltage Peak
	cs5490_sharedData.bufferRX[0] = CS5490_CMD_FUNC_READ_REG + 36;		// Select addr 36
	ytWrite(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, 1);
	ytRead(cs5490_sharedData.filedes, cs5490_sharedData.bufferRX, cs5490_sharedData.nBytes);
	// OLD ytRead(uart_fd, data, 3);
	voltagePeak = ((cs5490_sharedData.bufferRX[2]<<16) + (cs5490_sharedData.bufferRX[1]<<8) + (cs5490_sharedData.bufferRX[0]));
	voltagePeak |= (voltagePeak & 0x800000) ? 0xFF000000 : 0;	// Sign extension
	cs5490Data->voltagePeak = (float)voltagePeak * 0.25 * 1498 / 8388608;

	ytClose(cs5490_sharedData.filedes);

	/**out_val++ = powerAvg;
	 *out_val++ = currentRMS;
	 *out_val++ = voltageRMS;
	 *out_val++ = reactiveAvg;
	 *out_val++ = currentPeak;
	 *out_val++ = voltagePeak;
	 *out_val++ = aparentPower;
	 *out_val = powerFactor;*/


	return size;
}


/**
 *
 * @param filep
 * @param request
 * @param args
 * @return
 */
static retval_t cs5490_ioctl(dev_file_t* filep, uint16_t request, void* args){
	return RET_ERROR;
}

/* Register the init functions in the kernel Init system */
init_device_1(cs5490_init);
exit_device_1(cs5490_exit);

#endif
