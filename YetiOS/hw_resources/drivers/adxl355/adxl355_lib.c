/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adxl355_lib.c
 *
 *  Created on: 8 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adxl355_lib.c
 */

#include <adxl355_lib.h>
#include "platform-conf.h"

#define READ_MASK	0x01
#define WRITE_MASK	0xFE

#define INIT_ODR	ADXL355_ODR_4000Hz
#define INIT_SCALE	ADXL355_FULLSCALE_2
#define INIT_MODE	ADXL355_POWER_DOWN
#define INIT_AXIS	(AXIS_X_ENABLE | AXIS_Y_ENABLE | AXIS_Z_ENABLE)	//All axis enabled by default

#if ENABLE_NETSTACK_ARCH

/**
 *
 * @return
 */
adxl355_data_t* new_adxl355_data(void){
#if !ENABLE_SPI_DRIVER
	return NULL;
#else
	adxl355_data_t* new_adxl355_data = (adxl355_data_t*) ytMalloc(sizeof(adxl355_data_t));
	new_adxl355_data->spi_fd = 0;
	new_adxl355_data->mode = ADXL355_NO_INIT;
	new_adxl355_data->odr = ADXL355_ODR_NO_INIT;
	new_adxl355_data->scale = ADXL355_FULLSCALE_NO_INIT;

	return new_adxl355_data;
#endif
}

/**
 *
 * @param adxl_data
 * @return
 */
retval_t delete_adxl355_data(adxl355_data_t* adxl355_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	ytFree(adxl355_data);
	return RET_OK;
#endif
}

/**
 *
 * @param adxl355_data
 * @return
 */
retval_t ADXL355_init(adxl355_data_t* adxl355_data, gpioPin_t adxl355_csPin, char* spi_dev){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	if(adxl355_data == NULL){
		return RET_ERROR;
	}
	if(ADXL355_config_spi_device(adxl355_data, adxl355_csPin, spi_dev) != RET_OK){
		return RET_ERROR;
	}

	if(ADXL355_CheckDevice(adxl355_data) != RET_OK){
		ytClose(adxl355_data->spi_fd);
		return RET_ERROR;
	}
	if(ADXL355_SetODR(adxl355_data, INIT_ODR) != RET_OK){
		ytClose(adxl355_data->spi_fd);
		return RET_ERROR;
	}
	if(ADXL355_SetFullScale(adxl355_data, INIT_SCALE) != RET_OK){
		ytClose(adxl355_data->spi_fd);
		return RET_ERROR;
	}
	if(ADXL355_SetAxis(adxl355_data, INIT_AXIS) != RET_OK){
		ytClose(adxl355_data->spi_fd);
		return RET_ERROR;
	}
	if(ADXL355_SetMode(adxl355_data, INIT_MODE) != RET_OK){
		ytClose(adxl355_data->spi_fd);
		return RET_ERROR;
	}

	return RET_OK;
#endif
}

/**
 *
 * @param adxl355_data
 * @return
 */
retval_t ADXL355_deinit(adxl355_data_t* adxl355_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	if(ADXL355_SetMode(adxl355_data, ADXL355_POWER_DOWN) != RET_OK){
		return RET_ERROR;
	}
	adxl355_data->mode = ADXL355_NO_INIT;
	adxl355_data->odr = ADXL355_ODR_NO_INIT;
	adxl355_data->scale = ADXL355_FULLSCALE_NO_INIT;

	ytClose(adxl355_data->spi_fd);
	adxl355_data->spi_fd = 0;
	return RET_OK;
#endif
}

/**
 *
 * @param adxl355_data
 * @param odr
 * @return
 */
retval_t ADXL355_SetODR(adxl355_data_t* adxl355_data, ADXL355_ODR_t odr){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	uint8_t value;
	if(adxl355_data == NULL){
		return RET_ERROR;
	}
	if(adxl355_data->odr == odr){
	  return RET_ERROR;
	}
	if( ADXL355_ReadReg(adxl355_data, ADXL355_POWER_CTL, &value) != RET_OK){	//Set to standby to change odr
	return RET_ERROR;
	}

	value |= 0x03;

	if( ADXL355_WriteReg(adxl355_data, ADXL355_POWER_CTL, value) != RET_OK ){
	return RET_ERROR;
	}


	if( ADXL355_ReadReg(adxl355_data, ADXL355_FILTER, &value) != RET_OK ){	//ODR
	return RET_ERROR;
	}

	value &= 0xF0;
	value |= odr<<ADXL355_ODR_BIT;

	if( ADXL355_WriteReg(adxl355_data, ADXL355_FILTER, value) != RET_OK ){
	return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_POWER_CTL, &value) != RET_OK ){	//Set to standby to change range
	return RET_ERROR;
	}

	value &= 0xFE;			//Set to active mode

	if( ADXL355_WriteReg(adxl355_data, ADXL355_POWER_CTL, value) != RET_OK){
	return RET_ERROR;
	}
	adxl355_data->odr = odr;
	return RET_OK;
#endif
}


/**
 *
 * @param adxl355_data
 * @param md
 * @return
 */
retval_t ADXL355_SetMode(adxl355_data_t* adxl355_data, ADXL355_Mode_t md) {
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	uint8_t value;
	if(adxl355_data == NULL){
		return RET_ERROR;
	}
	if(adxl355_data->mode == md){
	  return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_POWER_CTL, &value) != RET_OK){
	return RET_ERROR;
	}

	value &= 0xFE;
	value |= md<<ADXL355_STANDBY;

	if( ADXL355_WriteReg(adxl355_data, ADXL355_POWER_CTL, value) != RET_OK){
	return RET_ERROR;
	}
	adxl355_data->mode = md;
	return RET_OK;
#endif
}

/**
 *
 * @param adxl355_data
 * @param axis
 * @return
 */
retval_t ADXL355_SetAxis(adxl355_data_t* adxl355_data, uint8_t axis){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	adxl355_data->axis = axis;
	return RET_OK;
#endif
}

/**
 *
 * @param adxl355_data
 * @param fs
 * @return
 */
retval_t ADXL355_SetFullScale(adxl355_data_t* adxl355_data, ADXL355_Fullscale_t fs) {
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	uint8_t value;
	if(adxl355_data == NULL){
		return RET_ERROR;
	}
	if(adxl355_data->scale == fs){
	  return RET_ERROR;
	}
	if( ADXL355_ReadReg(adxl355_data, ADXL355_POWER_CTL, &value) != RET_OK){	//Set to standby to change range
	return RET_ERROR;
	}

	value |= 0x03;

	if( ADXL355_WriteReg(adxl355_data, ADXL355_POWER_CTL, value) != RET_OK ){
	return RET_ERROR;
	}


	if( ADXL355_ReadReg(adxl355_data, ADXL355_RANGE, &value) != RET_OK ){
	return RET_ERROR;
	}


	value &= 0xFC;
	value |= (fs<<ADXL355_RANGE_BIT);

	if( ADXL355_WriteReg(adxl355_data, ADXL355_RANGE, value) != RET_OK){
	return RET_ERROR;
	}


	if( ADXL355_ReadReg(adxl355_data, ADXL355_POWER_CTL, &value) != RET_OK ){	//Set to standby to change range
	return RET_ERROR;
	}

	value &= 0xFE;			//Set to active mode

	if( ADXL355_WriteReg(adxl355_data, ADXL355_POWER_CTL, value) != RET_OK ){
	return RET_ERROR;
	}
	adxl355_data->scale = fs;
	adxl355_data->resolution = (float32_t) pow(2,((float32_t) adxl355_data->scale)+1-20);
	return RET_OK;
#endif
}


/**
 *
 * @param adxl355_data
 * @param buff
 * @return
 */
retval_t ADXL355_GetAccAxesRaw(adxl355_data_t* adxl355_data, AxesRaw_adxl_t* buff) {
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	int32_t value = 0;
	uint8_t *valueL = (uint8_t *)(&value);
	uint8_t *valueM = ((uint8_t *)(&value)+1);
	uint8_t *valueH = ((uint8_t *)(&value)+2);
	if(adxl355_data == NULL){
		return RET_ERROR;
	}
	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_X_L, valueL) != RET_OK ){
	return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_X_M, valueM) != RET_OK ){
	return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_X_H, valueH) != RET_OK ){
	return RET_ERROR;
	}

	buff->AXIS_X = (value<<8);		//Output data in 32 bit format instead 24 bits

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_Y_L, valueL) != RET_OK ){
	return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_Y_M, valueM) != RET_OK ){
	return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_Y_H, valueH) != RET_OK ){
	return RET_ERROR;
	}

	buff->AXIS_Y = (value<<8);

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_Z_L, valueL) != RET_OK ){
	return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_Z_M, valueM) != RET_OK){
	return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_Z_H, valueH) != RET_OK){
	return RET_ERROR;
	}

	buff->AXIS_Z = (value<<8);

	return RET_OK;
#endif
}


/**
 *
 * @param adxl355_data
 * @param val
 * @return
 */
retval_t ADXL355_GetFifoStoredSamples(adxl355_data_t* adxl355_data, uint8_t* val){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	if(adxl355_data == NULL){
		return RET_ERROR;
	}
	if( ADXL355_ReadReg(adxl355_data, ADXL355_FIFO_ENTRIES, val) != RET_OK ){
		return RET_ERROR;
	}

	(*val) &= 0x7F;
	return RET_OK;
#endif
}


/**
 *
 * @param adxl355_data
 * @return
 */
retval_t ADXL355_CheckDevice(adxl355_data_t* adxl355_data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	uint8_t value;
	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_DEVID_AD, &value) != RET_OK ){
		return RET_ERROR;
	}
	if(value != 0xAD){
		return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_DEVID_MST, &value) != RET_OK ){
		return RET_ERROR;
	}
	if(value != 0x1D){
		return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_PARTID, &value) != RET_OK ){
		return RET_ERROR;
	}
	if(value != 0xED){
		return RET_ERROR;
	}

	return RET_OK;
#endif
}

/**
 *
 * @param adxl355_data
 * @param reg
 * @param data
 * @return
 */
__weak retval_t ADXL355_ReadReg(adxl355_data_t* adxl355_data, uint8_t reg, uint8_t* data) {
	return RET_ERROR;
}


/**
 *
 * @param adxl355_data
 * @param buff
 * @return
 */
__weak retval_t ADXL355_GetAccAxesRaw_FIFO_Mode(adxl355_data_t* adxl355_data, AxesRaw_adxl_t* buff, uint16_t size) {
	return RET_ERROR;
}


/**
 *
 * @param adxl355_data
 * @param reg
 * @param data
 * @return
 */
__weak retval_t ADXL355_WriteReg(adxl355_data_t* adxl355_data, uint8_t reg, uint8_t data){
	return RET_ERROR;
}




/**
 *
 * @param adxl355_data
 * @param adxl355_csPin
 * @return
 */
__weak retval_t ADXL355_config_spi_device(adxl355_data_t* adxl355_data,	gpioPin_t adxl355_csPin, char* spi_dev){
	return RET_ERROR;
}
#endif
