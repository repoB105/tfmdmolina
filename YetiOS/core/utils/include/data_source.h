/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * data_source.h
 *
 *  Created on: 19 de jul. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file data_source.h
 */
#ifndef YETIOS_CORE_UTILS_INCLUDE_DATA_SOURCE_H_
#define YETIOS_CORE_UTILS_INCLUDE_DATA_SOURCE_H_

#include "types.h"

typedef enum data_source_id_{
	NO_SOURCE = 0,
	ADXL355_SOURCE = 1,
	AT30TS75_SOURCE = 2,

}data_source_id_t;

struct data_source_;

typedef retval_t (*sample_source_func_t)(struct data_source_* data_source);

typedef struct data_source_{
	uint32_t sampling_period;			//! Period of data source sampling in ms. The minimun period allowed is defined by ADAPTIVE_CORE_RESOLUTION
	uint32_t remaining_time;			//! Current remaining time to finish a sampling period
	void* data_buffer;					//! Data source input buffer. Can represent any kind of data
	sample_source_func_t sampling_func;	//! Sampling function used by the data source
	data_source_id_t data_source_id;	//! Data source id that defines the data type
	uint16_t buffer_size;				//! Size of the data source input buffer
	uint16_t buffer_counter;			//! Current counter of the input buffer
}data_source_t;


retval_t init_data_source(data_source_t* data_source, uint32_t sampling_period, sample_source_func_t sampling_func, data_source_id_t data_source_id);
retval_t deinit_data_source(data_source_t* data_source);

retval_t register_data_source(data_source_t* data_source);
retval_t unregister_data_source(data_source_t* data_source);

data_source_t* get_data_source(data_source_id_t data_source_id);
retval_t sample_data_sources(void);

/*Data sources auto initialization macros*/
#define init_source(fn) \
    static retval_t (*__src_initcall_##fn)(void) __attribute__((__used__)) \
    __attribute__((__section__(".src_initcall"))) = fn

#define deinit_source(fn) \
    static retval_t (*__src_deinitcall_##fn)(void) __attribute__((__used__)) \
    __attribute__((__section__(".src_deinitcall"))) = fn


retval_t init_sources(void);
retval_t deinit_sources(void);

#endif /* YETIOS_CORE_UTILS_INCLUDE_DATA_SOURCE_H_ */
