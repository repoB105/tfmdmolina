/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adaptive_io_modules_example.c
 *
 *  Created on: 20 de jul. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adaptive_io_modules_example.c
 */

#include "system_api.h"
#include "platform-conf.h"
#include "netstack.h"

#if ADAPTIVE_IO_MODULES_ALG

#define ADAPTIVE_IO_MODULES_PERIOD	150
#define NUM_DATA_SOURCES			1

#define ACEL_THR					0.1f
#define MODULES_ON_TIMEOUT			3000

#include "data_source.h"
#include "adaptive_algorithm.h"
#include "adxl355_data_source.h"

static adaptive_algorithm_t adaptive_io_modules;

static uint32_t timeout_timer;

/* Init and de-init functions */
static retval_t init_adaptive_io_modules(void);
static retval_t deinit_adaptive_io_modules(void);

static retval_t adaptive_io_modules_func(adaptive_algorithm_t* adaptive_io_modules);

static void timeout_timer_func(void const * argument);


/* Init and de-init functions */
/**
 *
 * @return
 */
static retval_t init_adaptive_io_modules(void){
	if(init_adaptive_algorithm(&adaptive_io_modules, NUM_DATA_SOURCES, adaptive_io_modules_func, ADAPTIVE_IO_MODULES_PERIOD) != RET_OK){
		return RET_ERROR;
	}

	if(link_source_to_algorithm(&adaptive_io_modules, ADXL355_SOURCE) != RET_OK){
		deinit_adaptive_algorithm(&adaptive_io_modules);
		return RET_ERROR;
	}

	if(register_adaptive_algorithm(&adaptive_io_modules) != RET_OK){
		deinit_adaptive_algorithm(&adaptive_io_modules);
		return RET_ERROR;
	}

	timeout_timer = ytTimerCreate(ytTimerOnce, timeout_timer_func, NULL);
	return RET_OK;
}

/**
 *
 * @return
 */
static retval_t deinit_adaptive_io_modules(void){

	deinit_adaptive_algorithm(&adaptive_io_modules);
	return RET_OK;
}


/* Algorithm function */
/**
 *
 * @param data_source
 * @return
 */
static retval_t adaptive_io_modules_func(adaptive_algorithm_t* adaptive_io_modules){
	float32_t max_val_x, min_val_x, max_val_y, min_val_y, max_val_z, min_val_z, sum_pp;
	uint16_t i;
	if(adaptive_io_modules == NULL){
		return RET_ERROR;
	}
	data_source_t* adxl355_data_source = get_data_source(ADXL355_SOURCE);
	if(adxl355_data_source == NULL){
		return RET_ERROR;
	}

	adlx355_data_source_t* adxl355_buffer = (adlx355_data_source_t*) adxl355_data_source->data_buffer;

	max_val_x = -5000;
	min_val_x = 5000;
	max_val_y = -5000;
	min_val_y = 5000;
	max_val_z = -5000;
	min_val_z = 5000;

	//Calculo los valores pico a pico de cada eje
	for(i=0; i<adxl355_data_source->buffer_counter; i ++){

		if(adxl355_buffer[i].x_axis< min_val_x){
			min_val_x = adxl355_buffer[i].x_axis;
		}
		if(adxl355_buffer[i].x_axis > max_val_x){
			max_val_x = adxl355_buffer[i].x_axis;
		}
		if(adxl355_buffer[i].y_axis< min_val_y){
			min_val_y = adxl355_buffer[i].y_axis;
		}
		if(adxl355_buffer[i].y_axis > max_val_y){
			max_val_y = adxl355_buffer[i].y_axis;
		}
		if(adxl355_buffer[i].z_axis< min_val_z){
			min_val_z = adxl355_buffer[i].z_axis;
		}
		if(adxl355_buffer[i].z_axis > max_val_z){
			max_val_z = adxl355_buffer[i].z_axis;
		}
	}

	//Calculo la suma de los valores pico a pico
	sum_pp = (max_val_x - min_val_x) + (max_val_y - min_val_y) + (max_val_z - min_val_z);

	if(sum_pp > ACEL_THR){
//		modules_on();
		stdio_init();
		netstack_init();
		leds_on(LEDS_BLUE);
		ytTimerStart(timeout_timer, MODULES_ON_TIMEOUT);
	}
	adxl355_data_source->buffer_counter = 0;

	return RET_OK;
}


/**
 *
 * @param argument
 */
static void timeout_timer_func(void const * argument){
//	modules_off();
	netstack_deinit();
	stdio_deinit();
	leds_off(LEDS_BLUE);
}

/*Initialization macros*/
init_algorithm(init_adaptive_io_modules);
deinit_algorithm(deinit_adaptive_io_modules);
#endif
