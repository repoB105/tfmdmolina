/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adaptive_algorithm.c
 *
 *  Created on: 19 de jul. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adaptive_algorithm.c
 */

#include "adaptive_algorithm.h"
#include "system_api.h"
#include "adaptive_core.h"

#define MAX_ADAPTIVE_ALGORITHMS	2

static adaptive_algorithm_t* adaptive_algorithms[MAX_ADAPTIVE_ALGORITHMS];
static uint16_t num_adaptive_algorithms = 0;


/**
 *
 * @param adaptive_algorithm
 * @param num_data_sources
 * @param algorithm_func
 * @param period
 * @return
 */
retval_t init_adaptive_algorithm(adaptive_algorithm_t* adaptive_algorithm, uint16_t num_data_sources, adaptive_algorithm_func_t algorithm_func, uint32_t period){
	if(adaptive_algorithm == NULL){
		return RET_ERROR;
	}
	if(algorithm_func == NULL){
		return RET_ERROR;
	}
	if(period%ADAPTIVE_CORE_RESOLUTION){
		adaptive_algorithm->algorithm_period = ((period/ADAPTIVE_CORE_RESOLUTION)+1) * ADAPTIVE_CORE_RESOLUTION;
	}
	else{
		adaptive_algorithm->algorithm_period = period;
	}

	adaptive_algorithm->remaining_time = adaptive_algorithm->algorithm_period;
	adaptive_algorithm->num_data_sources = num_data_sources;
	adaptive_algorithm->current_data_sources = 0;
	adaptive_algorithm->linked_data_sources = (data_source_t**) ytMalloc(num_data_sources*sizeof(data_source_t*));
	adaptive_algorithm->algorithm_func = algorithm_func;

	return RET_OK;
}

/**
 *
 * @param adaptive_algorithm
 * @return
 */
retval_t deinit_adaptive_algorithm(adaptive_algorithm_t* adaptive_algorithm){
	if(adaptive_algorithm == NULL){
		return RET_ERROR;
	}

	adaptive_algorithm->algorithm_period = 0;
	adaptive_algorithm->num_data_sources = 0;
	adaptive_algorithm->algorithm_func = NULL;
	ytFree(adaptive_algorithm->linked_data_sources);
	unregister_adaptive_algorithm(adaptive_algorithm);

	return RET_OK;
}

/**
 *
 * @param adaptive_algorithm
 * @param data_source_id
 * @return
 */
retval_t link_source_to_algorithm(adaptive_algorithm_t* adaptive_algorithm, data_source_id_t data_source_id){
	if(adaptive_algorithm == NULL){
		return RET_ERROR;
	}
	if(adaptive_algorithm->current_data_sources >= adaptive_algorithm->num_data_sources){
		return RET_ERROR;
	}
	if(data_source_id == NO_SOURCE){
		return RET_ERROR;
	}
	data_source_t* data_source = get_data_source(data_source_id);
	if(data_source == NULL){
		return RET_ERROR;
	}

	adaptive_algorithm->linked_data_sources[adaptive_algorithm->current_data_sources] = data_source;
	adaptive_algorithm->current_data_sources ++;


	return RET_OK;
}

/**
 *
 * @param adaptive_algorithm
 * @param data_source_id
 * @return
 */
retval_t unlink_source_to_algorithm(adaptive_algorithm_t* adaptive_algorithm, data_source_id_t data_source_id){
	uint16_t i,j;
	if(adaptive_algorithm == NULL){
		return RET_ERROR;
	}

	if(data_source_id == NO_SOURCE){
		return RET_ERROR;
	}
	data_source_t* data_source = get_data_source(data_source_id);
	if(data_source == NULL){
		return RET_ERROR;
	}

	for(i=0; i<adaptive_algorithm->current_data_sources; i++){
		if(adaptive_algorithm->linked_data_sources[i] == data_source){
			for(j=i; j<adaptive_algorithm->current_data_sources-1; j++){
				adaptive_algorithm->linked_data_sources[j] = adaptive_algorithm->linked_data_sources[j+1];
			}
			adaptive_algorithm->current_data_sources--;
			return RET_OK;
		}
	}

	return RET_ERROR;
}


/**
 *
 * @param adaptive_algorithm
 * @return
 */
retval_t register_adaptive_algorithm(adaptive_algorithm_t* adaptive_algorithm){
	if(adaptive_algorithm == NULL){
		return RET_ERROR;
	}
	if(num_adaptive_algorithms >= MAX_ADAPTIVE_ALGORITHMS){
		return RET_ERROR;
	}

	adaptive_algorithms[num_adaptive_algorithms] = adaptive_algorithm;
	num_adaptive_algorithms++;

	return RET_OK;
}

/**
 *
 * @param adaptive_algorithm
 * @return
 */
retval_t unregister_adaptive_algorithm(adaptive_algorithm_t* adaptive_algorithm){
	uint16_t i, j;
	if(adaptive_algorithm == NULL){
		return RET_ERROR;
	}
	for (i=0; i<num_adaptive_algorithms; i++){
		if(adaptive_algorithms[i] == adaptive_algorithm){
			for(j=i; j<num_adaptive_algorithms-1; j++){
				adaptive_algorithms[j] = adaptive_algorithms[j+1];
			}
			num_adaptive_algorithms--;
			return RET_OK;
		}
	}

	return RET_ERROR;
}

/**
 *
 * @return
 */
retval_t execute_adaptive_algorithms(void){
	uint16_t i;
	for (i=0; i<num_adaptive_algorithms; i++){
		adaptive_algorithms[i]->remaining_time -= ADAPTIVE_CORE_RESOLUTION;
		if(adaptive_algorithms[i]->remaining_time == 0){
			if(adaptive_algorithms[i]->algorithm_func != NULL){
				adaptive_algorithms[i]->algorithm_func(adaptive_algorithms[i]);
			}
			adaptive_algorithms[i]->remaining_time = adaptive_algorithms[i]->algorithm_period;
		}
	}

	return RET_OK;
}


/**
 *
 * @return
 */
retval_t init_adapt_algorithms(void){
	extern uint32_t (*_alg_initcall_start)(void); // Defined by the linker
	extern uint32_t (*_alg_initcall_end)(void); 	// Defined by the linker

	uint32_t* start = (uint32_t*) &_alg_initcall_start;
	uint32_t* end = (uint32_t*) &_alg_initcall_end;


	while(((*start) != (uint32_t)NULL) && (start < end)){
		retval_t (*func)(void)=(retval_t(*)(void))(*start);
		if(func != NULL){
			func();
		}
		start++;
	}

	return RET_OK;
}


/**
 *
 * @return
 */
retval_t deinit_adapt_algorithms(void){
	extern uint32_t (*_alg_deinitcall_start)(void); // Defined by the linker
	extern uint32_t (*_alg_deinitcall_end)(void); // Defined by the linker


	uint32_t* start = (uint32_t*) &_alg_deinitcall_start;
	uint32_t* end = (uint32_t*) &_alg_deinitcall_end;

	while(((*start) != (uint32_t)NULL) && (start < end)){
		retval_t (*func)(void)=(retval_t(*)(void))(*start);
		if(func != NULL){
			func();
		}
		start++;
	}

	return RET_OK;
}
