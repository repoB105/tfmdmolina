/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * hw_timer.c
 *
 *  Created on: 8 oct. 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file hw_timer.c
 */

//typedef retval_t (*cb_func_t)(void const * argument);
//
//
//typedef enum hw_timer_mode_{
//	hw_timer_once = 0,
//	hw_timer_periodic = 1,
//}hw_timer_mode_t;
//
//
//uint32_t y_hw_timer_init(uint32_t usec_resolution, cb_func_t timeout_func, void* argument);
//retval_t y_hw_timer_deinit(uint32_t hw_timer_id);
//
//retval_t y_hw_timer_start(uint32_t hw_timer_id, uint32_t timeout_usec, hw_timer_mode_t hw_timer_mode);
//retval_t y_hw_timer_stop(uint32_t hw_timer_id);
//
//
//
//
//
//
//retval_t hw_timer_arch_init();
//retval_t hw_timer_arch_deinit();
//
//retval_t hw_timer_arch_start();
//retval_t hw_timer_arch_stop();
//
//
//
//void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
//{
///* USER CODE BEGIN Callback 0 */
//
///* USER CODE END Callback 0 */
//  if (htim->Instance == TIM8) {
//    HAL_IncTick();
//  }
///* USER CODE BEGIN Callback 1 */
//
///* USER CODE END Callback 1 */
//}
