/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * netstack-conf.h
 *
 *  Created on: 21 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file netstack-conf.h
 */
#ifndef APPLICATION_CORE_NET_NETSTACK_CONF_H_
#define APPLICATION_CORE_NET_NETSTACK_CONF_H_

#include <yeti_tp.h>
#include "cerberus_phy.h"
#include "x_nucleo_IDS01A4.h"
#include "at86rf215_phy.h"
#include "yeti_mac.h"
#include "yeti_routing.h"

/* TRANSPORT LAYER */
#ifdef	PLATFORM_TRANSPORT_LAYER
#define TRANSPORT_LAYER		PLATFORM_TRANSPORT_LAYER
#else
#define TRANSPORT_LAYER		YETI_TRANSPORT_LAYER
#endif
#define USE_RELIABLE_FUNCS	1


/* ROUTING LAYER */
#ifdef PLATFORM_ROUTING_LAYER
#define ROUTING_LAYER		PLATFORM_ROUTING_LAYER
#else
#define ROUTING_LAYER		YETI_ROUTING_LAYER
#endif

/* MAC LAYER */
#ifdef PLATFORM_MAC_LAYER
#define MAC_LAYER			PLATFORM_MAC_LAYER
#else
#define MAC_LAYER			YETI_MAC_LAYER
#endif
#define USE_SYNC_MAC		PLATFORM_USE_SYNC			//USED BY YETIMAC
#define SYNC_OFFSET_MS		PLATFORM_SYNC_OFFSET_MS

/* PHY LAYER*/
#define PHY_LAYER			PLATFORM_PHY_LAYER



#endif /* APPLICATION_CORE_NET_NETSTACK_CONF_H_ */
