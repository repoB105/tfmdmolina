/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * packetbuffer.h
 *
 *  Created on: 22 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file packetbuffer.h
 */
#ifndef APPLICATION_CORE_NET_INCLUDE_PACKETBUFFER_H_
#define APPLICATION_CORE_NET_INCLUDE_PACKETBUFFER_H_

#include "system_api.h"
#include "netstack-conf.h"

#define FIXED_PACKET_SIZE	63


/* MAC */
#define MAC_LAYER_HDR_T			CONCAT(MAC_LAYER, _hdr_t)
#define MAC_HEADER_SIZE			(sizeof(MAC_LAYER_HDR_T))

/* ROUTING */
#define ROUTING_LAYER_HDR_T		CONCAT(ROUTING_LAYER, _hdr_t)
#define ROUTING_HEADER_SIZE		(sizeof(ROUTING_LAYER_HDR_T))

/* TRANSPORT */
#define TRANSPORT_LAYER_HDR_T	CONCAT(TRANSPORT_LAYER, _hdr_t)
#define TRANSPORT_HEADER_SIZE	(sizeof(TRANSPORT_LAYER_HDR_T))



typedef struct __packed packet_header_{
	MAC_LAYER_HDR_T mac_hdr;
	ROUTING_LAYER_HDR_T rt_hdr;
	TRANSPORT_LAYER_HDR_T tp_hdr;
}packet_header_t;


#define PACKET_HEADER_SIZE				(sizeof(packet_header_t))

#define PACKET_DATA_SIZE				(FIXED_PACKET_SIZE-PACKET_HEADER_SIZE)

#define PACKET_BUFFER_SIZE				3			//Number of packets in each packet buffer: 4 packets => 256 bytes

#define MAX_PACKET_BUFFER_DATA_SIZE		PACKET_DATA_SIZE*PACKET_BUFFER_SIZE	//Maximun size of data storable in packetbuffer


typedef struct __packed net_packet_{
	packet_header_t header;
	uint8_t data[PACKET_DATA_SIZE];

	//* Exra data used internally. This wont be sent in a packet */
	void* tp_extra_info;
	void* rt_extra_info;
	void* mac_extra_info;
	void* phy_extra_info;
	float32_t pckt_rssi;
	uint32_t rcv_tick_time;
	uint8_t used_packet_flag;
}net_packet_t;

typedef struct __packed packetbuffer_{
	net_packet_t packet_buffer[PACKET_BUFFER_SIZE];
	uint32_t packetbuffer_mutex;
	uint16_t current_num_packets;
}packetbuffer_t;


retval_t rx_packetbuffer_init(void);	//Initialize packetbuffer values, flags and mutex
retval_t rx_packetbuffer_deinit(void);	//DeInitialize packetbuffer values, flags and mutex
net_packet_t* rx_packetbuffer_get_free_packet(void);
retval_t rx_packetbuffer_release_packet(net_packet_t* packet);


retval_t tx_packetbuffer_init(void);	//Initialize packetbuffer values, flags and mutex
retval_t tx_packetbuffer_deinit(void);	//DeInitialize packetbuffer values, flags and mutex
net_packet_t* tx_packetbuffer_get_free_packet(void);
retval_t tx_packetbuffer_release_packet(net_packet_t* packet);


#endif /* APPLICATION_CORE_NET_INCLUDE_PACKETBUFFER_H_ */
