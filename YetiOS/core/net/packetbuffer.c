/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * packetbuffer.c
 *
 *  Created on: 28 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file packetbuffer.c
 */

#include "packetbuffer.h"

#if ENABLE_NETSTACK_ARCH

packetbuffer_t* rx_packetbuffer = NULL;
packetbuffer_t* tx_packetbuffer = NULL;

/**
 *
 * @return
 */
retval_t rx_packetbuffer_init(void){
	if(rx_packetbuffer != NULL){
		return RET_ERROR;
	}
	rx_packetbuffer = (packetbuffer_t*) ytMalloc(sizeof(packetbuffer_t));
	rx_packetbuffer->packetbuffer_mutex = ytMutexCreate();
	rx_packetbuffer->current_num_packets = 0;

	return RET_OK;
}
/**
 *
 * @return
 */
retval_t rx_packetbuffer_deinit(void){
	if(rx_packetbuffer == NULL){
		return RET_ERROR;
	}

	ytMutexDelete(rx_packetbuffer->packetbuffer_mutex);
	ytFree(rx_packetbuffer);
	rx_packetbuffer = NULL;
	return RET_OK;
}


/**
 *
 * @return
 */
net_packet_t* rx_packetbuffer_get_free_packet(void){
	uint16_t i;
	if(rx_packetbuffer == NULL){
		return NULL;
	}

	if(rx_packetbuffer->current_num_packets > PACKET_BUFFER_SIZE){
		return NULL;
	}

	ytMutexWait(rx_packetbuffer->packetbuffer_mutex, YT_WAIT_FOREVER);
	for(i=0; i<PACKET_BUFFER_SIZE; i++){
		if(!rx_packetbuffer->packet_buffer[i].used_packet_flag){
			rx_packetbuffer->packet_buffer[i].used_packet_flag++;
			rx_packetbuffer->current_num_packets++;

			ytMutexRelease(rx_packetbuffer->packetbuffer_mutex);
			return &(rx_packetbuffer->packet_buffer[i]);
		}
	}

	ytMutexRelease(rx_packetbuffer->packetbuffer_mutex);
	return NULL;

}

/**
 *
 * @param packet
 * @return
 */
retval_t rx_packetbuffer_release_packet(net_packet_t* packet){
	uint16_t i;
	if(rx_packetbuffer == NULL){
		return RET_ERROR;
	}
	if(packet == NULL){
		return RET_ERROR;
	}

	if(rx_packetbuffer->current_num_packets > PACKET_BUFFER_SIZE){
		return RET_ERROR;
	}

	ytMutexWait(rx_packetbuffer->packetbuffer_mutex, YT_WAIT_FOREVER);
	for(i=0; i<PACKET_BUFFER_SIZE; i++){
		if((&(rx_packetbuffer->packet_buffer[i])) == packet){
			if(rx_packetbuffer->packet_buffer[i].used_packet_flag){
				rx_packetbuffer->packet_buffer[i].used_packet_flag--;
				rx_packetbuffer->current_num_packets--;

				ytMutexRelease(rx_packetbuffer->packetbuffer_mutex);
				return RET_OK;
			}
		}
	}

	ytMutexRelease(rx_packetbuffer->packetbuffer_mutex);
	return RET_ERROR;
}


/* ******************************************************/

/**
 *
 * @return
 */
retval_t tx_packetbuffer_init(void){
	if(tx_packetbuffer != NULL){
		return RET_ERROR;
	}
	tx_packetbuffer = (packetbuffer_t*) ytMalloc(sizeof(packetbuffer_t));
	tx_packetbuffer->packetbuffer_mutex = ytMutexCreate();
	tx_packetbuffer->current_num_packets = 0;
	return RET_OK;
}
/**
 *
 * @return
 */
retval_t tx_packetbuffer_deinit(void){
	if(tx_packetbuffer == NULL){
		return RET_ERROR;
	}
	ytMutexDelete(tx_packetbuffer->packetbuffer_mutex);
	ytFree(tx_packetbuffer);
	tx_packetbuffer = NULL;
	return RET_OK;
}



/**
 *
 * @return
 */
net_packet_t* tx_packetbuffer_get_free_packet(void){
	uint16_t i;
	if(tx_packetbuffer == NULL){
		return NULL;
	}

	if(tx_packetbuffer->current_num_packets > PACKET_BUFFER_SIZE){
		return NULL;
	}

	ytMutexWait(tx_packetbuffer->packetbuffer_mutex, YT_WAIT_FOREVER);
	for(i=0; i<PACKET_BUFFER_SIZE; i++){
		if(!tx_packetbuffer->packet_buffer[i].used_packet_flag){
			tx_packetbuffer->packet_buffer[i].used_packet_flag++;
			tx_packetbuffer->current_num_packets++;

			ytMutexRelease(tx_packetbuffer->packetbuffer_mutex);
			return &(tx_packetbuffer->packet_buffer[i]);
		}
	}
	ytMutexRelease(tx_packetbuffer->packetbuffer_mutex);
	return NULL;

}

/**
 *
 * @param packet
 * @return
 */
retval_t tx_packetbuffer_release_packet(net_packet_t* packet){
	uint16_t i;
	if(tx_packetbuffer == NULL){
		return RET_ERROR;
	}
	if(packet == NULL){
		return RET_ERROR;
	}

	if(tx_packetbuffer->current_num_packets > PACKET_BUFFER_SIZE){
		return RET_ERROR;
	}

	ytMutexWait(tx_packetbuffer->packetbuffer_mutex, YT_WAIT_FOREVER);
	for(i=0; i<PACKET_BUFFER_SIZE; i++){
		if((&(tx_packetbuffer->packet_buffer[i])) == packet){
			if(tx_packetbuffer->packet_buffer[i].used_packet_flag){
				tx_packetbuffer->packet_buffer[i].used_packet_flag--;
				tx_packetbuffer->current_num_packets--;

				ytMutexRelease(tx_packetbuffer->packetbuffer_mutex);
				return RET_OK;
			}
		}
	}

	ytMutexRelease(tx_packetbuffer->packetbuffer_mutex);
	return RET_ERROR;
}

#endif
