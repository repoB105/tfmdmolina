/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * cerberus_phy.c
 *
 *  Created on: 19 de dic. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file cerberus_phy.c
 */

#include "phy_layer.h"
#include "mac_layer.h"
#include "null_phy.h"
#include "packetbuffer.h"
#include "spirit1_core.h"
#include "cc2500_core.h"
#include "cc2500_config.h"

#if ENABLE_NETSTACK_ARCH
#if ENABLE_CERBERUS_PHY_LAYER

#include "platform_cerberus_phy.h"

#define DEBUG 0
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#define SEND_PACKET_TIMEOUT		200		//Un paquete de 64 bytes no deberia tardar mas de 5ms en enviarse a 250 kbps

/* *********SPIRIT1 433 DEFAULT CONFIG ***********/
#if ENABLE_SPIRIT1_433
#define SPIRIT1_433_DEFAULT_XTAL		50e6

#define SPIRIT1_433_DEFAULT_BAUD_RATE	250e3	// 250 kbps
#define SPIRIT1_433_DEFAULT_SPACING		250e3	// 250 KHz
#define SPIRIT1_433_DEFAULT_FREQ_DEV	127e3	// 127 KHz
#define SPIRIT1_433_DEFAULT_MODULATION	GFSK_BT1
#define SPIRIT1_433_DEFAULT_FREQ		433.05e6	// 433.05 MHz
#define SPIRIT1_433_DEFAULT_OUT_POWER	11		// 11 dBm
#define SPIRIT1_433_DEFAULT_RX_BW		540e3	// 540 KHz
#define SPIRIT1_433_DEFAULT_CHANNEL		0

#define SPIRIT1_433_DEFAULT_PACKET_LENGTH	FIXED_PACKET_SIZE
#endif
/* *********SPIRIT1 433 DEFAULT CONFIG ***********/


/* *********SPIRIT1 868 DEFAULT CONFIG ***********/
#if ENABLE_SPIRIT1_868
#define SPIRIT1_868_DEFAULT_XTAL		50e6

#define SPIRIT1_868_DEFAULT_BAUD_RATE	250e3	// 250 kbps
#define SPIRIT1_868_DEFAULT_SPACING		250e3	// 250 KHz
#define SPIRIT1_868_DEFAULT_FREQ_DEV	127e3	// 127 KHz
#define SPIRIT1_868_DEFAULT_MODULATION	GFSK_BT1
#define SPIRIT1_868_DEFAULT_FREQ		868e6	// 868.00 MHz
#define SPIRIT1_868_DEFAULT_OUT_POWER	11		// 11 dBm
#define SPIRIT1_868_DEFAULT_RX_BW		540e3	// 540 KHz
#define SPIRIT1_868_DEFAULT_CHANNEL		0

#define SPIRIT1_868_DEFAULT_PACKET_LENGTH	FIXED_PACKET_SIZE
#endif
/* *********SPIRIT1 433 DEFAULT CONFIG ***********/


/* *********CC2500 DEFAULT CONFIG ***********/
#if ENABLE_CC2500
#define CC2500_DEFAULT_BAUD_RATE	250e3	// 250 kbps
#define CC2500_DEFAULT_SPACING		250e3	// 250 KHz
#define CC2500_DEFAULT_FREQ_DEV		0		// Do not apply to MSK modulation
#define CC2500_DEFAULT_MODULATION	CC2500_MSK
#define CC2500_DEFAULT_FREQ			2410	// 2.41 Ghz
#define CC2500_DEFAULT_OUT_POWER	1		// 1 dBm
#define CC2500_DEFAULT_RX_BW		540e3	// 540 KHz
#define CC2500_DEFAULT_CHANNEL		0

#define CC2500_DEFAULT_PACKET_LENGTH	FIXED_PACKET_SIZE
#endif
/* *********SPIRIT1 433 DEFAULT CONFIG ***********/

#define PROCESS_CHECK_TIMEOUT			2000



typedef struct __packed cerberus_phy_layer_data_{
#if ENABLE_SPIRIT1_433
	spirit1_data_t* spirit1_433_data;
#endif
#if ENABLE_SPIRIT1_868
	spirit1_data_t* spirit1_868_data;
#endif
#if ENABLE_CC2500
	cc2500_data_t* cc2500_data;
#endif
	uint32_t last_interrupt_time;
	uint32_t proc_semph_id;
	uint32_t cerberus_mutex_id;
	uint32_t send_time;
	net_packet_t* sending_packet;
	uint16_t last_send_pckt_length;
	uint16_t cerb_proc_id;
	uint16_t pending_int;
	uint16_t phy_channel;
	uint8_t posted_packet_sent;
}cerberus_phy_layer_data_t;

/* CHANNELS */
/* FROM CHANNEL 0 to CHANNEL 255, CC2500 CHANNELS: 2410 MHz to 2474 */
/* CHANNEL 256: SPIRIT 1 433MHz CHANNEL */
/* CHANNEL 512: SPIRIT 1 868MHz CHANNEL */
#define SPIRIT1_433_CHANNEL	256
#define SPIRIT1_868_CHANNEL	512

#define CERBERUS_DEFAULT_CHANNEL	SPIRIT1_868_CHANNEL


static cerberus_phy_layer_data_t* cerberus_phy_data = NULL;

/* ****************************************************/
static retval_t cerberus_phy_layer_init();
static retval_t cerberus_phy_layer_deinit();

static retval_t cerberus_phy_send_packet(net_packet_t* packet, uint16_t size);

static retval_t cerberus_phy_send_packet_done(net_packet_t* packet, uint16_t size);
static retval_t cerberus_phy_rcv_packet_done(net_packet_t* packet, uint16_t size);

//Funciones llamadas inmediatamente. No son posteadas en modo evento. Se puede hacer as� ya que solo deber�an ser llamadas desde la capa MAC
static retval_t cerberus_phy_set_mode_receiving();
static retval_t cerberus_phy_set_mode_idle();
static retval_t cerberus_phy_set_mode_sleep();

static retval_t cerberus_phy_check_channel_rssi(float32_t* read_rssi);
static retval_t cerberus_phy_get_last_rssi(float32_t* read_rssi);

static retval_t cerberus_phy_set_base_freq(uint32_t base_freq);
static retval_t cerberus_phy_get_base_freq(uint32_t* base_freq);

static retval_t cerberus_phy_get_channel_num(uint32_t* channel_num);	//Returns the maximun available channels in a freq_base
static retval_t cerberus_phy_set_freq_channel(uint32_t channel_num);
static retval_t cerberus_phy_get_freq_channel(uint32_t* channel_num);	//Get the current freq channel

static retval_t cerberus_phy_set_baud_rate(uint32_t baud_rate);
static retval_t cerberus_phy_set_out_power(int16_t baud_rate);

static retval_t cerberus_phy_encrypt_packet(net_packet_t* packet, uint8_t* key, uint16_t size);
static retval_t cerberus_phy_decrypt_packet(net_packet_t* packet, uint8_t* key, uint16_t size);

static retval_t cerberus_phy_set_process_priority(process_class_t priority);
static process_class_t cerberus_phy_get_process_priority(void);
/* ****************************************************/

/* ******Cerberus process Func*************************/
static void cerberus_process_func(const void* args);
/* ****************************************************/

/* ******Cerberus private Funcs*************************/

/* SPIRIT1 433 PRIVATE FUNCS*/
#if ENABLE_SPIRIT1_433
static retval_t spirit1_433_init(spirit1_data_t* spirit1_data, char* spi_dev);
static void spirit1_433_irq_cb(const void* args);
#endif
/* SPIRIT1 433 PRIVATE FUNCS*/

/* SPIRIT1 433 PRIVATE FUNCS*/
#if ENABLE_SPIRIT1_868
static retval_t spirit1_868_init(spirit1_data_t* spirit1_data, char* spi_dev);
static void spirit1_868_irq_cb(const void* args);
#endif
/* SPIRIT1 433 PRIVATE FUNCS*/

/* CC2500 PRIVATE FUNCS*/
#if ENABLE_CC2500
static retval_t cc2500_init(cc2500_data_t* cc2500_data, char* spi_dev);
static void cc2500_irq_cb(const void* args);
#endif
/* CC2500 PRIVATE FUNCS*/

/* ****************************************************/
phy_layer_funcs_t cerberus_phy_funcs = {
		.phy_layer_init = cerberus_phy_layer_init,
		.phy_layer_deinit = cerberus_phy_layer_deinit,
		.phy_send_packet = cerberus_phy_send_packet,
		.phy_send_packet_done = cerberus_phy_send_packet_done,
		.phy_rcv_packet_done = cerberus_phy_rcv_packet_done,
		.phy_set_mode_receiving = cerberus_phy_set_mode_receiving,
		.phy_set_mode_idle = cerberus_phy_set_mode_idle,
		.phy_set_mode_sleep = cerberus_phy_set_mode_sleep,
		.phy_check_channel_rssi = cerberus_phy_check_channel_rssi,
		.phy_get_last_rssi = cerberus_phy_get_last_rssi,
		.phy_set_base_freq = cerberus_phy_set_base_freq,
		.phy_get_base_freq = cerberus_phy_get_base_freq,
		.phy_get_channel_num = cerberus_phy_get_channel_num,
		.phy_set_freq_channel = cerberus_phy_set_freq_channel,
		.phy_get_freq_channel = cerberus_phy_get_freq_channel,
		.phy_set_baud_rate = cerberus_phy_set_baud_rate,
		.phy_set_out_power = cerberus_phy_set_out_power,
		.phy_encrypt_packet = cerberus_phy_encrypt_packet,
		.phy_decrypt_packet = cerberus_phy_decrypt_packet,
		.phy_set_process_priority = cerberus_phy_set_process_priority,
		.phy_get_process_priority = cerberus_phy_get_process_priority,
};


/**
 *
 * @return
 */
static retval_t cerberus_phy_layer_init(){

	if(cerberus_phy_data != NULL){
		return RET_ERROR;
	}
	if((cerberus_phy_data = (cerberus_phy_layer_data_t*) ytMalloc(sizeof(cerberus_phy_layer_data_t))) ==NULL){
		return RET_ERROR;
	}

#if ENABLE_SPIRIT1_433
	if((cerberus_phy_data->spirit1_433_data = new_spirit1_data(SPIRIT1_433_CS_PIN, SPIRIT1_433_SDN_PIN, SPIRIT1_433_GPIO3_PIN, spirit1_433_irq_cb, NULL)) == NULL){
		ytFree(cerberus_phy_data);
		cerberus_phy_data = NULL;
		return RET_ERROR;
	}

	if(spirit1_433_init(cerberus_phy_data->spirit1_433_data, CERBERUS_SPI_DEV) != RET_OK){
		delete_spirit1_data(cerberus_phy_data->spirit1_433_data);
		ytFree(cerberus_phy_data);
		cerberus_phy_data = NULL;
		return RET_ERROR;
	}
#endif

#if ENABLE_CC2500
	if((cerberus_phy_data->cc2500_data = new_cc2500_data(CC2500_CS_PIN, CC2500_GDO2_PIN, cc2500_irq_cb, NULL)) == NULL){
#if ENABLE_SPIRIT1_433
		delete_spirit1_data(cerberus_phy_data->spirit1_433_data);
#endif
		ytFree(cerberus_phy_data);
		cerberus_phy_data = NULL;
		return RET_ERROR;
	}

	if(cc2500_init(cerberus_phy_data->cc2500_data, CERBERUS_SPI_DEV) != RET_OK){
#if ENABLE_SPIRIT1_433
		delete_spirit1_data(cerberus_phy_data->spirit1_433_data);
#endif
		delete_cc2500_data(cerberus_phy_data->cc2500_data);
		ytFree(cerberus_phy_data);
		cerberus_phy_data = NULL;
		return RET_ERROR;
	}
#endif


#if ENABLE_SPIRIT1_868
	if((cerberus_phy_data->spirit1_868_data = new_spirit1_data(SPIRIT1_868_CS_PIN, SPIRIT1_868_SDN_PIN, SPIRIT1_868_GPIO3_PIN, spirit1_868_irq_cb, NULL)) == NULL){
#if ENABLE_SPIRIT1_433
		delete_spirit1_data(cerberus_phy_data->spirit1_433_data);
#endif
#if ENABLE_CC2500
		delete_cc2500_data(cerberus_phy_data->cc2500_data);
#endif
		ytFree(cerberus_phy_data);
		cerberus_phy_data = NULL;
		return RET_ERROR;
	}

	if(spirit1_868_init(cerberus_phy_data->spirit1_868_data, CERBERUS_SPI_DEV) != RET_OK){
#if ENABLE_SPIRIT1_433
		delete_spirit1_data(cerberus_phy_data->spirit1_433_data);
#endif
#if ENABLE_CC2500
		delete_cc2500_data(cerberus_phy_data->cc2500_data);
#endif
		delete_spirit1_data(cerberus_phy_data->spirit1_868_data);
		ytFree(cerberus_phy_data);
		cerberus_phy_data = NULL;
		return RET_ERROR;
	}
#endif

	cerberus_phy_data->phy_channel = CERBERUS_DEFAULT_CHANNEL;
	cerberus_phy_data->sending_packet = NULL;
	cerberus_phy_data->pending_int = 0;
	cerberus_phy_data->proc_semph_id = ytSemaphoreCreate(1);
	cerberus_phy_data->cerberus_mutex_id = ytMutexCreate();
	cerberus_phy_data->send_time = 0;
	cerberus_phy_data->posted_packet_sent = 0;
	cerberus_phy_data->last_interrupt_time = 0;
	ytSemaphoreWait(cerberus_phy_data->proc_semph_id, YT_WAIT_FOREVER);

	if(cerberus_phy_data->phy_channel < SPIRIT1_433_CHANNEL){	//CC2500 selected. Sleep the spirit1_433 and the spirit_868
#if ENABLE_SPIRIT1_433
		spirit1_set_mode_sleep(cerberus_phy_data->spirit1_433_data);
#endif
#if ENABLE_SPIRIT1_868
		spirit1_set_mode_sleep(cerberus_phy_data->spirit1_868_data);
#endif
	}

	if(cerberus_phy_data->phy_channel == SPIRIT1_433_CHANNEL){	//Spirit1_433 selected. Sleep the CC2500 and the spirit_868
#if ENABLE_CC2500
		cc2500_set_mode_sleep(cerberus_phy_data->cc2500_data);
#endif
#if ENABLE_SPIRIT1_868
		spirit1_set_mode_sleep(cerberus_phy_data->spirit1_868_data);
#endif
	}
	if(cerberus_phy_data->phy_channel == SPIRIT1_868_CHANNEL){	//Spirit1_868 selected. Sleep the spirit1_433 and the cc2500
#if ENABLE_CC2500
		cc2500_set_mode_sleep(cerberus_phy_data->cc2500_data);
#endif
#if ENABLE_SPIRIT1_433
		spirit1_set_mode_sleep(cerberus_phy_data->spirit1_433_data);
#endif
	}

	ytStartProcess("cerb_phy_proc", cerberus_process_func, PHY_LAYER_PROCESS_PRIORITY, 256, &(cerberus_phy_data->cerb_proc_id), NULL);

	return RET_OK;
}


/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t cerberus_phy_layer_deinit(){
	if(cerberus_phy_data == NULL){
		return RET_ERROR;
	}
	ytMutexDelete(cerberus_phy_data->cerberus_mutex_id);
	ytSemaphoreDelete(cerberus_phy_data->proc_semph_id);
	ytExitProcess(cerberus_phy_data->cerb_proc_id);

#if ENABLE_SPIRIT1_433
	if(spirit1_hw_deinit(cerberus_phy_data->spirit1_433_data) != RET_OK){
		return RET_ERROR;
	}
	if(delete_spirit1_data(cerberus_phy_data->spirit1_433_data) != RET_OK){
		return RET_ERROR;
	}
#endif
#if ENABLE_SPIRIT1_868
	if(spirit1_hw_deinit(cerberus_phy_data->spirit1_868_data) != RET_OK){
		return RET_ERROR;
	}
	if(delete_spirit1_data(cerberus_phy_data->spirit1_868_data) != RET_OK){
		return RET_ERROR;
	}
#endif
#if ENABLE_CC2500
	if(cc2500_hw_deinit(cerberus_phy_data->cc2500_data) != RET_OK){
		return RET_ERROR;
	}
	if(delete_cc2500_data(cerberus_phy_data->cc2500_data) != RET_OK){
		return RET_ERROR;
	}
#endif
	ytFree(cerberus_phy_data);
	cerberus_phy_data = NULL;
	return RET_OK;
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @return
 */
static retval_t cerberus_phy_send_packet(net_packet_t* packet, uint16_t size){

	PRINTF("CERBERUS: SENDING\r\n");

	ytMutexWait(cerberus_phy_data->cerberus_mutex_id, YT_WAIT_FOREVER);
	if(cerberus_phy_data->sending_packet != NULL){
		ytMutexRelease(cerberus_phy_data->cerberus_mutex_id);
		return RET_ERROR;
	}

#if ENABLE_SPIRIT1_433
	if(cerberus_phy_data->phy_channel == SPIRIT1_433_CHANNEL){
		cerberus_phy_data->sending_packet = packet;
		cerberus_phy_data->last_send_pckt_length = size;
		if(spirit1_send_data(cerberus_phy_data->spirit1_433_data, (uint8_t*) packet, size) != RET_OK){
			cerberus_phy_data->sending_packet = NULL;
			ytMutexRelease(cerberus_phy_data->cerberus_mutex_id);
			return RET_ERROR;
		}
		cerberus_phy_data->send_time = ytGetSysTickMilliSec();
		ytMutexRelease(cerberus_phy_data->cerberus_mutex_id);
		return RET_OK;
	}
#endif
#if ENABLE_SPIRIT1_868
	if(cerberus_phy_data->phy_channel == SPIRIT1_868_CHANNEL){
		cerberus_phy_data->sending_packet = packet;
		cerberus_phy_data->last_send_pckt_length = size;
		if(spirit1_send_data(cerberus_phy_data->spirit1_868_data, (uint8_t*) packet, size) != RET_OK){
			cerberus_phy_data->sending_packet = NULL;
			ytMutexRelease(cerberus_phy_data->cerberus_mutex_id);
			return RET_ERROR;
		}
		cerberus_phy_data->send_time = ytGetSysTickMilliSec();
		ytMutexRelease(cerberus_phy_data->cerberus_mutex_id);
		return RET_OK;
	}
#endif
#if ENABLE_CC2500
	if(cerberus_phy_data->phy_channel < SPIRIT1_433_CHANNEL){
		cerberus_phy_data->sending_packet = packet;
		cerberus_phy_data->last_send_pckt_length = size;
		if(cc2500_send_data(cerberus_phy_data->cc2500_data, (uint8_t*) packet, size) != RET_OK){
			cerberus_phy_data->sending_packet = NULL;
			ytMutexRelease(cerberus_phy_data->cerberus_mutex_id);
			return RET_ERROR;
		}
		cerberus_phy_data->send_time = ytGetSysTickMilliSec();

		ytMutexRelease(cerberus_phy_data->cerberus_mutex_id);
		return RET_OK;

	}
#endif
	ytMutexRelease(cerberus_phy_data->cerberus_mutex_id);
	return RET_ERROR;
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @return
 */
static retval_t cerberus_phy_send_packet_done(net_packet_t* packet, uint16_t size){

	PRINTF("CERBERUS: SEND DONE\r\n");
	ytMutexWait(cerberus_phy_data->cerberus_mutex_id, YT_WAIT_FOREVER);
	mac_packet_sent(packet, size);
	cerberus_phy_data->sending_packet = NULL;
	cerberus_phy_data->posted_packet_sent = 0;
	ytMutexRelease(cerberus_phy_data->cerberus_mutex_id);
	return RET_OK;
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @return
 */
static retval_t cerberus_phy_rcv_packet_done(net_packet_t* packet, uint16_t size){
	float32_t rssi_val;
	cerberus_phy_get_last_rssi(&rssi_val);
	PRINTF("RSSI: %.2f\r\n", rssi_val);
	mac_packet_received(packet, size);
	return RET_OK;
}

//Funciones llamadas inmediatamente. No son posteadas en modo evento. Se puede hacer as� ya que solo deber�an ser llamadas desde la capa MAC
/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t cerberus_phy_set_mode_receiving(){

	retval_t ret = RET_ERROR;
	if(cerberus_phy_data == NULL){
		return ret;
	}
#if ENABLE_SPIRIT1_433
	if(cerberus_phy_data->phy_channel == SPIRIT1_433_CHANNEL){
		ret = spirit1_set_mode_rx(cerberus_phy_data->spirit1_433_data);
	}
#endif
#if ENABLE_SPIRIT1_868
	if(cerberus_phy_data->phy_channel == SPIRIT1_868_CHANNEL){
		ret = spirit1_set_mode_rx(cerberus_phy_data->spirit1_868_data);
	}
#endif
#if ENABLE_CC2500
	else if(cerberus_phy_data->phy_channel < SPIRIT1_433_CHANNEL){
		ret = cc2500_set_mode_rx(cerberus_phy_data->cc2500_data);
	}
#endif

	return ret;
}
/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t cerberus_phy_set_mode_idle(){

	retval_t ret = RET_ERROR;
	if(cerberus_phy_data == NULL){
		return ret;
	}
#if ENABLE_SPIRIT1_433
	if(cerberus_phy_data->phy_channel == SPIRIT1_433_CHANNEL){
		ret = spirit1_set_mode_idle(cerberus_phy_data->spirit1_433_data);
	}
#endif
#if ENABLE_SPIRIT1_868
	if(cerberus_phy_data->phy_channel == SPIRIT1_868_CHANNEL){
		ret = spirit1_set_mode_idle(cerberus_phy_data->spirit1_868_data);
	}
#endif
#if ENABLE_CC2500
	if(cerberus_phy_data->phy_channel < SPIRIT1_433_CHANNEL){
		ret = cc2500_set_mode_idle(cerberus_phy_data->cc2500_data);
	}
#endif
	return ret;
}
/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t cerberus_phy_set_mode_sleep(){
	retval_t ret = RET_ERROR;
	if(cerberus_phy_data == NULL){
		return ret;
	}
#if ENABLE_SPIRIT1_433
	if(cerberus_phy_data->phy_channel == SPIRIT1_433_CHANNEL){
		ret = spirit1_set_mode_sleep(cerberus_phy_data->spirit1_433_data);
	}
#endif
#if ENABLE_SPIRIT1_868
	if(cerberus_phy_data->phy_channel == SPIRIT1_868_CHANNEL){
		ret = spirit1_set_mode_sleep(cerberus_phy_data->spirit1_868_data);
	}
#endif
#if ENABLE_CC2500
	if(cerberus_phy_data->phy_channel < SPIRIT1_433_CHANNEL){
		ret = cc2500_set_mode_sleep(cerberus_phy_data->cc2500_data);
	}
#endif
	return ret;
}

/**
 *
 * @param phy_layer_data
 * @param read_rssi
 * @return
 */
static retval_t cerberus_phy_check_channel_rssi(float32_t* read_rssi){

	if(cerberus_phy_data == NULL){
		return RET_ERROR;
	}
#if ENABLE_SPIRIT1_433
	if(cerberus_phy_data->phy_channel == SPIRIT1_433_CHANNEL){
		(*read_rssi) = spirit1_check_channel_rssi(cerberus_phy_data->spirit1_433_data);
	}
#endif
#if ENABLE_SPIRIT1_868
	if(cerberus_phy_data->phy_channel == SPIRIT1_868_CHANNEL){
		(*read_rssi) = spirit1_check_channel_rssi(cerberus_phy_data->spirit1_868_data);
	}
#endif
#if ENABLE_CC2500
	if(cerberus_phy_data->phy_channel < SPIRIT1_433_CHANNEL){
		(*read_rssi) = cc2500_get_last_rssi(cerberus_phy_data->cc2500_data);
	}
#endif
	return RET_OK;
}

/**
 *
 * @param phy_layer_data
 * @param read_rssi
 * @return
 */
static retval_t cerberus_phy_get_last_rssi(float32_t* read_rssi){

	if(cerberus_phy_data == NULL){
		return RET_ERROR;
	}
#if ENABLE_SPIRIT1_433
	if(cerberus_phy_data->phy_channel == SPIRIT1_433_CHANNEL){
		(*read_rssi) = spirit1_get_last_rssi(cerberus_phy_data->spirit1_433_data);
	}
#endif
#if ENABLE_SPIRIT1_868
	if(cerberus_phy_data->phy_channel == SPIRIT1_868_CHANNEL){
		(*read_rssi) = spirit1_get_last_rssi(cerberus_phy_data->spirit1_868_data);
	}
#endif
#if ENABLE_CC2500
	if(cerberus_phy_data->phy_channel < SPIRIT1_433_CHANNEL){
		(*read_rssi) = cc2500_get_last_rssi(cerberus_phy_data->cc2500_data);
	}
#endif
	return RET_OK;

}
/**
 *
 * @param phy_layer_data
 * @param base_freq
 * @return
 */
static retval_t cerberus_phy_set_base_freq(uint32_t base_freq){
	return RET_ERROR;
}
/**
 *
 * @param phy_layer_data
 * @param base_freq
 * @return
 */
static retval_t cerberus_phy_get_base_freq(uint32_t* base_freq){
	return RET_ERROR;
}
/**
 *
 * @param phy_layer_data
 * @param base_freq
 * @param channel_num
 * @return
 */
static retval_t cerberus_phy_get_channel_num(uint32_t* channel_num){
	return RET_ERROR;
}
/**
 *
 * @param phy_layer_data
 * @param channel_num
 * @return
 */
static retval_t cerberus_phy_set_freq_channel(uint32_t channel_num){

	if((channel_num <= ENABLE_SPIRIT1_433) || (channel_num == ENABLE_SPIRIT1_868)){
		cerberus_phy_data->phy_channel = (uint16_t) channel_num;
		return RET_OK;
	}
	return RET_ERROR;
}
/**
 *
 * @param phy_layer_data
 * @param channel_num
 * @return
 */
static retval_t cerberus_phy_get_freq_channel(uint32_t* channel_num){

	(*channel_num) = (uint32_t) cerberus_phy_data->phy_channel;

	return RET_OK;
}
/**
 *
 * @param phy_layer_data
 * @param baud_rate
 * @return
 */
static retval_t cerberus_phy_set_baud_rate(uint32_t baud_rate){
	return RET_ERROR;
}

/**
 *
 * @param phy_layer_data
 * @param baud_rate
 * @return
 */
static retval_t cerberus_phy_set_out_power(int16_t baud_rate){
	return RET_ERROR;
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @param key
 * @return
 */
static retval_t cerberus_phy_encrypt_packet(net_packet_t* packet, uint8_t* key, uint16_t size){
	if(cerberus_phy_data == NULL){
		return RET_ERROR;
	}
#if ENABLE_SPIRIT1_433
	if(cerberus_phy_data->phy_channel == SPIRIT1_433_CHANNEL){
		return spirit1_aes_encrypt_data(cerberus_phy_data->spirit1_433_data, (uint8_t*) packet, key, size);
	}
#endif
#if ENABLE_SPIRIT1_868
	if(cerberus_phy_data->phy_channel == SPIRIT1_868_CHANNEL){
		return spirit1_aes_encrypt_data(cerberus_phy_data->spirit1_868_data, (uint8_t*) packet, key, size);
	}
#endif
	return RET_ERROR;
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @param key
 * @return
 */
static retval_t cerberus_phy_decrypt_packet(net_packet_t* packet, uint8_t* key, uint16_t size){
	if(cerberus_phy_data == NULL){
		return RET_ERROR;
	}
#if ENABLE_SPIRIT1_433
	if(cerberus_phy_data->phy_channel == SPIRIT1_433_CHANNEL){
		return spirit1_aes_decrypt_data(cerberus_phy_data->spirit1_433_data, (uint8_t*) packet, key, size);
	}
#endif
#if ENABLE_SPIRIT1_868
	if(cerberus_phy_data->phy_channel == SPIRIT1_868_CHANNEL){
		return spirit1_aes_decrypt_data(cerberus_phy_data->spirit1_868_data, (uint8_t*) packet, key, size);
	}
#endif
	return RET_ERROR;
}

/**
 *
 * @param process_id
 * @param priority
 * @return
 */
static retval_t cerberus_phy_set_process_priority(process_class_t priority){

	return ytSetProcessPriority(cerberus_phy_data->cerb_proc_id, priority);
}

/**
 *
 * @param process_id
 * @return
 */
static process_class_t cerberus_phy_get_process_priority(void){
	return ytGetProcessPriority(cerberus_phy_data->cerb_proc_id);
}

/* ******Cerberus process Func*************************/
/**
 *
 * @param args
 */
static void cerberus_process_func(const void* args){
	net_packet_t* rcv_packet;
	uint16_t ret;
	uint8_t num_rcv_bytes = 0;

	while(1){
		if(!cerberus_phy_data->pending_int){
			if(ytSemaphoreWait(cerberus_phy_data->proc_semph_id, PROCESS_CHECK_TIMEOUT) != RET_OK){
			}
		}
		PRINTF("CERBERUS: CB\r\n");

		if(cerberus_phy_data->pending_int){
			cerberus_phy_data->pending_int--;
		}

		ytMutexWait(cerberus_phy_data->cerberus_mutex_id, YT_WAIT_FOREVER);
		if(cerberus_phy_data->sending_packet != NULL){
			if(ytGetSysTickMilliSec() - cerberus_phy_data->send_time > SEND_PACKET_TIMEOUT){		//Free the packet to the upper layers
				if(!cerberus_phy_data->posted_packet_sent){				//Only one packet can be sent at a time. Prevent calling packet_sent twice
					cerberus_phy_data->posted_packet_sent = 1;
					phy_post_event_packet_sent(cerberus_phy_data->sending_packet, cerberus_phy_data->last_send_pckt_length);
				}
			}
		}
		/* Process Spirit1 433 interrupt */
#if ENABLE_SPIRIT1_433
		if(cerberus_phy_data->phy_channel == SPIRIT1_433_CHANNEL){
			ret = spirit1_irq_routine(cerberus_phy_data->spirit1_433_data);

			if(ret & RET_PCKT_SENT){
				if(cerberus_phy_data->sending_packet != NULL){
					if(!cerberus_phy_data->posted_packet_sent){			//Only one packet can be sent at a time. Prevent calling packet_sent twice
						cerberus_phy_data->posted_packet_sent = 1;
						phy_post_event_packet_sent(cerberus_phy_data->sending_packet, cerberus_phy_data->last_send_pckt_length);
					}
				}

			}
			else if(ret & RET_PCKT_RCV){
				if((rcv_packet = rx_packetbuffer_get_free_packet()) != NULL){
					spirit1_read_num_rcv_bytes(cerberus_phy_data->spirit1_433_data, &num_rcv_bytes);
					if(num_rcv_bytes){
						spirit1_read_rcv_data(cerberus_phy_data->spirit1_433_data, (uint8_t*) rcv_packet, (uint16_t) num_rcv_bytes);
						rcv_packet->pckt_rssi = spirit1_get_last_rssi(cerberus_phy_data->spirit1_433_data);
						rcv_packet->rcv_tick_time = cerberus_phy_data->last_interrupt_time;
						phy_post_event_packet_received(rcv_packet, (uint16_t)num_rcv_bytes);
					}
					else{
						rx_packetbuffer_release_packet(rcv_packet);
					}
				}
				else{
					spirit1_read_num_rcv_bytes(cerberus_phy_data->spirit1_433_data,  &num_rcv_bytes);
					if(num_rcv_bytes){
						spirit1_flush_last_rcv_data(cerberus_phy_data->spirit1_433_data, (uint16_t)num_rcv_bytes);	//Descarto el paquete que me ha llegado si no hay hueco en el packetbuff
					}
				}
			}
			else{	//error IRQ. Maybe CRC error or RX FIFO error
			}
		}
#endif

		/* Process Spirit1 868 interrupt */
#if ENABLE_SPIRIT1_868
		if(cerberus_phy_data->phy_channel == SPIRIT1_868_CHANNEL){
			ret = spirit1_irq_routine(cerberus_phy_data->spirit1_868_data);

			PRINTF("RET: %d\r\n", ret);
			if(ret & RET_PCKT_SENT){
				if(cerberus_phy_data->sending_packet != NULL){
					if(!cerberus_phy_data->posted_packet_sent){			//Only one packet can be sent at a time. Prevent calling packet_sent twice
						cerberus_phy_data->posted_packet_sent = 1;
						phy_post_event_packet_sent(cerberus_phy_data->sending_packet, cerberus_phy_data->last_send_pckt_length);
					}
				}

			}
			else if(ret & RET_PCKT_RCV){
				if((rcv_packet = rx_packetbuffer_get_free_packet()) != NULL){
					PRINTF("PCKT: %d\r\n", (int)rcv_packet);
					spirit1_read_num_rcv_bytes(cerberus_phy_data->spirit1_868_data, &num_rcv_bytes);
					if(num_rcv_bytes){
						spirit1_read_rcv_data(cerberus_phy_data->spirit1_868_data, (uint8_t*) rcv_packet, (uint16_t) num_rcv_bytes);
						rcv_packet->pckt_rssi = spirit1_get_last_rssi(cerberus_phy_data->spirit1_868_data);
						rcv_packet->rcv_tick_time = cerberus_phy_data->last_interrupt_time;
						phy_post_event_packet_received(rcv_packet, (uint16_t)num_rcv_bytes);
					}
					else{
						rx_packetbuffer_release_packet(rcv_packet);
					}
				}
				else{
					spirit1_read_num_rcv_bytes(cerberus_phy_data->spirit1_868_data,  &num_rcv_bytes);
					if(num_rcv_bytes){
						spirit1_flush_last_rcv_data(cerberus_phy_data->spirit1_868_data, (uint16_t)num_rcv_bytes);	//Descarto el paquete que me ha llegado si no hay hueco en el packetbuff
					}
				}
			}
			else{	//error IRQ. Maybe CRC error or RX FIFO error
			}
		}
#endif

#if ENABLE_CC2500
		/* Process CC2500 interrupt */
		if(cerberus_phy_data->phy_channel < SPIRIT1_433_CHANNEL){
			ret = cc2500_irq_routine(cerberus_phy_data->cc2500_data);

			if(ret & RET_PCKT_SENT){
				if(cerberus_phy_data->sending_packet != NULL){
					if(!cerberus_phy_data->posted_packet_sent){			//Only one packet can be sent at a time. Prevent calling packet_sent twice
						cerberus_phy_data->posted_packet_sent = 1;
						phy_post_event_packet_sent(cerberus_phy_data->sending_packet, cerberus_phy_data->last_send_pckt_length);
					}
				}

			}
			else if(ret & RET_PCKT_RCV){
				if((rcv_packet = rx_packetbuffer_get_free_packet()) != NULL){
					cc2500_read_num_rcv_bytes(cerberus_phy_data->cc2500_data, &num_rcv_bytes);
					if(num_rcv_bytes){
						cc2500_read_rcv_data(cerberus_phy_data->cc2500_data, (uint8_t*) rcv_packet, (uint16_t) num_rcv_bytes);
						rcv_packet->pckt_rssi = cc2500_get_last_rssi(cerberus_phy_data->cc2500_data);
						rcv_packet->rcv_tick_time = cerberus_phy_data->last_interrupt_time;
						phy_post_event_packet_received(rcv_packet, (uint16_t)num_rcv_bytes);
					}
					else{
						rx_packetbuffer_release_packet(rcv_packet);
					}
				}
				else{
					cc2500_read_num_rcv_bytes(cerberus_phy_data->cc2500_data, &num_rcv_bytes);
					if(num_rcv_bytes){
						cc2500_flush_last_rcv_data(cerberus_phy_data->cc2500_data, (uint16_t)num_rcv_bytes);	//Descarto el paquete que me ha llegado si no hay hueco en el packetbuff
					}
				}
			}
			else{	//error IRQ. Maybe CRC error or RX FIFO error
			}

		}
#endif

		ytMutexRelease(cerberus_phy_data->cerberus_mutex_id);
	}
}
/* ****************************************************/

#if ENABLE_SPIRIT1_433
/**
 *
 * @param spirit1_data
 * @return
 */
static retval_t spirit1_433_init(spirit1_data_t* spirit1_data, char* spi_dev){
	spirit1_config_t* spirit1_init_config;
	spirit1_init_config = (spirit1_config_t*) ytMalloc(sizeof(spirit1_config_t));

	spirit1_init_config->baud_rate = SPIRIT1_433_DEFAULT_BAUD_RATE;
	spirit1_init_config->channel_num = SPIRIT1_433_DEFAULT_CHANNEL;
	spirit1_init_config->channel_spacing = SPIRIT1_433_DEFAULT_SPACING;
	spirit1_init_config->freq_deviation = SPIRIT1_433_DEFAULT_FREQ_DEV;
	spirit1_init_config->modulation = SPIRIT1_433_DEFAULT_MODULATION;
	spirit1_init_config->modulation_freq = SPIRIT1_433_DEFAULT_FREQ;
	spirit1_init_config->output_power = SPIRIT1_433_DEFAULT_OUT_POWER;
	spirit1_init_config->rx_bandwidth = SPIRIT1_433_DEFAULT_RX_BW;
	spirit1_init_config->xtal_freq = SPIRIT1_433_DEFAULT_XTAL;

	if(spirit1_hw_init(spirit1_data, spirit1_init_config, spi_dev) != RET_OK){

		ytFree(spirit1_init_config);
		return RET_ERROR;
	}

	ytFree(spirit1_init_config);
	return RET_OK;
}
#endif


#if ENABLE_SPIRIT1_868
/**
 *
 * @param spirit1_data
 * @return
 */
static retval_t spirit1_868_init(spirit1_data_t* spirit1_data, char* spi_dev){
	spirit1_config_t* spirit1_init_config;
	spirit1_init_config = (spirit1_config_t*) ytMalloc(sizeof(spirit1_config_t));

	spirit1_init_config->baud_rate = SPIRIT1_868_DEFAULT_BAUD_RATE;
	spirit1_init_config->channel_num = SPIRIT1_868_DEFAULT_CHANNEL;
	spirit1_init_config->channel_spacing = SPIRIT1_868_DEFAULT_SPACING;
	spirit1_init_config->freq_deviation = SPIRIT1_868_DEFAULT_FREQ_DEV;
	spirit1_init_config->modulation = SPIRIT1_868_DEFAULT_MODULATION;
	spirit1_init_config->modulation_freq = SPIRIT1_868_DEFAULT_FREQ;
	spirit1_init_config->output_power = SPIRIT1_868_DEFAULT_OUT_POWER;
	spirit1_init_config->rx_bandwidth = SPIRIT1_868_DEFAULT_RX_BW;
	spirit1_init_config->xtal_freq = SPIRIT1_868_DEFAULT_XTAL;

	if(spirit1_hw_init(spirit1_data, spirit1_init_config, spi_dev) != RET_OK){

		ytFree(spirit1_init_config);
		return RET_ERROR;
	}

	ytFree(spirit1_init_config);
	return RET_OK;
}
#endif

#if ENABLE_CC2500
/**
 *
 * @param cc2500_data
 * @param spi_dev
 * @return
 */
static retval_t cc2500_init(cc2500_data_t* cc2500_data, char* spi_dev){
	cc2500_config_t* cc2500_init_config;
	cc2500_init_config = (cc2500_config_t*) ytMalloc(sizeof(cc2500_config_t));

	cc2500_init_config->baud_rate = CC2500_DEFAULT_BAUD_RATE;
	cc2500_init_config->channel_num = CC2500_DEFAULT_CHANNEL;
	cc2500_init_config->channel_spacing = CC2500_DEFAULT_SPACING;
	cc2500_init_config->freq_deviation = CC2500_DEFAULT_FREQ_DEV;
	cc2500_init_config->modulation = CC2500_DEFAULT_MODULATION;
	cc2500_init_config->modulation_freq = CC2500_DEFAULT_FREQ;
	cc2500_init_config->output_power = CC2500_DEFAULT_OUT_POWER;
	cc2500_init_config->rx_bandwidth = CC2500_DEFAULT_RX_BW;

	if(cc2500_hw_init(cc2500_data, cc2500_init_config, spi_dev) != RET_OK){
		ytFree(cc2500_init_config);
		return RET_ERROR;
	}

	ytFree(cc2500_init_config);
	return RET_OK;
}

#endif


#if ENABLE_SPIRIT1_433
/**
 *
 * @param args
 */
static void spirit1_433_irq_cb(const void* args){
	cerberus_phy_data->last_interrupt_time = ytGetSysTickMilliSec();
	cerberus_phy_data->pending_int++;
	ytSemaphoreRelease(cerberus_phy_data->proc_semph_id);

}
#endif

#if ENABLE_SPIRIT1_868
/**
 *
 * @param args
 */
static void spirit1_868_irq_cb(const void* args){
	cerberus_phy_data->last_interrupt_time = ytGetSysTickMilliSec();
	cerberus_phy_data->pending_int++;
	ytSemaphoreRelease(cerberus_phy_data->proc_semph_id);

}
#endif

#if ENABLE_CC2500
/**
 *
 * @param args
 */
static void cc2500_irq_cb(const void* args){
	cerberus_phy_data->last_interrupt_time = ytGetSysTickMilliSec();
	cerberus_phy_data->pending_int++;
	ytSemaphoreRelease(cerberus_phy_data->proc_semph_id);
}
#endif

#endif
#endif
