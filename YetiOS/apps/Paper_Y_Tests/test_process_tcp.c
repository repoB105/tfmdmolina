/*
 * Copyright (c) 2019, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * test_process_tcp.c
 *
 *  Created on: 15/01/2019
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file test_process_tcp.c
 */

#include "system_api.h"
#include "system_net_api.h"
#include "time_meas.h"
//#include <stdio.h>

#define USE_TEST_PROCESS_TCP	0

#if USE_TEST_PROCESS_TCP

//#define NODE_1	1
//#define NODE_2	2
#define NODE_3	3

#define NET_PORT			200

uint16_t testProcTcpId;

static void set_addr_and_routes(void);

/* TEST PROCESS TCP ********************************/
YT_PROCESS void tcp_test_func(void const * argument);

ytInitProcess(tcp_test_proc, tcp_test_func, NORMAL_PRIORITY_PROCESS, 256, &testProcTcpId, NULL);

//#define BUF_SIZE	24
#define BUF_SIZE	4
uint8_t addr_str_buf[8];


YT_PROCESS void tcp_test_func(void const * argument){
	ytTimeMeas_t time_meas;
	ytNetAddr_t net_addr;
	uint32_t connection_fd;
	uint32_t pckt_num = 0;
	uint32_t missed_packets = 0;
	set_addr_and_routes();

#if NODE_1
	net_addr = ytNewNetAddr("3", 'D');	// Envio al nodo 3
#endif
#if NODE_3
	uint32_t net_server = ytRlCreateServer(NET_PORT);
	uint32_t current_pckt_num, first_packet_num;
	net_addr = ytNewEmptyNetAddr();	// Donde voy a guardar la direccion origen
#endif

	ytDelay(2000);

	while(1){
#if NODE_1
		if(!ytRlCheckConnection(connection_fd)){	// Si no estoy conectado me intento conectar
			ytLedsToggle(LEDS_RED1);
			connection_fd = ytRlConnectToServer(net_addr, NET_PORT);
		}

		ytStartTimeMeasure(&time_meas);

		if(ytRlSend(connection_fd, (uint8_t*) &pckt_num, sizeof(uint32_t)) == sizeof(uint32_t)){
			ytStopTimeMeasure(&time_meas);
			ytPrintf("SEND OK\r\n");
		}

		else{
			ytPrintf("SEND ERROR\r\n");
		}
		pckt_num++;
		ytLedsOn(LEDS_BLUE);
		ytDelay(20);
		ytLedsOff(LEDS_BLUE);
#endif

#if NODE_2
		ytDelay(400);
		ytLedsToggle(LEDS_BLUE);
		ytDelay(100);
		ytLedsToggle(LEDS_BLUE);
#endif

#if NODE_3

		if(!ytRlCheckConnection(connection_fd)){	// Si no estoy conectado me intento conectar
			ytLedsToggle(LEDS_RED1);
			connection_fd = ytRlServerAcceptConnection(net_server, net_addr);
		}

		if(ytRlRcv(connection_fd, (uint8_t*) &current_pckt_num, BUF_SIZE)){

			if(pckt_num != 0){	// It is not the first packet
//				missed_packets += current_pckt_num - (pckt_num+1);
//				pckt_num = current_pckt_num;
				if(current_pckt_num > pckt_num){
					missed_packets += current_pckt_num - (pckt_num+1);
					pckt_num = current_pckt_num;
				}
				else if (current_pckt_num < pckt_num){	//An unordered previous packet has arrived. We can substract a missed packet
					missed_packets--;
				}
				ytPrintf("Rcv: %d\t Miss: %d\t PM %.2f%% (Mem: %.3f KB)\r\n", pckt_num-first_packet_num, missed_packets, (((float32_t)missed_packets)/((float32_t)(pckt_num-first_packet_num))*100), (float32_t) ((float32_t)xPortGetFreeHeapSize()/1000));
			}

			else{				// It is the first packet
				missed_packets = 0;
				first_packet_num = current_pckt_num;
				pckt_num = current_pckt_num;
			}

			ytLedsToggle(LEDS_BLUE);
		}
#endif

	}
}
/* *************************************************/

static void set_addr_and_routes(void){

	ytNetAddr_t net_addr;
	ytNetAddr_t next_addr;

#if NODE_1
	/* Set node Addr */
	net_addr = ytNewNetAddr("1", 'D');		//Node address
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);

	ytSetNodeAsGw();
#endif

#if NODE_2
	/* Set node Addr */
	net_addr = ytNewNetAddr("2", 'D');		//Node address
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);
#endif

#if NODE_3
	/* Set node Addr */
	net_addr = ytNewNetAddr("3", 'D');		//Node address
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);
#endif
}

#endif	// USE_TEST_PROCESS_TCP
