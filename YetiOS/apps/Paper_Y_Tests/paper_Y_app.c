/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * paper_Y_app.c
 *
 *  Created on: 15 ene. 2019
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file paper_Y_app.c
 */

#include "system_api.h"
#include "system_net_api.h"
#include "time_meas.h"
#include <stdio.h>
#include <stdlib.h>


#define USE_PAPER_Y_PROCESS	0

#if USE_PAPER_Y_PROCESS
//#define NODE_1	1
//#define NODE_2	2
#define NODE_3	3


#define CPU_TYPE_1	10565		//2 ms
#define CPU_TYPE_2	52826		//10 ms
#define CPU_TYPE_3	264131		//50 ms

#define CPU_TYPE_1_MS	2		//2 ms
#define CPU_TYPE_2_MS	10		//10 ms
#define CPU_TYPE_3_MS	50		//50 ms

#define TEST_TYPE_UDP 	1
#define TEST_TYPE_TCP	2

#define NET_PORT			666

#define TIME_BETWEEN_PACKETS	50

#define TEST_OFFSET_TIME		5000		//Let some initial offset to the test

#define DATA_SIZE				40

#define START_TEST_MSG		1
#define STOP_TEST_MSG		2
#define DEFAULT_MSG			3

uint8_t data[DATA_SIZE];

uint32_t semaphore_tcp;
uint32_t semaphore_udp;
uint32_t semaphore_cpu;

uint32_t cpu_task_off_time;
uint32_t cpu_task_period;
uint32_t cpu_task_type_ms;		//Represents the on time
uint32_t cpu_task_type_ticks;	//Represents the on time in ticks
uint32_t cpu_task_dc;

uint32_t test_time;	//Test time in ms
uint16_t test_type;

uint16_t test_running;
uint16_t stop_received;

uint16_t paper_y_tcp_proc_id;
uint16_t paper_y_udp_proc_id;
uint16_t paper_y_cpu_proc_id;
uint16_t paper_y_mac_sync_proc_id;

YT_PROCESS static void paper_y_tcp_func(void const * argument);
YT_PROCESS static void paper_y_udp_func(void const * argument);
YT_PROCESS static void paper_y_cpu_func(void const * argument);
YT_PROCESS static void mac_sync_func(void const * argument);


static retval_t cpu_task_cmd_callback(uint16_t argc, char** argv);
static retval_t ytest_cmd_callback(uint16_t argc, char** argv);

static retval_t set_node_addr(void);

ytInitProcess(tcp_proc, paper_y_tcp_func, HIGH_PRIORITY_PROCESS, 256, &paper_y_tcp_proc_id, NULL);
ytInitProcess(udp_proc, paper_y_udp_func, HIGH_PRIORITY_PROCESS, 256, &paper_y_udp_proc_id, NULL);
#if NODE_1
ytInitProcess(cpu_proc, paper_y_cpu_func, NORMAL_PRIORITY_PROCESS, 256, &paper_y_cpu_proc_id, NULL);
#endif
ytInitProcess(mac_sync, mac_sync_func, NORMAL_PRIORITY_PROCESS, 256, &paper_y_mac_sync_proc_id, NULL);

void mac_sync_func(void const * argument){

	/*Initializacion*/
	semaphore_tcp = ytSemaphoreCreate(1);
	ytSemaphoreWait(semaphore_tcp, YT_WAIT_FOREVER);
	semaphore_udp = ytSemaphoreCreate(1);
	ytSemaphoreWait(semaphore_udp, YT_WAIT_FOREVER);
	semaphore_cpu = ytSemaphoreCreate(1);
	ytSemaphoreWait(semaphore_cpu, YT_WAIT_FOREVER);

	ytGpioInitPin(GPIO_PIN_A0, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);
	ytGpioPinSet(GPIO_PIN_A0);
	test_running = 0;
	stop_received = 0;
	test_type = TEST_TYPE_UDP;

	cpu_task_dc = 2;		//Default CPU task duty cycle 2%
	cpu_task_type_ms = CPU_TYPE_1_MS;		//Defautl task type 1: on time = 2 ms;
	cpu_task_type_ticks = CPU_TYPE_1;
	cpu_task_period = (cpu_task_type_ms * 100)/	cpu_task_dc;
	cpu_task_off_time = cpu_task_period - cpu_task_type_ms;

	set_node_addr();
#if NODE_1
	ytRegisterYetishellCommand("cpu_task",cpu_task_cmd_callback);
	ytRegisterYetishellCommand("ytest", ytest_cmd_callback);
#endif
	/* ********************/

	while(1){
		if(ytMacIsNodeLinked()){
			ytLedsOn(LEDS_RED1);
		}
		else{
			ytPrintf("Warning: Node not linked \r\n");
			ytLedsToggle(LEDS_RED1);
		}
		ytDelay(1000);
	}

}


void paper_y_tcp_func(void const * argument){

	ytDelay(1000);	//Let mac_sync task initialize everything
	while(1){
		ytDelay(1000);

	}
}

void paper_y_udp_func(void const * argument){
	ytNetAddr_t dest_net_addr;
	ytNetAddr_t from_net_addr;
	uint32_t start_time;
	uint32_t current_time;
	uint32_t* current_pckt_num = (uint32_t*)&data[0];
	uint32_t* t_time = (uint32_t*)&data[4];
	uint32_t* command = (uint32_t*)&data[8];
	uint64_t* timestamp = (uint64_t*)&data[12];
	ytDelay(1000);	//Let mac_sync task initialize everything

#if NODE_1
	uint16_t started;
	ytUrOpen(NET_PORT);	//Abro el puerto
	dest_net_addr = ytNewNetAddr("3", 'D');	//Envio al  nodo 3
#endif

#if NODE_3	//Espero recibir de
	ytUrOpen(NET_PORT);	//Abro el puerto
	from_net_addr = ytNewNetAddr("22222", 'D');	//Creo una direcci�n con cualquier valor donde se guardara la direccion recibida
#endif
	while(1){

#if NODE_1
		ytSemaphoreWait(semaphore_udp, YT_WAIT_FOREVER);
		test_running++;
		started = 0;
		(*current_pckt_num) = 0;
		(*t_time) = test_time;
		(*command) = STOP_TEST_MSG;
		ytUrSend(NET_PORT, dest_net_addr, data, DATA_SIZE);	//Stop test message just in case
		ytDelay(200);
		(*command) = START_TEST_MSG;
		while(ytUrSend(NET_PORT, dest_net_addr, data, DATA_SIZE) != DATA_SIZE){
			ytDelay(20);
			ytLedsToggle(LEDS_BLUE);
		}

		ytDelay(200);
		ytUrSend(NET_PORT, dest_net_addr, data, DATA_SIZE);	//Send another time the start message

		(*command) = DEFAULT_MSG;
		ytSemaphoreRelease(semaphore_cpu);
		ytDelay(500);
		start_time = ytGetSysTickMilliSec();
		current_time = start_time;
		while((current_time-start_time) < test_time){

			if(started){
				ytGpioPinReset(GPIO_PIN_A0);
				(*timestamp) = ytTimeGetTimestamp();
			}
			else{
				(*timestamp) = 0;
			}
			if(ytUrSend(NET_PORT, dest_net_addr,data, DATA_SIZE) == DATA_SIZE){	//Data message
				if((current_time-start_time) > TEST_OFFSET_TIME){
					started = 1;
					(*current_pckt_num)++;
				}
			}
			ytGpioPinSet(GPIO_PIN_A0);
			ytLedsOn(LEDS_BLUE);
			ytDelay(TIME_BETWEEN_PACKETS);
			ytLedsOff(LEDS_BLUE);

			current_time = ytGetSysTickMilliSec();
			if(stop_received){
				stop_received--;
				(*command) = STOP_TEST_MSG;
				ytUrSend(NET_PORT, dest_net_addr, data, DATA_SIZE);	//Send stop message
				ytPrintf("UDP_TASK test Stopped\r\n");
				break;
			}
		}
		(*command) = STOP_TEST_MSG;
		ytUrSend(NET_PORT, dest_net_addr, data, DATA_SIZE);	//Send stop message. just in case the receiver is still receiving
		test_running--;
//		ytPrintf("UDP_TASK test done\r\n");
		ytPrintf("UDP Packets sent: %d\r\n", (*current_pckt_num));
#endif
#if NODE_2
		ytDelay(2000);
		ytLedsToggle(LEDS_BLUE);
#endif

#if NODE_3

		uint32_t last_pckt_num, packet_bytes, num_bytes_rcv, missed_packets, pckt_received;
		uint32_t lat, sum_lat, max_lat;
		while((*command) != START_TEST_MSG){
			ytUrRcv(NET_PORT,from_net_addr, data, DATA_SIZE);
		}		//Start test
		ytLedsToggle(LEDS_BLUE);
		last_pckt_num = 0;
		test_time = (*t_time);
		num_bytes_rcv = 0;
		packet_bytes = 0;
		missed_packets = 0;
		pckt_received = 0;
		sum_lat = 0;
		max_lat = 0;
		start_time = ytGetSysTickMilliSec();
		current_time = start_time;
		uint16_t pin_reset = 0;
		while((current_time-start_time) < test_time){
			packet_bytes = ytUrRcv(NET_PORT,from_net_addr, data, DATA_SIZE);
				if(packet_bytes){
					if((*command) == DEFAULT_MSG){
					ytLedsToggle(LEDS_BLUE);

					if((*current_pckt_num) > 0){ //Packet number 0 is recived during OFFSET TIME. Ignore it
						ytGpioPinReset(GPIO_PIN_A0);
						pin_reset++;
						if((*current_pckt_num) > last_pckt_num){
							lat = (uint32_t) (ytTimeGetTimestamp() - (*timestamp));
							if(lat>max_lat){
								max_lat = lat;
							}
							sum_lat += lat;
							pckt_received++;
							num_bytes_rcv += packet_bytes;
							missed_packets += (*current_pckt_num) - (last_pckt_num+1);
							last_pckt_num = (*current_pckt_num);
						}
						else if ((*current_pckt_num) < last_pckt_num){	//An unordered previous packet has arrived. We can substract a missed packet
							lat = (uint32_t) (ytTimeGetTimestamp() - (*timestamp));
							if(lat>max_lat){
									max_lat = lat;
								}
							sum_lat += lat;
							pckt_received++;
							num_bytes_rcv += packet_bytes;
							missed_packets--;
						}
						else{	//The packet received is repeated
						}
						ytPrintf("Rcv Pckt index: %d\r\n", (*current_pckt_num));
						ytPrintf("LAT: %d\r\n", (uint32_t) lat);
//						ytDelay(2);
						ytGpioPinSet(GPIO_PIN_A0);
					}

				}
				else if((*command) == STOP_TEST_MSG){
					ytPrintf("UDP TEST STOPPED\r\n");
					break;
				}
			}

			current_time = ytGetSysTickMilliSec();

		}
		(*command) = DEFAULT_MSG;
		//Calculate and print stats
		ytPrintf("UDP TEST FINISHED\r\n");
		ytPrintf("Rcv: %d\t Miss: %d\t PM %.2f%% (Total Bytes Rcv: %d B)\r\n", pckt_received, missed_packets, (((float32_t)missed_packets)/((float32_t)(last_pckt_num))*100), num_bytes_rcv);
		ytPrintf("Throughput: %.2f bps\r\n", (((float32_t)num_bytes_rcv)*8*1000)/(((float32_t)test_time)-TEST_OFFSET_TIME));
		ytPrintf("Avg Lat: %d\r\n", sum_lat/pckt_received);
		ytPrintf("Max Lat: %d\r\n", max_lat);
		ytPrintf("Reset pin num: %d\r\n", pin_reset);
#endif

	}
}

void paper_y_cpu_func(void const * argument){
	uint32_t start_time;
	uint32_t t1;
	uint32_t current_time;
	volatile uint32_t i;
	ytDelay(1000);	//Let mac_sync task initialize everything
	while(1){
		ytSemaphoreWait(semaphore_cpu, YT_WAIT_FOREVER);
		test_running++;
		start_time = ytGetSysTickMilliSec();
		current_time = start_time;
		t1 = ytGetSysTickMilliSec();
		while(ytGetSysTickMilliSec() == t1);			//Sync to the hal milliseconds
		while((current_time-start_time) < test_time){
//			t1 = ytGetSysTickMilliSec();
//			while((ytGetSysTickMilliSec()-t1) < cpu_task_type);
			i=0;
			while(i<cpu_task_type_ticks){
				i++;
			}
			ytDelay(cpu_task_off_time);
			current_time = ytGetSysTickMilliSec();
			if(stop_received){
				stop_received--;
				ytPrintf("CPU_TASK stopped\r\n");
				break;
			}
		}
		test_running--;
		ytPrintf("CPU_TASK test done\r\n");
	}
}




#if NODE_1

static retval_t cpu_task_cmd_callback(uint16_t argc, char** argv){
	if(argc<2){
		ytPrintf("Wrong param num\r\n");
		return RET_ERROR;
	}
	else{
		if(strcmp(argv[1], "set_pri") == 0){
			if(argc != 3){
				ytPrintf("Wrong set_pri params\r\n");
				return RET_ERROR;
			}
			if(test_running){
				ytPrintf("Error:Test is running\r\n");
				return RET_ERROR;
			}
			int32_t pri = (int32_t)atoi(argv[2]);
			process_class_t priority;
			if((pri<1) || (pri>5)){
				ytPrintf("Wrong priority param\r\n");
				return RET_ERROR;
			}
			switch(pri){
			case 1:
				priority = VERY_LOW_PRIORITY_PROCESS;
				break;
			case 2:
				priority = LOW_PRIORITY_PROCESS;
				break;
			case 3:
				priority = NORMAL_PRIORITY_PROCESS;
				break;
			case 4:
				priority = HIGH_PRIORITY_PROCESS;
				break;
			case 5:
				priority = VERY_HIGH_PRIORITY_PROCESS;
				break;
			default:
				return RET_ERROR;
			}
			ytSetProcessPriority(paper_y_cpu_proc_id, priority);
			ytPrintf("CPU process priority set to %d\r\n", pri);
		}
		else if(strcmp(argv[1], "set_type") == 0){
			if(argc != 3){
				ytPrintf("Wrong set_type params\r\n");
				return RET_ERROR;
			}
			if(test_running){
				ytPrintf("Error:Test is running\r\n");
				return RET_ERROR;
			}
			uint32_t type = (uint32_t)atoi(argv[2]);
			if((type<1) || (type>3)){
				ytPrintf("Wrong type param [1-3]\r\n");
				return RET_ERROR;
			}
			switch(type){
			case 1:
				cpu_task_type_ticks = CPU_TYPE_1;
				cpu_task_type_ms = CPU_TYPE_1_MS;
				break;
			case 2:
				cpu_task_type_ticks = CPU_TYPE_2;
				cpu_task_type_ms = CPU_TYPE_2_MS;
				break;
			case 3:
				cpu_task_type_ticks = CPU_TYPE_3;
				cpu_task_type_ms = CPU_TYPE_3_MS;
				break;
				cpu_task_type_ticks = CPU_TYPE_1;
				break;
			default:
				return RET_ERROR;
			}
			cpu_task_period = (cpu_task_type_ms * 100)/	cpu_task_dc;
			cpu_task_off_time = cpu_task_period - cpu_task_type_ms;
			ytPrintf("CPU process type set to %d\r\n", type);
		}

		else if(strcmp(argv[1], "set_dc") == 0){
			if(argc != 3){
				ytPrintf("Wrong set_type params\r\n");
				return RET_ERROR;
			}
			if(test_running){
				ytPrintf("Error:Test is running\r\n");
				return RET_ERROR;
			}
			uint32_t dc = (uint32_t)atoi(argv[2]);
			if((dc<1) || (dc>100)){
				ytPrintf("Wrong type param [1-100]\r\n");
				return RET_ERROR;
			}
			cpu_task_dc = dc;
			cpu_task_period = (cpu_task_type_ms * 100)/	cpu_task_dc;
			cpu_task_off_time = cpu_task_period - cpu_task_type_ms;
			ytPrintf("CPU process duty cycle set to %d%\r\n", dc);
		}

		return RET_OK;
	}
}

static retval_t ytest_cmd_callback(uint16_t argc, char** argv){
	if(argc<2){
		ytPrintf("Wrong param num\r\n");
		return RET_ERROR;
	}
	else{
		if(strcmp(argv[1], "start") == 0){
			if(argc != 2){
				ytPrintf("Wrong start test params\r\n");
				return RET_ERROR;
			}
			if(test_running){
				ytPrintf("Error: Test already running\r\n");
				return RET_ERROR;
			}
			if(!ytMacIsNodeLinked()){
				ytPrintf("Error: Mac not linked yet\r\n");
				return RET_ERROR;
			}

			if(test_type == TEST_TYPE_UDP){

			}
			else if(test_type == TEST_TYPE_TCP){

			}
			else{
				ytPrintf("Wrong test type\r\n");
				return RET_ERROR;
			}


			ytSemaphoreRelease(semaphore_udp);
		}
		else if(strcmp(argv[1], "stop") == 0){
					if(argc != 2){
						ytPrintf("Wrong stop test params\r\n");
						return RET_ERROR;
					}
					if(!test_running){
						ytPrintf("Error: Test is not running\r\n");
						return RET_ERROR;
					}

					if(test_type == TEST_TYPE_UDP){

					}
					else if(test_type == TEST_TYPE_TCP){

					}
					else{
						ytPrintf("Wrong test type\r\n");
						return RET_ERROR;
					}


					stop_received +=2;
				}
		else if(strcmp(argv[1], "set_time") == 0){
			if(argc != 3){
				ytPrintf("Wrong set test time params\r\n");
				return RET_ERROR;
			}
			if(test_running){
				ytPrintf("Error: Test already running\r\n");
				return RET_ERROR;
			}
			uint32_t time = (uint32_t)atoi(argv[2]);
			if((time<1) || (time>10000)){
				ytPrintf("Wrong time param [1-1000000]\r\n");
				return RET_ERROR;
			}
			test_time = (time*1000)+TEST_OFFSET_TIME;
			ytPrintf("Test time set to: %d\r\n", time);
		}
		else if(strcmp(argv[1], "set_type") == 0){
			if(argc != 3){
				ytPrintf("Wrong set test type params\r\n");
				return RET_ERROR;
			}
			if(test_running){
				ytPrintf("Error: Test already running\r\n");
				return RET_ERROR;
			}
			uint32_t type = (uint32_t)atoi(argv[2]);
			if((type<1) || (type>2)){
				ytPrintf("Wrong time param [1(UDP)-2(TCP)]\r\n");
				return RET_ERROR;
			}
			test_type = type;
			ytPrintf("Test type set to: %d\r\n", type);
		}
		else{
			ytPrintf("Error: wrong command\r\n");
		}
		return RET_OK;
	}
}

#endif

static retval_t set_node_addr(void){
	ytNetAddr_t net_addr;

#if NODE_1
	/* Set node Addr */
	net_addr = ytNewNetAddr("1", 'D');		//Node address
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);
#endif

#if NODE_2
	/* Set node Addr */
	net_addr = ytNewNetAddr("2", 'D');		//Node address
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);
#endif

#if NODE_3
	/* Set node Addr */
	net_addr = ytNewNetAddr("3", 'D');		//Node address
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);

	ytSetNodeAsGw();
#endif

	return RET_OK;
}
#endif
