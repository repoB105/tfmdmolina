/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * current_measure.c
 *
 *  Created on: 3 may. 2019
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file current_measure.c
 */

#include "system_api.h"
#include "adc_driver.h"
#include <stdlib.h>

#define USE_CURRENT_MEASURE_APP		1

#if USE_CURRENT_MEASURE_APP

#define ADC_BUFFER_SIZE			512
#define DEFAULT_NUM_SAMPLES		32


#define VSYS_CTL_PIN			GPIO_PIN_C0
#define VDIG_CTL_PIN			GPIO_PIN_C2

#define VSYS_INTERRUPT_PIN		GPIO_PIN_C1
#define VDIG_INTERRUPT_PIN		GPIO_PIN_C3

#define VSYS_ADC_PIN			GPIO_PIN_A0
#define VDIG_ADC_PIN			GPIO_PIN_A4

#define INIT_FRAME_ID			0x0007FFFA

#define LOW_CURRENT_THRESHOLD	20
#define HIGH_CURRENT_THRESHOLD	62000

typedef enum meas_mode_t{
	HIGH_CURRENT_MODE = 0,
	LOW_CURRENT_MODE = 1,
}meas_mode_t;

typedef enum operating_mode_{
	FIXED_HIGH_CURRENT_MODE = 0,
	FIXED_LOW_CURRENT_MODE = 1,
	DYNAMIC_MODE = 2,
}operating_mode_t;

static meas_mode_t vsys_mode = HIGH_CURRENT_MODE;
static meas_mode_t vdig_mode = HIGH_CURRENT_MODE;

static uint32_t read_buffer[ADC_BUFFER_SIZE];			//This buffer stores both channels signals. Each uint32 sample contains two simultaneous uin16_t samples, one from each channel.
static uint32_t* current_output_ptr;
static uint32_t* next_read_ptr;
static uint16_t stored_samples;
static uint16_t num_samples_to_read;

static uint16_t readAdcProcId;
static uint16_t outputDataProcId;

static uint32_t data_ready_semaphore;
static uint32_t buffer_full_semaphore;

static uint16_t sw_happened = 0;
static operating_mode_t vsys_op_mode = DYNAMIC_MODE;
static operating_mode_t vdig_op_mode = DYNAMIC_MODE;

//Processes
YT_PROCESS static void read_adcs_func(void const * argument);
YT_PROCESS static void data_output_func(void const * argument);

static retval_t config_adc(uint32_t* adc_fd);
static void vsys_pin_callback(void const * argument);
static void vdig_pin_callback(void const * argument);

static retval_t input_config_command(uint16_t argc, char** argv);

ytInitProcess(read_adcs_proc, read_adcs_func, HIGH_PRIORITY_PROCESS, 256, &readAdcProcId, NULL);
ytInitProcess(data_out_proc, data_output_func, NORMAL_PRIORITY_PROCESS, 256, &outputDataProcId, NULL);

//Read data process
YT_PROCESS static void read_adcs_func(void const * argument){
	uint32_t adc_fd;
	next_read_ptr = &read_buffer[0];
	current_output_ptr = &read_buffer[0];
	stored_samples = 0;
	num_samples_to_read = DEFAULT_NUM_SAMPLES;

	data_ready_semaphore = ytSemaphoreCreate(1);
	ytSemaphoreWait(data_ready_semaphore, YT_WAIT_FOREVER);
	data_ready_semaphore = ytSemaphoreCreate(1);
	ytSemaphoreWait(buffer_full_semaphore, YT_WAIT_FOREVER);


	ytGpioInitPin(VSYS_INTERRUPT_PIN, GPIO_PIN_INTERRUPT_RISING, GPIO_PIN_NO_PULL);		//HIGH CURRENT INTERRUPT
	ytGpioInitPin(VDIG_INTERRUPT_PIN, GPIO_PIN_INTERRUPT_RISING, GPIO_PIN_NO_PULL);		//HIGH CURRENT INTERRUPT

	ytGpioInitPin(VSYS_CTL_PIN, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);		//CURRENT CONTROL PIN
	ytGpioPinSet(VSYS_CTL_PIN);
	ytGpioInitPin(VDIG_CTL_PIN, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);		//CURRENT CONTROL PIN
	ytGpioPinSet(VDIG_CTL_PIN);

	config_adc(&adc_fd);

	ytRegisterYetishellCommand("set_op_mode", input_config_command);
//	ytGpioPinSetCallbackInterrupt(VSYS_INTERRUPT_PIN, vsys_pin_callback, NULL);
//	ytGpioPinSetCallbackInterrupt(VDIG_INTERRUPT_PIN, vdig_pin_callback, NULL);

	while(1){

		ytRead(adc_fd, (uint8_t*)next_read_ptr, num_samples_to_read);				//Continuously read the data
		if(next_read_ptr+num_samples_to_read >= &read_buffer[ADC_BUFFER_SIZE]){
			next_read_ptr = &read_buffer[0];
		}
		else{
			next_read_ptr = next_read_ptr+num_samples_to_read;
		}

		stored_samples += num_samples_to_read;
		ytSemaphoreRelease(data_ready_semaphore);						//Process and send the data

		if(stored_samples > (ADC_BUFFER_SIZE - num_samples_to_read)){	//Check if there is enough space for more samples
			ytSemaphoreWait(buffer_full_semaphore, YT_WAIT_FOREVER);	//Wait until there is enough space in the buffer
		}
	}
}
/* *************************************************/

//Data output process
YT_PROCESS static void data_output_func(void const * argument){
	uint32_t avg_vsys;
	uint32_t avg_vdig;
	uint16_t i;
	uint32_t frame_id = INIT_FRAME_ID;


	while(1){

		ytSemaphoreWait(data_ready_semaphore, YT_WAIT_FOREVER);			//Wait till there is data available to be sent

		//Obtain average values
		avg_vsys = 0;
		avg_vdig = 0;
		for (i=0; i<num_samples_to_read; i++){
			avg_vsys += ((*current_output_ptr)&0x0000FFFF);
			avg_vdig += (((*current_output_ptr)&0xFFFF0000)>>16);
		}
		avg_vsys = avg_vsys/num_samples_to_read;
		avg_vdig = avg_vdig/num_samples_to_read;
		if(!sw_happened){		//Ignore this samples if a switching in the current mode has happened
			//Send the MSG IDENTIFIER
			ytStdoutSend((uint8_t*) &frame_id, sizeof(uint32_t), YT_WAIT_FOREVER);	//First send the frame ID
			//Send the operating mode
			ytStdoutSend((uint8_t*) &vsys_mode, sizeof(meas_mode_t), YT_WAIT_FOREVER);
			ytStdoutSend((uint8_t*) &vdig_mode, sizeof(meas_mode_t), YT_WAIT_FOREVER);
			//Send samples
			ytStdoutSend((uint8_t*) current_output_ptr, num_samples_to_read*sizeof(uint32_t), YT_WAIT_FOREVER);

			//Update the current mode
			if(vsys_op_mode == DYNAMIC_MODE){
				if(avg_vsys < LOW_CURRENT_THRESHOLD){
					if(vsys_mode != LOW_CURRENT_MODE){
						vsys_mode = LOW_CURRENT_MODE;			//Switch to low current mode
						sw_happened = 1;
						ytGpioPinReset(VSYS_CTL_PIN);
					}
				}
				else if(avg_vsys > HIGH_CURRENT_THRESHOLD){
					if(vsys_mode != HIGH_CURRENT_MODE){
						vsys_mode = HIGH_CURRENT_MODE;			//Switch to high current mode
						sw_happened = 1;
						ytGpioPinSet(VSYS_CTL_PIN);
					}
				}
			}

			if(vdig_op_mode == DYNAMIC_MODE){
				if(avg_vdig < LOW_CURRENT_THRESHOLD){
					if(vdig_mode != LOW_CURRENT_MODE){
						vdig_mode = LOW_CURRENT_MODE;			//Switch to low current mode
						sw_happened = 1;
						ytGpioPinReset(VDIG_CTL_PIN);
					}
				}
				else if(avg_vdig > HIGH_CURRENT_THRESHOLD){
					if(vdig_mode != HIGH_CURRENT_MODE){
						vdig_mode = HIGH_CURRENT_MODE;			//Switch to high current mode
						sw_happened = 1;
						ytGpioPinSet(VDIG_CTL_PIN);
					}
				}
			}
		}
		else{
			sw_happened = 0;
		}


		//Work with output buffers
		if(current_output_ptr+num_samples_to_read >= &read_buffer[ADC_BUFFER_SIZE]){
			current_output_ptr = &read_buffer[0];
		}
		else{
			current_output_ptr = current_output_ptr+num_samples_to_read;
		}

		if(stored_samples > (ADC_BUFFER_SIZE - num_samples_to_read)){	//Read thread should be waiting if this happens, so free the semaphore since now there is enough space in the buffer
			stored_samples -= num_samples_to_read;
			ytSemaphoreRelease(buffer_full_semaphore);					//Now there is space for more samples;
		}
		else{
			stored_samples -= num_samples_to_read;
		}


	}
}
/* *************************************************/

static void vsys_pin_callback(void const * argument){
	vsys_mode = HIGH_CURRENT_MODE;			//Switch to high current mode
	sw_happened = 1;
	ytGpioPinSet(VSYS_CTL_PIN);
}

static void vdig_pin_callback(void const * argument){
	vdig_mode = HIGH_CURRENT_MODE;			//Switch to high current mode
	sw_happened = 1;
	ytGpioPinSet(VDIG_CTL_PIN);
}

//Configuration command function for the acquisition module
static retval_t input_config_command(uint16_t argc, char** argv){

	if(argc < 2){
		return RET_ERROR;
	}
	else{
		if(argc == 3){
			if (argv[1][0] == 'S'){		//Op mode of Vsys
				if (argv[2][0] == 'H'){		//Fixed high current mode
					vsys_op_mode = FIXED_HIGH_CURRENT_MODE;
					vsys_mode = HIGH_CURRENT_MODE;			//Switch to high current mode
					sw_happened = 1;
					ytGpioPinSet(VSYS_CTL_PIN);
				}
				else if (argv[2][0] == 'L'){//Fixed low current mode
					vsys_op_mode = FIXED_LOW_CURRENT_MODE;
					vsys_mode = LOW_CURRENT_MODE;			//Switch to low current mode
					sw_happened = 1;
					ytGpioPinReset(VSYS_CTL_PIN);
				}
				else if (argv[2][0] == 'D'){//Dynamic current mode
					vsys_op_mode = DYNAMIC_MODE;
					vsys_mode = HIGH_CURRENT_MODE;			//Switch to high current mode
					sw_happened = 1;
					ytGpioPinSet(VSYS_CTL_PIN);
				}
			}
			else if (argv[1][0] == 'D'){//Op mode of Vsys
				if (argv[2][0] == 'H'){		//Fixed high current mode
					vdig_op_mode = FIXED_HIGH_CURRENT_MODE;
					vdig_mode = HIGH_CURRENT_MODE;			//Switch to high current mode
					sw_happened = 1;
					ytGpioPinSet(VDIG_CTL_PIN);
				}
				else if (argv[2][0] == 'L'){//Fixed low current mode
					vdig_op_mode = FIXED_LOW_CURRENT_MODE;
					vdig_mode = LOW_CURRENT_MODE;			//Switch to high current mode
					sw_happened = 1;
					ytGpioPinReset(VDIG_CTL_PIN);
				}
				else if (argv[2][0] == 'D'){//Dynamic current mode
					vdig_op_mode = DYNAMIC_MODE;
					vdig_mode = HIGH_CURRENT_MODE;			//Switch to high current mode
					sw_happened = 1;
					ytGpioPinSet(VDIG_CTL_PIN);
				}

			}
			else return RET_ERROR;
		}
		else{
			return RET_ERROR;
		}
	}
	return RET_OK;

}

static retval_t config_adc(uint32_t* adc_fd){
	adc_config_t* adc_config = (adc_config_t*) ytMalloc(sizeof(adc_config_t));
	adc_config->channel_speed = 3125;
	adc_config->dual_mode_enabled = 1;
	adc_config->diff2_mode_enabled = 0;
	adc_config->diff1_mode_enabled = 0;
	adc_config->adc_pin_se1 = VSYS_ADC_PIN;	//Channel 1
//	adc_config->adc_pin_diff1 = GPIO_PIN_A1;

	adc_config->adc_pin_se2 = VDIG_ADC_PIN;  //Channel 2
//	adc_config->adc_pin_diff2 = GPIO_PIN_A7;

	*adc_fd = ytOpen(ADC_DEV, 0);
	ytIoctl(*adc_fd, (uint16_t) CONFIG_ADC, (void*) adc_config);

	ytFree(adc_config);

	return RET_OK;
}



#endif
