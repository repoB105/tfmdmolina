/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * monitoring_migou.h
 *
 *  Created on: 10/10/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file monitoring_migou.h
 */

#ifndef YETIOS_APPS_INCLUDE_MONITORING_MIGOU_H_
#define YETIOS_APPS_INCLUDE_MONITORING_MIGOU_H_

#include "adc_driver.h"

// Current Circuit Values
#define VDDA					(float32_t)3320		// VDDA = 3.32 V (measured)
#define ADC_NSTEPS				(float32_t)65536	// 16-bit
#define VSTEP					(float32_t)VDDA/ADC_NSTEPS
#define SENSE_AMP_GAIN			(float32_t)50
#define RSENSE_SYS				(float32_t)0.2
#define RSENSE_TRX_LOW			(float32_t)60.4		// Sensing resistor for LOW  current consumption
#define RSENSE_TRX_HIGH			(float32_t)0.62		// Sensing resistor for HIGH current consumption

// Number of samples to average
#define NSAMPLES				300
#define CM_DELAY				500

// Masks for ADC Dual Mode
#define ISYS_MASK				0x0000FFFF
#define ITRX_MASK				0xFFFF0000

// System Current Monitoring Calibration
//#define ISYS_MEAS_CORRECTION	(float32_t) 1.041			// Performed with the BK Precision 8540 Electronic Load
//#define ISYS_MEAS_OFFSET		(float32_t)-1.4874			// Performed with the BK Precision 8540 Electronic Load
#define ISYS_MEAS_CORRECTION	(float32_t) 1.041			// Performed with the 121GW EEVBlog Multimeter
#define ISYS_MEAS_OFFSET		(float32_t)-0.8465			// Performed with the 121GW EEVBlog Multimeter

// Transceiver Current Monitoring Range
#define ITRX_S_RANGE_DEFAULT	HIGH_RANGE

// Thresholds (mA) for the change of VTRX Current Monitoring Range
#define HIGH_S_TH				(float32_t)1	// At his current Rsense is change from RSENSE_TRX_LOW to RSENSE_TRX_HIGH
#define LOW_S_TH				(float32_t)0.5	// At his current Rsense is change from RSENSE_TRX_HIGH to RSENSE_TRX_LOW

// VTRX High Current Monitoring Calibration
//#define ITRX_H_MEAS_CORRECTION	(float32_t)	1.078		// Performed with the BK Precision 8540 Electronic Load
//#define ITRX_H_MEAS_OFFSET		(float32_t)-0.1977		// Performed with the BK Precision 8540 Electronic Load
#define ITRX_H_MEAS_CORRECTION	(float32_t)	1.076			// Performed with the 121GW EEVBlog Multimeter
#define ITRX_H_MEAS_OFFSET		(float32_t)-0.0447			// Performed with the 121GW EEVBlog Multimeter

// VTRX Low Current Monitoring Calibration
#define ITRX_L_MEAS_CORRECTION	(float32_t)	1.11			// Performed with the 121GW EEVBlog Multimeter
#define ITRX_L_MEAS_OFFSET		(float32_t)-1.6158/1000		// Performed with the 121GW EEVBlog Multimeter. Divided by 1000 because the calibration was performed in uA, but the get_current() return the current in mA.


typedef enum itrx_sense_range_{
	LOW_RANGE	= 0x0,
	HIGH_RANGE	= 0x1
}itrx_sense_range_t;


static retval_t monitoring_mcu_init(uint32_t* adc_fd);
static void monitoring_mcu_deinit(uint32_t* adc_fd);
static void swap_trx_rsense(void);
static retval_t get_current(uint32_t* adc_fd, float32_t* Isys, float32_t* Itrx);

#endif /* YETIOS_APPS_INCLUDE_MONITORING_MIGOU_H_ */
