/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * tcp_example.c
 *
 *  Created on: 19 feb. 2019
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file tcp_example.c
 */

#include "system_api.h"
#include "system_net_api.h"
#include "time_meas.h"

#define USE_TCP_EXAMPLE_PROCESS		0

#if USE_TCP_EXAMPLE_PROCESS

#define NODE_1	1
//#define NODE_2	2

#define NET_PORT			232

uint16_t testProcTcpId;

static void set_node_addr(void);

/* TEST PROCESS TCP ********************************/
YT_PROCESS void tcp_test_func(void const * argument);

ytInitProcess(tcp_test_proc, tcp_test_func, NORMAL_PRIORITY_PROCESS, 256, &testProcTcpId, NULL);

#define BUF_SIZE	32
char tx_data[BUF_SIZE];
char rx_data[BUF_SIZE];

char addr_str_buf[8];

YT_PROCESS void tcp_test_func(void const * argument){
	ytNetAddr_t net_addr;
	uint32_t connection_fd = 0;
	uint32_t pckt_id = 0;
	set_node_addr();

#if NODE_2
	uint32_t net_server = ytRlCreateServer(NET_PORT);		//First create the server instance
	net_addr = ytNewEmptyNetAddr();	//The source address is stored in this object
#endif

	ytGpioInitPin(GPIO_PIN_B4, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);	//Init pin for Spirit1 led

	ytDelay(1000);

	while(1){
#if NODE_1
		if(!ytRlCheckConnection(connection_fd)){	// Check the TCP connection. Reconnect if the connection is closed
			net_addr = ytNewNetAddr("2", 'D');	//Send to node 2
			connection_fd = ytRlConnectToServer(net_addr, NET_PORT);
			ytDeleteNetAddr(net_addr);
		}

		if(connection_fd){
			sprintf(tx_data, "TEST_PACKET ID: %d\r\n", (int) pckt_id);

			if(ytRlSend(connection_fd, (uint8_t*) tx_data, strlen(tx_data) + 1) == (strlen(tx_data)+1)){	//Send the string including the \0 end character
				ytPrintf("SEND OK\r\n");
			}

			else{
				ytPrintf("SEND ERROR\r\n");
			}
			pckt_id++;
		}
		ytDelay(25);
		ytGpioPinSet(GPIO_PIN_B4);
		ytDelay(25);
		ytGpioPinReset(GPIO_PIN_B4);
#endif


#if NODE_2


		if(!ytRlCheckConnection(connection_fd)){	//Accept connections if no node is conected
			connection_fd = ytRlServerAcceptConnection(net_server, net_addr);
		}
		if(connection_fd){
			if(ytRlRcv(connection_fd, (uint8_t*) rx_data, BUF_SIZE)){
				ytNetAddrToString(net_addr, addr_str_buf, 'D');
				float32_t lvl =  ytRlGetSignalLevel(connection_fd);
				ytPrintf("RCV: %s from %s  LVL: %.2fdB\r\n",(uint8_t*) rx_data, addr_str_buf, lvl);

				ytGpioPinToggle(GPIO_PIN_B4);
			}
		}
		ytDelay(10);
#endif

	}
}
/* *************************************************/

static void set_node_addr(void){
	ytNetAddr_t net_addr;
#if NODE_1
	/* Set node Addr */
	net_addr = ytNewNetAddr("1", 'D');		//Node address
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);
#endif

#if NODE_2
	/* Set node Addr */
	net_addr = ytNewNetAddr("2", 'D');		//Node address
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);

	ytSetNodeAsGw();						//Define This node as the GW
#endif
}

#endif	// USE_TEST_PROCESS_TCP
