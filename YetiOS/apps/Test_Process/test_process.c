/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * test_process.c
 *
 *  Created on: 15 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file test_process.c
 */

#include "system_api.h"
#include "system_net_api.h"
#include "time_meas.h"
#include <stdio.h>

#define USE_TEST_PROCESS	0

#if USE_TEST_PROCESS

#define NODE_1	1
//#define NODE_2	2
//#define NODE_3	3

#define NET_PORT			100
#define BC_DEST_ADDR		"65535"

uint16_t testProcId;

static void set_addr_and_routes(void);
/* TEST PROCESS ************************************/
YT_PROCESS void test_func(void const * argument);


ytInitProcess(test_process, test_func, LOW_PRIORITY_PROCESS, 300, &testProcId, NULL);

#define BUF_SIZE	24
char tx_data[BUF_SIZE];
char rx_data[BUF_SIZE];

uint8_t addr_str_buf[8];



YT_PROCESS void test_func(void const * argument){
	ytTimeMeas_t time_meas;
	ytNetAddr_t net_addr;
	uint32_t pckt_num = 0;
	uint32_t missed_packets = 0;
	set_addr_and_routes();
	sprintf(tx_data, "TEST_PACKET\r\n");
//	ytGpioInitPin(GPIO_PIN_A0, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);
//	ytGpioInitPin(GPIO_PIN_B4, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);
	ytDelay(2000);
	while(1){

#if NODE_1
		ytUrOpen(NET_PORT);	//Abro el puerto

		net_addr = ytNewNetAddr("3", 'D');	//Envio al  nodo 3
		ytStartTimeMeasure(&time_meas);
//		if(ytUrSend(NET_PORT, net_addr,(uint8_t*) tx_data, strlen(tx_data) + 1) == (strlen(tx_data)+1)){
		if(ytUrSend(NET_PORT, net_addr,(uint8_t*) &pckt_num, sizeof(uint32_t)) == sizeof(uint32_t)){
			ytStopTimeMeasure(&time_meas);
			ytPrintf("SEND OK\r\n");
			pckt_num++;
		}
//		ytDelay(10);
//		if(ytUrSend(NET_PORT, net_addr,(uint8_t*) &pckt_num, sizeof(uint32_t)) == sizeof(uint32_t)){
//			ytStopTimeMeasure(&time_meas);
//			ytPrintf("SEND OK\r\n");
//			pckt_num++;
//		}
		ytUrClose(NET_PORT);
		ytDeleteNetAddr(net_addr);
//		ytDelay(150);
		ytLedsOn(LEDS_BLUE);
//		ytGpioPinSet(GPIO_PIN_B4);
		ytDelay(25);
//		ytGpioPinReset(GPIO_PIN_B4);
		ytDelay(25);
		ytLedsOff(LEDS_BLUE);

//		if((ytTimeGetTimestamp()%200) == 0){
//			ytGpioPinToggle(GPIO_PIN_A0);
//			ytDelay(150);
//		}
#endif

#if NODE_2
		ytDelay(3000);
		ytLedsToggle(LEDS_BLUE);
		ytDelay(100);
		ytLedsToggle(LEDS_BLUE);
#endif

#if NODE_3
//		if((ytTimeGetTimestamp()%200) == 0){
//			ytGpioPinToggle(GPIO_PIN_A0);
//			ytDelay(150);
//		}
		uint32_t current_pckt_num, first_packet_num;
		ytUrOpen(NET_PORT);	//Abro el puerto
		net_addr = ytNewNetAddr("22222", 'D');	//Creo una direcci�n con cualquier valor donde se guardara la direccion recibida
//		ytUrRcv(NET_PORT, net_addr,(uint8_t*) rx_data, BUF_SIZE);
		uint32_t num_rcv = ytUrRcv(NET_PORT, net_addr,(uint8_t*) &current_pckt_num, BUF_SIZE);

		if(num_rcv != 0){
			if(pckt_num != 0){	//It is not the first packet
				if(current_pckt_num > pckt_num){
					missed_packets += current_pckt_num - (pckt_num+1);
					pckt_num = current_pckt_num;
				}
				else if (current_pckt_num < pckt_num){	//An unordered previous packet has arrived. We can substract a missed packet
					missed_packets--;
				}
				ytPrintf("Rcv: %d\t Miss: %d\t PM %.2f%% (Rcv: %d B)\r\n", current_pckt_num-first_packet_num, missed_packets, (((float32_t)missed_packets)/((float32_t)(pckt_num-first_packet_num))*100), num_rcv);
			}
			else{
				missed_packets = 0;
				first_packet_num = current_pckt_num;
				pckt_num = current_pckt_num;
			}
		}
		else{
			ytDelay(10);
		}
//		ytNetAddrToString(net_addr, addr_str_buf, 'D');
//		ytPrintf("RCV: %s from %s\r\n",(uint8_t*) rx_data, addr_str_buf);

		ytUrClose(NET_PORT);
		ytDeleteNetAddr(net_addr);
//		ytGpioPinToggle(GPIO_PIN_B4);
		ytLedsToggle(LEDS_BLUE);
#endif


	}
}
/* *************************************************/

static void set_addr_and_routes(void){
	ytNetAddr_t net_addr;
	ytNetAddr_t next_addr;
#if NODE_1
	/* Set node Addr */
	net_addr = ytNewNetAddr("1", 'D');		//Node adress
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);

	ytSetNodeAsGw();

	/* Set node Routes */
//	net_addr = ytNewNetAddr("3", 'D');		//Dest adress
//	next_addr = ytNewNetAddr("3", 'D');		//Next adress
//	ytNetAddRoute(net_addr, next_addr, 1);
//	ytDeleteNetAddr(net_addr);
//	ytDeleteNetAddr(next_addr);
//
//	net_addr = ytNewNetAddr("2", 'D');		//Dest adress
//	next_addr = ytNewNetAddr("2", 'D');		//Next adress
//	ytNetAddRoute(net_addr, next_addr, 0);
//	ytDeleteNetAddr(net_addr);
//	ytDeleteNetAddr(next_addr);
#endif

#if NODE_2
	/* Set node Addr */
	net_addr = ytNewNetAddr("2", 'D');		//Node adress
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);

	/* Set node Routes */
//	net_addr = ytNewNetAddr("3", 'D');		//Dest adress
//	next_addr = ytNewNetAddr("3", 'D');		//Next adress
//	ytNetAddRoute(net_addr, next_addr, 1);
//	ytDeleteNetAddr(net_addr);
//	ytDeleteNetAddr(next_addr);
//
//	net_addr = ytNewNetAddr("1", 'D');		//Dest adress
//	next_addr = ytNewNetAddr("1", 'D');		//Next adress
//	ytNetAddRoute(net_addr, next_addr, 0);
//	ytDeleteNetAddr(net_addr);
//	ytDeleteNetAddr(next_addr);
#endif

#if NODE_3
	/* Set node Addr */
	net_addr = ytNewNetAddr("3", 'D');		//Node adress
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);

	/* Set node Routes */
//	net_addr = ytNewNetAddr("1", 'D');		//Dest adress
//	next_addr = ytNewNetAddr("2", 'D');		//Next adress
//	ytNetAddRoute(net_addr, next_addr, 1);
//	ytDeleteNetAddr(net_addr);
//	ytDeleteNetAddr(next_addr);
//
//	net_addr = ytNewNetAddr("2", 'D');		//Dest adress
//	next_addr = ytNewNetAddr("2", 'D');		//Next adress
//	ytNetAddRoute(net_addr, next_addr, 0);
//	ytDeleteNetAddr(net_addr);
//	ytDeleteNetAddr(next_addr);
#endif
}

#endif
