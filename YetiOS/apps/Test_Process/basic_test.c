/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * basic_test.c
 *
 *  Created on: 12 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file basic_test.c
 */

#include "system_api.h"
#define USE_BASIC_TEST	0

#if USE_BASIC_TEST


uint16_t basicTestProcId;

/* TBASIC EST PROCESS ************************************/
YT_PROCESS void basic_test_func(void const * argument);

ytInitProcess(basic_test_proc, basic_test_func, NORMAL_PRIORITY_PROCESS, 256, &basicTestProcId, NULL);

uint8_t test_buff[128];
YT_PROCESS void basic_test_func(void const * argument){
//	uint32_t sensor_fd;
//	float32_t temp_val = 0;
//	uint16_t proc_id;
//	usb_arch_init();
//	uint32_t uart_fd;

	while(1){


//		usb_arch_start_storing_data();
//
//		usb_arch_read_line(test_buff, 128);
//
//		usb_arch_write(test_buff, strlen(test_buff));
//		usb_arch_write("\r\n", 2);
//		usb_arch_stop_storing_data();

		ytDelay(1000);

	}
}
/* *************************************************/


#endif
