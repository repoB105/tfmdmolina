/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * ad7124_test.c
 *
 *  Created on: 3 may. 2019
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file ad7124_test.c
 */

#include "system_api.h"
#include "ad7124.h"
#include "ad7124_regs.h"
#include <stdlib.h>

#define USE_AD7124_TEST	0

#if USE_AD7124_TEST

#define AD7124_GPIO_DRY_PIN 	GPIO_PIN_C5

#define ADC_GAIN				1

#define NUMBER_OF_CHANNELS		4
#define CHANNEL_BUFFER_SIZE		512

#define NUM_2_POW_23			8388608

#define ENABLE_OUTPUT_DATA		1
#define INIT_FRAME_ID			0xFFFFEFAD
#define END_FRAME_ID			0xFFFFFADE

#define DEFAULT_FFT_SAMPLE_NUM	256				//Must be a 2s power
//Channel type
typedef struct channel_buff_{
	float32_t sample_buffer[CHANNEL_BUFFER_SIZE];
	float32_t* buff_ptr;
}channel_buff_t;


//Processing modes
typedef enum processing_mode_{
	PROC_TIME_DOMAIN = 0,
	PROC_FFT = 1,
	//MULTICHANNEL POST PROC
}processing_mode_t;

//Processing data configuration type
typedef struct processing_config_{
	processing_mode_t processing_mode;
	//Window
	uint16_t fft_sample_num;
	//Etc
}processing_config_t;

//Channel buffers
static channel_buff_t* channel_buff;

//Channel output buffers
static float32_t channel_output_buf[NUMBER_OF_CHANNELS][CHANNEL_BUFFER_SIZE];

//Processing configuration
static processing_config_t processing_config;
//Processing configuration used to update the current values from command interface
static processing_config_t update_processing_config;

//Processes instances
static uint16_t ad7124AcqProcId;
static uint16_t processingProcId;
static uint16_t dataOutProcId;

static uint32_t ad7124_read_semaphore;
static uint32_t all_channels_sampled_semaphore;
static uint32_t output_data_semaphore;

//Process FFT Function
static retval_t process_fft(float32_t* in_data, float32_t* out_data, uint16_t fft_sample_num);

//Data ready Pin Callback
static void data_ready_pin_callback(void const * argument);

//Configuration command function for the acquisition module
static retval_t acq_config_command(uint16_t argc, char** argv);

//AD7124 Acquisition process
YT_PROCESS static void ad7124_acq_func(void const * argument);
//Signals Processing process
YT_PROCESS static void processing_func(void const * argument);
//Data output process
YT_PROCESS static void data_out_func(void const * argument);

ytInitProcess(ad7124_acq_proc, ad7124_acq_func, VERY_HIGH_PRIORITY_PROCESS, 256, &ad7124AcqProcId, NULL);
ytInitProcess(processing_proc, processing_func, HIGH_PRIORITY_PROCESS, 256, &processingProcId, NULL);
ytInitProcess(data_out_proc, data_out_func, NORMAL_PRIORITY_PROCESS, 256, &dataOutProcId, NULL);

#define BLOCK_SIZE           256
#define NUM_TAPS 			 65

static arm_fir_instance_f32 S1;
static float32_t firStateF32[BLOCK_SIZE + NUM_TAPS - 1];

static float32_t filter_coefficients[NUM_TAPS] =
{
		0.0606467253536945722f, -0.0314183213878071535f, -0.00852681395914876156f, 0.011526883911874692f,
		0.0242251070547044624f, 0.0273471078870842592f, 0.0210370620252575136f, 0.00743938709260434736f,
		-0.00947297562820596448f, -0.0248110303640633016f, -0.0336954522736558765f, -0.032876389196422294f,
		-0.0217982870365519624f, -0.00339653119240760455f, 0.0167611524925375503f, 0.031977518605460005f,
		0.0365893564198917567f, 0.0281388222882209688f, 0.00844680046341009932f, -0.0165406165699413209f,
		-0.0384871302898627046f, -0.049594202439858695f, -0.0454732679720572797f, -0.0266465819451935648f,
		0.0013560839830527719f, 0.0292522658663128404f, 0.0474616479531444757f, 0.0497310286994902817f,
		0.0344935200828566726f, 0.00678786852775527423f, -0.0245091872884221733f, -0.0487863691581232745f,
		0.942044393667672786f, -0.0487863691581232745f, -0.0245091872884221733f, 0.00678786852775527423f,
		0.0344935200828566726f, 0.0497310286994902817f, 0.0474616479531444757f, 0.0292522658663128404f,
		0.0013560839830527719f, -0.0266465819451935648f, -0.0454732679720572797f, -0.049594202439858695f,
		-0.0384871302898627046f, -0.0165406165699413209f, 0.00844680046341009932f, 0.0281388222882209688f,
		0.0365893564198917567f, 0.031977518605460005f, 0.0167611524925375503f, -0.00339653119240760455f,
		-0.0217982870365519624f, -0.032876389196422294f, -0.0336954522736558765f, -0.0248110303640633016f,
		-0.00947297562820596448f, 0.00743938709260434736f, 0.0210370620252575136f, 0.0273471078870842592f,
		0.0242251070547044624f, 0.011526883911874692f, -0.00852681395914876156f, -0.0314183213878071535f,
		0.0606467253536945722f
};

YT_PROCESS static void ad7124_acq_func(void const * argument){
	ad7124_dev_t* ad7124_dev;
//	ytTimeMeas_t time_meas;
	int32_t sample;
	float64_t sample_double_volts;
	uint16_t current_channel;
	uint16_t expected_channel;
	uint16_t i;
	uint8_t current_status;
	channel_buff = (channel_buff_t*) ytMalloc(sizeof(channel_buff_t)*NUMBER_OF_CHANNELS);
	//Initialize buffer pointers
	for(i=0; i<NUMBER_OF_CHANNELS; i++){
		channel_buff[i].buff_ptr = &(channel_buff[i].sample_buffer[0]);
	}

	arm_fir_init_f32(&S1, NUM_TAPS, (float32_t *)&filter_coefficients[0], &firStateF32[0], BLOCK_SIZE);

	//Initialize semaphores
	ad7124_read_semaphore = ytSemaphoreCreate(1);
	ytSemaphoreWait(ad7124_read_semaphore, YT_WAIT_FOREVER);
	all_channels_sampled_semaphore = ytSemaphoreCreate(1);
	ytSemaphoreWait(all_channels_sampled_semaphore, YT_WAIT_FOREVER);
	output_data_semaphore = ytSemaphoreCreate(1);
	ytSemaphoreWait(output_data_semaphore, YT_WAIT_FOREVER);

	//Register Config command
	ytDelay(100);
	ytRegisterYetishellCommand("ACQ_CONFIG", acq_config_command);

	//Setup AD7124
	ad7124_setup(&ad7124_dev, ad7124_regs);		//DEFAULT CONFIGURATION
	ytDelay(100);

	//Initialize DRDY PIN
	ytGpioInitPin(AD7124_GPIO_DRY_PIN, GPIO_PIN_INTERRUPT_FALLING, GPIO_PIN_NO_PULL);		//CONFIG DRDY PIN
	ytGpioInitPin(GPIO_PIN_C1, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);		//TEST PIN

	expected_channel = 0;

	//Main acquisition Loop
	while(1){

//		ytStartTimeMeasure(&time_meas);	//Start Measure time

		//DISABLE DRDY PIN WHILE READING THE DATA
		ytGpioPinSetCallbackInterrupt(AD7124_GPIO_DRY_PIN, NULL, NULL);

		//Read the data from ADC
		ad7124_read_data_status_append(ad7124_dev, &sample, &current_status);

		//ENABLE DRDY PIN TO INTERRUPT WHEN NEW DATA IS AVAILABLE
	    ytGpioPinSetCallbackInterrupt(AD7124_GPIO_DRY_PIN, data_ready_pin_callback, NULL);

		//Calculate Volts
		sample_double_volts = ((((float64_t)sample)  / ((float64_t)NUM_2_POW_23)) - 1) *3.3;

		//Get current Channel
		current_channel = (uint16_t) AD7124_STATUS_REG_CH_ACTIVE(current_status);

		//Some data in any channel has been lost. Fill these data with zeros
		if(current_channel != expected_channel){		//Missed samples
			if (current_channel > expected_channel){
				for(i=expected_channel; i<current_channel; i++){
					*(channel_buff[i].buff_ptr) = 0;					//Fill with 0 the missed samples
					if(channel_buff[i].buff_ptr < (&(channel_buff[i].sample_buffer[CHANNEL_BUFFER_SIZE]))){
						channel_buff[i].buff_ptr++;
					}
					else{
						channel_buff[i].buff_ptr = (&(channel_buff[i].sample_buffer[0]));
					}
				}
			}
			else{
				for(i=expected_channel; i<NUMBER_OF_CHANNELS; i++){
					*(channel_buff[i].buff_ptr) = 0;					//Fill with 0 the missed samples
					if(channel_buff[i].buff_ptr < (&(channel_buff[i].sample_buffer[CHANNEL_BUFFER_SIZE]))){
						channel_buff[i].buff_ptr++;
					}
					else{
						channel_buff[i].buff_ptr = (&(channel_buff[i].sample_buffer[0]));
					}
				}
				for(i=0; i<current_channel; i++){
					*(channel_buff[i].buff_ptr) = 0;					//Fill with 0 the missed samples
					if(channel_buff[i].buff_ptr < (&(channel_buff[i].sample_buffer[CHANNEL_BUFFER_SIZE]))){
						channel_buff[i].buff_ptr++;
					}
					else{
						channel_buff[i].buff_ptr = (&(channel_buff[i].sample_buffer[0]));
					}
				}
			}
		}

		//Store the current chanel sample in the buffer
		*(channel_buff[current_channel].buff_ptr) = (float32_t) sample_double_volts;			//Store sample in Channel Buffer

		//Update the channel buffer pointer
		if(channel_buff[current_channel].buff_ptr < (&(channel_buff[current_channel].sample_buffer[CHANNEL_BUFFER_SIZE]))){
			channel_buff[current_channel].buff_ptr++;
		}
		else{
			channel_buff[current_channel].buff_ptr = (&(channel_buff[current_channel].sample_buffer[0]));
		}

		//Update the next expected channel
		if(current_channel >= (NUMBER_OF_CHANNELS-1)){
			expected_channel = 0;
			 ytSemaphoreRelease(all_channels_sampled_semaphore);//A new sample have been obtained in all channels. Free processing thread
		}
		else{
			expected_channel = current_channel +1;
		}

//		ytStopTimeMeasure(&time_meas);		//Stop measuring time

		//Wait till the DRDY interrupt is toggled
	    ytSemaphoreWait(ad7124_read_semaphore, YT_WAIT_FOREVER);
	    ytGpioPinToggle(GPIO_PIN_C1);
	}
}
/* *************************************************/


//Signals Processing process
YT_PROCESS void static processing_func(void const * argument){
	uint16_t current_fft_sample_index = 0;
	uint16_t i;

	//Initialize processing configuration to time domain
	processing_config.processing_mode = PROC_TIME_DOMAIN;
	processing_config.fft_sample_num = DEFAULT_FFT_SAMPLE_NUM;
	update_processing_config.processing_mode = processing_config.processing_mode;
	update_processing_config.fft_sample_num = processing_config.fft_sample_num;

	while(1){

		//Wait till the acquisition process signals that it has acquire a new sample in all channels
	    ytSemaphoreWait(all_channels_sampled_semaphore, YT_WAIT_FOREVER);

		switch(processing_config.processing_mode){

		case PROC_TIME_DOMAIN: //Nothing to do. Just output the signals. Entered each time a sample is acquired in every channel
			 ytSemaphoreRelease(output_data_semaphore); //Free the semaphore to output the acquired data
			break;

		case PROC_FFT:		//Do FFT when fft_samples_num are available and output the signal

			//Check if there are enough stored samples to perform the FFT
			if((channel_buff[NUMBER_OF_CHANNELS-1].buff_ptr >= &(channel_buff[NUMBER_OF_CHANNELS-1].sample_buffer[current_fft_sample_index + processing_config.fft_sample_num]))
					|| (channel_buff[NUMBER_OF_CHANNELS-1].buff_ptr < &(channel_buff[NUMBER_OF_CHANNELS-1].sample_buffer[current_fft_sample_index]))){

				for(i=0; i<NUMBER_OF_CHANNELS; i++){
					if(i == 0){
						process_fft(&(channel_buff[i].sample_buffer[current_fft_sample_index]), &channel_output_buf[i][0], processing_config.fft_sample_num);
					}
					else{
//						process_fft(&(channel_buff[i].sample_buffer[current_fft_sample_index]), &channel_output_buf[i][0], processing_config.fft_sample_num);
					}
				}

				current_fft_sample_index += processing_config.fft_sample_num;
				if(current_fft_sample_index >= CHANNEL_BUFFER_SIZE){
					current_fft_sample_index = 0;
				}
				ytSemaphoreRelease(output_data_semaphore); //Free the semaphore to output the fft processed data of all channels
			}

			break;
		default:
			break;
		}

	}
}
/* *************************************************/


//Data output process
YT_PROCESS static void data_out_func(void const * argument){
	uint32_t frame_id;
	float32_t* out_data_ptr;
	uint16_t i, j;

	//ATENTION!! This process has the lowest priority. Output data may overrun if other processes uses much CPU
	while(1){

		//Wait till the proccessing thread signals to output some data
		ytSemaphoreWait(output_data_semaphore, YT_WAIT_FOREVER);

		switch(processing_config.processing_mode){
			case PROC_TIME_DOMAIN: //Entered each time a sample is acquired in every channel

				frame_id = INIT_FRAME_ID;
				ytStdoutSend((uint8_t*) &frame_id, sizeof(float32_t), YT_WAIT_FOREVER);	//First send the frame ID

				for(i= 0; i<NUMBER_OF_CHANNELS; i++){	//For each channel output the last obtained value

					if(channel_buff[i].buff_ptr != (&(channel_buff[i].sample_buffer[0]))){
						out_data_ptr = channel_buff[i].buff_ptr - 1;
					}
					else{
						out_data_ptr = &(channel_buff[i].sample_buffer[CHANNEL_BUFFER_SIZE-1]);
					}

					ytStdoutSend((uint8_t*) out_data_ptr, sizeof(float32_t), YT_WAIT_FOREVER);
				}
				break;

			case PROC_FFT:		//Send FFT data output when it is processed
				frame_id = INIT_FRAME_ID;
				ytStdoutSend((uint8_t*) &frame_id, sizeof(float32_t), YT_WAIT_FOREVER);	//First send the init frame ID

				for(i= 0; i<NUMBER_OF_CHANNELS; i++){	//For each channel output all the processed FFT samples
					for(j=0; j<(processing_config.fft_sample_num/2); j++){
						ytStdoutSend((uint8_t*) &channel_output_buf[i][j], sizeof(float32_t), YT_WAIT_FOREVER);
					}

				}

				frame_id = END_FRAME_ID;
				ytStdoutSend((uint8_t*) &frame_id, sizeof(float32_t), YT_WAIT_FOREVER);	//Send the end frame ID
				break;
			default:
				break;
			}

		//Update config if necessary. This is the lowest priority thread, so it will be updated only when the processing thread is done
		if(update_processing_config.fft_sample_num != processing_config.fft_sample_num){
			processing_config.fft_sample_num = update_processing_config.fft_sample_num;
		}
		if(update_processing_config.processing_mode != processing_config.processing_mode){
			processing_config.processing_mode = update_processing_config.processing_mode;
		}

	}
}
/* *************************************************/


//AD7124 data ready pin callback. Release the semaphore to read the data
static void data_ready_pin_callback(void const * argument){
	ytSemaphoreRelease(ad7124_read_semaphore);
}

//Configuration command function for the acquisition module
static retval_t acq_config_command(uint16_t argc, char** argv){

	if(argc < 2){
		return RET_ERROR;
	}
	else{
		if(argc == 3){
			if(argv[1][0] == 'M'){		//Change mode command
				update_processing_config.processing_mode = (processing_mode_t) atoi(argv[2]);
			}
			if(argv[1][0] == 'F'){		//Change FFT samples command
				update_processing_config.fft_sample_num = (uint16_t) atoi(argv[2]);
			}
			else{
				return RET_ERROR;
			}
		}
		else{
			return RET_ERROR;
		}
	}
	return RET_OK;

}



//
///* -------------------------------------------------------------------
// * Declare State buffer of size (numTaps + blockSize - 1)
// * ------------------------------------------------------------------- */
//static float32_t firStateF32[BLOCK_SIZE + NUM_TAPS - 1];
//
///* ----------------------------------------------------------------------
//** FIR Coefficients buffer generated using fir1() MATLAB function.
//** ------------------------------------------------------------------- */
//static float32_t filter_coefficients[NUM_TAPS] =
//{
//		-3.27414835903473e-18,	-0.000224482338935084,	-0.000560146034538294,	-0.00110613172163582,
//		-0.00196260956910152,	-0.00322178433050895,	-0.00495892992267924,	-0.00722419322271586,
//		-0.0100358703230005,	-0.0133757629896308,	-0.0171870751701219,	-0.0213751202448101,
//		-0.0258108941069969,	-0.0303373443243691,	-0.0347779498345095,	-0.0389470366146975,
//		-0.0426611082987929,	-0.0457503792143622,	-0.0480696688008289,	-0.0495078537571182,
//		0.949908326603572,	-0.0495078537571182,	-0.0480696688008289,	-0.0457503792143622,
//		-0.0426611082987929,	-0.0389470366146975,	-0.0347779498345095,	-0.0303373443243691,
//		-0.0258108941069969,	-0.0213751202448101,	-0.0171870751701219,	-0.0133757629896308,
//		-0.0100358703230005,	-0.00722419322271586,	-0.00495892992267924,	-0.00322178433050895,
//		-0.00196260956910152,	-0.00110613172163582,	-0.000560146034538294,	-0.000224482338935084,
//		-3.27414835903473e-18
//};


float32_t post_filter[256];

//Process FFT function
static retval_t process_fft(float32_t* in_data, float32_t* out_data, uint16_t fft_sample_num){


	//FIR FILTER 50 HZ
	//ARM FIR FILTER (TIME 2 MS)
//	ytTimeMeas_t time_meas;
//	ytStartTimeMeasure(&time_meas);
//
//	arm_fir_instance_f32 S1;
//	arm_fir_init_f32(&S1, NUM_TAPS, (float32_t *)&filter_coefficients[0], &firStateF32[0], NUM_TAPS);
//
	arm_fir_f32(&S1, (float*) in_data, (float*) out_data, BLOCK_SIZE);
	uint16_t i;
	float32_t avg = 0;
	for(i=0; i<256; i++){
		avg += out_data[i];
	}

	avg /= BLOCK_SIZE;

	for(i=0; i<256; i++){
		out_data[i] -= avg;
	}

//	arm_fir_f32(&S1, (float*) in_data, (float*) post_filter, BLOCK_SIZE);

//	ytStopTimeMeasure(&time_meas);

//    arm_rfft_fast_instance_f32 S;
//
//    arm_rfft_fast_init_f32(&S, fft_sample_num);
//    arm_rfft_fast_f32(&S, post_filter, out_data, 0);


    //The real FFT output has fft_sample_num/2 complex samples, since the other half are their conjugate
//    arm_cmplx_mag_f32(out_data, out_data, fft_sample_num/2);
    return RET_OK;
}


#endif
