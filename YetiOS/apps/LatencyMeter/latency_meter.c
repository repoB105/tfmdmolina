/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * latency_meter.c
 *
 *  Created on: 26/11/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file latency_meter.c
 */

#define USE_LATENCY_METER	0

#if USE_LATENCY_METER

#include "platform-conf.h"
#include "system_api.h"

#define TIME	50		// ms

static uint16_t latency_meter_PID;
static uint16_t test_signals_PID;

uint32_t rx_semaphore;
uint32_t tx_timestamp;
uint32_t rx_timestamp;
uint8_t tx_flag = 0;

uint32_t min_lat, max_lat, avg_lat, sum_lat, num_lat;

/* TEST PROCESS ************************************/
YT_PROCESS static void latency_meter(void const * argument);
YT_PROCESS static void test_signals(void const * argument);

static void tx_interrupt_callback(void const* argument);
static void rx_interrupt_callback(void const* argument);

/* This line must be uncommented to launch this process */
ytInitProcess(latency_meter_process, latency_meter, NORMAL_PRIORITY_PROCESS, 256, &latency_meter_PID, NULL);
//ytInitProcess(test_signals_process, test_signals, NORMAL_PRIORITY_PROCESS, 128, &test_signals_PID, NULL);


retval_t latency_monitoring_enable(uint16_t argc, char** argv){

	min_lat = UINT32_MAX;
	max_lat = 0;
	avg_lat = 0;
	sum_lat = 0;
	num_lat = 0;
	tx_flag = 0;

	/*Configure interruption GPIO pins: PC1 and PC3*/
	ytGpioInitPin(GPIO_PIN_C1, GPIO_PIN_INTERRUPT_FALLING, GPIO_PIN_PULLUP);
	ytGpioPinSetCallbackInterrupt(GPIO_PIN_C1, tx_interrupt_callback, NULL);
	ytGpioInitPin(GPIO_PIN_C3, GPIO_PIN_INTERRUPT_FALLING, GPIO_PIN_PULLUP);
	ytGpioPinSetCallbackInterrupt(GPIO_PIN_C3, rx_interrupt_callback, NULL);

	return RET_OK;
}


retval_t latency_monitoring_disable(uint16_t argc, char** argv){

	ytGpioDeInitPin(GPIO_PIN_C1);
	ytGpioDeInitPin(GPIO_PIN_C3);

	return RET_OK;
}


retval_t latency_monitoring_print(uint16_t argc, char** argv){
	ytPrintf("Min. Latency:  %d ms\r\n", min_lat);
	ytPrintf("Max. Latency:  %d ms\r\n", max_lat);
	ytPrintf("Avg. Latency:  %.2f ms\r\n", ((float32_t)sum_lat/num_lat));
	ytPrintf("Total samples: %d \r\n", num_lat);

	return RET_OK;
}


YT_PROCESS static void latency_meter(void const * argument){

	uint32_t current_lat = 0;

	ytDelay(2000);

	ytRegisterYetishellCommand("lat_on", latency_monitoring_enable);
	ytRegisterYetishellCommand("lat_off", latency_monitoring_disable);
	ytRegisterYetishellCommand("lat_print", latency_monitoring_print);

	rx_semaphore = ytSemaphoreCreate(1);
	ytSemaphoreWait(rx_semaphore, YT_WAIT_FOREVER);

	while(1){
		ytSemaphoreWait(rx_semaphore, YT_WAIT_FOREVER);
		current_lat = rx_timestamp - tx_timestamp;
		sum_lat += current_lat;
		num_lat++;

		if(current_lat < min_lat) {
			min_lat = current_lat;
		}

		if(current_lat > max_lat) {
			max_lat = current_lat;
		}

		ytPrintf(">Latency: %d ms\r\n", current_lat);
	}
}

YT_PROCESS static void test_signals(void const * argument){

	uint32_t countdown = 0;

	// Initialize test pins
	ytGpioInitPin(GPIO_PIN_B10, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);
	ytGpioInitPin(GPIO_PIN_B11, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);

	ytGpioPinSet(GPIO_PIN_B10);
	ytGpioPinSet(GPIO_PIN_B11);

	countdown = TIME;
	while(1){
		ytGpioPinReset(GPIO_PIN_B10);
		ytGpioPinSet(GPIO_PIN_B11);
		ytDelay(countdown);
		ytGpioPinReset(GPIO_PIN_B11);
		ytGpioPinSet(GPIO_PIN_B10);
		if(countdown != 1){
			countdown--;
		}
		else{
			countdown = TIME;
		}
		ytDelay(50);
	}
}

static void tx_interrupt_callback(void const* argument){
	tx_timestamp = ytGetSysTickMilliSec();
//	tx_flag = 1;
}

static void rx_interrupt_callback(void const* argument){
//	if (tx_flag == 1){
		rx_timestamp = ytGetSysTickMilliSec();
		tx_flag = 0;
		ytSemaphoreRelease(rx_semaphore);
//	}
}

#endif	// USE_LATENCY_METER
