/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * monitoring_mcu.c
 *
 *  Created on: 10/10/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file monitoring_mcu.c
 */

#define USE_MONITORING_MCU	0

#if USE_MONITORING_MCU

#include "platform-conf.h"
#include "system_api.h"
#include "monitoring_migou.h"

static uint16_t status_led_PID;
static uint16_t monitoring_mcu_PID;

static itrx_sense_range_t itrx_s_range;
static float32_t rsense_trx;
static float32_t itrx_meas_correction;
static float32_t itrx_meas_offset;

static uint32_t cm_semaphore_id;

/* TEST PROCESS ************************************/
YT_PROCESS static void status_led(void const * argument);
YT_PROCESS static void monitoring_mcu(void const * argument);

/* This line must be uncommented to launch this process */
ytInitProcess(status_led_process, status_led, NORMAL_PRIORITY_PROCESS, 128, &status_led_PID, NULL);
ytInitProcess(monitoring_mcu_process, monitoring_mcu, NORMAL_PRIORITY_PROCESS, 256, &monitoring_mcu_PID, NULL);


retval_t current_monitoring_enable(uint16_t argc, char** argv){

	ytGpioPinSet(RS1_1);
	ytGpioPinReset(RS1_2);
	if(itrx_s_range == HIGH_RANGE){
		ytGpioPinSet(RS2_2);
		ytGpioPinReset(RS2_1);
	}
	else if(itrx_s_range == LOW_RANGE){
		ytGpioPinSet(RS2_1);
		ytGpioPinReset(RS2_2);
	}

	if(ytSemaphoreRelease(cm_semaphore_id) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}

retval_t current_monitoring_disable(uint16_t argc, char** argv){

	if(ytSemaphoreWait(cm_semaphore_id, YT_WAIT_FOREVER) != RET_OK){
		return RET_ERROR;
	}

	ytGpioPinReset(RS1_1);
	ytGpioPinReset(RS1_2);
	ytGpioPinReset(RS2_1);
	ytGpioPinReset(RS2_2);

	return RET_OK;
}


YT_PROCESS static void monitoring_mcu(void const * argument){

	uint32_t adc_fd;
	float32_t Isys;
	float32_t Itrx;

	cm_semaphore_id = ytSemaphoreCreate(1);
	ytSemaphoreWait(cm_semaphore_id, YT_WAIT_FOREVER);

	ytRegisterYetishellCommand("cm_on", current_monitoring_enable);
	ytRegisterYetishellCommand("cm_off", current_monitoring_disable);

	if(monitoring_mcu_init(&adc_fd) != RET_OK){
		monitoring_mcu_deinit(&adc_fd);
	}

	while(1){

		ytSemaphoreWait(cm_semaphore_id, YT_WAIT_FOREVER);

		// Current Monitoring
		if(get_current(&adc_fd, &Isys, &Itrx) != RET_OK){
			monitoring_mcu_deinit(&adc_fd);
			while(1);
		}

		if(Isys != 0){
			ytPrintf("I_SYS: %.1f mA\r\n", Isys);
		}
		else{
			ytPrintf("I_SYS: 0.00 mA\r\n");
		}


		if(Itrx != 0){
			if(itrx_s_range == HIGH_RANGE){
				ytPrintf("I_TRX: %.1f mA\tRange: %d\r\n", Itrx, itrx_s_range);
			}
			else{
				ytPrintf("I_TRX: %.1f uA\tRange: %d\r\n", Itrx*1000, itrx_s_range);
			}
		}
		else{
			if(itrx_s_range == HIGH_RANGE){
				ytPrintf("I_TRX: 0.00 mA\tRange: %d\r\n", itrx_s_range);
			}
			else{
				ytPrintf("I_TRX: 0.00 uA\tRange: %d\r\n", itrx_s_range);
			}
		}
		ytPrintf("\n\n");

		ytSemaphoreRelease(cm_semaphore_id);

		ytDelay(CM_DELAY);
	}

	monitoring_mcu_deinit(&adc_fd);
}


YT_PROCESS static void status_led(void const * argument){
	while(1){
		ytLedsOn(LEDS_BLUE);
		ytDelay(50);
		ytLedsOff(LEDS_BLUE);
		ytDelay(1950);
	}
}


static retval_t monitoring_mcu_init(uint32_t* adc_fd){

	adc_config_t adc_config;
	float32_t dummy_read[2];

	// Channel 1 in single-ended mode (Channel 2 is not used as dual mode is disabled)
	adc_config.diff1_mode_enabled = 0;		// Differential mode disabled
	adc_config.diff2_mode_enabled = 0;		// Differential mode disabled
	adc_config.dual_mode_enabled = 1;		// Dual mode enabled
	adc_config.channel_speed = 7500;		// Samples per second
	adc_config.adc_pin_se1 = VSUP_SENSE;
	adc_config.adc_pin_se2 = TRX_VDD_SENSE;

	// ADC Open
	*adc_fd = ytOpen(ADC_DEV, 0);
	if(*adc_fd == 0){
		return RET_ERROR;
	}

	// ADC Configuration
	if(ytIoctl(*adc_fd, (uint16_t)CONFIG_ADC, (void*) &adc_config) != RET_OK){
		return RET_ERROR;
	}

	// Initialize Rsense control pins
	ytGpioInitPin(RS1_1, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);
	ytGpioInitPin(RS1_2, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);
	ytGpioInitPin(RS2_1, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);
	ytGpioInitPin(RS2_2, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);

	ytGpioPinReset(RS1_1);
	ytGpioPinReset(RS1_2);
	ytGpioPinReset(RS2_1);
	ytGpioPinReset(RS2_2);

	itrx_s_range = ITRX_S_RANGE_DEFAULT;

	if(itrx_s_range == HIGH_RANGE){
//		ytGpioPinSet(RS2_2);
//		ytGpioPinReset(RS2_1);
		rsense_trx = RSENSE_TRX_HIGH;
		itrx_meas_correction = ITRX_H_MEAS_CORRECTION;
		itrx_meas_offset = ITRX_H_MEAS_OFFSET;
	}
	else if(itrx_s_range == LOW_RANGE){
//		ytGpioPinSet(RS2_1);
//		ytGpioPinReset(RS2_2);
		rsense_trx = RSENSE_TRX_LOW;
		itrx_meas_correction = ITRX_L_MEAS_CORRECTION;
		itrx_meas_offset = ITRX_L_MEAS_OFFSET;
	}
	ytDelay(1000);

	if(ytRead(*adc_fd, dummy_read, 2) != 2){
		monitoring_mcu_deinit(adc_fd);
	}

	return RET_OK;
}


static void monitoring_mcu_deinit(uint32_t* adc_fd){

	ytClose(*adc_fd);

	// De-Initialize Rsense control pins
	ytGpioDeInitPin(RS1_1);
	ytGpioDeInitPin(RS1_2);
	ytGpioDeInitPin(RS2_1);
	ytGpioDeInitPin(RS2_2);
}


static void swap_trx_rsense(void){

	if(itrx_s_range == HIGH_RANGE){
		ytGpioPinSet(RS2_1);
		ytGpioPinReset(RS2_2);
		itrx_s_range = LOW_RANGE;
		rsense_trx = RSENSE_TRX_LOW;
		itrx_meas_offset = ITRX_L_MEAS_OFFSET;
		itrx_meas_correction = ITRX_L_MEAS_CORRECTION;
	}
	else if(itrx_s_range == LOW_RANGE){
		ytGpioPinSet(RS2_2);
		ytGpioPinReset(RS2_1);
		itrx_s_range = HIGH_RANGE;
		rsense_trx = RSENSE_TRX_HIGH;
		itrx_meas_offset = ITRX_H_MEAS_OFFSET;
		itrx_meas_correction = ITRX_H_MEAS_CORRECTION;
	}
}


static retval_t get_current(uint32_t* adc_fd, float32_t* Isys, float32_t* Itrx){

	uint16_t i = 0;
	uint32_t* read_buff;
	float32_t mean_Isys = 0;
	float32_t mean_Itrx = 0;


	read_buff = (uint32_t*)ytMalloc(NSAMPLES*sizeof(uint32_t));

	if(ytRead(*adc_fd, read_buff, NSAMPLES) != NSAMPLES){
		ytFree(read_buff);
		return RET_ERROR;
	}

	for(i=0; i < NSAMPLES; i++){
		mean_Isys += (read_buff[i] & ISYS_MASK);
		mean_Itrx += ((read_buff[i] & ITRX_MASK) >> 16);
	}

	ytFree(read_buff);

	// System current
	if(mean_Isys == 0){
		*Isys = 0;
	}
	else{
		mean_Isys = mean_Isys/NSAMPLES;
		*Isys = ((((float32_t)mean_Isys) * VSTEP)/SENSE_AMP_GAIN)/RSENSE_SYS;
		*Isys = (*Isys * ISYS_MEAS_CORRECTION) + ISYS_MEAS_OFFSET;
	}

	// Transceiver current
	if(mean_Itrx == 0){
		*Itrx = 0;
	}
	else{
		mean_Itrx = mean_Itrx/NSAMPLES;
		*Itrx = ((((float32_t)mean_Itrx) * VSTEP)/SENSE_AMP_GAIN)/rsense_trx;
		*Itrx = (*Itrx * itrx_meas_correction) + itrx_meas_offset;
	}

	// Measuring range selector
	if((itrx_s_range == LOW_RANGE  && *Itrx >= HIGH_S_TH) ||
	   (itrx_s_range == HIGH_RANGE && *Itrx <= LOW_S_TH)  ){
		swap_trx_rsense();
	}

	return RET_OK;
}


#endif	// USE_MONITORING_MCU
