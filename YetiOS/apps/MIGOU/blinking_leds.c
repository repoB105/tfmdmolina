/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * blinking_leds.c
 *
 *  Created on: 30/05/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file blinking_leds.c
 */

#define USE_BLINKING_LEDS	0

#if USE_BLINKING_LEDS

#include "platform-conf.h"
#include "system_api.h"
#include "at86rf215_arch.h"
#include "mss_spi.h"
#include "at86rf215_const.h"
#include "at86rf215_utils.h"
#include "at86rf215_config.h"
#include "at86rf215_core.h"
#include "unique_id.h"
#include "uart_driver.h"

#define MIGOU_ID_01		(uint32_t)0x0010000E
#define MIGOU_ID_04		(uint32_t)0x001A000E


static uint16_t transmitterPID;
static uint16_t blinkingLedsPID;
static uint16_t irqRoutinePID;

at86rf215_data_t* at86rf215_data;
uint32_t transmitter_semaphore;
uint32_t irq_routine_semaphore;

/* TEST PROCESS ************************************/
YT_PROCESS static void transmitter(void const * argument);
YT_PROCESS static void blinking_leds(void const * argument);
YT_PROCESS static void irq_routine(void const * argument);

static void gpio_interrupt_callback(void const* argument);
static void at86rf215_irq_callback(void const* argument);

/* This line must be uncommented to launch this process */
//ytInitProcess(transmitter_process, transmitter, NORMAL_PRIORITY_PROCESS, YT_MINIMAL_STACK_SIZE*2, &transmitterPID, NULL);
ytInitProcess(status_process, blinking_leds, NORMAL_PRIORITY_PROCESS, YT_MINIMAL_STACK_SIZE*2, &blinkingLedsPID, NULL);
//ytInitProcess(irq_process, irq_routine, NORMAL_PRIORITY_PROCESS, YT_MINIMAL_STACK_SIZE*2, &irqRoutinePID, NULL);

YT_PROCESS static void transmitter(void const * argument){
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------

	at86rf215_config_t at86rf215_init_config;
	char data[20];
	uint8_t i = 0;
	uint32_t my_id;


	// Get my ID
	my_id = get_uinque_32_bits_id();

	// GENERAL CONFIGURATION
	// cs_pin 	= 0xFF 		-> HW Chip Select is used
	// irq_pin 	= TRX_IRQ	-> The corresponding pin routed on the board
	// band		= RF24		-> 2.4 GHz Transceiver
	at86rf215_data = new_at86rf215_data(0xFF, TRX_IRQ, RF24, at86rf215_irq_callback, NULL);
	if(at86rf215_data == NULL){
		while(1);
	}

	// CHANNEL CONFIGURATION
	// IEEE 802.15.4-2012 PHY: MR-FSK operating mode #3 in 2400-2483.5 MHz band
	//      				   CS 0.4 MHz; TotalNumCh 207; CCF0 2400.4 MHz
	at86rf215_init_config.ch_mode = MODE_IEEE;			// Channel Mode: IEEE 802.15.4g-2012 compliant channel scheme
	at86rf215_init_config.ch0_center_freq = 2400400;	// Channel 0 Center Frequency in kHz
	at86rf215_init_config.ch_number = 0;				// Channel Number
	at86rf215_init_config.ch_spacing = 400000;			// Channel Spacing in Hz

	// BASEBAND CONFIGURATION
	at86rf215_init_config.modulation = BFSK;			// 2-FSK
	at86rf215_init_config.baud_rate	= 200e3;			// 200 kbps
	at86rf215_init_config.output_pwr = ZERO_DBM;		// Output Power: 0 dBm

	if(at86rf215_hw_init(at86rf215_data, &at86rf215_init_config, SPI_DEV_1) != RET_OK){
		while(1);
	}

	at86rf215_arch_transceiver_sleep(at86rf215_data, RF09);


	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------

	// MIGOU_ID_01 -------------------------------------------------------------
	if(my_id == MIGOU_ID_01){

		at86rf215_set_mode_sleep(at86rf215_data);
		at86rf215_set_mode_idle(at86rf215_data);

		transmitter_semaphore = ytSemaphoreCreate(1);
		ytSemaphoreWait(transmitter_semaphore, YT_WAIT_FOREVER);

		ytGpioInitPin(GPIO_3V3_10, GPIO_PIN_INTERRUPT_FALLING, GPIO_PIN_NO_PULL);
		ytGpioPinSetCallbackInterrupt(GPIO_3V3_10, gpio_interrupt_callback, NULL);

		strcpy(data, "ABCDEFGHIJKLMN");

		while(1){
			ytSemaphoreWait(transmitter_semaphore, YT_WAIT_FOREVER);
			for(i=0; i<10; i++){
				if(at86rf215_send_data(at86rf215_data, (uint8_t*)data, 15) != RET_OK){
					while(1);
				}
				ytDelay(50);
			}
		}

	} // MIGOU_ID_01 -----------------------------------------------------------


	// MIGOU_ID_04 -------------------------------------------------------------
	else if(my_id == MIGOU_ID_04){

		if(at86rf215_set_mode_rx(at86rf215_data) != RET_OK){
			while(1);
		}

		while(1){
			ytDelay(500);
		}

	} // MIGOU_ID_04 -----------------------------------------------------------


	else{
		while(1);
	}
}


YT_PROCESS static void blinking_leds(void const * argument){

	uint8_t i = 0;
	uint32_t fuart = 0;
	char data[20];

	strcpy(data, "Hola mundo!\r\n");

	fuart = ytOpen("DEV_UART1", 0);

	if(ytIoctl(fuart, UART_ENABLE_ECHO, NULL) != RET_OK){
		while(1);
	}


	for(i=0; i<3; i++){
		ytWrite(fuart, data, 13);
	}

	ytIoctl(fuart, SET_UART_DATA_BITS_LENGTH, (void *)UART_DATA_6_BITS);

	for(i=0; i<3; i++){
		ytWrite(fuart, (void*)data, 13);
	}

	ytIoctl(fuart, SET_UART_DATA_BITS_LENGTH, (void *)UART_DATA_8_BITS);

	for(i=0; i<3; i++){
		ytWrite(fuart, (void*)data, 13);
	}

	ytIoctl(fuart, SET_UART_NUMBER_STOP_BITS, (void *)UART_ONEHALF_STOP_BIT);

	for(i=0; i<3; i++){
		ytWrite(fuart, (void*)data, 13);
	}

	ytIoctl(fuart, SET_UART_NUMBER_STOP_BITS, (void *)UART_ONE_STOP_BIT);

	for(i=0; i<3; i++){
		ytWrite(fuart, (void*)data, 13);
	}

	ytIoctl(fuart, SET_UART_PARITY, (void *)UART_EVEN_PARITY);

	for(i=0; i<3; i++){
		ytWrite(fuart, (void*)data, 13);
	}

	ytIoctl(fuart, SET_UART_PARITY, (void *)UART_NO_PARITY);

	for(i=0; i<3; i++){
		ytWrite(fuart, (void*)data, 13);
	}


	while(1){
		ytLedsOn(LEDS_BLUE);
		ytDelay(50);
		ytLedsOff(LEDS_BLUE);
		ytDelay(1950);
	}
}

YT_PROCESS static void irq_routine(void const * argument){

	irq_routine_semaphore = ytSemaphoreCreate(1);
	ytSemaphoreWait(irq_routine_semaphore, YT_WAIT_FOREVER);

	while(1){
		ytSemaphoreWait(irq_routine_semaphore, YT_WAIT_FOREVER);
		at86rf215_irq_routine(at86rf215_data);
	}
}

static void at86rf215_irq_callback(void const* argument){
	ytSemaphoreRelease(irq_routine_semaphore);
}

static void gpio_interrupt_callback(void const* argument){
	ytLedsToggle(LEDS_RED1);
	ytSemaphoreRelease(transmitter_semaphore);
}
#endif
