/*
 * Copyright (c) 2019, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * migou_low_power.c
 *
 *  Created on: 31/01/2019
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file migou_low_power.c
 */

#define USE_MIGOU_LOW_POWER	0

#if USE_MIGOU_LOW_POWER

#include "platform-conf.h"
#include "system_api.h"
#include "core_cm3.h"
#include "mss_sys_services.h"
#include "mss_rtc.h"
#include "flash_freeze_arch.h"
#include "spi_arch.h"
#include "spi_driver.h"
#include "at86rf215_arch.h"
#include "m2sxxx.h"

#define SYSTICK_ENABLE_BIT			( 1UL << 0UL )

static uint16_t spi_PID;
static uint16_t sf2_ds_PID;
static uint16_t migou_low_power_PID;
static uint16_t migou_rtc_single_shot_test_PID;
static uint16_t migou_flash_freeze_test_PID;

#define SYSREG_RTC_WAKEUP_C_EN_MASK    0x00000004u
uint32_t cnt_enter, cnt_exit = 0;
uint8_t flag = 0;

#define SIZE 1945
uint8_t read_val[SIZE];
uint8_t write_val[SIZE];
uint8_t tx_val[2 + SIZE];

retval_t enter_callback(void);
retval_t exit_callback(void);

/* TEST PROCESS ************************************/
YT_PROCESS static void spi(void const * argument);
YT_PROCESS static void sf2_ds_main(void const * argument);
YT_PROCESS static void migou_low_power(void const * argument);
YT_PROCESS static void migou_rtc_single_shot_test(void const * argument);
YT_PROCESS static void migou_flash_freeze_test(void const * argument);


/* This line must be uncommented to launch this process */
//ytInitProcess(spi_process, spi, NORMAL_PRIORITY_PROCESS, YT_MINIMAL_STACK_SIZE*3, &spi_PID, NULL);
//ytInitProcess(sf2_ds_process, sf2_ds_main, NORMAL_PRIORITY_PROCESS, YT_MINIMAL_STACK_SIZE*3, &sf2_ds_PID, NULL);
ytInitProcess(migoulp_process, migou_low_power, NORMAL_PRIORITY_PROCESS, YT_MINIMAL_STACK_SIZE*3, &migou_low_power_PID, NULL);
//ytInitProcess(migou_flash_freeze_process, migou_flash_freeze_test, NORMAL_PRIORITY_PROCESS, YT_MINIMAL_STACK_SIZE*2, &migou_flash_freeze_test_PID, NULL);
//ytInitProcess(migourtc_single_shot_process, migou_rtc_single_shot_test, NORMAL_PRIORITY_PROCESS, YT_MINIMAL_STACK_SIZE*2, &migou_rtc_single_shot_test_PID, NULL);

YT_PROCESS static void spi(void const * argument){

	uint16_t i, j = 0;
	uint8_t dummy = 0;
	spi_data_t spi_data;
	spi_read_write_buffs_t rd_wr_buffs;
	uint8_t cmd_size = 2;
	uint16_t WRITE_ACCESS_COMMAND = 0x8000;
	uint16_t READ_ACCESS_COMMAND = 0x0000;
	uint16_t ADDR = 0x3800 + 100;


//	ytGpioInitPin(GPIO_1V8_0, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);
//	ytGpioPinReset(GPIO_1V8_0);

	ytDelay(20);

	spi_data.spi_hw_num = 1;
	spi_data.config.speed = 25000000;
	spi_data.config.cpha = 0;
	spi_data.config.cpol = 0;
	spi_data.config.hw_cs = MSS_SPI_SLAVE_0;
	spi_data.config.mode = MASTER;

	if(SystemCoreClock == 50000000u){		// M3_CLK is 50 MHz
		spi_data.config.pdma_write_adj = PDMA_SPI_WRITE_ADJ_50MHZ;
	}
	else if(SystemCoreClock == 100000000u){	// M3_CLK is 100 MHz
		spi_data.config.pdma_write_adj = PDMA_SPI_WRITE_ADJ_100MHZ;
	}
	else{
		ytLedsOn(LEDS_BLUE);
		while(1);
	}

//	MSS_SPI_init( &g_mss_spi0 );

//	if(spi_arch_set_config(&spi_data) != RET_OK){
//		ytLedsOn(LEDS_BLUE);
//		while(1);
//	}

	while(1){
for(j=0; j<5; j++){
		for(i=0; i<SIZE; i++){
			write_val[i] = i;
		}

//		ytGpioPinSet(GPIO_1V8_0);

		// Escritura
		tx_val[0] = (WRITE_ACCESS_COMMAND | ADDR) >> 8;
		tx_val[1] = (uint8_t)ADDR;

		memcpy(&tx_val[cmd_size], write_val, SIZE);

		if(spi_arch_write(&spi_data, tx_val, (uint16_t) (cmd_size + SIZE)) != RET_OK){
			ytLedsOn(LEDS_BLUE);
			while(1);
		}

//		ytGpioPinReset(GPIO_1V8_0);
//		dummy = 1;
		ytDelay(500);
//		ytGpioPinSet(GPIO_1V8_0);

		// Lectura
		rd_wr_buffs.ptx = (uint8_t*) ytMalloc(cmd_size + SIZE);
		rd_wr_buffs.prx = (uint8_t*) ytMalloc(cmd_size + SIZE);

		rd_wr_buffs.ptx[0] = (READ_ACCESS_COMMAND | ADDR) >> 8;
		rd_wr_buffs.ptx[1] = (uint8_t)ADDR;

		rd_wr_buffs.size = cmd_size + SIZE;

		if(spi_arch_read_write(&spi_data, rd_wr_buffs.ptx, rd_wr_buffs.prx, rd_wr_buffs.size) != RET_OK){
			ytLedsOn(LEDS_BLUE);
			while(1);
		}

		memcpy(read_val, &rd_wr_buffs.prx[cmd_size], SIZE);

		ytFree(rd_wr_buffs.ptx);
		ytFree(rd_wr_buffs.prx);

//		ytGpioPinReset(GPIO_1V8_0);

		// Comprobación
		uint16_t cnt = 0;
		for(i=0; i<SIZE; i++){
			if(read_val[i] != write_val[i]){
				ytPrintf("Error: %d\r\n", i);
				cnt++;
			}
		}
		if(cnt == 0){
			ytPrintf("Bucle %d sin errores.\r\n", j+1);
		}
}
		while(1){
			ytLedsToggle(LEDS_BLUE);
			ytDelay(300);
		}
	}
}

YT_PROCESS static void sf2_ds_main(void const * argument){

	int8_t i;

	// Set the priority of all interrupts to 6.
	for(i = -14; i <= 81; i++){
		NVIC_SetPriority((IRQn_Type)i, (uint32_t) 6);
	}

	// Set the priority of SysTick_IRQn to 5.
	NVIC_SetPriority(SysTick_IRQn, (uint32_t) 5);

	// Set the SLEEPDEEP bit of the SCR to 1.
	// The processor uses deep sleep as its low power mode.
	SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

	while(1){
		ytLedsToggle(LEDS_BLUE);			// LEDS_BLUE pin is a MSS_GPIO
//		ytPrintf("Deep Sleep Test\r\n");	// Printed through MMUART_0
		ytDelay(5);

		// Mask all interrupts with priority equal to or less than 6.
		// The processor does not process any exception with a priority value
		// greater than or equal to BASEPRI.
		__set_BASEPRI((6u) << (8U - __NVIC_PRIO_BITS));

		// Wait For Interrupt:
		__WFI();

		 // Remove the BASEPRI masking
		__set_BASEPRI(0U);
	}
}


YT_PROCESS static void migou_low_power(void const * argument){

	int8_t i;
	uint8_t cnt = 0;
	uint32_t primask, faultmask, basepri;
	uint8_t flash_freeze = 15;
	uint32_t SysTickPriority = 0;
	uint32_t priority = 8;
	uint32_t reg = 0;

	ytDelay(1000);
	ytPrintf("Pre FF\r\n");
	ytDelay(10000);

	if(enter_flash_freeze() != RET_OK){
		while(1);
	}

	ytPrintf("Post FF\r\n");
	ytDelay(10000);

	ytPrintf("Pre DS\r\n");
	SysTick->CTRL &= ~SYSTICK_ENABLE_BIT;
	SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
	reg=SCB->SCR;
	__WFI();

	ytPrintf("Post DS\r\n");

	reg = SysTick->CTRL;
	reg = SysTick->LOAD;
	reg = SysTick->VAL;
	reg = SysTick->CALIB;

	reg = SYSREG->M3_CR;



	ytDelay(10);

	for(i = -14; i <= 81; i++){
		priority = NVIC_GetPriority((IRQn_Type)i);
		if(priority != 0){
			cnt++;
		}
		ytPrintf("IRQn_Type: %d\t Priority: %d\r\n", i, priority);
	}

	for(i = -14; i <= 81; i++){
		NVIC_SetPriority((IRQn_Type)i, (uint32_t) 6);
	}

	NVIC_SetPriority(SysTick_IRQn, (uint32_t) 5);

	cnt = 0;
	for(i = -14; i <= 81; i++){
		priority = NVIC_GetPriority((IRQn_Type)i);
		ytPrintf("IRQn_Type: %d\t Priority: %d\r\n", i, priority);
		if(priority != 6){
			cnt++;
		}
	}

//	__set_PRIMASK(1);
//	__set_FAULTMASK(1);

	while(1){
		ytLedsToggle(LEDS_BLUE);
		ytDelay(500);
		SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
//		SCB->SCR |= 0;
		i = SCB->SCR;

		__set_BASEPRI((6u) << (8U - __NVIC_PRIO_BITS));
		__WFI();
		ytPrintf("Mem: %.3f KB\r\n", (float32_t) ((float32_t)xPortGetFreeHeapSize()/1000));
	}
}

YT_PROCESS static void migou_flash_freeze_test(void const * argument){

	uint64_t reg = 0;
	rtc_time_date_t date;

	MSS_RTC_init(MSS_RTC_BINARY_MODE, 32);
	MSS_RTC_set_binary_count(INIT_TIMESTAMP);
	NVIC_ClearPendingIRQ(RTC_Wakeup_IRQn);
	MSS_RTC_enable_irq();
	MSS_RTC_start();

	ytDelay(1000);

	set_enter_flash_freeze_cb(enter_callback);
	set_exit_flash_freeze_cb(exit_callback);

	while(1){

		reg = SYSREG->RTC_WAKEUP_CR;

		ytPrintf("FF Enter: %d\r\n", cnt_enter+1);
		if(enter_flash_freeze() != RET_OK){
			while(1);
		}

		while(flash_freeze_status() == FF_ACTIVE){
			ytPrintf("FF Mode\r\n");
			MSS_RTC_set_binary_count_alarm((uint64_t)(MSS_RTC_get_binary_count() + 2000), MSS_RTC_SINGLE_SHOT_ALARM);
			flag = 1;
			while(flag != 0){
				ytPrintf("Waiting RTC alarm\r\n");
				ytDelay(500);
			}
			ytPrintf("FF Mode\r\n");
			ytDelay(3000);
			exit_flash_freeze();
//			if(exit_flash_freeze() != RET_OK){
//				while(1);
//			}
		}

		ytPrintf("FF Exit: %d\r\n", cnt_exit);
		ytDelay(1000);
	}
}



YT_PROCESS static void migou_rtc_single_shot_test(void const * argument){

	// The alarm asserts a wakeup interrupt to the Cortex-M3. This function
	// enables the RTC’s wakeup interrupt output, however the RTC wakeup
	// interrupt input to the Cortex-M3 NVIC must be enabled separately by
	// calling the MSS_RTC_enable_irq() function.
	MSS_RTC_init(MSS_RTC_BINARY_MODE, 32);
	MSS_RTC_set_binary_count(INIT_TIMESTAMP);
	NVIC_ClearPendingIRQ(RTC_Wakeup_IRQn);
	MSS_RTC_enable_irq();
	MSS_RTC_start();


	while(1){
		MSS_RTC_set_binary_count_alarm((uint64_t)(MSS_RTC_get_binary_count() + 2000), MSS_RTC_SINGLE_SHOT_ALARM);
		flag = 1;
		do{
			ytPrintf("Wait\r\n");
			ytDelay(200);
		}while(flag != 0);
		ytPrintf("RTC Alarm\r\n");
	}
}

//void RTC_Wakeup_IRQHandler(void){
//	MSS_RTC_clear_irq();
//	flag = 0;
//}

retval_t enter_callback(void){
	cnt_enter++;
	return RET_OK;
}

retval_t exit_callback(void){
	cnt_exit++;
	return RET_OK;
}

#endif // USE_MIGOU_LOW_POWER
