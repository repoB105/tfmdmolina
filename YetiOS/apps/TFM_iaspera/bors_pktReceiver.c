/*
 * bors_pktReceiver.c
 *
 *  Created on: 20 feb. 2019
 *      Author: iaspera
 */

#include "platform-conf.h"

#if BORS_V2
#define USE_PKTRCVR	1

#if USE_PKTRCVR

#include "platform-conf.h"
#include "system_api.h"
#include "system_net_api.h"
#include "../apps/TFM_iaspera/include/outgoingESPpkts.h"


#define NET_PORT			444

uint16_t pktRcvrId;
struct dataPacket_t* pktToShare;
extern uint32_t messageQueueInstance;


/* packetProcessing PROCESS ************************************/
YT_PROCESS static void packet_receiver(void const * argument);


/* This line must be uncommented to launch this process */
ytInitProcess(packet_processing_process, packet_receiver, NORMAL_PRIORITY_PROCESS, 512, &pktRcvrId, NULL);

YT_PROCESS static void packet_receiver(void const * argument){
	uint8_t addr_str_buf[8];
	ytNetAddr_t net_addr;

	pktToShare = ytMalloc(sizeof(struct dataPacket_t));



	/* Set node Addr */
	net_addr = ytNewNetAddr("1", 'D');		//Node adress
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);
	ytSetNodeAsGw();		//Define This node as the GW


	ytUrOpen(NET_PORT);	//Abro el puerto
	net_addr = ytNewNetAddr("22222", 'D');	//Creo una direcci�n con cualquier valor donde se guardara la direccion recibida
	while(1){
		ytUrRcv(NET_PORT, net_addr,(uint8_t*) pktToShare, sizeof(struct dataPacket_t));
		ytNetAddrToString(net_addr, addr_str_buf, 'D');
		float32_t lvl =  ytUrGetSignalLevel(NET_PORT);
		ytPrintf("RCV: %s from %s  LVL: %.2fdB\r\n",(uint8_t*) pktToShare, addr_str_buf, lvl);

		if(messageQueueInstance != 0)
			ytMessageqPut(messageQueueInstance, pktToShare, 0);

	}

	ytUrClose(NET_PORT);
	ytDeleteNetAddr(net_addr);

}

#endif
#endif
