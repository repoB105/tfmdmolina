/*
 * kay_movDect.c
 *
 *  Created on: 5 oct. 2018
 *      Author: iaspera
 */

#include "platform-conf.h"

#if KAY_V2

#define USE_KAY_MOVDECT	1
#if USE_KAY_MOVDECT


#include "system_api.h"
#define DEFAULT_TIMEOUT	100

uint16_t kay_movDectId;
extern uint8_t isMovDetected;
int32_t isMovSemaphore;

/* TBASIC EST PROCESS ************************************/
YT_PROCESS void kay_movDectfunc(void const * argument);

ytInitProcess(kay_movDectproc, kay_movDectfunc, NORMAL_PRIORITY_PROCESS, 256, &kay_movDectId, NULL);


YT_PROCESS void kay_movDectfunc(void const * argument){

	ytGpioInitPin(GPIO_PIN_A8, GPIO_PIN_INPUT, GPIO_PIN_PULLDOWN);
	isMovSemaphore = ytSemaphoreCreate(1);

	while(1){
		if(ytGpioPinGet(GPIO_PIN_A8)){
			if(ytSemaphoreWait(isMovSemaphore, 100) == RET_OK){
				isMovDetected = 1;
				ytSemaphoreRelease(isMovSemaphore);
			}
		}
		ytDelay(1);
	}
}
/* *************************************************/
#endif
#endif
