/*
 * bors_remCtrl.c
 *
 *  Created on: 12 oct. 2018
 *      Author: Ilde
 */

#include "platform-conf.h"

#if BORS_V2

#define USE_BORS_REMCTRL	0

#if USE_BORS_REMCTRL

#include "system_api.h"
#include "unique_id.h"
#include "cs5490_platform.h"
#include "../apps/TFM_iaspera/include/outgoingESPpkts.h"


uint16_t bors_remCtrlProcId;

struct cs5490_data_t cs5490Data;
struct dataPacket_t* pktToShare;
extern uint32_t messageQueueInstance;


/* Used PROCESS ************************************/
YT_PROCESS void bors_remCtrl_func(void const * argument);
//YT_PROCESS static void packet_processing(void const * argument);


ytInitProcess(bors_remCtrl_proc, bors_remCtrl_func, NORMAL_PRIORITY_PROCESS, 512, &bors_remCtrlProcId, NULL);


YT_PROCESS void bors_remCtrl_func(void const * argument){
	uint8_t write_data[4];
	uint32_t cs5490_fd;
	struct dataPacket_t newPacket;
	char tmpNumber[7];		// Big enough for max value of int16_t, plus sign, plus null char
	char espBuffer[135];	// Maximum required space for one outgoing packet, including terminating character is 169, using more just in case
	uint32_t esp8266_fd;

	ytPrintf(">bors_remCtrl_func init\r\n");


	ytGpioInitPin(GPIO_PIN_A0, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);
	ytGpioInitPin(GPIO_PIN_A1, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);
	ytGpioPinReset(GPIO_PIN_A0);
	ytGpioPinReset(GPIO_PIN_A1);
	ytDelay(1);
	ytGpioPinSet(GPIO_PIN_A0);
	ytGpioPinSet(GPIO_PIN_A1);
	ytDelay(10);


	messageQueueInstance = ytMessageqCreate(4);
	pktToShare = ytMalloc(sizeof(struct dataPacket_t));

	/*cs5490_fd = ytOpen(UART_DEV_2, 0);
	write_data[0] = 0xFF;
	write_data[3] = 0x50;
	write_data[2] = 0x02;
	write_data[1] = 0x00;
	ytWrite(cs5490_fd, write_data, 4);
	ytClose(cs5490_fd);*/


	while(1){

		cs5490_fd = ytOpen(CS5490_DEV, 0);
		ytRead(cs5490_fd, &cs5490Data, sizeof(cs5490Data));

		ytPrintf(">CS5490 Read:\r\n");
		ytPrintf(">CS5490 Vrms: %4.2fV\r\n", cs5490Data.voltageRMS);
		ytPrintf(">CS5490 Vpk: %4.2fV\r\n", cs5490Data.voltagePeak);
		ytPrintf(">CS5490 Irms: %4.3fA\r\n", cs5490Data.currentRMS);
		ytPrintf(">CS5490 Ipk: %4.3fA\r\n", cs5490Data.currentPeak);
		ytPrintf(">CS5490 Pavg: %4.3fW\r\n", cs5490Data.powerAvg);
		ytPrintf(">CS5490 PF: %2.5f\r\n", cs5490Data.powerFactor);
		ytPrintf(">CS5490 Qavg: %4.3fVA\r\n", cs5490Data.reactiveAvg);

		ytClose(cs5490_fd);


		pktToShare->remSwitchData.activePwr = (int16_t) (cs5490Data.powerAvg / PKT_SCALE_ACTIVE_POWER);
		pktToShare->remSwitchData.lineFreq = 25000;	// TODO: cambiarlo por valor de verdad
		pktToShare->remSwitchData.powerFactor = (int16_t)(cs5490Data.powerFactor / PKT_SCALE_POWER_FACTOR);
		pktToShare->remSwitchData.reactivePwr = (int16_t)(cs5490Data.reactiveAvg / PKT_SCALE_REACTIVE_POWER);
		pktToShare->remSwitchData.relayState = ytGpioPinGet(GPIO_PIN_A0)? 1 : 0;
		pktToShare->remSwitchData.rmsCurrent = (int16_t)(cs5490Data.currentRMS / PKT_SCALE_RMS_CURRENT);
		pktToShare->remSwitchData.rmsVoltage = (int16_t)(cs5490Data.voltageRMS / PKT_SCALE_RMS_VOLTAGE);
		pktToShare->remSwitchData.reactivePwr = (int16_t)(cs5490Data.aparentPower / PKT_SCALE_REACTIVE_POWER);
		pktToShare->type = remSwitch;
		pktToShare->origID = get_uinque_16_bits_id();

		if(messageQueueInstance != 0)
			ytMessageqPut(messageQueueInstance, pktToShare, 0);


		ytDelay(1000);	// TODO: sustituir tarea con delay por interrupcion temporizada

	}
}
/* *************************************************/


#endif
#endif
