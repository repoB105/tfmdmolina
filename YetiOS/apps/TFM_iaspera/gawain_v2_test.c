
#define USE_GAWAIN_V2_TEST	0

#if USE_GAWAIN_V2_TEST
#include "system_api.h"
#include "touchsensing.h"

#define DEFAULT_TIMEOUT	100

uint16_t gawain_v2_testId;
int32_t isMovSemaphore;
extern const TSL_LinRotB_T MyLinRotsB[TSLPRM_TOTAL_LINROTS];
tsl_user_status_t tsl_status;
uint8_t pwmLED1 = 0;
uint8_t i = 0;

/* TBASIC EST PROCESS ************************************/
YT_PROCESS void gawain_v2_testfunc(void const * argument);

ytInitProcess(gawain_v2_testproc, gawain_v2_testfunc, NORMAL_PRIORITY_PROCESS, 256, &gawain_v2_testId, NULL);


YT_PROCESS void gawain_v2_testfunc(void const * argument){

	ytGpioInitPin(GPIO_PIN_C15, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);

	while(1){

		tsl_status = tsl_user_Exec();

		if (tsl_status != TSL_USER_STATUS_BUSY)
		{
			if(MyLinRotsB[0].p_Data->StateId == TSL_STATEID_DETECT){
				pwmLED1 = MyLinRotsB[0].p_Data->RawPosition;

				if(pwmLED1 >= 2){
					if(pwmLED1 & 0x01)
						ytGpioPinSet(GPIO_PIN_C15);
					else
						ytGpioPinReset(GPIO_PIN_C15);
				}
			}
		}

	}
}
/* *************************************************/


#endif
