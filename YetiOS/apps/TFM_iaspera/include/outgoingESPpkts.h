/*
 * outgoingESPpkts.h
 *
 *  Created on: 23 sept. 2018
 *      Author: Ilde
 */

#ifndef YETIOS_BOARD_PLATFORM_OUTGOINGESPPKTS_OUTGOINGESPPKTS_H_
#define YETIOS_BOARD_PLATFORM_OUTGOINGESPPKTS_OUTGOINGESPPKTS_H_

#include "system_api.h"

/* Definition of each type of data in a packet */
typedef enum {presence, remSwitch, remCtrl} pktType;

// Presence sensor data
struct presenceData_t {
	int8_t temp;
	uint8_t hum;
	uint8_t movDect;
};

// Remote switch data
struct remSwitchData_t {
	int16_t activePwr;		// Increments of 150mW
	int16_t reactivePwr;	// Increments of 150mVA
	int16_t rmsVoltage;		// Increments of 10mV
	int16_t rmsCurrent;		// Increments of 1mA
	int16_t powerFactor;	// Increments of 1/2^15
	uint16_t lineFreq;		// Increments of 2mHz
	uint8_t relayState;		// 0 -> Open, 1-> Closed
};

// Remote control data
struct remCtrlData_t {
	uint8_t topBtn : 1;
	uint8_t midBtn : 1;
	uint8_t btmBtn : 1;
	uint8_t slider : 5;
};

// Definition of content of packets
struct dataPacket_t {

	pktType type;
	uint16_t origID;
	union {
		struct presenceData_t presenceData;
		struct remSwitchData_t remSwitchData;
		struct remCtrlData_t remCtrlData;
	};
};

//Scales for remote switch packet
#define PKT_SCALE_ACTIVE_POWER		0.15
#define PKT_SCALE_REACTIVE_POWER	0.15
#define PKT_SCALE_RMS_VOLTAGE		0.01
#define PKT_SCALE_RMS_CURRENT		0.001
#define PKT_SCALE_POWER_FACTOR		32768
#define PKT_SCALE_LINE_FREQUENCY	0.002

#endif /* YETIOS_BOARD_PLATFORM_OUTGOINGESPPKTS_OUTGOINGESPPKTS_H_ */
