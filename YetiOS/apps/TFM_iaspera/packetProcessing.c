/* Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * packetProcessing.c
 *
 *  Created on: 22 de sept. de 2018
 *      Author: Ildefonso Aspera Olmo  <iaspera@b105.upm.es>
 *
 */
/**
 * @file packetProcessing.c
 */

#include "platform-conf.h"

#if BORS_V2

#define USE_PKTPROC	1

#if USE_PKTPROC

#include "platform-conf.h"
#include "system_api.h"
#include "outgoingESPpkts.h"

#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

//------------------------------------------------------

//----------------------------------------------------------

uint16_t pktProcProcId;

uint32_t messageQueueInstance = 0;


/* packetProcessing PROCESS ************************************/
YT_PROCESS static void packet_processing(void const * argument);


/* This line must be uncommented to launch this process */
ytInitProcess(packet_processing_process, packet_processing, NORMAL_PRIORITY_PROCESS, 512, &pktProcProcId, NULL);

YT_PROCESS static void packet_processing(void const * argument){

	struct dataPacket_t newPacket;
	char tmpNumber[7];		// Big enough for max value of int16_t, plus sign, plus null char
	char espBuffer[135];	// Maximum required space for one outgoing packet, including terminating character is 169, using more just in case
	uint32_t esp8266_fd;
	struct dataPacket_t* tmp;

	ytPrintf(">packetProcessing init\r\n");

	while(1){

		// Get new packet here
		tmp = (struct dataPacket_t*) ytMessageqGet(messageQueueInstance, osWaitForever);

		if(tmp != NULL){

			memcpy(&newPacket, tmp, sizeof(struct dataPacket_t));

			// Determine which type of packet we have received and prepare for transmission
			switch(newPacket.type){
			case presence:
				strcpy(espBuffer, ", \"Temp\":");
				snprintf(tmpNumber, sizeof(tmpNumber), "%i", newPacket.presenceData.temp);
				strcat(espBuffer, tmpNumber);
				strcat(espBuffer, ", \"Hum\":");
				snprintf(tmpNumber, sizeof(tmpNumber), "%i", newPacket.presenceData.hum);
				strcat(espBuffer, tmpNumber);
				strcat(espBuffer, ", \"Mov_dect\":");
				snprintf(tmpNumber, sizeof(tmpNumber), "%i", newPacket.presenceData.movDect);
				strcat(espBuffer, tmpNumber);
				strcat(espBuffer, "}");
				strcat(espBuffer, "PRSNC_");
				snprintf(tmpNumber, sizeof(tmpNumber), "%i", newPacket.origID);
				strcat(espBuffer, tmpNumber);
				strcat(espBuffer, "\r");
				break;
			case remSwitch:
				strcpy(espBuffer, ", \"Active_pwr\":");
				snprintf(tmpNumber, sizeof(tmpNumber), "%4.2f", newPacket.remSwitchData.activePwr*PKT_SCALE_ACTIVE_POWER);
				strcat(espBuffer, tmpNumber);
				strcat(espBuffer, ", \"Rective_pwr\":");
				snprintf(tmpNumber, sizeof(tmpNumber), "%3.2f", newPacket.remSwitchData.reactivePwr*PKT_SCALE_REACTIVE_POWER);
				strcat(espBuffer, tmpNumber);
				strcat(espBuffer, ", \"Vrms\":");	//13
				snprintf(tmpNumber, sizeof(tmpNumber), "%3.2f", newPacket.remSwitchData.rmsVoltage*PKT_SCALE_RMS_VOLTAGE);
				strcat(espBuffer, tmpNumber);
				strcat(espBuffer, ", \"Irms\":"); //13
				snprintf(tmpNumber, sizeof(tmpNumber), "%1.3f", newPacket.remSwitchData.rmsCurrent*PKT_SCALE_RMS_CURRENT);
				strcat(espBuffer, tmpNumber);
				strcat(espBuffer, ", \"Power_factor\":");
				snprintf(tmpNumber, sizeof(tmpNumber), "%1.3f", (float)newPacket.remSwitchData.powerFactor/PKT_SCALE_POWER_FACTOR);
				strcat(espBuffer, tmpNumber);
				strcat(espBuffer, ", \"Line_freq\":");
				snprintf(tmpNumber, sizeof(tmpNumber), "%2.2f", newPacket.remSwitchData.lineFreq*PKT_SCALE_LINE_FREQUENCY);
				strcat(espBuffer, tmpNumber);
				strcat(espBuffer, ", \"Relay_state\":");
				snprintf(tmpNumber, sizeof(tmpNumber), "%i", newPacket.remSwitchData.relayState);
				strcat(espBuffer, tmpNumber);
				strcat(espBuffer, "}");
				strcat(espBuffer, "REMSW_");
				snprintf(tmpNumber, sizeof(tmpNumber), "%i", newPacket.origID);
				strcat(espBuffer, tmpNumber);
				strcat(espBuffer, "\r");
				break;
			case remCtrl:
				strcpy(espBuffer, ", \"Top_btn\":");
				newPacket.remCtrlData.topBtn == 1 ? strcat(espBuffer, "1") : strcat(espBuffer, "0");
				strcat(espBuffer, ", \"Mid_btn\":");
				newPacket.remCtrlData.midBtn == 1 ? strcat(espBuffer, "1") : strcat(espBuffer, "0");
				strcat(espBuffer, ", \"Btm_btn\":");
				newPacket.remCtrlData.btmBtn == 1 ? strcat(espBuffer, "1") : strcat(espBuffer, "0");
				strcat(espBuffer, "}");
				strcat(espBuffer, "RMCTL_");
				snprintf(tmpNumber, sizeof(tmpNumber), "%i", newPacket.origID);
				strcat(espBuffer, tmpNumber);
				strcat(espBuffer, "\r");
				break;
			default:
				// Do nothing if erroneous package
				break;
			}

			esp8266_fd = ytOpen(UART_DEV_1, 0);

			ytWrite(esp8266_fd, espBuffer, strlen(espBuffer));
			// Debug
			tmpNumber[0] = strlen(espBuffer);
			strcpy(espBuffer, "Used sized in outgoing buffer was: ");
			snprintf(tmpNumber, sizeof(tmpNumber), "%i", tmpNumber[0]);
			strcat(espBuffer, tmpNumber);
			strcat(espBuffer, " Bytes\n");
			ytWrite(esp8266_fd, espBuffer, strlen(espBuffer));
			// Debug

			ytClose(esp8266_fd);

			ytPrintf("Packet sent\n");
		}

	}
}

#endif
#endif
