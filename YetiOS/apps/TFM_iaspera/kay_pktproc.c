/*
 * kay_pktproc.c
 *
 *  Created on: 4 oct. 2018
 *      Author: iaspera
 */

#include "platform-conf.h"

#if KAY_V2

#define USE_KAY_PKTPROC	1
#if USE_KAY_PKTPROC

#include "platform-conf.h"
#include "system_api.h"
#include "system_net_api.h"
#include "../apps/TFM_iaspera/include/outgoingESPpkts.h"
#include "unique_id.h"


#define NET_PORT			444

uint16_t kayPktProcId;
uint8_t isMovDetected;
extern int32_t isMovSemaphore;

/* TBASIC EST PROCESS ************************************/
YT_PROCESS void kay_pktProc_func(void const * argument);

ytInitProcess(kay_pktProc_func, kay_pktProc_func, NORMAL_PRIORITY_PROCESS, 320, &kayPktProcId, NULL);


YT_PROCESS void kay_pktProc_func(void const * argument){
	uint32_t si7020_fd;
	float32_t read_buff[2];
	struct dataPacket_t outgoingESPpkt;

	/* Set node Addr */
	ytNetAddr_t net_addr = ytNewNetAddr("2", 'D');		//Node adress
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);
	net_addr = ytNewNetAddr("1", 'D');	//Envio al nodo BORS v2

	while(1){


		si7020_fd = ytOpen(SI7020_DEV, 0);
		ytRead(si7020_fd, &read_buff, 2);
		ytClose(si7020_fd);

		ytPrintf("Temp: %2.1f Hum: %2.1f. ", read_buff[0], read_buff[1]);

		if(ytSemaphoreWait(isMovSemaphore, 100) == RET_OK){
			if(isMovDetected)
				ytPrintf("Movement detected\r\n");
			else
				ytPrintf("Movement not detected\r\n");

			outgoingESPpkt.presenceData.movDect = isMovDetected;
			isMovDetected = 0;
			ytSemaphoreRelease(isMovSemaphore);
		}
		else{
			outgoingESPpkt.presenceData.movDect = 0;
		}

		outgoingESPpkt.type = presence;
		outgoingESPpkt.origID = get_uinque_16_bits_id();
		outgoingESPpkt.presenceData.temp = (int8_t) read_buff[0];
		outgoingESPpkt.presenceData.hum =  (uint8_t) (read_buff[1]*2);


		ytPrintf("Pkt type:%i, origID:%i, temp:%i, hum:%2.1f, mov:%i\r\n", outgoingESPpkt.type, outgoingESPpkt.origID, \
				outgoingESPpkt.presenceData.temp, (float32_t)outgoingESPpkt.presenceData.hum/2, outgoingESPpkt.presenceData.movDect);


		// Envio de paquete por RF
		ytUrOpen(NET_PORT);	//Abro el puerto
		if(ytUrSend(NET_PORT, net_addr,(uint8_t*) &outgoingESPpkt, sizeof(struct dataPacket_t)) == sizeof(struct dataPacket_t)){
			ytPrintf("SEND OK\r\n");
		}
		ytUrClose(NET_PORT);
		//ytDeleteNetAddr(net_addr);
		ytLedsOn(LEDS_RED1);
		ytDelay(20);
		ytLedsToggle(LEDS_RED1);

		ytDelay(1000);

	}
}
/* *************************************************/

#endif
#endif
