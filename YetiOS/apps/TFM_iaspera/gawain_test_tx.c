/*
 * gawain_test_tx.c
 *
 *  Created on: 4 mar. 2019
 *      Author: iaspera
 */



#include "platform-conf.h"

#if GAWAIN_V2

#define USE_GAWAIN_TEST_TX	1
#if USE_GAWAIN_TEST_TX

#include "../apps/TFM_iaspera/include/outgoingESPpkts.h"
#include "system_api.h"
#include "system_net_api.h"
#include "unique_id.h"

#define DEFAULT_TIMEOUT	100
#define NET_PORT		444

uint16_t gawain_test_txId;
char i = 0;


/* TBASIC EST PROCESS ************************************/
YT_PROCESS void gawain_test_txfunc(void const * argument);

ytInitProcess(gawain_test_txproc, gawain_test_txfunc, NORMAL_PRIORITY_PROCESS, 256, &gawain_test_txId, NULL);


YT_PROCESS void gawain_test_txfunc(void const * argument){
	struct dataPacket_t outgoingESPpkt;

	/* Set node Addr */
		ytNetAddr_t net_addr = ytNewNetAddr("3", 'D');		//Node adress
		ytNetAddNodeAddr(net_addr);
		ytDeleteNetAddr(net_addr);
		net_addr = ytNewNetAddr("1", 'D');	//Envio al nodo BORS v2

	while(1){

		outgoingESPpkt.type = remCtrl;
		outgoingESPpkt.origID = get_uinque_16_bits_id();
		outgoingESPpkt.remCtrlData.topBtn = i>>8;
		outgoingESPpkt.remCtrlData.midBtn = (i>>7) & 0x01;
		outgoingESPpkt.remCtrlData.btmBtn = (i>>1) & 0x01;
		outgoingESPpkt.remCtrlData.slider = i & 0x1F;
		i++;

		ytPrintf("Pkt type:%i, origID:%i, topBtn:%i, midBtn:%i, btmBtn:%i, slider:%i\r\n", outgoingESPpkt.type, \
				outgoingESPpkt.origID, outgoingESPpkt.remCtrlData.topBtn, outgoingESPpkt.remCtrlData.midBtn, \
				outgoingESPpkt.remCtrlData.btmBtn, outgoingESPpkt.remCtrlData.slider);

		// Envio de paquete por RF
		ytUrOpen(NET_PORT);	//Abro el puerto
		if(ytUrSend(NET_PORT, net_addr,(uint8_t*) &outgoingESPpkt, sizeof(struct dataPacket_t)) == sizeof(struct dataPacket_t)){
			ytPrintf("SEND OK\r\n");
		}
		ytUrClose(NET_PORT);
		//ytDeleteNetAddr(net_addr);
		ytLedsOn(LEDS_RED1);
		ytDelay(20);
		ytLedsToggle(LEDS_RED1);

		ytDelay(1000);

	}
}
/* *************************************************/
#endif
#endif
