/*
 * Copyright (c) 2019, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * dynCpuFreqApp.c
 *
 *  Created on: 27 ene. 2020
 *  Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file dynCpuFreqApp.c
 */

#include "yetiOS.h"
#include "lowPower.h"
#include "arm_math.h"

#if DYN_CPU_FREQ_APP

//#define SAMPLE_RATE_1000HZ_US	1000
//#define SAMPLE_RATE_500HZ_US	2000
//#define SAMPLE_RATE_250HZ_US	4000
//#define SAMPLE_RATE_125HZ_US	8000
//
//#define DEFAULT_SAMPLE_RATE		SAMPLE_RATE_1000HZ_US

#define USE_INT_OPERATIONS		0
#define USE_FLOAT_OPERATIONS	1

#define APP_MODE_PERIODIC		1
#define APP_MODE_TWO_LOOPS		0
#define APP_MODE_ALL_LOOPS_1	0
#define APP_MODE_DYN			0

#define MEAS_OVERRUN_COUNT		1

#define RAND_EXTRA_THREAD		1
#define MAX_NUM_LOOPS_RAND		300000
#define MAX_DELAY_RAND			15000
/*Used num for loops 150000; 50000; 10000; 1000*/
#define NUM_FOR_LOOPS_0			150000
#define NUM_FOR_LOOPS_1			1000
#define NUM_FOR_LOOPS_2			50000
#define NUM_FOR_LOOPS_3			10000
#define NUM_FOR_LOOPS_4			1000

/*Delay Times: 100; 500; 750; 1000; 2000;*/
#define DELAY_TIME				1000

#define CHANGE_LOOPS_TIME		8000

#define DYN_TIMEOUT_1			900000	/*15 s*/
#define DYN_TIMEOUT_2			300000	/*5 s*/

#if CONFIG_SELECTED_PLATFORM == HEIMDALL_PLATFORM


static osSemaphoreId samplingSemph;
static osMutexId procFlagsMutex;
static volatile uint16_t processing = 0;
volatile uint32_t overrunCount = 0;
static volatile uint8_t readBuff[1024];

static uint16_t change_num_loops = 0;
static osTimerId dynNumLoopsTimer;
#if MEAS_OVERRUN_COUNT
static osTimerId measOverrunTimer;
#endif

static osThreadId processingTestThreadHandle;
static osThreadId samplingTestThreadHandle;
static osThreadId randTestThreadHandle;

static void dynNumLoopsTimerFunc(void const * argument);
static void processingTestThreadFunc(void const * argument);
static void samplingTestThreadFunc(void const * argument);
static void randTestThreadFunc(void const * argument);
static void measOverrunTimeout(void const * argument);

YtAutoInitThread(processingTestThread, processingTestThreadFunc, osPriorityAboveNormal, 256, &processingTestThreadHandle, NULL);
YtAutoInitThread(samplingTestThread, samplingTestThreadFunc, osPriorityHigh, 256, &samplingTestThreadHandle, NULL);
#if RAND_EXTRA_THREAD
YtAutoInitThread(randTestThread, randTestThreadFunc, osPriorityAboveNormal, 256, &randTestThreadHandle, NULL);
#endif


static void dynNumLoopsTimerFunc(void const * argument){
	change_num_loops++;
#if APP_MODE_DYN
	osTimerStop(dynNumLoopsTimer);
	if(currentDynTimeout == DYN_TIMEOUT_1){
		currentDynTimeout = DYN_TIMEOUT_2;
		osTimerStart(dynNumLoopsTimer, DYN_TIMEOUT_2);
	}
	else{
		currentDynTimeout = DYN_TIMEOUT_1;
		osTimerStart(dynNumLoopsTimer, DYN_TIMEOUT_1);
	}

#endif
}

static void measOverrunTimeout(void const * argument){
	if(overrunCount){
		overrunCount++;

	}
	else{
		overrunCount--;
	}
}

/**
 *
 * @param argument
 */
static void samplingTestThreadFunc(void const * argument){

	procFlagsMutex = ytMutexCreate();
	samplingSemph = ytSemaphoreCreate(1);


	ytGpioInitPin(GPIO_PIN_B1, GPIO_PIN_INPUT, GPIO_PIN_NO_PULL);


//	deviceFileHandler_t* spiDev = ytOpen(PLATFORM_SPI1_DEVICE_ID, 0);
//
//	ytSpiMode_t spiMode = SPI_MASTER;
//	uint32_t speed = 2000000;
//	ytIoctl(spiDev, SPI_SET_SPEED,(void*) &speed);
//	ytIoctl(spiDev, SPI_SET_HW_CS, NULL);
//	ytIoctl(spiDev, SPI_SET_MODE, (void*) &spiMode);
	osDelay(10);

	while(1){

//		ytRead(spiDev, readBuff, 1024);

		osDelay(DELAY_TIME);
		osMutexWait(procFlagsMutex, osWaitForever);
		if(processing){
			overrunCount++;
		}
		osMutexRelease(procFlagsMutex);
		osSemaphoreRelease(samplingSemph);

	}
}

/**
 *
 * @param argument
 */
static void processingTestThreadFunc(void const * argument){
#if USE_INT_OPERATIONS
	volatile int32_t var1, var2, var3, var4;
#elif USE_FLOAT_OPERATIONS
	volatile float32_t var1, var2, var3, var4;
#endif
	var1 = 0;
	var2 = 0;
	var3 = 0;
	var4 = 0;
	uint32_t i;
//	volatile timeMeas_t timeMeas;
	uint32_t num_for_loops = NUM_FOR_LOOPS_0;
	dynNumLoopsTimer = ytTimerCreate(osTimerPeriodic, dynNumLoopsTimerFunc, NULL);
#if !APP_MODE_PERIODIC
	#if APP_MODE_DYN
		currentDynTimeout = DYN_TIMEOUT_1;
		osTimerStart(dynNumLoopsTimer, DYN_TIMEOUT_1);
	#else
		osTimerStart(dynNumLoopsTimer, CHANGE_LOOPS_TIME);
	#endif
#endif

#if MEAS_OVERRUN_COUNT
		measOverrunTimer = ytTimerCreate(osTimerOnce, measOverrunTimeout, NULL);
		osTimerStart(measOverrunTimer, 2000000);
#endif
	osDelay(50);

	while(1){
		osSemaphoreWait(samplingSemph, osWaitForever);
		osMutexWait(procFlagsMutex, osWaitForever);
		processing++;
		osMutexRelease(procFlagsMutex);

		for(i=0; i < num_for_loops; i++){
			var1 = var1 + 1;
			var2 = var2 - 2;
			var3 = var1 + var2;
			var4 = var3 * var1;
			var1 = var4 - var3;
		}

#if APP_MODE_TWO_LOOPS
		if(change_num_loops){
			change_num_loops = 0;
			if(num_for_loops == NUM_FOR_LOOPS_0){
				num_for_loops = NUM_FOR_LOOPS_1;
			}
			else{
				num_for_loops = NUM_FOR_LOOPS_0;
			}
		}
#endif

#if APP_MODE_ALL_LOOPS_1
		if(change_num_loops){
			change_num_loops = 0;
			if(num_for_loops == NUM_FOR_LOOPS_0){
				num_for_loops = NUM_FOR_LOOPS_1;
			}
			else if(num_for_loops == NUM_FOR_LOOPS_1){
				num_for_loops = NUM_FOR_LOOPS_2;
			}
			else{
				num_for_loops = NUM_FOR_LOOPS_0;
			}
		}
#endif


#if APP_MODE_ALL_LOOPS_2
		if(change_num_loops){
			change_num_loops = 0;
			if(num_for_loops == NUM_FOR_LOOPS_0){
				num_for_loops = NUM_FOR_LOOPS_4;
			}
			else if(num_for_loops == NUM_FOR_LOOPS_4){
				num_for_loops = NUM_FOR_LOOPS_2;
			}
			else{
				num_for_loops = NUM_FOR_LOOPS_0;
			}
		}
#endif

#if APP_MODE_DYN
		if(change_num_loops){
			change_num_loops = 0;
			if(num_for_loops == NUM_FOR_LOOPS_0){
				num_for_loops = NUM_FOR_LOOPS_1;
			}
			else{
				num_for_loops = NUM_FOR_LOOPS_0;
			}
		}
#endif


		osMutexWait(procFlagsMutex, osWaitForever);
		processing--;
		osMutexRelease(procFlagsMutex);
	}
}

#if RAND_EXTRA_THREAD
static void randTestThreadFunc(void const * argument){
	volatile float32_t var1, var2, var3, var4;
	uint32_t i;
	var1 = 0;
	var2 = 0;
	var3 = 0;
	var4 = 0;
	srand(1000);
	uint32_t num_for_loops = rand() % MAX_NUM_LOOPS_RAND;
	while(1)
	{
		osDelay(rand() % MAX_DELAY_RAND);
		for(i=0; i < num_for_loops; i++){
			var1 = var1 + 1;
			var2 = var2 - 2;
			var3 = var1 + var2;
			var4 = var3 * var1;
			var1 = var4 - var3;
		}
		num_for_loops = rand() % MAX_NUM_LOOPS_RAND;
	}
}
#endif

#endif
#endif



