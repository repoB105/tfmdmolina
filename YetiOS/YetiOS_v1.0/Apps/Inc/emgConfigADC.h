/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgment:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * emgConfigADC.h
 *
 *  Created on: 18 Feb. 2021
 *	Author: David Molina Toro <dmolina@b105.upm.es>
 */

/*
 * @file emgConfigADC.h
 * @configuration system header for EMG control
 */

#ifndef EMG_CONFIG_ADC_H_
#define EMG_CONFIG_ADC_H_

//YetiOS header
#include "yetiOs.h"

#ifdef SLAVE_NODE

//Communication header
#include "emgComms.h"

//ADC headers
#include "ad7124.h"
#include "ad7124Regs.h"

//Configuration initialization function declaration
uint16_t initConfigADC();

//Get the current configuration function declaration
ad7124Dev_t* getConfigADC();

//Get the configured sample rate
void setSampleRate(uint32_t iFilter);
float32_t getSampleRate();

//Get the configured active channels
void setActiveChannels(uint16_t *iChannels);
uint16_t getActiveChannels();

#if DATA_CONVERSION

//Conversion constant definitions
#define CODING_CONST 	0x800000
#define REF_VOLTAGE 	3.3

//Gain bits definitions
#define GAIN_BITS_POS 0
#define GAIN_BITS_NUM 3

//Data gain relation function declarations
void setGainRelation(uint16_t *iGain);
void getGainRelation(float32_t *fRelation);

#endif

#endif
#endif
