/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgment:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * emgComms.h
 *
 *  Created on: 18 Feb. 2021
 *	Author: David Molina Toro <dmolina@b105.upm.es>
 */

/*
 * @file emgComms.h
 * @communication system header for EMG control
 */

#ifndef EMG_COMMS_H_
#define EMG_COMMS_H_

//YetiOS header
#include "yetiOs.h"

//Network YetiOS header
#include "netYetiOS.h"

//Initialization delay definition
#define WAIT_SYSTEMS 100

//Queue parameters definition
#define NO_WAIT		0
#define PACKET_SIZE	32

//Serial buffer definitions
#define HEADER_MESSAGE_SIZE 4
#define CTRL_MESSAGE_SIZE 	10
#define STAT_MESSAGE_SIZE 	12
#define DATA_MESSAGE_SIZE 	26
#define CONF_MESSAGE_SIZE 	26
#define CAPT_MESSAGE_SIZE	25

//Message header definitions (mustn't exceed 4 characters)
#define CTRL_HEADER "CTRL"
#define STAT_HEADER "STAT"
#define DATA_HEADER "DATA"
#define CONF_HEADER "CONF"
#define CAPT_HEADER "CAPT"

//Default error definitions
#define NO_ERROR 		0x00
#define ARGUMENT_ERROR 	0x01

//UDP message structure definition
typedef struct
{
	uint8_t dataBuffer[PACKET_SIZE];
	uint16_t iBufferSize;
} udpMessage_t;

//Communication system initialization function declaration
uint16_t initComms();

//Message addition to the UDP send queue declaration
void addMessageUdp(uint8_t *data, uint16_t iSize);

#ifdef SLAVE_NODE

//Buffer system header
#include "emgBuffer.h"

//Test label maximum length definition
#define MAX_LABEL_LENGTH 	11

//FSM default flags for messaging
#define FSM_DEFAULT_STATUS 	0
#define FSM_DEFAULT_ERROR 	1

//Control command definitions
typedef enum
{
	NONE 	= 0,
	INIT 	= 1,
	START 	= 2,
	STOP 	= 3,
	CONFIG	= 4,
	ADCSET	= 5,
	STOERR 	= 6,
	STATUS 	= 7,
	RESTART	= 8
} controlCommand_t;

//Control message structure definition
typedef struct
{
	uint16_t iCommand;
	uint16_t iError;
} controlMessage_t;

//Status message structure definition
typedef struct
{
	uint16_t iCurrentState;
	uint16_t iLastState;
	uint16_t iError;
} statusMessage_t;

//Configuration message structure definition
typedef struct
{
	uint16_t iChannel[CHANNEL_NUM];
	uint16_t iGain[CHANNEL_NUM];
	uint32_t iFilter;
} configMessage_t;

//Capture message structure definition
typedef struct
{
	int64_t iUnixTime;
	char cTestLabel[MAX_LABEL_LENGTH];
} captureMessage_t;

//Status message queue definition
osPoolId poolControl;
osMessageQId queueControl;

//Configuration message queue definition
osPoolId poolConfig;
osMessageQId queueConfig;

//Capture message queue definition
osPoolId poolCapture;
osMessageQId queueCapture;

//Message status addition to the UDP send queue declaration
void addDataMessageUdp(dataFrame_t *frame);
void addStatusMessageUdp(statusMessage_t statusMessage);

#endif
#endif
