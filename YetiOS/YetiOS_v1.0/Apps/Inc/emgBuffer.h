/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgment:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * emgBuffer.h
 *
 *  Created on: 21 Mar. 2021
 *	Author: David Molina Toro <dmolina@b105.upm.es>
 */

/*
 * @file emgBuffer.c
 * @data buffer system for EMG control
 */

#ifndef EMG_BUFFER_H_
#define EMG_BUFFER_H_

//YetiOS header
#include "yetiOs.h"

#ifdef SLAVE_NODE

//Buffer length definition
#define BUFFER_LENGTH 256

//Number of channels to configure
#define CHANNEL_NUM 4

//Data frame structure definition
typedef struct
{
	uint32_t iFrameID;
	uint32_t iData[CHANNEL_NUM];
} dataFrame_t;

//Buffer structure definition
typedef struct
{
	dataFrame_t dataFrames[BUFFER_LENGTH];
	uint16_t iHead;
	uint16_t iTail;
	uint16_t iFull;
	uint16_t iEmpty;
} dataBuff_t;

//Data buffer declaration
dataBuff_t dataBuffer;

//Buffer initialization function declaration
void buffInit();

//Buffer reset function declaration
void buffReset();

//Add data to buffer function declaration
retval_t buffPut(dataFrame_t frame);

//Get frame from buffer function declaration
retval_t buffPop(dataFrame_t *frame);

//Get range of frames from buffer function declaration
retval_t buffPopRange(dataFrame_t *frames, uint16_t iRange);

#endif
#endif
