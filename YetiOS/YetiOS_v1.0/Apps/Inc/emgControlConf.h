/*
 * Copyright (c) 2019, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgment:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * emgControlConf.h
 *
 *  Created on: 9 Feb. 2021
 *  Author: David Molina Toro <dmolina@b105.upm.es>
 *
 */

/*
 * @file emgControlConf.h
 */

#ifndef YETIOS_APPS_INC_EMG_CONTROL_CONF_H_
#define YETIOS_APPS_INC_EMG_CONTROL_CONF_H_

#ifdef APP_CUSTOM_CONFIG_DEFINED
#error "A custom configuration have already been selected. Please select only one custom configuration in appConfig.h file"
#endif
#define APP_CUSTOM_CONFIG_DEFINED

#if CONFIG_SELECTED_PLATFORM != HEIMDALL_PLATFORM
#error "This application is not supported for this board"
#endif

//Network stack transceiver selection
#define CERBERUS_DEFAULT_TRANSCEIVER CERBERUS_CC2500_TRANSCEIVER

//Enable the network stack for UDP communications
#define YETIOS_ENABLE_NETSTACK 1

//Disable the shell thread for manual serial utilization
#define YETIOS_ENABLE_YETISHELL 0

//Master and slave defines for UDP communications
#define SLAVE_NODE 1
//#define MASTER_NODE 2

//Address definitions for UDP communications
#define UDP_COMMS_PORT		0xAAAA
#define SLAVE_UDP_ADDRESS	"2"
#define MASTER_UDP_ADDRESS 	"1"
#define UDP_ADDRESS_FORMAT 	'D'

//Data conversion enabling definition
#define DATA_CONVERSION 0

//Trace analyzer usage definition
#define CONFIG_USE_TRACEALYZER_SW 0

#endif
