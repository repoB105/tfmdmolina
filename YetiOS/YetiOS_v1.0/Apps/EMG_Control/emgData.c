/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgment:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * emgData.c
 *
 *  Created on: 18 Feb. 2021
 *	Author: David Molina Toro <dmolina@b105.upm.es>
 */

/*
 * @file emgData.c
 * @data system for EMG control
 */

//Data header
#include "emgData.h"

#if EMG_CONTROL_APP
#ifdef SLAVE_NODE

//Missing data definitions
#define MISSING_SMPL_VALUE 	0xFFFFFFFF
#define NAN_LENGTH			3
#define NAN_VALUE 			"NaN"

//Acquisition status definitions
#define CAPTURE_STOP 	0
#define CAPTURE_START 	1

//Stop signal definition
#define SIGNAL_STOP 0x01

//Samples per transmission definitions
#define SAMPLES_FOR_SD	100
#define SAMPLES_FOR_UDP 30
#define MIN_SAMPLES_UDP 1

//Capture queue length definition
#define CAPTURE_QUEUE_LENGTH 4

//Test label definitions
#define RAW_LINE_LENGTH		22
#define DATA_SINGLE_CHAR	1
#define DATA_LINE_LENGTH	128
#define TEST_FILE_LENGTH	64
#define TEST_DATE_LENGTH	15
#define TEST_DATE_FORMAT 	"%Y%m%d%H%M%S"

//File constant definitions
#define FILE_LINE_END		"\n"
#define FILE_VALUE_END		","
#define FILE_SEPARATOR 		"/"
#define FILE_FRAME_ID		"\r\n"
#define FILE_EXT_BIN 		".bin"
#define FILE_EXT_CSV 		".csv"

//Numeric conversion definitions
#define DECIMAL_CONV 		10
#define EXP_ARRAY_LENGTH	12
#define EXP_CONVERSION		"%12.5e"

//Storage sector definitions
#define SECTOR_DIV 			2
#define MIN_SPACE_MIB 		128
#define CONV_KIB_TO_MIB 	1024

//Status message queue definition
extern osPoolId poolControl;
extern osMessageQId queueControl;

//Capture message queue definition
extern osPoolId poolCapture;
extern osMessageQId queueCapture;

//Capture queue definition
osPoolDef(poolCapture, CAPTURE_QUEUE_LENGTH, captureMessage_t);
osMessageQDef(queueCapture, CAPTURE_QUEUE_LENGTH, uint32_t);

//Capture control semaphores
osSemaphoreId storageSemaphore;
osSemaphoreId captureSemaphore;

//Channel control semaphores
osSemaphoreId readSemaphore;
osSemaphoreId samplingSemaphore;

//Samples required for UDP variable
uint16_t iSamplesUdp;

//Channel control variables
uint32_t iLastSentUdp;
uint16_t iExpectedUdp;
uint32_t iCurrentChannel;
uint16_t iExpectedChannel;

//Current frame structure objects
dataFrame_t currentFrame;
dataFrame_t previousFrame;

//FatFs logical drive object declaration
FATFS driveSD;
char cPathSD[4];

//Data to write in SD array declaration
uint8_t dataSD[SAMPLES_FOR_SD * RAW_LINE_LENGTH];

#if DATA_CONVERSION
//Conversion constant variable
float32_t fGainRelation[CHANNEL_NUM];
#endif

//Processes instances
osThreadId dataAcqProcId;
osThreadId dataStorageProcId;

//Data acquisition function
void dataAcqFunc(void const *arg);

//Data storage function
void dataStorageFunc(void const *arg);

//Data ready callback function declaration
void dataReadyPinCallback(void const *arg);

//Test storage initialization function declaration
retval_t initStorage(captureMessage_t capture, FIL *file);

//Frame storage function declaration
void saveFrameSample(int32_t iSample);

//Data storage function declarations
#if DATA_CONVERSION
void saveData(dataFrame_t *frames, FIL *file);
#else
void saveDataRaw(dataFrame_t *frames, FIL *file);
#endif

/*
Data acquisition initialization function
 - Input: N/A
 - Output:
 	 Status
*/
uint16_t initAcq()
{
	//Initialize the storage semaphore
	storageSemaphore = ytSemaphoreCreate(1);
	osSemaphoreWait(storageSemaphore, osWaitForever);

	//Initialize the capture semaphore
	captureSemaphore = ytSemaphoreCreate(1);
	osSemaphoreWait(captureSemaphore, osWaitForever);

	//Initialize the sampling semaphore
	samplingSemaphore = ytSemaphoreCreate(1);
	osSemaphoreWait(samplingSemaphore, osWaitForever);

	//Initialize the read semaphore
	readSemaphore = ytSemaphoreCreate(1);
	osSemaphoreWait(readSemaphore, osWaitForever);

	//Initialize the capture queuing system
	poolCapture 	= osPoolCreate(osPool(poolCapture));				//This queue receives messages
	queueCapture 	= osMessageCreate(osMessageQ(queueCapture), NULL);	//from the FSM control thread

	//Initialize the circular buffer
	buffInit(&dataBuffer);

	//Link the FatFs driver
	if (!FATFS_LinkDriver(&SD_Driver, cPathSD))
	{
		//Initialize the BSP driver module
		if (BSP_SD_Init() == MSD_OK)
		{
			//Register the file system object to the module
			if (f_mount(&driveSD, (TCHAR const*)cPathSD, 1) == FR_OK)
			{
				//Initialize the data acquisition thread
				ytStartThread("data_acq_proc", dataAcqFunc, osPriorityAboveNormal, 256, &dataAcqProcId, NULL);

				//Set the stop signal
				osSignalSet(dataAcqProcId, SIGNAL_STOP);

				//Release the capture semaphore
				osSemaphoreRelease(captureSemaphore);

				//Initialize the data storage thread
				ytStartThread("data_storage_proc", dataStorageFunc, osPriorityLow, 1024, &dataStorageProcId, NULL);

				//Set the stop signal
				osSignalSet(dataStorageProcId, SIGNAL_STOP);

				//Release the capture semaphore
				osSemaphoreRelease(storageSemaphore);

				return SUCCESS;
			}
		}
		else
		{
			//Unlink the driver if an error has occurred
			FATFS_UnLinkDriver(cPathSD);
		}
	}

	return ERROR;
}

/*
Data acquisition function
 - Input:
 	 Arguments
 - Output: N/A
*/
void dataAcqFunc(void const *arg)
{
	//Sample control variables
	int32_t iCurrentSample;
	uint8_t iCurrentStatus;

	//Configure the data ready pin
	ytGpioInitPin(GPIO_PIN_C5, GPIO_PIN_INTERRUPT_FALLING, GPIO_PIN_NO_PULL);
	ytGpioInitPin(GPIO_PIN_C1, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);

	//Wait for the capture semaphore from the control FSM
	osSemaphoreWait(captureSemaphore, osWaitForever);

	//Main acquisition loop
	while(1)
	{
		//Check the acquisition status
		if (osSignalWait(SIGNAL_STOP, NO_WAIT).status == osEventSignal)
		{
			//Reset the data ready callback
			ytGpioPinSetCallback(GPIO_PIN_C5, NULL, NULL);

			//Reset the expected channels
			iExpectedUdp 		= 0;
			iExpectedChannel 	= 0;

			//Reset the current frame identifiers
			currentFrame.iFrameID 	= 0;
			previousFrame.iFrameID 	= 0;

			//Reset the last frame sent via UDP indicator
			iLastSentUdp = 0;

			//Reset the circular buffer
			buffReset(&dataBuffer);

			//Wait for the capture semaphore from the storage thread
			osSemaphoreWait(captureSemaphore, osWaitForever);

			//Get the samples required for UDP and check the minimum value
			iSamplesUdp = (getSampleRate() / SAMPLES_FOR_UDP) / ((float32_t)getActiveChannels() / CHANNEL_NUM);
			iSamplesUdp = iSamplesUdp <= 0 ? MIN_SAMPLES_UDP : iSamplesUdp;

			//Set the data ready callback
			ytGpioPinSetCallback(GPIO_PIN_C5, dataReadyPinCallback, NULL);
		}

		//Wait for the read semaphore from the data ready pin callback
		osSemaphoreWait(readSemaphore, osWaitForever);

		//Reset the data ready callback
		ytGpioPinSetCallback(GPIO_PIN_C5, NULL, NULL);

		//Read the ADC data
		ad7124ReadDataStatusAppend(getConfigADC(), &iCurrentSample, &iCurrentStatus);

		//Set the data ready callback
		ytGpioPinSetCallback(GPIO_PIN_C5, dataReadyPinCallback, NULL);

		//Check if the current channel corresponds to the expected one
		iCurrentChannel = (uint32_t)AD7124_STATUS_REG_CH_ACTIVE(iCurrentStatus);
		if(iExpectedChannel != iCurrentChannel)
		{
			//Check if the current channel is not as expected
			while(iExpectedChannel > iCurrentChannel)
			{
				//Call the frame storage function
				saveFrameSample(MISSING_SMPL_VALUE);
			}

			//Check if the current channel is not as expected
			while(iExpectedChannel < iCurrentChannel)
			{
				//Call the frame storage function
				saveFrameSample(MISSING_SMPL_VALUE);
			}
		}

		//Call the frame storage function
		saveFrameSample(iCurrentSample);
	}
}

/*
Frame sample storage function
 - Input:
 	 Current channel
 	 Expected UDP channel
 	 Data frame
 	 Current sample
 - Output: N/A
*/
void saveFrameSample(int32_t iSample)
{
	//Set the current data sample
	currentFrame.iData[iExpectedChannel] = iSample;

	//Check the channel for transmission
	if (iExpectedChannel == iExpectedUdp)
	{
		//Check if the frame has advanced
		if (previousFrame.iFrameID != iLastSentUdp)
		{
			//Check if there have been enough samples for UDP transmission
			if (previousFrame.iFrameID > 0 && !(previousFrame.iFrameID % iSamplesUdp))
			{
				//Send the current frame via UDP
				addDataMessageUdp(&previousFrame);

				//Set the last frame sent via UDP indicator
				iLastSentUdp = previousFrame.iFrameID;

				//Increase the expected UDP channel
				iExpectedUdp++;

				//Check the status of the expected UDP channel
				if (iExpectedUdp >= CHANNEL_NUM)
				{
					//Reset the expected UDP channel
					iExpectedUdp = 0;
				}
			}
		}
	}

	//Increase the expected channel
	iExpectedChannel++;

	//Check the channel for storage
	if (iExpectedChannel >= CHANNEL_NUM)
	{
		//Put the data in the buffer
		buffPut(currentFrame);

		//Set the previous frame
		previousFrame = currentFrame;

		//Check if there are enough samples for SD storage
		if (!(currentFrame.iFrameID % SAMPLES_FOR_SD))
		{
			osSemaphoreRelease(samplingSemaphore);
		}

		//Increase the current frame ID
		currentFrame.iFrameID++;

		//Reset the expected channel
		iExpectedChannel = 0;
	}
}

/*
Data storage function
 - Input:
 	 Arguments
 - Output: N/A
*/
void dataStorageFunc(void const *arg)
{
	//Capture received event
	osEvent eventCapture;

	//Data frames storage object
	dataFrame_t dataFrames[SAMPLES_FOR_SD];

	//Storage system variables
	retval_t iInitRes = ERROR;
	FIL testFile;

	//Wait for the storage semaphore from the control FSM
	osSemaphoreWait(storageSemaphore, osWaitForever);

	//Main storage loop
	while(1)
	{
		//Check the acquisition status
		if (osSignalWait(SIGNAL_STOP, NO_WAIT).status == osEventSignal)
		{
			//Close the file if able
			if (!iInitRes)
			{
				f_close(&testFile);
			}

			//Wait for the storage semaphore from the control FSM
			osSemaphoreWait(storageSemaphore, osWaitForever);

			//Check if there is a capture message in the queue
			eventCapture = osMessageGet(queueCapture, NO_WAIT);
			if (eventCapture.status == osEventMessage)
			{
				//Set the received capture
				captureMessage_t *pReceivedCapture;
				pReceivedCapture = eventCapture.value.p;

				//Initialize the test
				iInitRes = initStorage(*pReceivedCapture, &testFile);
				if (iInitRes)
				{
					//Add the storage error command to the control queue
					controlMessage_t *pPendingControl = osPoolAlloc(poolControl);
					if (pPendingControl != NULL)
					{
						//Set the control message
						pPendingControl->iCommand 	= STOERR;
						pPendingControl->iError 	= ARGUMENT_ERROR;
						osMessagePut(queueControl, (uint32_t)pPendingControl, NO_WAIT);
					}
				}
				else
				{
#if DATA_CONVERSION
					//Set the gain relation constant
					getGainRelation(fGainRelation);
#endif

					//Release the capture semaphore
					osSemaphoreRelease(captureSemaphore);
				}

				//Free the capture queue pool
				osPoolFree(poolCapture, pReceivedCapture);
			}
		}

		//Store the acquired data if able
		if (!iInitRes)
		{
			//Wait for enough samples to be available
			osSemaphoreWait(samplingSemaphore, osWaitForever);

			//Get the values from the buffer
			while (!buffPopRange(dataFrames, SAMPLES_FOR_SD))
			{
				//Save the retrieved data
#if DATA_CONVERSION
				saveData(dataFrames, &testFile);
#else
				saveDataRaw(dataFrames, &testFile);
#endif
			}
		}
		else
		{
			//Set the stop signal for the storage thread
			osSignalSet(dataStorageProcId, SIGNAL_STOP);
		}
	}
}

/*
Data ready pin callback function
 - Input:
 	 Callback arguments
 - Output: N/A
*/
void dataReadyPinCallback(void const *arg)
{
	//Release the data read semaphore
	osSemaphoreRelease(readSemaphore);
}

/*
Starts the data acquisition
 - Input: N/A
 - Output: N/A
*/
void startAcq()
{
	//Release the storage semaphore
	osSemaphoreRelease(storageSemaphore);
}

/*
Stops the data acquisition
 - Input: N/A
 - Output: N/A
*/
void stopAcq()
{
	//Set the stop signal for the acquisition thread
	osSignalSet(dataAcqProcId, SIGNAL_STOP);

	//Set the stop signal for the storage thread
	osSignalSet(dataStorageProcId, SIGNAL_STOP);

	//Set the sampling semaphore for the storage thread
	osSemaphoreRelease(samplingSemaphore);
}

/*
Initializes the storage system
 - Input:
 	 Capture message
 - Output:
 	 File handler
 	 Status
*/
retval_t initStorage(captureMessage_t capture, FIL *file)
{
	//Drive information variables
	FATFS *drive;
	DWORD iFreeSectors;

	//Check the available disk space
	if (f_getfree(cPathSD, &iFreeSectors, &drive) == FR_OK)
	{
		//Get the free space in megabytes
		DWORD iFreeSpace = iFreeSectors * drive->csize / SECTOR_DIV / CONV_KIB_TO_MIB;

		//Check if the minimum is available
		if (iFreeSpace > MIN_SPACE_MIB)
		{
			//Test file path string
			char cFilePath[TEST_FILE_LENGTH];

			//File information object
			FILINFO fileInfo;

			//Set the disk and label for the folder
			strcpy(cFilePath, cPathSD);
			strcat(cFilePath, capture.cTestLabel);

			//Create a folder for the given label if able
			FRESULT iDirRes = FR_OK;
			if (f_stat("TEST", &fileInfo) != FR_OK)
			{
				//Make the given directory
				iDirRes = f_mkdir(capture.cTestLabel);
			}

			//Check if the file path is available
			if (iDirRes == FR_OK)
			{
				//Get the time stamp for the current test
				struct tm localTime;
				char cTestDate[TEST_DATE_LENGTH];
				localtime_r(&capture.iUnixTime, &localTime);

				//Add the time stamp to the file path
				strftime(cTestDate, TEST_DATE_LENGTH, TEST_DATE_FORMAT, &localTime);
				strcat(cFilePath, FILE_SEPARATOR);
				strcat(cFilePath, cTestDate);

				//Add the file extension
		#if DATA_CONVERSION
				strcat(cFilePath, FILE_EXT_CSV);
		#else
				strcat(cFilePath, FILE_EXT_BIN);
		#endif

				//Open the file for the test
				if (f_open(file, cFilePath, FA_CREATE_ALWAYS | FA_WRITE) == FR_OK)
				{
					return SUCCESS;
				}
			}
		}
	}

	return ERROR;
}

#if DATA_CONVERSION

/*
Stores the given data frames converted
 - Input:
 	 Data frames
 - Output:
 	 File handler
*/
void saveData(dataFrame_t *frames, FIL *file)
{
	//Data storage variables
	char dataLine[DATA_LINE_LENGTH];
	uint16_t iLineSize;
	UINT iByteCount;

	//Write all the retrieved frames
	for (int i = 0; i < SAMPLES_FOR_SD; i++)
	{
		//Reset the line size
		iLineSize = 0;

		//Set the frame identifier
		itoa(frames[i].iFrameID, dataLine, DECIMAL_CONV);
		iLineSize += strlen(dataLine);

		//Write the value separator
		memcpy(dataLine + iLineSize, FILE_VALUE_END, DATA_SINGLE_CHAR);
		iLineSize++;

		//Iterate through all the channels
		for (int j = 0; j < CHANNEL_NUM; j++)
		{
			//Check if the value is missing
			if (frames[i].iData[j] == MISSING_SMPL_VALUE)
			{
				//Write the missing value for the channel
				memcpy(dataLine + iLineSize, NAN_VALUE, NAN_LENGTH);
				iLineSize += NAN_LENGTH;

				//Write the value separator
				memcpy(dataLine + iLineSize, FILE_VALUE_END, DATA_SINGLE_CHAR);
				iLineSize++;
			}
			else
			{
				//Convert the data to decimal value
				float32_t fDataValue = ((frames[i].iData[j] / CODING_CONST) - 1) * fGainRelation[j];

				//Store the value as an array of characters
				snprintf(dataLine + iLineSize, DATA_LINE_LENGTH - iLineSize, EXP_CONVERSION, fDataValue);
				iLineSize += EXP_ARRAY_LENGTH;

				//Write the value separator
				memcpy(dataLine + iLineSize, FILE_VALUE_END, DATA_SINGLE_CHAR);
				iLineSize++;
			}
		}

		//Set the line separator
		memcpy(dataLine + iLineSize, FILE_LINE_END, DATA_SINGLE_CHAR);
		iLineSize++;

		//Write the processed line
		f_write(file, dataLine, iLineSize, &iByteCount);
	}
}

#else

/*
Stores the given data frames in raw format
 - Input:
 	 Data frames
 - Output:
 	 File handler
*/
void saveDataRaw(dataFrame_t *frames, FIL *file)
{
	//Bytes written variable
	UINT iByteCount;

	//Write all the retrieved frames
	for (int i = 0; i < SAMPLES_FOR_SD; i++)
	{
		//Copy the frame into the data array
		memcpy(dataSD + i * RAW_LINE_LENGTH, &frames[i], sizeof(dataFrame_t));

		//Copy the frame end identifier into the data array
		memcpy(dataSD + i * RAW_LINE_LENGTH + sizeof(dataFrame_t), FILE_FRAME_ID, strlen(FILE_FRAME_ID));
	}

	//Write all the data
	f_write(file, dataSD, SAMPLES_FOR_SD * RAW_LINE_LENGTH, &iByteCount);
}

#endif

#endif
#endif
