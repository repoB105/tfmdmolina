/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgment:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * emgComms.c
 *
 *  Created on: 18 Feb. 2021
 *	Author: David Molina Toro <dmolina@b105.upm.es>
 */

/*
 * @file emgComms.c
 * @communication system for EMG control
 */

//Communication header
#include "emgComms.h"

#if EMG_CONTROL_APP

//UDP in and out queue length definition
#define UDP_QUEUE_LENGTH 4

//Data to send via UDP queue definition
osPoolId poolSendUdp;
osMessageQId queueSendUdp;

//UDP message sending queue definition
osPoolDef(poolSendUdp, UDP_QUEUE_LENGTH, udpMessage_t);
osMessageQDef(queueSendUdp, UDP_QUEUE_LENGTH, uint32_t);

//Data received via UDP queue definition
osPoolId poolRecvUdp;
osMessageQId queueRecvUdp;

//UDP message receiving queue definition
osPoolDef(poolRecvUdp, UDP_QUEUE_LENGTH, udpMessage_t);
osMessageQDef(queueRecvUdp, UDP_QUEUE_LENGTH, uint32_t);

//UDP address variable definitions
netAddr_t netAddrMaster;
netAddr_t netAddrSlave;

//UDP communication thread identifiers
osThreadId udpInitCommsProcId;
osThreadId udpReadCommsProcId;
osThreadId udpReceptionProcId;
osThreadId udpWriteCommsProcId;

//UDP communications function declarations
void udpInitCommsFunc(void const *arg);
void udpReadCommsFunc(void const *arg);
void udpReceptionFunc(void const *arg);
void udpWriteCommsFunc(void const *arg);

//Checksum definitions
#define CHECKSUM_SIZE 	2
#define BITS_IN_BYTE 	8
#define HALF_LENGTH 	2

//Checksum processing function declaration
uint16_t processChecksum(const udpMessage_t *udpMessage);

/*
UDP communications initialization function
 - Input: N/A
 - Output: N/A
*/
uint16_t initComms()
{
	//Initialize the sending UDP queuing system
	poolSendUdp 	= osPoolCreate(osPool(poolSendUdp));				//This queue receives messages
	queueSendUdp 	= osMessageCreate(osMessageQ(queueSendUdp), NULL); 	//from the UDP reading thread

	//Initialize the receiving UDP queuing system
	poolRecvUdp 	= osPoolCreate(osPool(poolRecvUdp));				//This queue receives messages
	queueRecvUdp 	= osMessageCreate(osMessageQ(queueRecvUdp), NULL); 	//from the data and control threads

	//Create the UDP node addresses
	netAddrMaster 	= ytNetNewAddr(MASTER_UDP_ADDRESS, UDP_ADDRESS_FORMAT);
	netAddrSlave 	= ytNetNewAddr(SLAVE_UDP_ADDRESS, UDP_ADDRESS_FORMAT);

#ifdef MASTER_NODE
	//Add the master node to the system
	ytNetAddNodeAddr(netAddrMaster);
#endif
#ifdef SLAVE_NODE
	//Add the slave node to the system
	ytNetAddNodeAddr(netAddrSlave);
#endif

	//Check if the UDP port is open
	if(ytUdpOpen(UDP_COMMS_PORT) != RET_OK)
	{
		//Set the red led on
		ytLedOn(LED_RED_1);

		return ERROR;
	}

#ifdef MASTER_NODE
	//Initialize the UDP reading thread
	ytStartThread("udp_read_comms_proc", udpReadCommsFunc, osPriorityNormal, 256, &udpReadCommsProcId, NULL);

	//Initialize the UDP writing thread
	ytStartThread("udp_write_comms_proc", udpWriteCommsFunc, osPriorityNormal, 256, &udpWriteCommsProcId, NULL);
#endif
#ifdef SLAVE_NODE
	//Initialize the UDP reading thread
	ytStartThread("udp_read_comms_proc", udpReadCommsFunc, osPriorityNormal, 256, &udpReadCommsProcId, NULL);

	//Initialize the UDP writing thread
	ytStartThread("udp_write_comms_proc", udpWriteCommsFunc, osPriorityNormal, 256, &udpWriteCommsProcId, NULL);

	//Initialize the message reception thread
	ytStartThread("udp_reception_proc", udpReceptionFunc, osPriorityNormal, 256, &udpReceptionProcId, NULL);
#endif

	//Set the blue led on
	ytLedOn(LED_BLUE_1);

	return SUCCESS;
}

/*
UDP communications reading thread
 - Input:
 	 Input arguments
 - Output: N/A
*/
void udpReadCommsFunc(void const *arg)
{
	//UDP data buffer for incoming messages
	uint8_t dataRecvUdp[PACKET_SIZE];

	//Main UDP read loop
	while(1)
	{
#ifdef MASTER_NODE
		//Check if a message has been received via UDP
		uint16_t iBytesRecv = ytUdpRcv(netAddrMaster, UDP_COMMS_PORT, dataRecvUdp, PACKET_SIZE);
#endif
#ifdef SLAVE_NODE
		//Wait for the data to be available
		uint32_t iBytesRecv = ytUdpRcv(netAddrSlave, UDP_COMMS_PORT, dataRecvUdp, PACKET_SIZE);
#endif
		if (iBytesRecv > 0)
		{
			//Create a new UDP message
			udpMessage_t *pRecvUdp 	= osPoolAlloc(poolRecvUdp);
			if (pRecvUdp != NULL)
			{
				//Set the received buffer size
				pRecvUdp->iBufferSize = iBytesRecv;

				//Write the data in the message buffer
				memcpy(pRecvUdp->dataBuffer, dataRecvUdp, iBytesRecv);

				//Send the given message to the queue
				osMessagePut(queueRecvUdp, (uint32_t)pRecvUdp, NO_WAIT);
			}
		}
	}
}

/*
UDP communications writing thread
 - Input:
 	 Input arguments
 - Output: N/A
*/
void udpWriteCommsFunc(void const *arg)
{
	//Output queue control variable
	osEvent eventSendUdp;

	//Main UDP write loop
	while(1)
	{
		//Check if there is an UDP message in the queue
		eventSendUdp = osMessageGet(queueSendUdp, osWaitForever);
		if (eventSendUdp.status == osEventMessage)
		{
			//Set the received message
			udpMessage_t *pSendUdp;
			pSendUdp = eventSendUdp.value.p;

#ifdef MASTER_NODE
			//Try sending the message via UDP to the slave node
			ytUdpSend(netAddrSlave, UDP_COMMS_PORT, pSendUdp->dataBuffer, pSendUdp->iBufferSize);
#endif
#ifdef SLAVE_NODE
			//Try sending the message via UDP to the master node
			ytUdpSend(netAddrMaster, UDP_COMMS_PORT, pSendUdp->dataBuffer, pSendUdp->iBufferSize);
#endif

			//Free the UDP queue pool
			osPoolFree(poolSendUdp, pSendUdp);
		}
	}
}

/*
Adds the given message to the pending queue
 - Input:
 	 Message to send
 - Output: N/A
*/
void addMessageUdp(uint8_t *data, uint16_t iSize)
{
	//Create a new UDP message
	udpMessage_t *pPendingUdp 	= osPoolAlloc(poolSendUdp);
	if (pPendingUdp != NULL)
	{
		//Set the message buffer size
		pPendingUdp->iBufferSize = iSize;

		//Write the data in the message buffer
		memcpy(pPendingUdp->dataBuffer, data, iSize);

		//Send the given message to the queue
		osMessagePut(queueSendUdp, (uint32_t)pPendingUdp, NO_WAIT);
	}
}

/*
Adds each data byte of the message to a checksum of 16 bits
 - Input:
 	 Message
 - Output:
 	 Checksum
*/
uint16_t processChecksum(const udpMessage_t *udpMessage)
{
	//Set the data size of the message
	uint16_t iDataSize = udpMessage->iBufferSize - HEADER_MESSAGE_SIZE - CHECKSUM_SIZE;

	//Add the data bytes to the checksum with carry
	uint16_t iChecksum = 0;
	for (uint16_t i = 0; i < iDataSize / HALF_LENGTH; i++)
	{
		//Get the current byte
		uint8_t iCurrentByte = udpMessage->dataBuffer[HEADER_MESSAGE_SIZE + HALF_LENGTH * i];

		//Get the next byte
		uint8_t iNextByte = udpMessage->dataBuffer[HEADER_MESSAGE_SIZE + HALF_LENGTH * i + 1];

		//Add both bytes
		iChecksum ^= iCurrentByte + (iNextByte << BITS_IN_BYTE);
	}

	//Check if a data byte is left and add it
	if(iDataSize % HALF_LENGTH)
	{
		iChecksum ^= udpMessage->dataBuffer[iDataSize + HEADER_MESSAGE_SIZE - 1];
	}

	return ~iChecksum;
}

#ifdef MASTER_NODE

//Serial communication thread identifiers
osThreadId serialReadCommsProcId;
osThreadId serialWriteCommsProcId;

//Serial communications function declarations
void serialReadCommsFunc(void const *arg);
void serialWriteCommsFunc(void const *arg);

//UDP communications system initialization thread
YtAutoInitThread(udp_init_comms_proc, udpInitCommsFunc, osPriorityHigh, 256, &udpInitCommsProcId, NULL);

//Serial communication system initialization threads
YtAutoInitThread(serial_read_comms_proc, serialReadCommsFunc, osPriorityBelowNormal, 256, &serialReadCommsProcId, NULL);
YtAutoInitThread(serial_write_comms_proc, serialWriteCommsFunc, osPriorityBelowNormal, 256, &serialWriteCommsProcId, NULL);

/*
UDP communications initialization thread
 - Input:
 	 Input arguments
 - Output: N/A
*/
void udpInitCommsFunc(void const *arg)
{
	//Call the default UDP initialization function
	initComms(NULL);
}

/*
Serial read communications thread
 - Input:
 	 Input arguments
 - Output: N/A
*/
void serialReadCommsFunc(void const *arg)
{
	//Serial read buffer variables
	int16_t iSerialPos = 0;
	uint8_t serialBuffer[PACKET_SIZE];

	//Main serial read loop
	while (1)
	{
		//Get a byte from the serial buffer
		if (ytStdioRead(serialBuffer + iSerialPos, 1) == RET_OK)
		{
			//Check if the header has been read
			if (iSerialPos == HEADER_MESSAGE_SIZE - 1)
			{
				//Copy the header of the message
				char cHeader[HEADER_MESSAGE_SIZE + 1];
				memcpy(cHeader, serialBuffer, HEADER_MESSAGE_SIZE);
				cHeader[HEADER_MESSAGE_SIZE] = '\0';

				//Check the header of the message
				int16_t iMsgSize = 0;
				if (!strcmp(cHeader, CTRL_HEADER))
				{
					iMsgSize = CTRL_MESSAGE_SIZE;
					iSerialPos++;
				}
				else if (!strcmp(cHeader, CONF_HEADER))
				{
					iMsgSize = CONF_MESSAGE_SIZE;
					iSerialPos++;
				}
				else if (!strcmp(cHeader, CAPT_HEADER))
				{
					iMsgSize = CAPT_MESSAGE_SIZE;
					iSerialPos++;
				}
				else
				{
					iSerialPos = 0;
					continue;
				}

				//Read until the message is complete
				while (iSerialPos < iMsgSize)
				{
					if (ytStdioRead(serialBuffer + iSerialPos, 1) == RET_OK)
					{
						iSerialPos++;
					}
				}

				//Add the message to the UDP send queue
				addMessageUdp(serialBuffer, iMsgSize);

				//Reset the serial buffer position
				iSerialPos = 0;
			}
			else
			{
				//Increase the serial buffer position
				iSerialPos++;
			}
		}
	}
}

/*
Serial write communications thread
 - Input:
 	 Input arguments
 - Output: N/A
*/
void serialWriteCommsFunc(void const *arg)
{
	//Main serial write loop
	while (1)
	{
		//Check if there is an UDP message in the queue
		osEvent eventRecvUdp = osMessageGet(queueRecvUdp, osWaitForever);
		if (eventRecvUdp.status == osEventMessage)
		{
			//Set the received message
			udpMessage_t *pRecvUdp;
			pRecvUdp = eventRecvUdp.value.p;

			//Send the UDP data via serial
			ytStdoutSend(pRecvUdp->dataBuffer, pRecvUdp->iBufferSize);

			//Free the UDP queue pool
			osPoolFree(poolRecvUdp, pRecvUdp);
		}
	}
}

#endif
#ifdef SLAVE_NODE

//Checksum comparison function declaration
uint16_t compareChecksum(const udpMessage_t *udpMessage);

/*
UDP reception thread
 - Input:
 	 Input arguments
 - Output: N/A
*/
void udpReceptionFunc(void const *arg)
{
	//Main UDP reception loop
	while (1)
	{
		//Check if there is an UDP message in the queue
		osEvent eventRecvUdp = osMessageGet(queueRecvUdp, osWaitForever);
		if (eventRecvUdp.status == osEventMessage)
		{
			//Set the received message
			udpMessage_t *pRecvUdp;
			pRecvUdp = eventRecvUdp.value.p;

			//Create a new control message
			controlMessage_t *pPendingControl = osPoolAlloc(poolControl);
			if (pPendingControl != NULL)
			{
				//Set the control message
				pPendingControl->iCommand 	= FSM_DEFAULT_STATUS;
				pPendingControl->iError 	= FSM_DEFAULT_ERROR;

				//Check if the message contains a header
				if (pRecvUdp->iBufferSize >= HEADER_MESSAGE_SIZE)
				{
					//Copy the header of the message
					char cHeader[HEADER_MESSAGE_SIZE + 1];
					memcpy(cHeader, pRecvUdp->dataBuffer, HEADER_MESSAGE_SIZE);
					cHeader[HEADER_MESSAGE_SIZE] = '\0';

					//Check the header of the message
					if (!strcmp(cHeader, CTRL_HEADER))
					{
						//Check the size of the control message
						if (pRecvUdp->iBufferSize >= CTRL_MESSAGE_SIZE && compareChecksum(pRecvUdp))
						{
							//Copy the contents of the received message
							memcpy(pPendingControl, pRecvUdp->dataBuffer + HEADER_MESSAGE_SIZE, sizeof(controlMessage_t));
						}
					}
					else if (!strcmp(cHeader, CONF_HEADER))
					{
						//Check the size of the control message
						if (pRecvUdp->iBufferSize >= CONF_MESSAGE_SIZE && compareChecksum(pRecvUdp))
						{
							//Create a new configuration message
							configMessage_t *pPendingConfig = osPoolAlloc(poolConfig);
							if (pPendingConfig != NULL)
							{
								//Set the control message for the FSM
								pPendingControl->iCommand 	= CONFIG;
								pPendingControl->iError 	= FSM_DEFAULT_STATUS;

								//Set the received command
								memcpy(pPendingConfig, pRecvUdp->dataBuffer + HEADER_MESSAGE_SIZE, sizeof(configMessage_t));

								//Send the given message to the queue
								osMessagePut(queueConfig, (uint32_t)pPendingConfig, NO_WAIT);
							}
						}
					}
					else if (!strcmp(cHeader, CAPT_HEADER))
					{
						//Check the size of the capture message
						if (pRecvUdp->iBufferSize >= CAPT_MESSAGE_SIZE && compareChecksum(pRecvUdp))
						{
							//Create a new capture message
							captureMessage_t *pPendingCapture = osPoolAlloc(poolCapture);
							if (pPendingCapture != NULL)
							{
								//Set the control message for the FSM
								pPendingControl->iCommand 	= START;
								pPendingControl->iError 	= FSM_DEFAULT_STATUS;

								//Set the received command
								memcpy(pPendingCapture, pRecvUdp->dataBuffer + HEADER_MESSAGE_SIZE, sizeof(captureMessage_t));

								//Send the given message to the queue
								osMessagePut(queueCapture, (uint32_t)pPendingCapture, NO_WAIT);
							}
						}
					}
				}

				//Send the control message to the queue
				osMessagePut(queueControl, (uint32_t)pPendingControl, NO_WAIT);
			}

			//Free the UDP queue pool
			osPoolFree(poolRecvUdp, pRecvUdp);
		}
	}
}

/*
Adds the given status message to the pending queue
 - Input:
 	 Message to send
 - Output: N/A
*/
void addStatusMessageUdp(statusMessage_t statusMessage)
{
	//Create a new UDP message
	udpMessage_t *pPendingUdp = osPoolAlloc(poolSendUdp);
	if (pPendingUdp != NULL)
	{
		//Set the message buffer size
		pPendingUdp->iBufferSize = STAT_MESSAGE_SIZE;

		//Write the data in the message buffer
		memcpy(pPendingUdp->dataBuffer, STAT_HEADER, HEADER_MESSAGE_SIZE);
		memcpy(pPendingUdp->dataBuffer + HEADER_MESSAGE_SIZE, &statusMessage, sizeof(statusMessage_t));

		//Set the message checksum
		uint16_t iChecksum = processChecksum(pPendingUdp);
		memcpy(pPendingUdp->dataBuffer + STAT_MESSAGE_SIZE - CHECKSUM_SIZE, &iChecksum, CHECKSUM_SIZE);

		//Send the given message to the queue
		osMessagePut(queueSendUdp, (uint32_t)pPendingUdp, NO_WAIT);
	}
}

/*
Adds the given data message to the pending queue
 - Input:
 	 Message to send
 - Output: N/A
*/
void addDataMessageUdp(dataFrame_t *frame)
{
	//Create a new UDP message
	udpMessage_t *pPendingUdp = osPoolAlloc(poolSendUdp);
	if (pPendingUdp != NULL)
	{
		//Set the message buffer size
		pPendingUdp->iBufferSize = DATA_MESSAGE_SIZE;

		//Write the data in the message buffer
		memcpy(pPendingUdp->dataBuffer, DATA_HEADER, HEADER_MESSAGE_SIZE);
		memcpy(pPendingUdp->dataBuffer + HEADER_MESSAGE_SIZE, frame, sizeof(dataFrame_t));

		//Set the message checksum
		uint16_t iChecksum = processChecksum(pPendingUdp);
		memcpy(pPendingUdp->dataBuffer + DATA_MESSAGE_SIZE - CHECKSUM_SIZE, &iChecksum, CHECKSUM_SIZE);

		//Send the given message to the queue
		osMessagePut(queueSendUdp, (uint32_t)pPendingUdp, NO_WAIT);
	}
}

/*
Compares the given checksum and data
 - Input:
 	 Message
 - Output:
 	 Result
*/
uint16_t compareChecksum(const udpMessage_t *udpMessage)
{
	//Set the data size of the message
	int16_t iDataSize = udpMessage->iBufferSize - HEADER_MESSAGE_SIZE - CHECKSUM_SIZE;

	//Get the checksum of the received message
	uint16_t iRecvChecksum;
	memcpy(&iRecvChecksum, udpMessage->dataBuffer + HEADER_MESSAGE_SIZE + iDataSize, CHECKSUM_SIZE);

	//Process the message checksum
	uint16_t iProcChecksum = processChecksum(udpMessage);

	return iProcChecksum == iRecvChecksum;
}

#endif
#endif
