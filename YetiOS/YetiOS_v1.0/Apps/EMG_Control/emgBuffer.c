/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgment:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * emgBuffer.c
 *
 *  Created on: 21 Mar. 2021
 *	Author: David Molina Toro <dmolina@b105.upm.es>
 */

/*
 * @file emgBuffer.c
 * @data buffer system for EMG control
 */

//Data buffer header
#include "emgBuffer.h"

//Standard library include
#include <stdlib.h>

#if EMG_CONTROL_APP
#ifdef SLAVE_NODE

//Circular buffer semaphore control
osMutexId buffMutex;

/*
Buffer initialization function
 - Input: N/A
 - Output: N/A
*/
void buffInit()
{
	//Initialize the buffer flag semaphore
	buffMutex = ytMutexCreate();

	//Reset the buffer variables
	buffReset();
}

/*
Reset the buffer variables function
 - Input: N/A
 - Output: N/A
*/
void buffReset()
{
	//Wait for the buffer semaphore
	osMutexWait(buffMutex, osWaitForever);

	//Reset the buffer variables
	dataBuffer.iHead 	= 0;
	dataBuffer.iTail 	= 0;
	dataBuffer.iFull 	= 0;
	dataBuffer.iEmpty 	= 1;

	//Release the buffer semaphore
	osMutexRelease(buffMutex);
}

/*
Increase the head position function
 - Input: N/A
 - Output: N/A
*/
void buffIncHead()
{
	//Set the buffer to not empty
	dataBuffer.iEmpty = 0;

	//Check if the buffer is full
	if(dataBuffer.iFull)
   	{
		//Increase the tail and check if it exceeds the maximum size
		if (++dataBuffer.iTail == BUFFER_LENGTH)
		{
			dataBuffer.iTail = 0;
		}
	}

	//Increase the head and check if it exceeds the maximum size
	if (++dataBuffer.iHead == BUFFER_LENGTH)
	{
		dataBuffer.iHead = 0;
	}

	//Set if the buffer is full
	dataBuffer.iFull = dataBuffer.iHead == dataBuffer.iTail;
}

/*
Increase the tail position function
 - Input: N/A
 - Output: N/A
*/
void buffIncTail()
{
	//Set the buffer to not full
	dataBuffer.iFull = 0;

	//Increase the tail and check if it exceeds the maximum size
	if (++dataBuffer.iTail == BUFFER_LENGTH)
	{
		dataBuffer.iTail = 0;
	}

	//Set if the buffer is empty
	dataBuffer.iEmpty = (dataBuffer.iHead == dataBuffer.iTail);
}

/*
Returns the elements in the buffer
 - Input: N/A
 - Output:
 	 Elements in buffer
*/
uint16_t buffSize()
{
	//Initialize the size variable
	uint16_t size = BUFFER_LENGTH;

	//Check if the buffer is full
	if(!dataBuffer.iFull)
	{
		//Compare the tail and head positions
		if(dataBuffer.iHead >= dataBuffer.iTail)
		{
			//Set the size to the difference between the tail and head positions
			size = (dataBuffer.iHead - dataBuffer.iTail);
		}
		else
		{
			//Set the size to the maximum plus the difference between the positions
			size = BUFFER_LENGTH + dataBuffer.iHead - dataBuffer.iTail;
		}
	}

	return size;
}

/*
Adds the data value to the buffer, overwrites if it is full
 - Input:
 	 Data frame
 - Output:
 	 Status
*/
retval_t buffPut(dataFrame_t frame)
{
	//Set the return value to error
	retval_t iError = ERROR;

	//Wait for the buffer semaphore
	osMutexWait(buffMutex, osWaitForever);

	//Set the return status if the buffer is not full
	if (!dataBuffer.iFull)
	{
		iError = SUCCESS;
	}

	//Set the current data frame
	dataBuffer.dataFrames[dataBuffer.iHead] = frame;

	//Increase the buffer head
	buffIncHead();

	//Release the buffer semaphore
	osMutexRelease(buffMutex);

    return iError;
}

/*
Returns a data frame from the buffer
 - Input: N/A
 - Output:
  	 Status
 	 Data requested
*/
retval_t buffPop(dataFrame_t *frame)
{
	//Set the return value to error
	retval_t iError = ERROR;

	//Wait for the buffer semaphore
	osMutexWait(buffMutex, osWaitForever);

	//Check if the buffer is empty
	if(!dataBuffer.iEmpty)
	{
		//Set the data output frame
		*frame = dataBuffer.dataFrames[dataBuffer.iTail];

		//Increase the buffer tail
		buffIncTail();

		//Set the return value
		iError = SUCCESS;
	}

	//Release the buffer semaphore
	osMutexRelease(buffMutex);

	return iError;
}

/*
Returns a data frame range from the buffer
 - Input: N/A
 - Output:
  	 Status
 	 Data requested
 	 Range
*/
retval_t buffPopRange(dataFrame_t *frames, uint16_t iRange)
{
	//Set the return value to error
	retval_t iError = ERROR;

	//Wait for the buffer semaphore
	osMutexWait(buffMutex, osWaitForever);

	//Check if the buffer is empty
	if(buffSize() >= iRange)
	{
		//Iterate through all the stored frames
		for (uint16_t i = 0; i < iRange; i++)
		{
			//Set the data output frame
			frames[i] = dataBuffer.dataFrames[dataBuffer.iTail];

			//Increase the buffer tail
			buffIncTail();
		}

		//Set the return value
		iError = SUCCESS;
	}

	//Release the buffer semaphore
	osMutexRelease(buffMutex);

	return iError;
}

#endif
#endif
