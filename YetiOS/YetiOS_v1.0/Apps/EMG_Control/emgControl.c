/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgment:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * emgControl.c
 *
 *  Created on: 9 Feb. 2021
 *	Author: David Molina Toro <dmolina@b105.upm.es>
 */

/*
 * @file emgControl.c
 * @control interface entry point for the EMG system
 */

//EMG control header
#include "emgControl.h"

#if EMG_CONTROL_APP
#ifdef SLAVE_NODE

//FSM state definitions
typedef enum
{
	DEFAULT		= 0,	//Default state for messaging
	IDLE 		= 1, 	//Resets all the systems
	PREPARED 	= 2, 	//Sets the initial configuration and allows incoming ones
	CONFIGURING = 3,	//Waits for the ADC configuration to be applied
	CAPTURING 	= 4 	//Starts broadcasting/storing the activation data
} fsmState_t;

//Control queue length definition
#define CONTROL_QUEUE_LENGTH 4

//Status message queue definition
extern osPoolId poolControl;
extern osMessageQId queueControl;

//Control queue definition
osPoolDef(poolControl, CONTROL_QUEUE_LENGTH, controlMessage_t);
osMessageQDef(queueControl, CONTROL_QUEUE_LENGTH, uint32_t);

//Configuration semaphore definition
extern osSemaphoreId configSemaphore;

//FSM current state indicator
fsmState_t fsmCurrent;

//Last transition control variables
statusMessage_t statusCurrent;

//Processes instances
osThreadId controlFSMProcId;

//FSM control function declaration
void controlFSMFunc(void const *arg);

//Change state function declaration
void changeState(uint16_t iStatus, fsmState_t fsmNext);

//Main control system initialization thread
YtAutoInitThread(control_fsm_proc, controlFSMFunc, osPriorityHigh, 512, &controlFSMProcId, NULL);

/*
Reset function
 - Input: N/A
 - Output: N/A
*/
uint16_t reset()
{
	//Set the red led on
	ytLedOn(LED_RED_1);

	//Wait for reset
	osDelay(WAIT_SYSTEMS);

	//Reset the system completely
	NVIC_SystemReset();

	return 0;
}

/*
Initialization function
 - Input: N/A
 - Output: N/A
*/
uint16_t init()
{
	//Error flag variable
	uint16_t iError = SUCCESS;

	//Initialize the configuration, data and communication systems
	iError += initConfigADC();
	iError += initAcq();
	iError += initComms();

	return iError;
}

/*
ADC configuration function
 - Input: N/A
 - Output: N/A
*/
uint16_t config()
{
	//Set the stored configurations
	osSemaphoreRelease(configSemaphore);

	return SUCCESS;
}

/*
Start capturing function
 - Input:
 	 FSM object reference
 	 Number of arguments
 	 Arguments
 - Output: N/A
*/
uint16_t startCapture()
{
	//Start the data capture
	startAcq();

	return SUCCESS;
}

/*
Stop function
 - Input: N/A
 - Output: N/A
*/
uint16_t stopCapture()
{
	//Stop the data capture
	stopAcq();

	return SUCCESS;
}

/*
FSM control thread
 - Input:
 	 Input arguments
 - Output: N/A
*/
void controlFSMFunc(void const *arg)
{
	//Initialize the FSM status
	fsmCurrent = IDLE;

	//Set the status storage
	statusCurrent.iCurrentState = IDLE;
	statusCurrent.iLastState 	= IDLE;
	statusCurrent.iError 		= NO_ERROR;

	//Initialize the control queuing system
	osEvent eventControl;
	poolControl 	= osPoolCreate(osPool(poolControl));				//This queue receives messages
	queueControl 	= osMessageCreate(osMessageQ(queueControl), NULL);	//from the UDP reception thread

	//Add an initial transition to the control queue
	controlMessage_t *pInitialControl = osPoolAlloc(poolControl);
	pInitialControl->iCommand 	= INIT;
	pInitialControl->iError 	= NO_ERROR;
	osMessagePut(queueControl, (uint32_t)pInitialControl, NO_WAIT);

	//Run the finite state machine
	while (1)
	{
		//Check if there is an status message in the queue
		eventControl = osMessageGet(queueControl, osWaitForever);
		if (eventControl.status == osEventMessage)
		{
			//Set the received command
			controlMessage_t *pReceivedControl;
			pReceivedControl = eventControl.value.p;

			//Check the current state value
			switch (fsmCurrent)
			{
				case IDLE:
					switch (pReceivedControl->iCommand)
					{
						case STATUS:
							addStatusMessageUdp(statusCurrent);
							break;
						case INIT:
							changeState(init(), PREPARED);
							break;
						case RESTART:
							changeState(reset(), IDLE);
							break;
						default:
							changeState(ARGUMENT_ERROR, IDLE);
							break;
					}
					break;
				case PREPARED:
					switch (pReceivedControl->iCommand)
					{
						case STATUS:
							addStatusMessageUdp(statusCurrent);
							break;
						case CONFIG:
							changeState(config(), CONFIGURING);
							break;
						case START:
							changeState(startCapture(), CAPTURING);
							break;
						case RESTART:
							changeState(reset(), IDLE);
							break;
						default:
							changeState(ARGUMENT_ERROR, PREPARED);
							break;
					}
					break;
				case CONFIGURING:
					switch (pReceivedControl->iCommand)
					{
						case STATUS:
							addStatusMessageUdp(statusCurrent);
							break;
						case ADCSET:
							changeState(pReceivedControl->iError, PREPARED);
							break;
						case RESTART:
							changeState(reset(), IDLE);
							break;
						default:
							changeState(ARGUMENT_ERROR, PREPARED);
							break;
					}
					break;
				case CAPTURING:
					switch (pReceivedControl->iCommand)
					{
						case STATUS:
							addStatusMessageUdp(statusCurrent);
							break;
						case STOERR:
							changeState(pReceivedControl->iError, PREPARED);
							break;
						case STOP:
							changeState(stopCapture(), PREPARED);
							break;
						case RESTART:
							changeState(reset(), IDLE);
							break;
						default:
							changeState(ARGUMENT_ERROR, CAPTURING);
							break;
					}
					break;
				default:
					changeState(reset(), IDLE);
					break;
			}

			//Free the status queue pool
			osPoolFree(poolControl, pReceivedControl);
		}
	}
}

/*
Change state function
 - Input:
 	 Function status
 	 Next state to set
 	 Next state name
 - Output: N/A
*/
void changeState(uint16_t iStatus, fsmState_t fsmNext)
{
	//Save the last transition status
	statusCurrent.iCurrentState = fsmNext;
	statusCurrent.iLastState 	= fsmCurrent;
	statusCurrent.iError 		= iStatus;

    //Set the current state
	fsmCurrent = fsmNext;
}

#endif
#endif
