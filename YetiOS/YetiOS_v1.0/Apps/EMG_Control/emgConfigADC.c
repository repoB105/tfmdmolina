/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgment:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * emgConfigADC.c
 *
 *  Created on: 18 Feb. 2021
 *	Author: David Molina Toro <dmolina@b105.upm.es>
 */

/*
 * @file emgConfigADC.c
 * @configuration system for EMG control
 */

//Configuration header
#include "emgConfigADC.h"

#if EMG_CONTROL_APP
#ifdef SLAVE_NODE

//Configuration error bit definitions
#define CHANNEL_CONFIG_ERROR 	0x02
#define GAIN_CONFIG_ERROR 		0x04
#define FILTER_CONFIG_ERROR 	0x08

//Filter bits definitions
#define MODE_BITS_POS 		21
#define MODE_BITS_NUM 		0x3
#define CYCLE_BITS_POS 		0x10
#define CYCLE_BITS_NUM 		0x1
#define DIVIDER_BITS_POS 	0x0
#define DIVIDER_BITS_NUM 	0xA
#define CHANNEL_BITS_POS 	0xF
#define CHANNEL_BITS_NUM 	0x1

//Filter type definitions
#define SYNC_4_ZERO_FILTER 	0x1
#define SYNC_3_ZERO_FILTER 	0x3
#define SYNC_4_FAST_FILTER 	0x4
#define SYNC_3_FAST_FILTER 	0x5

//ADC clock definitions (full power mode)
#define ADC_CLOCK_RATE 		614400
#define ADC_CLOCK_DIV		32
#define SYNC_4_CLOCK_DIV 	4
#define SYNC_3_CLOCK_DIV 	3
#define FILTER_AVERAGE 		16


//ADC SPI bus speed
#define AD7124_SPI_SPEED 6000000

//Configuration queue length definition
#define CONFIG_QUEUE_LENGTH 4

//Status message queue definition
extern osPoolId poolControl;
extern osMessageQId queueControl;

//Configuration message queue definition
extern osPoolId poolConfig;
extern osMessageQId queueConfig;

//Configuration queue definition
osPoolDef(poolConfig, CONFIG_QUEUE_LENGTH, configMessage_t);
osMessageQDef(queueConfig, CONFIG_QUEUE_LENGTH, uint32_t);

#if DATA_CONVERSION

//Gain look up table definition
const uint16_t iGainLut[] = { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 };

//Conversion relation semaphore control
osMutexId conversionMutex;

//Gain relation constant array
float32_t fGainRelation[CHANNEL_NUM];

#endif

//Configuration semaphore definition
osSemaphoreId configSemaphore;

//Sample rate semaphore control
osMutexId sampleRateMutex;

//Active channels semaphore control
osMutexId activeChannelsMutex;

//Sample rate variable declaration
float32_t fSampleRate = 0;

//Active channels variable declaration
uint16_t iActiveChannels = CHANNEL_NUM;

//ADC object instance
ad7124Dev_t *ad7124Dev;

//Processes instances
osThreadId configADCProcId;

//Main configuration function
void configADCFunc(void const *arg);

//Register configuration function declaration
int32_t writeRegisterADC(uint8_t iReg, int32_t iValue);

/*
Configuration initialization function
 - Input: N/A
 - Output:
 	 Status
*/
uint16_t initConfigADC()
{
	//Initialize the configuration semaphore
	configSemaphore = ytSemaphoreCreate(1);
	osSemaphoreWait(configSemaphore, osWaitForever);

	//Initialize the configuration queuing system
	poolConfig 	= osPoolCreate(osPool(poolConfig));					//This queue receives messages
	queueConfig = osMessageCreate(osMessageQ(queueConfig), NULL);	//from the FSM control thread

	//Initialize the sample rate semaphore
	sampleRateMutex = ytMutexCreate();

	//Initialize the active channels semaphore
	activeChannelsMutex = ytMutexCreate();

	//Set the default sample rate
	setSampleRate(ad7124Regs[AD7124_Filter_0].value);

#if DATA_CONVERSION
	//Set a default gain configuration array
	uint16_t iDefaultGain[CHANNEL_NUM] =
	{
		ad7124Regs[AD7124_Config_0].value,
		ad7124Regs[AD7124_Config_1].value,
		ad7124Regs[AD7124_Config_2].value,
		ad7124Regs[AD7124_Config_3].value
	};

	//Initialize the conversion constant semaphore
	conversionMutex = ytMutexCreate();

	//Set the default gain relation
	setGainRelation(iDefaultGain);
#endif

	//Delay before the initial configuration
	osDelay(WAIT_SYSTEMS);

	//Setup the analog to digital converter
	uint16_t iError = SUCCESS;
	if(ad7124Setup(&ad7124Dev, ad7124Regs))
	{
		iError = ERROR;
	}

	//Initialize the configuration thread
	ytStartThread("config_adc_proc", configADCFunc, osPriorityBelowNormal, 256, &configADCProcId, NULL);

	return iError;
}

/*
Returns the ADC configuration pointer
 - Input: N/A
 - Output:
 	 ADC configuration pointer
*/
ad7124Dev_t* getConfigADC()
{
	return ad7124Dev;
}

/*
System configuration thread
 - Input:
 	 Input arguments
 - Output: N/A
*/
void configADCFunc(void const *arg)
{
	//Common configuration status variable
	uint16_t iConfigStatus;

	//Configuration received event
	osEvent eventConfig;

	//Run the configuration loop
	while (1)
	{
		//Wait for the configuration semaphore from the control FSM
		osSemaphoreWait(configSemaphore, osWaitForever);

		do
		{
			//Check if there is a configuration message in the queue
			eventConfig = osMessageGet(queueConfig, NO_WAIT);
			if (eventConfig.status == osEventMessage)
			{
				//Set the received configuration
				configMessage_t *pReceivedConfig;
				pReceivedConfig = eventConfig.value.p;

				//Reset the configuration status flag
				iConfigStatus = NO_ERROR;

				//Channel enabling variable and error flag
				uint16_t iChannelError = 0;

				//Set each channel status
				iChannelError += writeRegisterADC(AD7124_Channel_0, pReceivedConfig->iChannel[0]);
				iChannelError += writeRegisterADC(AD7124_Channel_1, pReceivedConfig->iChannel[1]);
				iChannelError += writeRegisterADC(AD7124_Channel_2, pReceivedConfig->iChannel[2]);
				iChannelError += writeRegisterADC(AD7124_Channel_3, pReceivedConfig->iChannel[3]);

				//Check for channel configuration errors
				if (iChannelError)
				{
					iConfigStatus |= CHANNEL_CONFIG_ERROR;
				}

				//Gain configuration error flag
				uint16_t iGainError = 0;

				//Set the ADC gain for each channel
				iGainError += writeRegisterADC(AD7124_Config_0, pReceivedConfig->iGain[0]);
				iGainError += writeRegisterADC(AD7124_Config_1, pReceivedConfig->iGain[1]);
				iGainError += writeRegisterADC(AD7124_Config_2, pReceivedConfig->iGain[2]);
				iGainError += writeRegisterADC(AD7124_Config_3, pReceivedConfig->iGain[3]);

				//Check for gain configuration errors
				if (iGainError)
				{
					iConfigStatus |= GAIN_CONFIG_ERROR;
				}

				//Filter configuration error flag
				uint16_t iFilterError = 0;

				//Set the common filter configuration for each channel
				iFilterError += writeRegisterADC(AD7124_Filter_0, pReceivedConfig->iFilter);
				iFilterError += writeRegisterADC(AD7124_Filter_1, pReceivedConfig->iFilter);
				iFilterError += writeRegisterADC(AD7124_Filter_2, pReceivedConfig->iFilter);
				iFilterError += writeRegisterADC(AD7124_Filter_3, pReceivedConfig->iFilter);

				//Check for filter configuration errors
				if (iFilterError)
				{
					iConfigStatus |= FILTER_CONFIG_ERROR;
				}

				//Set the configured sample rate
				setSampleRate(pReceivedConfig->iFilter);

				//Set the active channels
				setActiveChannels(pReceivedConfig->iChannel);

#if DATA_CONVERSION
				//Set the conversion constant array
				setGainRelation(pReceivedConfig->iGain);
#endif

				//Free the configuration queue pool
				osPoolFree(poolConfig, pReceivedConfig);
			}
		} while (eventConfig.status == osEventMessage);

		//Add the ADC set command to the control queue
		controlMessage_t *pPendingControl = osPoolAlloc(poolControl);
		if (pPendingControl != NULL)
		{
			//Set the control message
			pPendingControl->iCommand 	= ADCSET;
			pPendingControl->iError 	= iConfigStatus;
			osMessagePut(queueControl, (uint32_t)pPendingControl, NO_WAIT);
		}
	}
}

/*
Writes a register for the ADC configuration
 - Input:
 	 Register number
 	 Register value to be written
 - Output:
 	 Write status
*/
int32_t writeRegisterADC(uint8_t iReg, int32_t iValue)
{
	//Set the register structure
	struct ad7124StReg stReg;
	stReg.addr 	= ad7124Regs[iReg].addr;
	stReg.value = iValue;
	stReg.size 	= ad7124Regs[iReg].size;
	stReg.rw 	= ad7124Regs[iReg].rw;

	//Write the register into the ADC
	return ad7124WriteRegister(ad7124Dev, stReg);
}

/*
SPI configuration function
 - Input:
 	 Device pointer
 - Output:
 	 Status
*/
int32_t ad7124ConfigSpiDevice(ad7124Dev_t *ad7124Dev)
{
	ytIoctlCmd_t spiIoctlCmd;

	ytSpiPolarity_t spiPol 	= SPI_POL_HIGH;
	ytSpiEdge_t spiEdge 	= SPI_EDGE_SECOND;
	ytSpiMode_t spiMode 	= SPI_MASTER;
	uint32_t spiSpeed 		= AD7124_SPI_SPEED;
	uint32_t csPin 			= GPIO_PIN_A4;

	if ((ad7124Dev->spiFd = ytOpen(PLATFORM_SPI1_DEVICE_ID, 0)) == NULL)
	{
		return -1;
	}

	spiIoctlCmd = SPI_SET_POLARITY;
	if (ytIoctl(ad7124Dev->spiFd, spiIoctlCmd, &spiPol) != RET_OK)
	{
		return -1;
	}

	spiIoctlCmd = SPI_SET_EDGE;
	if (ytIoctl(ad7124Dev->spiFd, spiIoctlCmd, &spiEdge) != RET_OK)
	{
		return -1;
	}

	spiIoctlCmd = SPI_SET_MODE;
	if (ytIoctl(ad7124Dev->spiFd, spiIoctlCmd, &spiMode) != RET_OK)
	{
		return -1;
	}
	spiIoctlCmd = SPI_SET_SPEED;
	if (ytIoctl(ad7124Dev->spiFd, spiIoctlCmd, &spiSpeed) != RET_OK)
	{
		return -1;
	}

	spiIoctlCmd = SPI_SET_SW_CS_ALWAYS;
	if(ytIoctl(ad7124Dev->spiFd, spiIoctlCmd, &(csPin)) != RET_OK)
	{
		return -1;
	}

	return 0;
}

/*
Sets the configured sample rate
 - Input:
 	 Filter configuration
 - Output: N/A
*/
void setSampleRate(uint32_t iFilter)
{
	//Create a temporary variable
	float32_t fClockDivider = 1;

	//Get the filter configuration bits
	uint16_t iMode 			= ((1 << MODE_BITS_NUM) - 1) & (iFilter >> MODE_BITS_POS);
	uint16_t iSingleCycle 	= ((1 << CYCLE_BITS_NUM) - 1) & (iFilter >> CYCLE_BITS_POS);
	uint16_t iFreqDivider 	= ((1 << DIVIDER_BITS_NUM) - 1) & (iFilter >> DIVIDER_BITS_POS);

	//Add the single cycle bit to the mode
	iMode += iSingleCycle;

	//Check the type of filter selected
	switch (iMode)
	{
		case SYNC_4_ZERO_FILTER:
			fClockDivider = (float32_t)SYNC_4_CLOCK_DIV * ADC_CLOCK_DIV;
			break;
		case SYNC_3_ZERO_FILTER:
			fClockDivider = (float32_t)SYNC_3_CLOCK_DIV * ADC_CLOCK_DIV;
			break;
		case SYNC_4_FAST_FILTER:
			fClockDivider = (float32_t)(SYNC_4_CLOCK_DIV + FILTER_AVERAGE - 1) * ADC_CLOCK_DIV;
			break;
		case SYNC_3_FAST_FILTER:
            fClockDivider = (float32_t)(SYNC_3_CLOCK_DIV + FILTER_AVERAGE - 1) * ADC_CLOCK_DIV;
			break;
	}

	//Wait for the sample rate semaphore
	osMutexWait(sampleRateMutex, osWaitForever);

	//Set the sample rate
	fSampleRate = (float32_t)ADC_CLOCK_RATE / CHANNEL_NUM / fClockDivider / iFreqDivider;

	//Release the sample rate semaphore
	osMutexRelease(sampleRateMutex);
}

/*
Returns the configured sample rate
 - Input: N/A
 - Output:
 	 Sample rate
*/
float32_t getSampleRate()
{
	//Create a temporary variable
	float32_t fRate;

	//Wait for the sample rate semaphore
	osMutexWait(sampleRateMutex, osWaitForever);

	fRate = fSampleRate;

	//Release the sample rate semaphore
	osMutexRelease(sampleRateMutex);

	return fRate;
}

/*
Sets the configured active channels
 - Input:
 	 Channel configurations
 - Output: N/A
*/
void setActiveChannels(uint16_t *iChannels)
{
	//Create a temporary variable
	float32_t iActive = 0;

	//Cycle all the channel configurations
	for (int i = 0; i < CHANNEL_NUM; i++)
	{
		iActive += ((1 << CHANNEL_BITS_NUM) - 1) & (iChannels[i] >> CHANNEL_BITS_POS);
	}

	//Wait for the active channels semaphore
	osMutexWait(activeChannelsMutex, osWaitForever);

	//Set the active channels
	iActiveChannels = iActive;

	//Release the active channels semaphore
	osMutexRelease(activeChannelsMutex);
}

/*
Returns the configured active channels
 - Input: N/A
 - Output:
 	 Active channels
*/
uint16_t getActiveChannels()
{
	//Create a temporary variable
	float32_t iActive;

	//Wait for the active channels semaphore
	osMutexWait(activeChannelsMutex, osWaitForever);

	//Set the active channels
	iActive = iActiveChannels;

	//Release the active channels semaphore
	osMutexRelease(activeChannelsMutex);

	return iActive;
}

#if DATA_CONVERSION

/*
Sets the gain relation constant
 - Input:
 	 Gain configuration
 - Output: N/A
*/
void setGainRelation(uint16_t *iGain)
{
	//Wait for the conversion semaphore
	osMutexWait(conversionMutex, osWaitForever);

	//Set the conversion constant array
	for (int i = 0; i < CHANNEL_NUM; i++)
	{
		//Get the gain bits
		uint16_t iGainPos = ((1 << GAIN_BITS_NUM) - 1) & (iGain[i] >> GAIN_BITS_POS);

		//Set the gain relation value for the channel
		fGainRelation[i] = REF_VOLTAGE / iGainLut[iGainPos];
	}

	//Release the conversion semaphore
	osMutexRelease(conversionMutex);
}

/*
Returns the gain relation constant
 - Input: N/A
 - Output:
 	 Gain relation
*/
void getGainRelation(float32_t *fRelation)
{
	//Wait for the conversion semaphore
	osMutexWait(conversionMutex, osWaitForever);

	//Set the gain relation return
	for (int i = 0; i < CHANNEL_NUM; i++)
	{
		fRelation[i] = fGainRelation[i];
	}

	//Release the conversion semaphore
	osMutexRelease(conversionMutex);
}

#endif

#endif
#endif
