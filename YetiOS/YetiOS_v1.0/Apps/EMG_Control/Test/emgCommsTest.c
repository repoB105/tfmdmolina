/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgment:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * emgCommsTest.c
 *
 *  Created on: 5 May. 2021
 *	Author: David Molina Toro <dmolina@b105.upm.es>
 */

/*
 * @file emgCommsTest.c
 * @communication system for EMG control
 */

//YetiOS header
#include "yetiOs.h"

//Network YetiOS header
#include "netYetiOS.h"

#if EMG_COMMS_TEST_APP

//Communication configuration header
#include "emgCommsTestConf.h"

//Initialization delay definition
#define WAIT_SYSTEMS 	500
#define WAIT_NETSTACK 	1000

//Microseconds in a millisecond definition
#define MICROS_IN_MILLIS 1000

//Queue parameters definition
#define NO_WAIT		0
#define PACKET_SIZE	32

//UDP configuration definitions
#define WAIT_UDP_ERROR 	1000
#define UDP_PORT		0xAAAA

//Data frame size in integers definition
#define DATA_FRAME_SIZE 5

//Serial buffer definitions
#define HEADER_MESSAGE_SIZE 4

//Test control definitions
#define TEST_MSG_NUM 	5000
#define TEST_MSG_PERIOD	2000

//UDP message structure definition
typedef struct
{
	uint8_t dataBuffer[PACKET_SIZE];
	size_t iBufferSize;
} udpMessage_t;

//Data message structure definition
typedef struct
{
	char cHeader[HEADER_MESSAGE_SIZE];
	int32_t iDataFrame[DATA_FRAME_SIZE];
} dataMessage_t;

//Messages processed indicator
uint16_t iMessagesProc = 0;

//Button control semaphore
osSemaphoreId buttonSemaphore;

//Button pin callback function declaration
void buttonPinCallback(void const *arg);

//UDP address variable definitions
netAddr_t netAddrMaster;
netAddr_t netAddrSlave;

//UDP communication thread identifiers
osThreadId udpInitCommsProcId;
osThreadId udpReadCommsProcId;
osThreadId udpWriteCommsProcId;

//UDP communications function declarations
void udpInitCommsFunc(void const *arg);
void udpReadCommsFunc(void const *arg);
void udpWriteCommsFunc(void const *arg);

//UDP communications system initialization thread
YtAutoInitThread(udp_init_comms_proc, udpInitCommsFunc, osPriorityBelowNormal, 256, &udpInitCommsProcId, NULL);

/*
UDP communications initialization thread
 - Input:
 	 Input arguments
 - Output: N/A
*/
void udpInitCommsFunc(void const *arg)
{
	//Wait for the systems to initialize
	osDelay(WAIT_NETSTACK);

	//Initialize the button interrupt
	ytGpioInitPin(GPIO_PIN_B1, GPIO_PIN_INTERRUPT_FALLING, GPIO_PIN_NO_PULL);
	ytGpioPinSetCallback(GPIO_PIN_B1, buttonPinCallback, NULL);

	//Create the UDP node addresses
	netAddrMaster 	= ytNetNewAddr(MASTER_UDP_ADDRESS, UDP_ADDRESS_FORMAT);
	netAddrSlave 	= ytNetNewAddr(SLAVE_UDP_ADDRESS, UDP_ADDRESS_FORMAT);

#ifdef MASTER_NODE
	//Add the master node to the system
	ytNetAddNodeAddr(netAddrMaster);
#endif
#ifdef SLAVE_NODE
	//Add the slave node to the system
	ytNetAddNodeAddr(netAddrSlave);
#endif

	//Check if the UDP port is open
	if(ytUdpOpen(UDP_PORT) != RET_OK)
	{
		//Set the red led on
		ytLedOn(LED_RED_1);
		while(1)
		{
			osDelay(WAIT_UDP_ERROR);
		}
	}

	//Initialize the button semaphore
	buttonSemaphore = ytSemaphoreCreate(1);
	osSemaphoreWait(buttonSemaphore, osWaitForever);

	//Main control loop
	while(1)
	{
		//Wait for the button semaphore
		osSemaphoreWait(buttonSemaphore, osWaitForever);

		//Reset the messages processed indicator
		iMessagesProc = 0;

		//Set the blue led on
		ytLedOn(LED_BLUE_1);

#ifdef MASTER_NODE
		//Initialize the UDP reading thread
		ytStartThread("udp_read_comms_proc", udpReadCommsFunc, osPriorityNormal, 256, &udpReadCommsProcId, NULL);

		//Set the button callback
		ytGpioPinSetCallback(GPIO_PIN_B1, buttonPinCallback, NULL);
#endif
#ifdef SLAVE_NODE
		//Initialize the UDP writing thread
		ytStartThread("udp_write_comms_proc", udpWriteCommsFunc, osPriorityNormal, 256, &udpWriteCommsProcId, NULL);
#endif
	}
}


/*
Button pin callback function
 - Input:
 	 Callback arguments
 - Output: N/A
*/
void buttonPinCallback(void const *arg)
{
	//Reset the button callback
	ytGpioPinSetCallback(GPIO_PIN_B1, NULL, NULL);

	//Release the button semaphore
	osSemaphoreRelease(buttonSemaphore);
}

/*
UDP communications reading thread
 - Input:
 	 Input arguments
 - Output: N/A
*/
void udpReadCommsFunc(void const *arg)
{
	//UDP data buffer for incoming messages
	uint8_t dataRecvUdp[PACKET_SIZE];

	//Main UDP read loop
	while(1)
	{
		//Check if a message has been received via UDP
		uint16_t iBytesRecv = ytUdpRcv(netAddrMaster, UDP_PORT, dataRecvUdp, PACKET_SIZE);

		//Check if any data has been received
		if (iBytesRecv > 0)
		{
			//Increase the messages processed indicator
			iMessagesProc++;
		}
	}
}

/*
UDP communications writing thread
 - Input:
 	 Input arguments
 - Output: N/A
*/
void udpWriteCommsFunc(void const *arg)
{
	//Data transmission control variables
	timeMeas_t timeMeas;

	//Create a data message prototype
	dataMessage_t *dataMessage;
	dataMessage = pvPortMalloc(sizeof(dataMessage_t));

	//Set the data message header
	memcpy(dataMessage->cHeader, "DATA", sizeof(dataMessage->cHeader));

	//Set the data message information
	dataMessage->iDataFrame[0] = 0;
	dataMessage->iDataFrame[1] = -1;
	dataMessage->iDataFrame[2] = -1;
	dataMessage->iDataFrame[3] = -1;
	dataMessage->iDataFrame[4] = -1;

	//Failed messages indicator
	uint16_t iMessagesFail = 0;

	//Mean send time storage variables
	uint32_t iSendCount = 0;
	uint32_t iMeanSend 	= 0;

	//Main UDP write loop
	while(dataMessage->iDataFrame[0] < TEST_MSG_NUM)
	{
		//Start the period measurement
		ytStartTimeMeasure(&timeMeas);

		//Try sending the message via UDP to the master node
		if (ytUdpSend(netAddrMaster, UDP_PORT, (uint8_t*)dataMessage, sizeof(dataMessage_t)) == RET_OK)
		{
			iMessagesProc++;
		}

		//Stop the period measurement
		ytStopTimeMeasure(&timeMeas);

		//Increase the data frame counter
		dataMessage->iDataFrame[0]++;

		//Add the send time to the mean
		iMeanSend += timeMeas.elapsedTime;
		iSendCount++;

		//Check the elapsed time and wait for the next transmission
		int32_t iTimeDiff = TEST_MSG_PERIOD - timeMeas.elapsedTime;
		if (iTimeDiff > 0)
		{
			//Wait for the next transmission
			osDelay(iTimeDiff / MICROS_IN_MILLIS);
		}
		else
		{
			iMessagesFail++;
		}
	}

	//Set the blue led off
	ytLedOff(LED_BLUE_1);

	//Get the send mean time
	iMeanSend = iMeanSend / iSendCount;

	//Set the button callback
	ytGpioPinSetCallback(GPIO_PIN_B1, buttonPinCallback, NULL);
}

#endif
