/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgment:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * stm32l476g_sd.c
 *
 *  Created on: 24 May. 2021
 *	Author: David Molina Toro <dmolina@b105.upm.es>
 */

/*
 * @file stm32l476g_sd.c
 * @device specific SD driver file
 */

//Device header include
#include <stm32l476g_sd.h>

//SD handler variable declaration
SD_HandleTypeDef hsdHeimdall;

//DMA configuration function declarations
static HAL_StatusTypeDef SD_DMAConfigRx(SD_HandleTypeDef *hsd);
static HAL_StatusTypeDef SD_DMAConfigTx(SD_HandleTypeDef *hsd);

/*
SD driver initialization function
 - Input: N/A
 - Output:
 	 Status
*/
uint8_t BSP_SD_Init(void)
{
	//Initialization status
	uint8_t sdState = MSD_OK;

	//SD card device interface configuration
	hsdHeimdall.Instance 					= SDMMC1;
	hsdHeimdall.Init.ClockEdge           	= SDMMC_CLOCK_EDGE_RISING;
	hsdHeimdall.Init.ClockBypass         	= SDMMC_CLOCK_BYPASS_DISABLE;
	hsdHeimdall.Init.ClockPowerSave      	= SDMMC_CLOCK_POWER_SAVE_DISABLE;
	hsdHeimdall.Init.BusWide             	= SDMMC_BUS_WIDE_1B;
	hsdHeimdall.Init.HardwareFlowControl 	= SDMMC_HARDWARE_FLOW_CONTROL_ENABLE;
	hsdHeimdall.Init.ClockDiv            	= SDMMC_TRANSFER_CLK_DIV;

	//MSP SD initialization
	BSP_SD_MspInit(&hsdHeimdall, NULL);

	//HAL SD initialization
	if(HAL_SD_Init(&hsdHeimdall))
	{
		sdState = MSD_ERROR;
	}

	//Configure SD Bus width
	if(sdState == MSD_OK)
	{
		//Enable wide operation
		if(HAL_SD_ConfigWideBusOperation(&hsdHeimdall, SDMMC_BUS_WIDE_1B))
		{
			sdState = MSD_ERROR;
		}
		else
		{
			sdState = MSD_OK;
		}
	}

	return  sdState;
}

/*
SD driver reset function
 - Input: N/A
 - Output:
 	 Status
*/
uint8_t BSP_SD_DeInit(void)
{ 
	//Reset status
	uint8_t sdState = MSD_OK;

	//HAL SD reset
	hsdHeimdall.Instance = SDMMC1;
	if(HAL_SD_DeInit(&hsdHeimdall))
	{
		sdState = MSD_ERROR;
	}

	//MSP SD reset
	BSP_SD_MspDeInit(&hsdHeimdall, NULL);

	return  sdState;
}

/*
SD detection function
 - Input: N/A
 - Output:
 	 Status
*/
uint8_t BSP_SD_IsDetected(void)
{
  __IO uint8_t sdState = SD_PRESENT;

  return sdState;
}

/*
Read blocks from address
 - Input:
 	 Sector address
 	 Block count
 	 Timeout
 - Output:
 	 Data buffer
 	 Status
*/
uint8_t BSP_SD_ReadBlocks(uint32_t *pData, uint32_t ReadAddr, uint32_t NumOfBlocks, uint32_t Timeout)
{
	//Read the blocks from the given address
	if (!HAL_SD_ReadBlocks(&hsdHeimdall, (uint8_t*)pData, ReadAddr, NumOfBlocks, Timeout))
	{
		return MSD_OK;
	}
	else
	{
		return MSD_ERROR;
	}
}

/*
Write blocks into address
 - Input:
  	 Data buffer
 	 Sector address
 	 Block count
 	 Timeout
 - Output:
 	 Status
*/
uint8_t BSP_SD_WriteBlocks(uint32_t *pData, uint32_t WriteAddr, uint32_t NumOfBlocks, uint32_t Timeout)
{
	//Write the blocks into the given address
	if (!HAL_SD_WriteBlocks(&hsdHeimdall, (uint8_t*)pData, WriteAddr, NumOfBlocks, Timeout))
	{
		return MSD_OK;
	}
	else
	{
		return MSD_ERROR;
	}
}

/*
Read blocks from address with DMA
 - Input:
 	 Sector address
 	 Block count
 - Output:
   	 Data buffer
 	 Status
*/
uint8_t BSP_SD_ReadBlocks_DMA(uint32_t *pData, uint32_t ReadAddr, uint32_t NumOfBlocks)
{
	//Read status
	HAL_StatusTypeDef sdState = HAL_OK;

	//Invalidate the DMA transmission handle
	hsdHeimdall.hdmatx = NULL;

	//Prepare the DMA channel for a read operation
	sdState = SD_DMAConfigRx(&hsdHeimdall);
	if(sdState == HAL_OK)
	{
		//Read blocks in DMA transfer mode
		sdState = HAL_SD_ReadBlocks_DMA(&hsdHeimdall, (uint8_t *)pData, ReadAddr, NumOfBlocks);
	}

	//Check the read status
	if( sdState == HAL_OK)
	{
		return MSD_OK;
	}
	else
	{
		return MSD_ERROR;
	}
}

/*
Write blocks into address with DMA
 - Input:
   	 Data buffer
 	 Sector address
 	 Block count
 - Output:
 	 Status
*/
uint8_t BSP_SD_WriteBlocks_DMA(uint32_t *pData, uint32_t WriteAddr, uint32_t NumOfBlocks)
{
	//Write status
	HAL_StatusTypeDef sdState = HAL_OK;

	//Invalidate the DMA reception handle
	hsdHeimdall.hdmarx = NULL;

	//Prepare the DMA channel for a read operation
	sdState = SD_DMAConfigTx(&hsdHeimdall);
	if(sdState == HAL_OK)
	{
		//Write blocks in DMA transfer mode
		sdState = HAL_SD_WriteBlocks_DMA(&hsdHeimdall, (uint8_t *)pData, WriteAddr, NumOfBlocks);
	}

	//Check the write status
	if( sdState == HAL_OK)
	{
		return MSD_OK;
	}
	else
	{
		return MSD_ERROR;
	}
}

/*
Erase blocks from SD
 - Input:
 	 Start address
 	 End address
 - Output:
 	 Status
*/
uint8_t BSP_SD_Erase(uint32_t StartAddr, uint32_t EndAddr)
{
	//Erase the blocks from the SD
	if (!HAL_SD_Erase(&hsdHeimdall, StartAddr, EndAddr))
	{
		return MSD_OK;
	}
	else
	{
		return MSD_ERROR;
	}
}

/*
IRQ interrupt handler
 - Input: N/A
 - Output: N/A
*/
void BSP_SD_IRQHandler(void)
{
	HAL_SD_IRQHandler(&hsdHeimdall);
}

/*
IRQ transmission handler
 - Input: N/A
 - Output: N/A
*/
void BSP_SD_DMA_Tx_IRQHandler(void)
{
	HAL_DMA_IRQHandler(hsdHeimdall.hdmatx);
}

/*
IRQ reception handler
 - Input: N/A
 - Output: N/A
*/
void BSP_SD_DMA_Rx_IRQHandler(void)
{
	HAL_DMA_IRQHandler(hsdHeimdall.hdmarx);
}

/*
Returns the SD card status
 - Input: N/A
 - Output:
 	 Status
*/
uint8_t BSP_SD_GetCardState(void)
{
	//Card status
	HAL_SD_CardStateTypedef sdState;

	//Get the card status
	sdState = HAL_SD_GetCardState(&hsdHeimdall);
	if (sdState == HAL_SD_CARD_TRANSFER)
	{
		return SD_TRANSFER_OK;
	}
	else if (sdState == HAL_SD_CARD_SENDING ||
			 sdState == HAL_SD_CARD_RECEIVING ||
			 sdState == HAL_SD_CARD_PROGRAMMING)
	{
		return  SD_TRANSFER_BUSY;
	}
	else
	{
		return SD_TRANSFER_ERROR;
	}
}

/*
Returns the SD card information
 - Input: N/A
 - Output:
 	 Status
*/
void BSP_SD_GetCardInfo(BSP_SD_CardInfo *CardInfo)
{
	HAL_SD_GetCardInfo(&hsdHeimdall, CardInfo);
}

/*
SD abort operation callback
 - Input:
 	 SD handler
 - Output: N/A
*/
void HAL_SD_AbortCallback(SD_HandleTypeDef *hsd)
{
	BSP_SD_AbortCallback();
}

/*
SD transmission completed callback
 - Input:
 	 SD handler
 - Output: N/A
*/
void HAL_SD_TxCpltCallback(SD_HandleTypeDef *hsd)
{
	BSP_SD_WriteCpltCallback();
}

/*
SD reception completed callback
 - Input:
 	 SD handler
 - Output: N/A
*/
void HAL_SD_RxCpltCallback(SD_HandleTypeDef *hsd)
{
	BSP_SD_ReadCpltCallback();
}

/*
MSP initialization for the SD
 - Input:
 	 SD handler
 	 Parameters
 - Output: N/A
*/
__weak void BSP_SD_MspInit(SD_HandleTypeDef *hsd, void *Params)
{
	//GPIO initialization structure
	GPIO_InitTypeDef gpioInitStruct = {0};

	//Prevent unused arguments compilation warning
	UNUSED(Params);

	//Enable SDMMC1 clock
	__HAL_RCC_SDMMC1_CLK_ENABLE();

	//Enable DMA2 clocks
	__DMAx_TxRx_CLK_ENABLE();

	//Enable GPIO clocks
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();

	//Common GPIO configuration
	gpioInitStruct.Mode      = GPIO_MODE_AF_PP;
	gpioInitStruct.Pull      = GPIO_PULLUP;
	gpioInitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	gpioInitStruct.Alternate = GPIO_AF12_SDMMC1;

	//GPIOC configuration
	gpioInitStruct.Pin = GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12;
	HAL_GPIO_Init(GPIOC, &gpioInitStruct);

	//GPIOD configuration
	gpioInitStruct.Pin = GPIO_PIN_2;
	HAL_GPIO_Init(GPIOD, &gpioInitStruct);

	//NVIC configuration for SDMMC1 interrupts
	HAL_NVIC_SetPriority(SDMMC1_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(SDMMC1_IRQn);
}

/*
MSP reset for the SD
 - Input:
 	 SD handler
 	 Parameters
 - Output: N/A
*/
__weak void BSP_SD_MspDeInit(SD_HandleTypeDef *hsd, void *Params)
{
	//Prevent unused arguments compilation warning
	UNUSED(Params);

	//Disable all interrupts
	HAL_NVIC_DisableIRQ(SDMMC1_IRQn);
	HAL_NVIC_DisableIRQ(DMA2_Channel4_IRQn);

	//Reset all pins
	HAL_GPIO_DeInit(GPIOC, (GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12));
	HAL_GPIO_DeInit(GPIOD, GPIO_PIN_2);

	//DMA handler object
	DMA_HandleTypeDef hdma;

	//Reset DMA channels
	hdma.Instance = DMA2_Channel4;
	HAL_DMA_DeInit(&hdma);

	//Deactivate clock of SDMMC
	__HAL_RCC_SDMMC1_CLK_DISABLE();
}

/*
BSP abort operation callback
 - Input: N/A
 - Output: N/A
*/
__weak void BSP_SD_AbortCallback(void)
{

}

/*
BSP write operation callback
 - Input: N/A
 - Output: N/A
*/
__weak void BSP_SD_WriteCpltCallback(void)
{

}

/*
BSP read operation callback
 - Input: N/A
 - Output: N/A
*/
__weak void BSP_SD_ReadCpltCallback(void)
{

}

/*
SD configuration for DMA reception
 - Input:
 	 SD handler
 - Output:
 	 Status
*/
static HAL_StatusTypeDef SD_DMAConfigRx(SD_HandleTypeDef *hsd)
{
	//Function variables
	static DMA_HandleTypeDef hdmaRx;
	HAL_StatusTypeDef status = HAL_ERROR;

	//Configure DMA reception parameters
	hdmaRx.Instance 					= DMA2_Channel4;
	hdmaRx.Init.Request             	= DMA_REQUEST_7;
	hdmaRx.Init.Direction           	= DMA_PERIPH_TO_MEMORY;
	hdmaRx.Init.PeriphInc           	= DMA_PINC_DISABLE;
	hdmaRx.Init.MemInc              	= DMA_MINC_ENABLE;
	hdmaRx.Init.PeriphDataAlignment 	= DMA_PDATAALIGN_WORD;
	hdmaRx.Init.MemDataAlignment    	= DMA_MDATAALIGN_WORD;
	hdmaRx.Init.Priority            	= DMA_PRIORITY_VERY_HIGH;

	//Associate the DMA handle
	__HAL_LINKDMA(hsd, hdmarx, hdmaRx);

	//Stop any ongoing transfer and reset the state
	HAL_DMA_Abort(&hdmaRx);

	//Reset the Channel for new transfer
	HAL_DMA_DeInit(&hdmaRx);

	//Configure the DMA Channel
	status = HAL_DMA_Init(&hdmaRx);

	//NVIC configuration for DMA transfer complete interrupt
	HAL_NVIC_SetPriority(DMA2_Channel4_IRQn, 6, 0);
	HAL_NVIC_EnableIRQ(DMA2_Channel4_IRQn);

	return status;
}

/*
SD configuration for DMA transmission
 - Input:
 	 SD handler
 - Output:
 	 Status
*/
static HAL_StatusTypeDef SD_DMAConfigTx(SD_HandleTypeDef *hsd)
{
	//Function variables
	static DMA_HandleTypeDef hdmaTx;
	HAL_StatusTypeDef status;

	//Configure DMA transmission parameters
	hdmaTx.Instance 					= DMA2_Channel4;
	hdmaTx.Init.Request             	= DMA_REQUEST_7;
	hdmaTx.Init.Direction           	= DMA_MEMORY_TO_PERIPH;
	hdmaTx.Init.PeriphInc           	= DMA_PINC_DISABLE;
	hdmaTx.Init.MemInc              	= DMA_MINC_ENABLE;
	hdmaTx.Init.PeriphDataAlignment 	= DMA_PDATAALIGN_WORD;
	hdmaTx.Init.MemDataAlignment    	= DMA_MDATAALIGN_WORD;
	hdmaTx.Init.Priority            	= DMA_PRIORITY_VERY_HIGH;

	//Associate the DMA handle
	__HAL_LINKDMA(hsd, hdmatx, hdmaTx);

	//Stop any ongoing transfer and reset the state
	HAL_DMA_Abort(&hdmaTx);

	//Reset the Channel for new transfer
	HAL_DMA_DeInit(&hdmaTx);

	//Configure the DMA Channel
	status = HAL_DMA_Init(&hdmaTx);

	//NVIC configuration for DMA transfer complete interrupt
	HAL_NVIC_SetPriority(DMA2_Channel4_IRQn, 6, 0);
	HAL_NVIC_EnableIRQ(DMA2_Channel4_IRQn);

	return status;
}

/*
IRQ interrupt handler
 - Input: N/A
 - Output: N/A
*/
void SDMMC1_IRQHandler(void)
{
	HAL_SD_IRQHandler(&hsdHeimdall);
}

/*
Channel 4 DMA interrupt handle
 - Input: N/A
 - Output: N/A
*/
void DMA2_Channel4_IRQHandler(void)
{
	//Check the given handler context
	if(hsdHeimdall.Context == (SD_CONTEXT_DMA | SD_CONTEXT_READ_SINGLE_BLOCK) ||
	   hsdHeimdall.Context == (SD_CONTEXT_DMA | SD_CONTEXT_READ_MULTIPLE_BLOCK))
	{
		BSP_SD_DMA_Rx_IRQHandler();
	}
	else if(hsdHeimdall.Context == (SD_CONTEXT_DMA | SD_CONTEXT_WRITE_SINGLE_BLOCK) ||
			hsdHeimdall.Context == (SD_CONTEXT_DMA | SD_CONTEXT_WRITE_MULTIPLE_BLOCK))
	{
		BSP_SD_DMA_Tx_IRQHandler();
	}
}
