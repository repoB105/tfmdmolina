/*----------------------------------------------------------------------------/
/  FatFs - Generic FAT file system module  R0.12c                             /
/-----------------------------------------------------------------------------/
/
/ Copyright (C) 2017, ChaN, all right reserved.
/
/ FatFs module is an open source software. Redistribution and use of FatFs in
/ source and binary forms, with or without modification, are permitted provided
/ that the following condition is met:

/ 1. Redistributions of source code must retain the above copyright notice,
/    this condition and the following disclaimer.
/
/ This software is provided by the copyright holder and contributors "AS IS"
/ and any warranties related to this software are DISCLAIMED.
/ The copyright owner or contributors be NOT LIABLE for any damages caused
/ by use of this software.
/----------------------------------------------------------------------------*/

#include "ff.h"

#if _FS_REENTRANT
/*------------------------------------------------------------------------*/
/* Create a Synchronization Object                                        */
/*------------------------------------------------------------------------*/

int ff_cre_syncobj(BYTE vol, _SYNC_t *sobj)
{

    int ret;
#if _USE_MUTEX

#if (osCMSIS < 0x20000U)
    osMutexDef(MTX);
    *sobj = osMutexCreate(osMutex(MTX));
#else
    *sobj = osMutexNew(NULL);
#endif

#else

#if (osCMSIS < 0x20000U)
    osSemaphoreDef(SEM);
    *sobj = osSemaphoreCreate(osSemaphore(SEM), 1);
#else
    *sobj = osSemaphoreNew(1, 1, NULL);
#endif

#endif
    ret = (*sobj != NULL);

    return ret;
}

/*------------------------------------------------------------------------*/
/* Delete a Synchronization Object                                        */
/*------------------------------------------------------------------------*/

int ff_del_syncobj(_SYNC_t sobj)
{
#if _USE_MUTEX
    osMutexDelete (sobj);
#else
    osSemaphoreDelete (sobj);
#endif
    return 1;
}

/*------------------------------------------------------------------------*/
/* Request Grant to Access the Volume                                     */
/*------------------------------------------------------------------------*/

int ff_req_grant(_SYNC_t sobj)
{
  int ret = 0;
#if (osCMSIS < 0x20000U)

#if _USE_MUTEX
  if(osMutexWait(sobj, _FS_TIMEOUT) == osOK)
#else
  if(osSemaphoreWait(sobj, _FS_TIMEOUT) == osOK)
#endif

#else

#if _USE_MUTEX
   if(osMutexAcquire(sobj, _FS_TIMEOUT) == osOK)
#else
   if(osSemaphoreAcquire(sobj, _FS_TIMEOUT) == osOK)
#endif

#endif
  {
    ret = 1;
  }

  return ret;
}

/*------------------------------------------------------------------------*/
/* Release Grant to Access the Volume                                     */
/*------------------------------------------------------------------------*/

void ff_rel_grant(_SYNC_t sobj)
{
#if _USE_MUTEX
  osMutexRelease(sobj);
#else
  osSemaphoreRelease(sobj);
#endif
}

#endif

#if _USE_LFN == 3 /* LFN with a working buffer on the heap */

/*------------------------------------------------------------------------*/
/* Allocate a memory block                                                */
/*------------------------------------------------------------------------*/
void* ff_memalloc (UINT msize)
{
	return ff_malloc(msize);	/* Allocate a new memory block with POSIX API */
}


/*------------------------------------------------------------------------*/
/* Free a memory block                                                    */
/*------------------------------------------------------------------------*/

void ff_memfree(void* mblock)
{
	ff_free(mblock);	/* Discard the memory block with POSIX API */
}

#endif
