/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgment:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * sd_diskio_dma_rtos.c
 *
 *  Created on: 24 May. 2021
 *	Author: David Molina Toro <dmolina@b105.upm.es>
 */

/*
 * @file sd_diskio_dma_rtos.c
 * @general SD I/O driver
 */

//FatFs module header include
#include "ff_gen_drv.h"

//SD I/O driver header include
#include "sd_diskio_dma_rtos.h"

//Driver definitions
#define QUEUE_SIZE 		(uint32_t) 10
#define READ_CPLT_MSG 	(uint32_t) 1
#define WRITE_CPLT_MSG 	(uint32_t) 2
#define SD_TIMEOUT 		(30 * 1000)

//Disk status variable definition
static volatile DSTATUS Stat = STA_NOINIT;

//SD message queue definition
static osMessageQId SDQueueID;

//SD function declarations
DSTATUS SD_initialize (BYTE);
DSTATUS SD_status (BYTE);
DRESULT SD_read (BYTE, BYTE*, DWORD, UINT);
#if _USE_WRITE == 1
  DRESULT SD_write (BYTE, const BYTE*, DWORD, UINT);
#endif
#if _USE_IOCTL == 1
  DRESULT SD_ioctl (BYTE, BYTE, void*);
#endif

const Diskio_drvTypeDef SD_Driver =
{
  SD_initialize,
  SD_status,
  SD_read,
#if  _USE_WRITE == 1
  SD_write,
#endif /* _USE_WRITE == 1 */

#if  _USE_IOCTL == 1
  SD_ioctl,
#endif /* _USE_IOCTL == 1 */
};

/*
Initialize the SD card the card status
 - Input:
 	 Multi-Lun parameter
 - Output:
 	 Status
*/
DSTATUS SD_initialize(BYTE lun)
{
	//Function variables
	Stat = STA_NOINIT;

	//CHeck the card status
	if(BSP_SD_GetCardState() == MSD_OK)
	{
		Stat &= ~STA_NOINIT;

		//Create the SD operation Queue
		osMessageQDef(SD_Queue, QUEUE_SIZE, uint16_t);
		SDQueueID = osMessageCreate (osMessageQ(SD_Queue), NULL);
	}

	return Stat;
}

/*
Returns the card status
 - Input:
 	 Multi-Lun parameter
 - Output:
 	 Status
*/
DSTATUS SD_status(BYTE lun)
{
	return Stat;
}

/*
Read sectors function
 - Input:
 	 Multi-Lun parameter
 	 Sector to write
 	 Write count
 - Output:
 	 Data buffer
 	 Result
*/
DRESULT SD_read(BYTE lun, BYTE *buff, DWORD sector, UINT count)
{
	//Function variables
	DRESULT res = RES_ERROR;
	osEvent event;
	uint32_t timer;

	//Read the given blocks
	if(BSP_SD_ReadBlocks_DMA((uint32_t*)buff, (uint32_t)sector, count) == MSD_OK)
	{
		//Wait for a message from the queue or a timeout */
		event = osMessageGet(SDQueueID, SD_TIMEOUT);
		if (event.status == osEventMessage)
		{
			if (event.value.v == READ_CPLT_MSG)
			{
				timer = osKernelSysTick() + SD_TIMEOUT;
				//Block until SDIO IP is ready or a timeout occur
				while(timer > osKernelSysTick())
				{
					if (BSP_SD_GetCardState() == SD_TRANSFER_OK)
					{
						res = RES_OK;
						break;
					}
				}
			}
		}
	}

	return res;
}

#if _USE_WRITE == 1

/*
Write sectors function
 - Input:
 	 Multi-Lun parameter
 	 Data buffer
 	 Sector to write
 	 Write count
 - Output:
 	 Result
*/
DRESULT SD_write(BYTE lun, const BYTE *buff, DWORD sector, UINT count)
{
	//Function variables
	osEvent event;
	DRESULT res = RES_ERROR;
	uint32_t timer;

	//Write the given blocks
	if(BSP_SD_WriteBlocks_DMA((uint32_t*)buff, (uint32_t)sector, count) == MSD_OK)
	{
		//Get the message from the queue
		event = osMessageGet(SDQueueID, SD_TIMEOUT);
		if (event.status == osEventMessage)
		{
			if (event.value.v == WRITE_CPLT_MSG)
			{
				timer = osKernelSysTick() + SD_TIMEOUT;
				//Block until SDIO IP is ready or a timeout occur
				while(timer > osKernelSysTick())
				{
					if (BSP_SD_GetCardState() == SD_TRANSFER_OK)
					{
						res = RES_OK;
						break;
					}
				}
			}
		}
	}

	return res;
}

#endif

#if _USE_IOCTL == 1

/*
Applies the given I/O command
 - Input:
 	 Multi-Lun parameter
 	 Command
 	 Data buffer
 - Output:
 	 Result
*/
DRESULT SD_ioctl(BYTE lun, BYTE cmd, void *buff)
{
	//Function variables
	DRESULT res = RES_ERROR;
	BSP_SD_CardInfo CardInfo;

	//Check the initialization status
	if (Stat & STA_NOINIT)
	{
		return RES_NOTRDY;
	}

	switch (cmd)
	{
		//Make sure that no pending write process
		case CTRL_SYNC :
			res = RES_OK;
			break;
		//Get number of sectors on the disk
		case GET_SECTOR_COUNT :
			BSP_SD_GetCardInfo(&CardInfo);
			*(DWORD*)buff = CardInfo.LogBlockNbr;
			res = RES_OK;
			break;
		//Get R/W sector size
		case GET_SECTOR_SIZE :
			BSP_SD_GetCardInfo(&CardInfo);
			*(WORD*)buff = CardInfo.LogBlockSize;
			res = RES_OK;
			break;
		//Get erase block size in unit of sector
		case GET_BLOCK_SIZE :
			BSP_SD_GetCardInfo(&CardInfo);
			*(DWORD*)buff = CardInfo.LogBlockSize;
			res = RES_OK;
			break;
		default:
			res = RES_PARERR;
	}

	return res;
}

#endif

/*
Write completed callback
 - Input: N/A
 - Output: N/A
*/
void BSP_SD_WriteCpltCallback(void)
{
	osMessagePut(SDQueueID, WRITE_CPLT_MSG, osWaitForever);
}

/*
Read completed callback
 - Input: N/A
 - Output: N/A
*/
void BSP_SD_ReadCpltCallback(void)
{
	osMessagePut(SDQueueID, READ_CPLT_MSG, osWaitForever);
}
