/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgment:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * diskio.c
 *
 *  Created on: 24 May. 2021
 *	Author: David Molina Toro <dmolina@b105.upm.es>
 */

/*
 * @file diskio.c
 * @low level disk interface module include file
 */

//Disk I/0 header include
#include "diskio.h"

//FatFs system header include
#include "ff_gen_drv.h"

//Compiler definitions
#if defined ( __GNUC__ )
#ifndef __weak
#define __weak __attribute__((weak))
#endif
#endif

//Disk system variable definition
extern Disk_drvTypeDef disk;

/*
Returns the disk status
 - Input:
 	 Physical drive
 - Output:
 	 Status
*/
DSTATUS disk_status(BYTE pdrv)
{
  DSTATUS stat;

  stat = disk.drv[pdrv]->disk_status(disk.lun[pdrv]);
  return stat;
}

/*
Initializes the given disk
 - Input:
 	 Physical drive
 - Output:
 	 Status
*/
DSTATUS disk_initialize(BYTE pdrv)
{
  DSTATUS stat = RES_OK;

  if(disk.is_initialized[pdrv] == 0)
  {
    disk.is_initialized[pdrv] = 1;
    stat = disk.drv[pdrv]->disk_initialize(disk.lun[pdrv]);
  }

  return stat;
}

/*
Read sectors from disk
 - Input:
 	 Physical drive
 	 Sector address
 	 Read count
 - Output:
 	 Status
 	 Data buffer
*/
DRESULT disk_read(BYTE pdrv, BYTE *buff, DWORD sector, UINT count)
{
  DRESULT res = disk.drv[pdrv]->disk_read(disk.lun[pdrv], buff, sector, count);
  return res;
}

#if _USE_WRITE == 1

/*
Read sectors from disk
 - Input:
 	 Physical drive
 	 Data buffer
 	 Sector address
 	 Read count
 - Output:
 	 Status
*/
DRESULT disk_write(BYTE pdrv, const BYTE *buff, DWORD sector, UINT count)
{
  DRESULT res = disk.drv[pdrv]->disk_write(disk.lun[pdrv], buff, sector, count);
  return res;
}

#endif

#if _USE_IOCTL == 1

/*
I/O control operation
 - Input:
 	 Physical drive
 	 Command
 	 Data buffer
 - Output:
 	 Status
*/
DRESULT disk_ioctl(BYTE pdrv, BYTE cmd, void *buff)
{
  DRESULT res = disk.drv[pdrv]->disk_ioctl(disk.lun[pdrv], cmd, buff);
  return res;
}

#endif

/*
Returns the time from the RTC
 - Input: N/A
 - Output:
 	 Time
*/
__weak DWORD get_fattime(void)
{
  return 0;
}
