/*
 * Copyright (c) 2019, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * platformTimer.c
 *
 *  Created on: 14 jul. 2019
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file platformTimer.c
 */

#include "yetiOS.h"
#ifdef USE_HEIMDALL_L4
#include "stm32l4xx_hal.h"
#endif

#include <platformTimer.h>

#if CONFIG_SELECTED_PLATFORM == HEIMDALL_PLATFORM


#define HW_TIMER_RESOLUTION_FREQ	100000		/*100000 KHz resolution*/
#define HW_TIMER_RESOLUTION_TIME	10			/*10 us resolution*/

#if PLATFORM_USE_HW_TIMER
static ytThreadFunc_t currentTimeoutCbFunc = NULL;
static void* currentTimeoutCbArgs = NULL;
static uint32_t lastRunningPeriod = 0;
static  hwTimerMode_t hwTimerMode;
#endif

#if PLATFORM_USE_TIME_MEAS

static uint32_t lastReferenceCpuClock;

/**
 *
 * @return
 */
retval_t initTimeMeasureTimer(void){

	  /* Enable TIM5 clock */
	  __HAL_RCC_TIM5_CLK_ENABLE();

	/* Initialize TIM5 */
	  htim5.Instance = TIM5;

	  /*
	   * Configure the timer
	  */
	  htim5.Init.Period = 0xFFFFFFFF;	/*MAX count available in 32 bits timer*/
	  if(configCPU_CLOCK_HZ < PLATFORM_TIME_MEASURE_RESOLUTION){
		  htim5.Init.Prescaler = 0;
	  }
	  else{
		  htim5.Init.Prescaler = (configCPU_CLOCK_HZ/PLATFORM_TIME_MEASURE_RESOLUTION) - 1;
	  }
	  htim5.Init.ClockDivision = 0;
	  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
	  if(HAL_TIM_Base_Init(&htim5) == HAL_OK)
	  {
	    /* Start the TIM time Base generation*/
	    HAL_TIM_Base_Start(&htim5);
	  }
	  lastReferenceCpuClock = configCPU_CLOCK_HZ;
	  return RET_OK;
}


/**
 *
 */
void timeMeasureResetCount(void){
	if(lastReferenceCpuClock != configCPU_CLOCK_HZ){	/*If the system clock has changed update the timer prescaler*/
		  if(configCPU_CLOCK_HZ < PLATFORM_TIME_MEASURE_RESOLUTION){
			  htim5.Init.Prescaler = 0;
		  }
		  else{
			  htim5.Init.Prescaler = (configCPU_CLOCK_HZ/PLATFORM_TIME_MEASURE_RESOLUTION) - 1;
		  }
		  HAL_TIM_Base_Stop(&htim5);
		  if(HAL_TIM_Base_Init(&htim5) == HAL_OK)
		  {
		    /* Start the TIM time Base generation*/
		    HAL_TIM_Base_Start(&htim5);
		  }
		  lastReferenceCpuClock = configCPU_CLOCK_HZ;
	}
	else{
		__HAL_TIM_SET_COUNTER(&htim5,0);
	}
}
#endif

#if PLATFORM_USE_HW_TIMER
/**
 *
 * @return
 */
retval_t ytInitHwTimer(void){
#if	PLATFORM_DEFAULT_INIT_LOW_POWER_MODE !=	SLEEP_MODE
#warning "The sleep mode is the only low power mode allowed when using the hardware timer. Disable the hw timer to use Stop modes"
#endif
	ytSetDeviceLowPowerMode((uint32_t) SLEEP_MODE);	/*SLEEP MODE is the only low power mode allowed*/

	currentTimeoutCbFunc = NULL;
	currentTimeoutCbArgs = NULL;

	/* Enable TIM2 clock */
	__HAL_RCC_TIM2_CLK_ENABLE();

	/* Initialize TIM2 */
	htim2.Instance = TIM2;
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim2.Init.Prescaler = (configCPU_CLOCK_HZ/HW_TIMER_RESOLUTION_FREQ) - 1;
	htim2.Init.ClockDivision = 0;
	htim2.Init.Period = 0xFFFFFFFF;

	if(HAL_TIM_Base_Init(&htim2) != HAL_OK){
		return RET_ERROR;
	}
	HAL_NVIC_SetPriority(TIM2_IRQn, 5 ,0);		/*Maximum priority*/
	HAL_NVIC_ClearPendingIRQ(TIM2_IRQn);
	HAL_NVIC_EnableIRQ(TIM2_IRQn);

	return RET_OK;
}

/**
 *
 * @return
 */
retval_t ytDeInitHwTimer(void){
	HAL_NVIC_DisableIRQ(TIM2_IRQn);
	__HAL_RCC_TIM2_CLK_DISABLE();
	HAL_TIM_Base_DeInit(&htim2);

	taskENTER_CRITICAL();
	currentTimeoutCbFunc = NULL;
	currentTimeoutCbArgs = NULL;
	taskEXIT_CRITICAL();

	return RET_OK;
}

/**
 *
 * @param timeout
 * @param timeoutCbFunc
 * @param timeoutCbArgs
 * @return
 */
retval_t ytStartHwTimer(uint32_t timeout, hwTimerMode_t timerMode, ytThreadFunc_t timeoutCbFunc, void* timeoutCbArgs){

	if(currentTimeoutCbFunc != NULL){
		HAL_TIM_Base_Stop_IT(&htim2);
	}
	taskENTER_CRITICAL();	/*Prevent executing the timer interrupt if changing the callback*/
	currentTimeoutCbFunc = timeoutCbFunc;
	currentTimeoutCbArgs = timeoutCbArgs;
	hwTimerMode = timerMode;

	htim2.Init.Prescaler = (configCPU_CLOCK_HZ/HW_TIMER_RESOLUTION_FREQ) - 1;

	lastRunningPeriod = (timeout/HW_TIMER_RESOLUTION_TIME) - 1;
	htim2.Init.Period = lastRunningPeriod;
	taskEXIT_CRITICAL();

	HAL_TIM_Base_Init(&htim2);

	HAL_TIM_Base_Start_IT(&htim2);

	return RET_OK;
}

/**
 *
 * @return
 */
retval_t ytStopHwTimer(void){

	HAL_TIM_Base_Stop_IT(&htim2);

	taskENTER_CRITICAL();	/*Prevent executing the timer interrupt if changing the callback*/
	currentTimeoutCbFunc = NULL;
	currentTimeoutCbArgs = NULL;
	taskEXIT_CRITICAL();

	return RET_OK;
}

/**
 *
 * @return
 */
retval_t ytUpdateHwTimeFreq(void){
	taskENTER_CRITICAL();
	if(currentTimeoutCbFunc != NULL){	/*In this case the timer is running. It is necessary to run the timer the remaining time and update it*/

		/*At this point the CPU frequency has already changed, so there will always be a little glitch in the timer*/
		htim2.Init.Period = lastRunningPeriod - __HAL_TIM_GET_COUNTER(&htim2);
		HAL_TIM_Base_Stop_IT(&htim2);
	}
	htim2.Init.Prescaler = (configCPU_CLOCK_HZ/HW_TIMER_RESOLUTION_FREQ) - 1;	/*New prescaler*/

	HAL_TIM_Base_Init(&htim2);		/*Reconfigure the timer*/

	if(currentTimeoutCbFunc != NULL){
		HAL_TIM_Base_Start_IT(&htim2);	/*Restart the timer if necessary*/
	}

	taskEXIT_CRITICAL();

	return RET_OK;
}

#endif

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM8 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if (htim->Instance == TIM8) {
		HAL_IncTick();
	}
#if PLATFORM_USE_HW_TIMER
	if (htim->Instance == TIM2) {
		if(currentTimeoutCbFunc != NULL){

			currentTimeoutCbFunc(currentTimeoutCbArgs);	/*Call the callback function*/


			if(hwTimerMode == HW_TIMER_ONCE){	/*Stop the timer*/
				HAL_TIM_Base_Stop_IT(&htim2);
				currentTimeoutCbFunc = NULL;
				currentTimeoutCbArgs = NULL;
			}
			else{
				if(lastRunningPeriod != htim2.Init.Period){	/*The frequency has changed and a fraction of the timeout has lapsed. Set now the period to the timeout value*/
					htim2.Init.Period = lastRunningPeriod;
					HAL_TIM_Base_Stop_IT(&htim2);
					HAL_TIM_Base_Init(&htim2);		/*Reconfigure the timer*/
					HAL_TIM_Base_Start_IT(&htim2);	/*Restart the timer */
				}
			}

		}
	}
#endif
}


#if PLATFORM_USE_HW_TIMER
/**
 *
 */
void TIM2_IRQHandler(void){
	HAL_TIM_IRQHandler(&htim2);
}
#endif

#endif
