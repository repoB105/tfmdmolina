/*
 * Copyright (c) 2019, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * lowPower.c
 *
 *  Created on: 17 jul. 2019
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file lowPower.c
 */

#include "yetiOS.h"
#include "lowPower.h"

#define SET_RUN_MODE_TIMEOUT	3000

#ifndef PLATFORM_NUM_LOW_POWER_MODES
#define PLATFORM_NUM_LOW_POWER_MODES	0
#endif

#ifndef PLATFORM_NUM_RUN_MODES
#define PLATFORM_NUM_RUN_MODES			0
#endif

/*Default Q Learning Values*/ //TODO They should be configured in YetiOSConf once it is tested. Let these default values here for now
#define YETIOS_DEFAULT_LEARNING_RATE				0.9f
#define YETIOS_DEFAULT_DISCOUNT_FACTOR				0.1f
#define YETIOS_DEFAULT_FREQ_REFRESH_PERIOD			250		/*This value wont be exact. It is a multiple of YETIOS_DYN_FREQ_CHECK_TIME*/
#define YETIOS_MAX_STARVATION_CPU_LOAD				95

#define YETIOS_MIN_DYN_FREQ_CHECK_TIME			10		/*A check time lower than that introduces too much overhead, so if the calculated dyn freq check time is lower than this value, this value is selected and real time cannot be guaranteed */
/*Dynamic frequency real-time configuration time constants. This time constants must be set adequately in each application to guarantee hard real-time constraints are fit*/
#define YETIOS_MAX_PROCESSING_TIME				1000	/*The maximum time the most restrictive task is allowed to execute to fit real-time constraints */
#define YETIOS_MAX_FREQ_EXEC_TIME				107		/*The time required by the most restrictive task to execute when running at the maximum cpu frequency*/
#define PLATFORM_CHANGE_TIME_MIN_MAX_FREQ		2		/*The time required to change from the minimum frequency to the maximum frequency*/
#define YETIOS_MAX_PERIPHERAL_TIME				1		/*The maximum time all the peripherals can be running in the application */

#define YETIOS_DYN_FREQ_CHECK_TIME				250		/*Comment this line to calculate the check time based on real-time constants*/

#ifndef	YETIOS_DYN_FREQ_CHECK_TIME
#define YETIOS_DYN_FREQ_CHECK_TIME				(((YETIOS_MAX_PROCESSING_TIME - YETIOS_MAX_FREQ_EXEC_TIME)/2) - PLATFORM_CHANGE_TIME_MIN_MAX_FREQ - YETIOS_MAX_PERIPHERAL_TIME)
#endif
#if YETIOS_DYN_FREQ_CHECK_TIME < YETIOS_MIN_DYN_FREQ_CHECK_TIME
#undef YETIOS_DYN_FREQ_CHECK_TIME
#define YETIOS_DYN_FREQ_CHECK_TIME 	YETIOS_MIN_DYN_FREQ_CHECK_TIME
#endif

#if YETIOS_DEFAULT_FREQ_REFRESH_PERIOD >= YETIOS_DYN_FREQ_CHECK_TIME
#define YETIOS_NUM_CHECK_TO_REFRESH				(YETIOS_DEFAULT_FREQ_REFRESH_PERIOD/YETIOS_DYN_FREQ_CHECK_TIME)
#else
#define YETIOS_NUM_CHECK_TO_REFRESH				1
#endif

#define STARVATION_PENALTY				1

static uint32_t currentLowPowerModeConsumption = 0;
static uint32_t currentLowPowerMode = 0;
static uint16_t lowPowerBlockingPeripherals = 0;
static uint32_t defaultLowPowerMode = 0;
static uint32_t devicePowerModeChanged = 0;

static uint32_t currentRunModeConsumption = 0;
static uint32_t currentRunMode = 0;
static uint32_t usingAnyPeripheral = 0;

static osMutexId lowPowerModeMutex;
static osSemaphoreId changeRunModeSemaphore;

/*Variables used for current consumption estimation*/
static uint32_t estimatedAcumulatedConsumption = 0;
static uint32_t estimationInit = 0;
static uint32_t estimationAcumulatedLowPowerTicks = 0;
static uint32_t estimationLapsed = 0;
static uint32_t acumulatedRunTicks = 0;

#if YETIOS_USE_DYN_CPU_FREQ
static osThreadId cpuFreqLearnThreadId;

static float32_t lRate = YETIOS_DEFAULT_LEARNING_RATE;
static float32_t discFactor = YETIOS_DEFAULT_DISCOUNT_FACTOR;

static float32_t qTable[PLATFORM_NUM_RUN_MODES][PLATFORM_NUM_RUN_MODES];

static void cpuFreqLearnThreadFunc(void const * argument);
#endif

/*Platform functions declarations that might be defined. If not, use default __weak definitions*/
extern void platformInitLowPowerManager();
extern uint32_t platformUpdateLowPowerModeConsumption();
extern retval_t platformSetCurrentLowPowerMode(uint32_t newLowPowerMode);
extern uint32_t platformGetCurrentLowPowerMode(void);

extern uint32_t platformUpdateRunModeConsumption();
extern retval_t platformSetCurrentRunMode(uint32_t newRunMode);
extern uint32_t platformGetCurrentRunMode(void);
/**
 *
 * @return
 */
retval_t ytInitLowPowerManager(){
	estimatedAcumulatedConsumption = 0;
	estimationInit = 0;
	estimationAcumulatedLowPowerTicks = 0;
	currentLowPowerMode = 0;
	estimationLapsed = 0;
	usingAnyPeripheral = 0;
	devicePowerModeChanged = 0;
	defaultLowPowerMode = 0;
	acumulatedRunTicks = 0;
	lowPowerModeMutex = ytMutexCreate();
	changeRunModeSemaphore = ytSemaphoreCreate(1);
	/*Initialize Run and low power modes to platform defaults*/
	platformInitLowPowerManager();
	currentRunModeConsumption = platformUpdateRunModeConsumption();
	currentRunMode = platformGetCurrentRunMode();

	currentLowPowerMode = platformGetCurrentLowPowerMode();
	currentLowPowerModeConsumption = platformUpdateLowPowerModeConsumption();
	defaultLowPowerMode = currentLowPowerMode;

	/*Change the running mode clock to the selected platform default value*/
	ytSetCurrentRunMode(PLATFORM_DEFAULT_INIT_RUN_MODE);

	/*Start the Q learning dynamic CPU frequency function*/
#if YETIOS_USE_DYN_CPU_FREQ
	ytStartThread("dynCpuFreqThread", cpuFreqLearnThreadFunc, osPriorityRealtime, 200, &cpuFreqLearnThreadId, NULL);
#endif


	return RET_OK;
}

/**
 * @brief	Some low power modes may change its current consumption depending on the system frequency
 * @return
 */
void ytUpdateCurrentLowPowerModeConsumption(){
	currentLowPowerModeConsumption = platformUpdateLowPowerModeConsumption();
}

/**
 *
 * @return
 */
uint32_t ytGetCurrentLowPowerMode(){
	return currentLowPowerMode;
}


/**
 *
 * @param newLowPowerMode
 * @return
 */
retval_t ytSetCurrentLowPowerMode(uint32_t newLowPowerMode){
	if(newLowPowerMode < PLATFORM_NUM_LOW_POWER_MODES){
		if(newLowPowerMode != currentLowPowerMode){
			/*Estimate the current consumption till this moment*/
			uint32_t lapsedTime = osKernelSysTick() - estimationInit;
			estimationInit = osKernelSysTick();
			estimationLapsed += lapsedTime;
			uint32_t lastRunTicks = lapsedTime - estimationAcumulatedLowPowerTicks;
			estimatedAcumulatedConsumption += ((lastRunTicks*currentRunModeConsumption) +
								(estimationAcumulatedLowPowerTicks*currentLowPowerModeConsumption));
			estimationAcumulatedLowPowerTicks = 0;
			acumulatedRunTicks += lastRunTicks;
			/*Set the new Low power mode*/
			currentLowPowerMode = newLowPowerMode;
			platformSetCurrentLowPowerMode(currentLowPowerMode);
			currentLowPowerModeConsumption = platformUpdateLowPowerModeConsumption();
		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

uint32_t ytGetCurrentLowPowerConsumption(){
	return currentLowPowerModeConsumption;
}

/**
 *
 */
void ytPeripheralBlockLowPower(){
	lowPowerBlockingPeripherals++;
}

/**
 *
 */
void ytPeripheralReleaseLowPower(){
	lowPowerBlockingPeripherals--;
}

/**
 *
 * @return
 */
uint16_t ytIsPeripheralBlockingSleep(){
	return lowPowerBlockingPeripherals;
}


/**
 * @brief			Acumulates the low current consumption for the specified ticks
 * @param ticks
 */
void ytAcumulateLowPowerTicks(uint32_t ticks){
	estimationAcumulatedLowPowerTicks += ticks;
}

/**
 * @brief	Resets and start estimating the current consumption
 */
void ytStartEstimatingConsumption(){
	estimationAcumulatedLowPowerTicks = 0;
	estimatedAcumulatedConsumption = 0;
	estimationLapsed = 0;
	acumulatedRunTicks = 0;
	estimationInit = osKernelSysTick();
}

/**
 * @brief	Returns the average	 estimated current consumption since the last startEstimatingConsumption call
 * @return
 */
uint32_t ytGetLastEstimatedConsumption(uint32_t* cpuLoad){
	uint32_t currentTick = osKernelSysTick();
	uint32_t lapsedTime = currentTick - estimationInit;
	estimationInit = currentTick;
	estimationLapsed += lapsedTime;
	uint32_t lastRunTicks = lapsedTime - estimationAcumulatedLowPowerTicks;
	acumulatedRunTicks += lastRunTicks;
	*cpuLoad = (acumulatedRunTicks*100)/estimationLapsed;
	estimatedAcumulatedConsumption += ((lastRunTicks*currentRunModeConsumption) +
			(estimationAcumulatedLowPowerTicks*currentLowPowerModeConsumption));
	return estimatedAcumulatedConsumption/estimationLapsed;
}


/**
 *
 * @return
 */
uint32_t ytGetCurrentRunMode(){
	return currentRunMode;
}


/**
 *
 * @param newRunMode
 * @return
 */
retval_t ytSetCurrentRunMode(uint32_t newRunMode){

	if(newRunMode < PLATFORM_NUM_RUN_MODES){
		if(newRunMode != currentRunMode){

			/*Set the new Run mode*/
			/*Suspend the scheduler while changing the clock*/
			taskENTER_CRITICAL();		/*Evaluate usingAnyPeripheral in a critical region to prevent race conditions*/
			while(usingAnyPeripheral){	/*If any peripheral is being used, the clock cannot be changed*/
				taskEXIT_CRITICAL();
				osSemaphoreWait(changeRunModeSemaphore, YETIOS_MAX_PERIPHERAL_TIME);
				taskENTER_CRITICAL();
			}
			platformSetCurrentRunMode(newRunMode);
			ytUpdateDevicesCpuFreq();		/*Update the clock config of the loaded peripheral devices*/
			taskEXIT_CRITICAL();		/*Exit the critical region*/


			/*Estimate the current consumption till this moment*/
			uint32_t lapsedTime = osKernelSysTick() - estimationInit;
			estimationInit = osKernelSysTick();
			estimationLapsed += lapsedTime;
			uint32_t lastRunTicks = lapsedTime - estimationAcumulatedLowPowerTicks;
			acumulatedRunTicks += lastRunTicks;
			estimatedAcumulatedConsumption += ((lastRunTicks*currentRunModeConsumption) +
								(estimationAcumulatedLowPowerTicks*currentLowPowerModeConsumption));

			estimationAcumulatedLowPowerTicks = 0;
			currentRunMode = newRunMode;
			currentRunModeConsumption = platformUpdateRunModeConsumption();
			/*Some low power modes may change its current consumption depending on the system frequency*/
			currentLowPowerModeConsumption = platformUpdateLowPowerModeConsumption();
		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

uint32_t ytGetCurrentRunConsumption(){
	return currentRunModeConsumption;
}

/**
 *
 */
void ytPeripheralFreqChangeLock(){
	usingAnyPeripheral++;
}

/**
 *
 */
void ytPeripheralFreqChangeUnlock(){
	usingAnyPeripheral--;
	osSemaphoreRelease(changeRunModeSemaphore);
}

/**
 * @param nextMode
 */
void ytSetDeviceLowPowerMode(uint32_t selectedLowPowerMode){
	osMutexWait(lowPowerModeMutex, osWaitForever);
	devicePowerModeChanged++;
	if(selectedLowPowerMode < currentLowPowerMode){	/*A more restrictive mode selected*/
		ytSetCurrentLowPowerMode(selectedLowPowerMode);
	}
	osMutexRelease(lowPowerModeMutex);
}

/**
 *
 */
void ytSetDeviceDefaultLowPowerMode(){
	osMutexWait(lowPowerModeMutex, osWaitForever);
	devicePowerModeChanged--;
	if(!devicePowerModeChanged){
		ytSetCurrentLowPowerMode(defaultLowPowerMode);
	}
	osMutexRelease(lowPowerModeMutex);
}

/**
 *
 * @param newLowPowerMode
 */
void ytUpdateDefaultLowPowerMode(uint32_t newLowPowerMode){
	osMutexWait(lowPowerModeMutex, osWaitForever);
	if(newLowPowerMode != defaultLowPowerMode){
		ytSetCurrentLowPowerMode(newLowPowerMode);
	}
	osMutexRelease(lowPowerModeMutex);
}

#if YETIOS_USE_DYN_CPU_FREQ

/**
 *
 * @param argument
 */
static void cpuFreqLearnThreadFunc(void const * argument){

	/*Dyn Q Table allocation*/
	uint16_t i, starvation;
	uint32_t lastCpuLoad, nextRunMode, prevRunMode, starvationLastRunMode, numRefreshTimes, avgLastConsumption,
			numCheckToRefresh, dynFreqCheckTime;
	float32_t reward;
	float32_t bestNextQ;
	uint32_t lastAction = currentRunMode;
	prevRunMode = currentRunMode;
	starvationLastRunMode = currentRunMode;
	nextRunMode = currentRunMode;

	/*Initialize q-table*/
	memset(qTable, 0, PLATFORM_NUM_RUN_MODES*PLATFORM_NUM_RUN_MODES*sizeof(float32_t));
	numCheckToRefresh = YETIOS_NUM_CHECK_TO_REFRESH;
	dynFreqCheckTime = YETIOS_DYN_FREQ_CHECK_TIME;

	/* Start measuring consumption and cpu load estimation for the first period*/
	ytStartEstimatingConsumption();

	while(1){
		starvation = 0;
		numRefreshTimes = 0;
		avgLastConsumption = 0;

		while(numRefreshTimes < numCheckToRefresh){

			osDelay(dynFreqCheckTime);	/*Wait the refresh period to evaluate the decision made*/

			avgLastConsumption += ytGetLastEstimatedConsumption(&lastCpuLoad);

			/* Start measuring consumption and cpu load estimation for the next period*/
			ytStartEstimatingConsumption();

			if(lastCpuLoad >= YETIOS_MAX_STARVATION_CPU_LOAD){	/*Starvation is happening. Change to the maximum CPU freq*/
				if(currentRunMode){	/*If the currentRunMode is 0 (max freq) the frequency cannot be increased*/

					while (ytSetCurrentRunMode(0) != RET_OK);	/*Change to the maximum frequency*/

				}
				starvationLastRunMode = currentRunMode;
				nextRunMode = 0;
				starvation++;
			}
			numRefreshTimes++;
		}

		reward = -((float32_t)avgLastConsumption)/numCheckToRefresh;	/*The reward is minus the avg power consumption*/

		if(starvation){	/*Starvation has happened. Calculate the best Q of the state I was before starvation (which is the next state selected by the previous action)*/
			reward *= STARVATION_PENALTY;	/*Add a penalty in the reward when there has been starvation*/
			bestNextQ = -100000000;
			for(i=0; i <PLATFORM_NUM_RUN_MODES; i++){
				if(qTable[starvationLastRunMode][i] > bestNextQ){
					bestNextQ = qTable[starvationLastRunMode][i];
				}
			}
		}
		else{
			bestNextQ = -100000000;					/*Calculate the best Q of the current state (which is the next state selected by the previous action)*/
			for(i=0; i <PLATFORM_NUM_RUN_MODES; i++){
				if(qTable[currentRunMode][i] > bestNextQ){
					bestNextQ = qTable[currentRunMode][i];
					nextRunMode = i;					/*Select the new action. The one with best Q*/
				}
			}

		}

		qTable[prevRunMode][(uint16_t)lastAction] = qTable[prevRunMode][(uint16_t)lastAction]
															+ lRate*(reward + (discFactor*bestNextQ) - qTable[prevRunMode][(uint16_t)lastAction]);

		if(!starvation){
			prevRunMode = currentRunMode;
		}
		else{
			prevRunMode = starvationLastRunMode;
		}
		lastAction = nextRunMode;

		/*Perform the action selected*/
		if(nextRunMode != prevRunMode){
			while (ytSetCurrentRunMode(nextRunMode) != RET_OK);
		}

	}

}

#endif

/* Weak implementations in case the platform does not implement low power modes*/
__weak void platformInitLowPowerManager(){
	return;
}
__weak uint32_t platformGetCurrentLowPowerMode(){
	return 0;
}
__weak uint32_t platformUpdateLowPowerModeConsumption(){
	return 0;
}
__weak retval_t platformSetCurrentLowPowerMode(uint32_t newLowPowerMode){
	return RET_OK;
}

__weak uint32_t platformGetCurrentRunMode(){
	return 0;
}
__weak uint32_t platformUpdateRunModeConsumption(){
	return 0;
}
__weak retval_t platformSetCurrentRunMode(uint32_t newRunMode){
	return RET_OK;
}

