/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * snr.h
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file snr.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_SNR_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_SNR_H_


#include "fft_signal.h"

#define DEFAULT_FAR_FREQ_SAMPLE		85
#define DEFAULT_FAR_CONSTANT_VALUE	0.7f

typedef enum{
	Standard_snr,
	No_snr,
}snr_calc_t;

typedef struct snr{
	retval_t (*snr_func_ptr)(struct snr* snr, fft_signal_t* fft_signal);
	float32_t* snr_signal;
	float32_t noise_lvl;
	uint16_t far_freq_sample;
	float32_t far_freq_constant_value;
	uint16_t fft_sample_number;
	uint16_t effective_sample_number;
	snr_calc_t snr_calc;
}snr_t;


snr_t* new_snr(uint16_t fft_sample_number, uint16_t effective_sample_number, snr_calc_t snr_calc);
retval_t remove_snr(snr_t* snr);

retval_t change_snr_type(snr_t* snr, snr_calc_t new_snr_calc);

retval_t change_snr_effective_sample_number(snr_t* snr, uint16_t effective_sample_number);


#endif /* APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_SNR_H_ */
