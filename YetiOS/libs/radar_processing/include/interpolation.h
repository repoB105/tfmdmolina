/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * interpolation.h
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file interpolation.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_INTERPOLATION_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_INTERPOLATION_H_


#include "signal_window.h"
#include "arm_math.h"

typedef enum{
	Zero_Padding,
	No_Interpolation,
}interpolation_type_t;

typedef struct interpolation{
	retval_t (*interpolation_func_ptr)(struct interpolation* interpolation, signal_window_t* window);
	float32_t* signal_post_interpolation;
	uint16_t input_sample_number;
	uint16_t output_sample_number;
	interpolation_type_t interpolation_type;
}interpolation_t;


interpolation_t* new_interpolation(uint16_t input_sample_number, uint16_t output_sample_number, interpolation_type_t interpolation_type);
retval_t remove_interpolation(interpolation_t* interpolation);

retval_t change_interpolation_type(interpolation_t* interpolation, interpolation_type_t new_interpolation_type, uint16_t new_output_sample_number);

retval_t change_interpolation_intput_sample_number(interpolation_t* interpolation, uint16_t adc_sample_number);
retval_t change_interpolation_output_sample_number(interpolation_t* interpolation, uint16_t fft_sample_number);


#endif /* APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_INTERPOLATION_H_ */
