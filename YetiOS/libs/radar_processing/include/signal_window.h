/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * signal_window.h
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file signal_window.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_SIGNAL_WINDOW_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_SIGNAL_WINDOW_H_


#include "adc_iq_signals.h"

#define DEFAULT_TUKEY_ALPHA	0.5f

typedef enum {
	Tukey,
	Hanning,
	Hamming,
	No_Window,
}window_type_t;

typedef struct signal_window{
	retval_t (*window_func_ptr)(struct signal_window* window, adc_iq_signal_t* adc_iq_signals);
	float32_t* signal_post_window;
	uint16_t sample_number;
	window_type_t window_type;
	float32_t tukey_alpha;
}signal_window_t;

signal_window_t* new_signal_window(uint16_t sample_number, window_type_t window_type);
retval_t remove_signal_window(signal_window_t* signal_window);

retval_t change_window_type(signal_window_t* signal_window, window_type_t new_window_type);

retval_t change_window_sample_number(signal_window_t* signal_window, uint16_t adc_sample_number);


#endif /* APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_SIGNAL_WINDOW_H_ */
