/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * distances_selection.c
 *
 *  Created on: 31/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file distances_selection.c
 */

#include "platform-conf.h"
#include "system_api.h"
#include "distances_selection.h"


distances_selection_t* new_distances_selection(uint16_t max_num_distances, uint16_t num_guard_low_distances){
#if USE_RADAR_PROCESSING
	uint16_t i;
	distances_selection_t* new_distances_selection = (distances_selection_t*) ytMalloc(sizeof(distances_selection_t));

	new_distances_selection->distances = (float32_t*) ytMalloc(max_num_distances*sizeof(float32_t));
	new_distances_selection->distances_levels = (float32_t*) ytMalloc(max_num_distances*sizeof(float32_t));
	new_distances_selection->distances_snr = (float32_t*) ytMalloc(max_num_distances*sizeof(float32_t));

	new_distances_selection->max_num_distances = max_num_distances;
	new_distances_selection->calibration_factor = 1.24f;
	new_distances_selection->downramp_calibration_factor = 1.06f;

	new_distances_selection->current_num_distances = 0;			//First set everything to 0

	new_distances_selection->num_guard_low_distances = num_guard_low_distances;
	new_distances_selection->threshold_lvl_calibration_near = 0.60f;
	new_distances_selection->threshold_lvl_calibration_mid = 0.55f;
	new_distances_selection->threshold_lvl_calibration_far = 0.54f;
	new_distances_selection->threshold_lvl_calibration_vfar = 0.4f;
	new_distances_selection->threshold_snr_calibration_near = 0.5f;
	new_distances_selection->threshold_snr_calibration_mid = 0.5f;
	new_distances_selection->threshold_snr_calibration_far = 0.4f;
	new_distances_selection->threshold_snr_calibration_vfar = 0.4f;

	new_distances_selection->discard_speed = DISCARD_SPEED;

	for(i=0; i<new_distances_selection->max_num_distances; i++){
		new_distances_selection->distances[i]	= 0;
		new_distances_selection->distances_levels[i] = 0;
	}

	return new_distances_selection;
#endif
	return NULL;
}


retval_t remove_distances_selection(distances_selection_t* distances_selection){
#if USE_RADAR_PROCESSING
	if(distances_selection != NULL){
		if ((distances_selection->distances != NULL) && (distances_selection->distances_levels != NULL)){
			ytFree(distances_selection->distances);
			ytFree(distances_selection->distances_levels);
			ytFree(distances_selection->distances_snr);
		}
		else{
			return RET_ERROR;
		}
		ytFree(distances_selection);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
#endif
	return RET_ERROR;
}

retval_t set_max_num_distances(distances_selection_t* distances_selection, uint16_t max_num_distances){
#if USE_RADAR_PROCESSING
	if(distances_selection != NULL){
		ytFree(distances_selection->distances);
		ytFree(distances_selection->distances_levels);

		distances_selection->max_num_distances = max_num_distances;

		distances_selection->distances = (float32_t*) ytMalloc(max_num_distances*sizeof(float32_t));
		distances_selection->distances_levels = (float32_t*) ytMalloc(max_num_distances*sizeof(float32_t));

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
#endif
	return RET_ERROR;
}

